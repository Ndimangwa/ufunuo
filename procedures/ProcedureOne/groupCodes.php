/*BEGIN: GoodReceivedNoteManagement*/if ($page == "managegoodreceivednote_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a></div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managegoodreceivednote_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = GoodReceivedNote::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("GoodReceivedNote[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$note1 = null;
			try	{
				$note1 = new GoodReceivedNote($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($note1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managegoodreceivednote_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managegoodreceivednote_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managegoodreceivednote_csv&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegoodreceivednote_csv" && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegoodreceivednote_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, GoodReceivedNote::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Managegement</a>
					</div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegoodreceivednote_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$note1 = null;
	$captionText = "Unknown";
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		 $note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
		 $captionText = $note1->getNoteName();
		 $note1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a GoodReceivedNote which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The GoodReceivedNote <?= $note1->getNoteName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the GoodReceivedNote <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a>
					</div>
					Rule: managegoodreceivednote_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegoodreceivednote_delete", "Deleted ".$note1->getNoteName());
} else if ($page == "managegoodreceivednote_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$note1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		mysql_close($conn);
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $note1->getNoteName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a GoodReceivedNote <b><?= $note1->getNoteName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the GoodReceivedNote <?= $note1->getNoteName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegoodreceivednote_delete&id=<?= $note1->getNoteId() ?>&report=yes&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managegoodreceivednote_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$note1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	
	if ($extraInformation != $note1->getExtraInformation())	{
		$note1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $note1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$note1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$note1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for GoodReceivedNote Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit GoodReceivedNote (Report) <i><?= $note1->getNoteName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			GoodReceivedNote <?= $note1->getNoteName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the order <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Details</a></div>
					Rule: managegoodreceivednote_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegoodreceivednote_edit", "Edited ".$note1->getNoteName());
} else if ($page == "managegoodreceivednote_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$note1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
		$note1->setExtraFilter($extraFilter);
		$note1->commitUpdate();
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit GoodReceivedNote <i><?= $note1->getNoteName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegoodreceivednote_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $note1->getNoteId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $note1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
						<div class="pure-control-group">
							<label for="orderName">GoodReceivedNote Name </label>
							<input value="<?= $note1->getNoteName() ?>" type="text" name="orderName" id="orderName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="GoodReceivedNote Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="pId">Parent GoodReceivedNote</label>
							<select id="pId" name="pId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Parent GoodReceivedNote">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = GoodReceivedNote::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected="";
		if ($note1->getNoteId() == $alist['id']) continue;
		if ($alist['id'] == $note1->getParentGoodReceivedNote()->getNoteId()) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $note1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Details</a></div>
					Rule: managegoodreceivednote_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$note1 = null;
	try {
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for order : <i><?= $note1->getNoteNumber() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$documentToDisplay = "";
	try {
		$note1->putData('logoFolder', '../data/profile/logo/');
		$note1->putData('dataFolder', '../data/document/goodReceivedNote/');
		$note1->putData('currentLoginInId', $login1->getLoginId());
		$note1->putData('systemTime', $systemTime1->getDateAndTimeString());
		$note1->putData('optionalStoreToAdd', 'ItemDemandStore');
		$documentToDisplay = $note1->getDocumentUI();
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	mysql_close($conn);
	if ($promise1->isPromising())	{
?>
					<div class="display-background-black">
						<!--START DOCUMENT DETAILS-->
<?php 
	echo $documentToDisplay;
?>						
						<!--STOP DOCUMENT DETAILS-->
					</div>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managegoodreceivednote_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $note1->getNoteNumber() ?>" href="<?= $thispage ?>?page=managegoodreceivednote_edit&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if ( Authorize::isAllowable($config, "managegoodreceivednote_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $note1->getNoteNumber() ?>" href="<?= $thispage ?>?page=managegoodreceivednote_delete&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
<?php 
	} else {
?>
		<div class="ui-sys-warning">
			There were problems in displaying document <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a>
					</div>
					Rule: managegoodreceivednote_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('button.button-sign-document').on('click', function(event)	{
		var $button1 = $(this).closest('button');
		if (! $button1.length) return;
		var loginId = $button1.attr('data-login-id');
		if (! loginId) return;
		var jobId = $button1.attr('data-job-id');
		if (! jobId) return;
		var schemaId = $button1.attr('data-schema-id');
		if (! schemaId) return;
		var time = $button1.attr('data-approval-time');
		if (! time) return;
		var optionalStoreToAdd = $button1.attr('data-optional-store-to-add');
		if (! optionalStoreToAdd) return;
		var optionalStoreToRemove = $button1.attr('data-optional-store-to-remove');
		if (! optionalStoreToRemove) return;
		var className = $button1.attr('data-class-name');
		if (! className) return;
		var documentId = $button1.attr('data-document-id');
		if (! documentId) return;
		var documentFolder = "goodReceivedNote";
		$target1 = $('#__approval_error_control');
		if (! $target1.length) return;
		$target1.empty();
		$.ajax({
			url: "../server/service_system_document_approval.php",
			method: "POST",
			data: { param1: loginId, param2: jobId, param3: schemaId, param4: time, param6: className, param7: documentId, param8: optionalStoreToAdd, param9: optionalStoreToRemove, param10: documentFolder },
			dataType: "json",
			cache: false,
			async: true
		}).done(function(data, textStatus, jqXHR)	{
			if (parseInt(data.code) === 0)	{
				window.location.href = '<?= $thispage ?>?page=<?= $page ?>&id=<?= $note1->getId() ?>&sysmenu=<?= $_REQUEST['sysmenu'] ?>';
			} else	{
				//Failed 
				$target1.html("1-- Failed " + data.message); return;
			}
		}).fail(function(jqXHR, textStatus, errorThrown)	{
			$target1.html("2-- Failed " + textStatus); return;
		}).always(function(data, textStatus, jqXHR)	{
			//Default Always
		})
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegoodreceivednote_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managegoodreceivednote_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$storeId = mysql_real_escape_string($_REQUEST['storeId']);
		$supplierName = mysql_real_escape_string($_REQUEST['supplierName']);
		$supplierAddress = mysql_real_escape_string($_REQUEST['supplierAddress']);
		$supplierPhone = mysql_real_escape_string($_REQUEST['supplierPhone']);
		$currencyId = mysql_real_escape_string($_REQUEST['currencyId']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Create A New Good Received Note (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$itemArray1 = Document::mergeUserData($_REQUEST['item'], $_REQUEST['quantity'], $_REQUEST['price'], null, null, 0);
	$extraFilter = System::getCodeString(8);
	$note1 = null;
	try {
		$orderNumber = rand(1000, 999999999);
		$documentId = Document::getDocumentIdFromDocumentCode($database, $conn, Document::$__DOC_RAW_GOOD_RECEIVED_NOTE);
		$orderId = GoodReceivedNote::addRecord($database, $conn, $orderNumber, $documentId, $storeId, $supplierName, $supplierAddress, $supplierPhone, $systemTime1, $currencyId, $extraFilter, $extraInformation, 0);
		//Create Now Object 
		$note1 = new GoodReceivedNote($database, $orderId, $conn);
		$orderNumber = Document::createReferenceNumber("PO-", 6, $orderId);
		$note1->setNoteNumber($orderNumber);
		$document1 = $note1->getDocument();
		//First Approving Schema 
		$schema1 = $document1->getFirstSchemaToApprove();
		if (! is_null($schema1)) $note1->setNextSchemaToApprove($schema1->getSchemaId());
		if (is_null($document1)) Object::shootException("Failed to get a reference Document ");
		$filename = $document1->getDocumentClass()."".$note1->getNoteId().".xml";
		$note1->setFilename($filename);
		$note1->setCompleted("1");
		//Approval Schema 
		$folderPath = "../data/document/goodReceivedNote/";
		$filename = $folderPath.$filename;
		//Now create Good Received Note 
		$listType = "Item";
		$controlFlags = 0;
		$tree1 = GoodReceivedNote::createGoodReceivedNote($database, $conn, $profile1, $note1->getNoteId(), $itemArray1, $listType, $controlFlags);
		$tree1->setFilename($filename);
		$note1->setFilenameHash($tree1->save());
		$note1->commitUpdate();
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
		if (! is_null($note1))	{
			try {
				$note1->commitDelete();
			} catch (Exception $e)	{
				$promise1->setReason("Delete Resource ".$e->getMessage());
			}
		}
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The GoodReceivedNote <?= $note1->getNoteNumber() ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the order<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another GoodReceivedNote";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegoodreceivednote_add&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Manager</a>
					</div>
					Rule: managegoodreceivednote_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegoodreceivednote_add", "Added ".$note1->getNoteNumber());
} else if ($page == "managegoodreceivednote_add" && Authorize::isAllowable($config, "managegoodreceivednote_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Creating A Good Received Note</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (isset($_REQUEST['count']))	{
		$count = intval($_REQUEST['count']);
?>
					<div class="text-center ui-sys-warning">
						NOTE: If an item is added more than once the highest price will be considered!!!
					</div>
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegoodreceivednote_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
						<div class="pure-control-group">
							<label for="storeId">Physical Store</label>
							<select id="storeId" name="storeId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Physical Store">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = PhysicalStore::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="supplierName">Name of Supplier </label>
							<input type="text" name="supplierName" id="supplierName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Name of Supplier : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="supplierAddress">Address of Supplier </label>
							<input type="text" name="supplierAddress" id="supplierAddress" size="48" required pattern="<?= $exprL64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Address of Supplier : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="supplierPhone">Phone of Supplier </label>
							<input type="text" name="supplierPhone" id="supplierPhone" size="48" required pattern="<?= $exprPhone ?>" validate="true" validate_control="text" validate_expression="<?= $exprPhone ?>" validate_message="Phone of Supplier : <?= $msgPhone ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="currencyId">Currency</label>
							<select id="currencyId" name="currencyId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Currency">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Currency::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
<?php 
	if ($count > 0)	{
?>
		<br/><table style="font-size: 0.9em;" class="pure-table">
			<thead><tr><th>S/N</th><th>Item</th><th>Quantity</th><th>Unit Price</th><th>Amount</th></tr></thead>
			<tbody>
<?php 
	$list = Item::loadAllData($database, $conn);
	//Preparing ItemList 
	$itemList = array();
	foreach ($list as $alist)	{
		try {
			$item1 = new Item($database, $alist['id'], $conn);
			$listsize = sizeof($itemList);
			$itemList[$listsize] = array();
			$itemList[$listsize]['id'] = $item1->getItemId();
			$itemList[$listsize]['val'] = $item1->getItemName();
			$itemList[$listsize]['measure'] = $item1->getMeasure()->getMeasureName();
		} catch (Exception $e)	{
			continue;
		}
	}
	for ($i = 0; $i < $count; $i++)	{
?>
		<tr class="data-row">
			<td><?= $i + 1 ?></td>
			<td><select class="select-item" name="item[<?= $i ?>]" validate="true" validate_control="select" validate_expression="true" validate_message="Kindly Select Item at Row <?= $i + 1 ?>">
				<option value="_@32767@_" data-measure="">--select--</option>
<?php 
	foreach ($itemList as $alist)	{
?>
		<option value="<?= $alist['id'] ?>" data-measure="<?= $alist['measure'] ?>"><?=  $alist['val'] ?></option>
<?php
	}
?>
			</select></td>
			<td><input data-index="<?= $i ?>" class="change-amount" type="text" name="quantity[<?= $i ?>]" size="4" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Fill Quantity at Row <?= $i + 1 ?>"/>&nbsp;&nbsp;<label class="label-measure"></label></td>
			<td><input data-index="<?= $i ?>" class="change-amount" type="text" name="price[<?= $i ?>]" size="8" required pattern="<?= $expr8Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr8Number ?>" validate_message="Fill Unit Price at Row <?= $i + 1 ?>"/></td>
			<td class="text-right"><label class="label-amount"></label></td>
		</tr>
<?php
	} //end.for.i .. count
?>		
				<tr><td colspan="4"><b>Estimated Grand Total : </b></td><td class="text-right"><label class="grand-total"></label></td></tr>
			</tbody>
		</table><br/>
<?php
	} //end if.count>0
?>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
<?php 
	} else {
		//Default Landing Page 
?>
		<div class="text-center">
			<form id="form1" method="POST" action="<?= $thispage ?>">
				<input type="hidden" name="page" value="managegoodreceivednote_add"/>
				<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
				<label>Enter Number of Raw Materials : <input type="text" name="count" id="count" size="32" required pattern="<?= $expr2Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr2Number ?>" validate_message="Raw Materials Count : <?= $msg2Number ?>"/></label>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="__add_record" value="Continue"/><br/>
				<span id="perror" class="ui-sys-error-message"></span>
			</form>
		</div>
<?php
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a></div>
					Rule: managegoodreceivednote_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('input.change-amount').on('keyup', function(event)	{
		var $tr1 = $(this).closest('tr');
		if (! $tr1.length) return;
		var $numberBoxes = $tr1.find('input.change-amount');
		if (! $numberBoxes.length) return;
		var product = 1;
		$numberBoxes.each(function(index, val)	{
			var $box1 = $(val);
			var value = 0;
			if ($box1.val()) value = parseInt($box1.val()) 	;
			product = product * value;
		});
		var $amount1 = $tr1.find('label.label-amount');
		if (! $amount1.length) return;
		$amount1.html(getCommaSeparatorFormat(product));
		//We also need to get the grandTotal 
		var $tbody1 = $tr1.closest('tbody');
		if (! $tbody1.length) return;
		var $trList = $tbody1.find('tr.data-row');
		if (! $trList.length) return;
		var total = 0;
		$trList.each(function(index, val)	{
			$tr1 = $(val);
			product = 1;
			$tr1.find('input.change-amount').each(function(index, val)	{
				$box1 = $(val);
				value = 0;
				if ($box1.val()) value = parseInt($box1.val());
				product = product * value;
			});
			total = total + product;
		});
		var $grandTotal1 = $('label.grand-total');
		if (! $grandTotal1.length) return;
		$grandTotal1.html(getCommaSeparatorFormat(total));
	});
	$('select.select-item').on('change', function(event)	{
		var $select1 = $(this).closest('select'); 
		var $option1 = $select1.find('option:selected');
		if (! $option1.length) return; 
		var $tr1 = $select1.closest('tr');
		if (! $tr1.length) return;
		var measureName = $option1.attr('data-measure');
		var $label1 = $tr1.find('label.label-measure');
		if (! $label1.length) return;
		$label1.html(measureName);
	});
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote" && Authorize::isAllowable($config, "managegoodreceivednote", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-order-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managegoodreceivednote_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th></th>
				<th>Order Number</th>
				<th>Supplier</th>
				<th>Created:Date</th>
				<th>Closed:Date</th>
				<th>Currency</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = GoodReceivedNote::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch order data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$note1 = null;
		try {
			$note1 = new GoodReceivedNote($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($note1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
			$creationTime = "";
			if (! is_null($note1->getCreationTime())) $creationTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($note1->getCreationTime());
			$closingTime = "";
			if (! is_null($note1->getClosingTime())) $closingTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($note1->getClosingTime());
			$currencyCode = "";
			if (! is_null($note1->getCurrency())) $currencyCode = $note1->getCurrency()->getCurrencyCode();
			$bgcolor = "blue";
			if ($note1->isStocked()) $bgcolor = "gold";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><span style="background-color: <?= $bgcolor ?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
				<td><?= $note1->getNoteNumber() ?></td>
				<td><?= $note1->getSupplierName() ?></td>
				<td><?= $creationTime ?></td>
				<td><?= $closingTime ?></td>
				<td><?= $currencyCode ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managegoodreceivednote_csv&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managegoodreceivednote_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Create a New Good Received Note" href="<?= $thispage ?>?page=managegoodreceivednote_add&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managegoodreceivednote</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: GoodReceivedNoteManagement*/ else 