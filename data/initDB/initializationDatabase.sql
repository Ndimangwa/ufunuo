-- MySQL dump 10.13  Distrib 5.5.32, for Linux (x86_64)
--
-- Host: localhost    Database: dbufunuo
-- ------------------------------------------------------
-- Server version	5.5.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `dbufunuo`
--
DROP DATABASE IF EXISTS dbufunuo;
CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbufunuo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbufunuo`;

--
-- Table structure for table `AntanaNarivo`
--

DROP TABLE IF EXISTS `AntanaNarivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AntanaNarivo` (
  `antanarivoId` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AntanaNarivo`
--

LOCK TABLES `AntanaNarivo` WRITE;
/*!40000 ALTER TABLE `AntanaNarivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `AntanaNarivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `accountId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `overheadAbsorptionRate` varchar(12) NOT NULL,
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'1.091');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bindingType`
--

DROP TABLE IF EXISTS `bindingType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bindingType` (
  `bindingTypeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bindingTypeName` varchar(16) NOT NULL,
  `bindingTypeDescription` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`bindingTypeId`),
  UNIQUE KEY `bindingTypeName` (`bindingTypeName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bindingType`
--

LOCK TABLES `bindingType` WRITE;
/*!40000 ALTER TABLE `bindingType` DISABLE KEYS */;
INSERT INTO `bindingType` VALUES (1,'Pin',NULL),(2,'Perfect Bind',NULL),(3,'Hard Bind',NULL),(4,'Repair',NULL);
/*!40000 ALTER TABLE `bindingType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextDefinition`
--

DROP TABLE IF EXISTS `contextDefinition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextDefinition` (
  `cId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cChar` char(1) NOT NULL,
  `cVal` int(2) NOT NULL,
  PRIMARY KEY (`cId`),
  UNIQUE KEY `cChar` (`cChar`),
  UNIQUE KEY `cVal` (`cVal`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextDefinition`
--

LOCK TABLES `contextDefinition` WRITE;
/*!40000 ALTER TABLE `contextDefinition` DISABLE KEYS */;
INSERT INTO `contextDefinition` VALUES (1,'0',0),(2,'1',1),(3,'2',2),(4,'3',3),(5,'4',4),(6,'5',5),(7,'6',6),(8,'7',7),(9,'8',8),(10,'9',9),(11,'A',10),(12,'B',11),(13,'C',12),(14,'D',13),(15,'E',14),(16,'F',15),(17,'G',16),(18,'H',17),(19,'I',18),(20,'J',19),(21,'K',20),(22,'L',21),(23,'M',22),(24,'N',23),(25,'O',24),(26,'P',25),(27,'Q',26),(28,'R',27),(29,'S',28),(30,'T',29),(31,'U',30),(32,'X',31);
/*!40000 ALTER TABLE `contextDefinition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextManager`
--

DROP TABLE IF EXISTS `contextManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextManager` (
  `defaultXValue` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`defaultXValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextManager`
--

LOCK TABLES `contextManager` WRITE;
/*!40000 ALTER TABLE `contextManager` DISABLE KEYS */;
INSERT INTO `contextManager` VALUES (1);
/*!40000 ALTER TABLE `contextManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextPosition`
--

DROP TABLE IF EXISTS `contextPosition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextPosition` (
  `cId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cName` varchar(56) NOT NULL,
  `cPosition` int(4) NOT NULL,
  `caption` varchar(96) NOT NULL,
  PRIMARY KEY (`cId`),
  UNIQUE KEY `cName` (`cName`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextPosition`
--

LOCK TABLES `contextPosition` WRITE;
/*!40000 ALTER TABLE `contextPosition` DISABLE KEYS */;
INSERT INTO `contextPosition` VALUES (1,'system_login',1,'Logging into System'),(2,'my_account',2,'My Account Settings'),(3,'manage_groups',3,'General Department Management'),(4,'manage_users',4,'User Management'),(5,'control_panel',5,'Control Panel Operations'),(6,'sign_out',6,'Logout from the System'),(7,'procurement',7,'Procurement Operations'),(8,'store',8,'Store Operations'),(9,'production',9,'Production Operations'),(10,'warehouse',10,'Warehouse Operations'),(11,'depot',11,'Depot Operations'),(12,'accounts',12,'Accounts Operations'),(13,'group_add',13,'Add Department'),(14,'group_delete',14,'Delete Department'),(15,'group_edit',15,'Edit Department'),(16,'group_firewall',16,'Change/View Department Firewall'),(17,'manage_jobtitle',17,'Job Title Management'),(18,'group_view',18,'View group details'),(19,'managecurrency_view',19,'View Currency details'),(20,'managecurrency_delete',20,'Delete Currency Definition'),(21,'managecurrency_edit',21,'Edit Currency Definition'),(22,'managecurrency_add',22,'Add a new Currency Definition'),(23,'managecurrency',23,'Global Currency Management'),(24,'manageitem_view',24,'View Item details'),(25,'manageitem_delete',25,'Delete an Item Definition'),(26,'manageitem_edit',26,'Edit an Item Definition'),(27,'manageitem_add',27,'An an Item Definition'),(28,'manageitem',28,'Global Item Management'),(29,'managemeasure_view',29,'View Measurement details'),(30,'managemeasure_delete',30,'Delete Measurement Definition'),(31,'managemeasure_edit',31,'Edit Measurement Definition'),(32,'managemeasure_add',32,'Add a Measurement Definition'),(33,'managemeasure',33,'Global Measurement Definition'),(34,'files',34,'Global Files Management'),(35,'donotcarelastresort',35,'Last resort for Do not care'),(36,'managejobtitle_view',36,'View Job Title Details'),(37,'managejobtitle_delete',37,'Delete a Job Title'),(38,'managejobtitle_edit',38,'Edit a Job Title'),(39,'managejobtitle_add',39,'Add a New Job Title'),(40,'manageuser_view',40,'View User details'),(41,'manageuser_delete',41,'Delete a user'),(42,'manageuser_edit',42,'Edit a user'),(43,'manageuser_add',43,'Add a user'),(44,'controlpanel_firewall',44,'Firewall Rules'),(45,'controlpanel_firewall_jobtitle',45,'Job Title Firewall'),(46,'controlpanel_firewall_user',46,'User Firewall'),(47,'controlpanel_firewall_group',47,'Group Firewall'),(48,'managepurchasingorder',48,'Purchasing Order Management'),(49,'manageitemstore',49,'General Management of Item Store'),(50,'manageitemdemandstore',50,'General Management of Item Demand Store'),(51,'managegoodreturnednote',51,'General Management of Good Returned Note'),(52,'managedeliverynote',52,'General Management of Delivery Note'),(53,'manageinvoice',53,'General Management of Invoices'),(54,'managepricequote',54,'General Management of Price Quote'),(55,'managefinishedgoodreport',55,'General Management of Finished Good Report'),(56,'manageissuenote',56,'General Management of Issue Note'),(57,'managestockrequisition',57,'General Management of Stock and Store Requisitions'),(58,'manageitemstockrecordcard',58,'General Management of Item Stock Record Card'),(59,'managegoodreceivednote',59,'General Management of Good Received Note'),(88,'managejobticket',61,'General Management of Job Ticket'),(89,'manageproduct',62,'General Management of Products'),(90,'managejobticket_create',63,'Allow creation of Job Ticket'),(91,'managejobticket_details',64,'Allow viewing details of a Job Ticket document'),(92,'manageproduct_add',65,'Allow adding a new Product'),(93,'manageproduct_edit',66,'Allow editing an existing Product'),(94,'manageproduct_view',67,'Allow viewing an existing Product'),(95,'manageproduct_delete',68,'Allow deleting an existing Product'),(96,'managepurchasingorder_snapshotviewer',69,'Allow viewing a snapshot of a Purchasing Order'),(97,'managepurchasingorder_create',70,'Allow creattion of a Purchasing Order'),(98,'managepurchasingorder_details',71,'Allowing viewing details of a Purchasing Order document'),(99,'verificationschema',72,'General Management of Approval Definitions'),(100,'verificationschema_drop',73,'Allow dropping/deleting an existing Approval Definition'),(101,'verificationschema_create',74,'Allow creating of a new Approval Definition'),(102,'verificationschema_details',75,'Allow viewing details of an Approval Definition'),(103,'managestockrequisition_snapshotviewer',76,'Allow viewing a snapshot of a Stock or Store Requisition'),(104,'managestockrequisition_create',77,'Allow creating a New Stock or Store Requisition'),(105,'managestockrequisition_details',78,'Allow viewing details of a Stock or Store Requisition Document'),(106,'managegoodreceivednote_create',79,'Allow creation of a New Good Received Note'),(107,'managegoodreceivednote_details',80,'Allow viewing details of a Good Received Note Document'),(108,'manageissuenote_create',81,'Allow creation of a new Issue Note'),(109,'manageissuenote_details',82,'Allow viewing details of an Issue Note Document'),(110,'managefinishedgoodreport_create',83,'Allow creation of a New Finished Good Report'),(111,'managefinishedgoodreport_details',84,'Allow viewing details of a Finished Good Report Document'),(112,'managepricequote_create',85,'Allow creation of a New Price Quote'),(113,'managepricequote_details',86,'Allow viewing details of a Price Quote Document'),(114,'managefinishedgoodreport_addproduct',87,'Allow Add more product quantity to an existing Finished Good Report'),(115,'managesystemlogs',88,'Allow System Logs management'),(116,'manageitemrequisitionstore',89,'Perform Item Requisition Store management'),(117,'managedeliverynote_create',60,'Create Delivery Note'),(118,'managedeliverynote_details',90,'View Delivery Note Document'),(119,'manageinvoice_create',91,'Create a New Invoice'),(120,'manageinvoice_details',92,'View details of an Invoice Document'),(121,'managegoodreturnednote_create',93,'Create Good Returned Note'),(122,'managegoodreturnednote_details',94,'View/Details of Good Returned Note Document'),(123,'manageitemstockrecordcard_create',95,'Create Item Stock Record Card'),(124,'manageitemstockrecordcard_details',96,'View/Details of Item Stock Record Card'),(125,'managewarehouse',97,'Warehouse Management'),(126,'managejobcostsheet',98,'General Management of a Job Cost Sheet'),(127,'managejobcostsheet_create',99,'Create a New Job Cost Sheet'),(128,'managejobcostsheet_details',100,'View Details of a Job Cost Sheet Document'),(129,'manageformulabank',101,'Formula Bank Management'),(130,'manageoverheadabsorptionrate',102,'Absorption Rate Management'),(131,'manageproductmargin',103,'Product Margin');
/*!40000 ALTER TABLE `contextPosition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `currencyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currencyName` varchar(32) NOT NULL,
  `currencyCode` varchar(4) NOT NULL,
  PRIMARY KEY (`currencyId`),
  UNIQUE KEY `currencyName` (`currencyName`),
  UNIQUE KEY `currencyCode` (`currencyCode`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--


--
-- Table structure for table `deliveryNote`
--

DROP TABLE IF EXISTS `deliveryNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveryNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `noteDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `deliveryNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveryNote`
--


--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `documentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentName` varchar(32) NOT NULL,
  PRIMARY KEY (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (1,'Purchasing Order'),(2,'Good Received Note'),(3,'Job Ticket'),(4,'Stock Requisition'),(5,'Store Requisition'),(6,'Issue Note'),(7,'Finished Good Report'),(8,'Price Quote'),(9,'Delivery Note'),(10,'Invoice'),(11,'Job Cost Sheet'),(12,'Good Returned Note');
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentTransaction`
--

DROP TABLE IF EXISTS `documentTransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentTransaction` (
  `transactionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction` varchar(128) NOT NULL,
  `docRefNumber` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`transactionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentTransaction`
--


--
-- Table structure for table `finishedGoodReport`
--

DROP TABLE IF EXISTS `finishedGoodReport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finishedGoodReport` (
  `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportNumber` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `reportDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  PRIMARY KEY (`reportId`),
  UNIQUE KEY `reportNumber` (`reportNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `finishedGoodReport_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finishedGoodReport`
--


--
-- Table structure for table `goodReceivedNote`
--

DROP TABLE IF EXISTS `goodReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goodReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `noteDate` varchar(19) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `orderId` int(10) unsigned DEFAULT NULL,
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  KEY `orderId` (`orderId`),
  CONSTRAINT `goodReceivedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `goodReceivedNote_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `purchasingOrder` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goodReceivedNote`
--


--
-- Table structure for table `goodReturnedNote`
--

DROP TABLE IF EXISTS `goodReturnedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goodReturnedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `noteDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `documentId` (`documentId`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  CONSTRAINT `goodReturnedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `goodReturnedNote_ibfk_2` FOREIGN KEY (`deliveryNoteId`) REFERENCES `deliveryNote` (`noteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goodReturnedNote`
--


--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `groupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(32) NOT NULL,
  `groupDetails` varchar(64) DEFAULT NULL,
  `context` char(128) NOT NULL DEFAULT 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB  AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--
LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Local Group','Tanzania Adventist Press','XXXXXXXXXX0X10XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX0XXXXXXXXXXXXXXXXXXXXXXXXXXX0X0XXXXXXXXXXXXX0X0XXXXX0XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;
--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoiceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `invoiceDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  PRIMARY KEY (`invoiceId`),
  UNIQUE KEY `invoiceNumber` (`invoiceNumber`),
  KEY `documentId` (`documentId`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`deliveryNoteId`) REFERENCES `deliveryNote` (`noteId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--


--
-- Table structure for table `issueNote`
--

DROP TABLE IF EXISTS `issueNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issueNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `requisitionId` int(10) unsigned NOT NULL,
  `issueDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `documentId` (`documentId`),
  KEY `requisitionId` (`requisitionId`),
  CONSTRAINT `issueNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `issueNote_ibfk_2` FOREIGN KEY (`requisitionId`) REFERENCES `stockRequisition` (`reqId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issueNote`
--


--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `itemId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemName` varchar(128) NOT NULL,
  `itemDescription` varchar(128) DEFAULT NULL,
  `measureId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`itemId`),
  UNIQUE KEY `itemName` (`itemName`),
  KEY `measureId` (`measureId`),
  CONSTRAINT `item_ibfk_1` FOREIGN KEY (`measureId`) REFERENCES `measure` (`measureId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--


--
-- Table structure for table `itemDemandStore`
--

DROP TABLE IF EXISTS `itemDemandStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemDemandStore` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `itemId_2` (`itemId`),
  CONSTRAINT `itemDemandStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemDemandStore`
--

--
-- Table structure for table `itemRequisitionStore`
--

DROP TABLE IF EXISTS `itemRequisitionStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemRequisitionStore` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(17) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `itemId_2` (`itemId`),
  CONSTRAINT `itemRequisitionStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemRequisitionStore`
--

-- Table structure for table `itemStore`
--

DROP TABLE IF EXISTS `itemStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemStore` (
  `itemStoreId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `itemValue` int(17) unsigned NOT NULL DEFAULT '0',
  `minStockLevel` int(17) unsigned NOT NULL DEFAULT '0',
  `orderingLevel` int(17) unsigned NOT NULL DEFAULT '0',
  `itemPrice` int(17) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemStoreId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `itemStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemStore`
--


--
-- Table structure for table `jobCostSheet`
--

DROP TABLE IF EXISTS `jobCostSheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobCostSheet` (
  `jobCostId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobCostNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `jobCostOpeningDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `jobCostWantedDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `jobCostClosingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `productId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`jobCostId`),
  KEY `documentId` (`documentId`),
  KEY `productId` (`productId`),
  CONSTRAINT `jobCostSheet_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `jobCostSheet_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobCostSheet`
--


--
-- Table structure for table `jobTicket`
--

DROP TABLE IF EXISTS `jobTicket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobTicket` (
  `ticketId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticketNumber` varchar(24) NOT NULL,
  `ticketDetails` varchar(64) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `openingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `wantedDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(40) DEFAULT NULL,
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ticketId`),
  UNIQUE KEY `ticketNumber` (`ticketNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `jobTicket_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobTicket`
--


--
-- Table structure for table `jobTitle`
--

DROP TABLE IF EXISTS `jobTitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobTitle` (
  `jobId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobName` varchar(64) NOT NULL,
  `context` char(128) NOT NULL DEFAULT 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
  PRIMARY KEY (`jobId`),
  UNIQUE KEY `jobName` (`jobName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobTitle`
--

LOCK TABLES `jobTitle` WRITE;
/*!40000 ALTER TABLE `jobTitle` DISABLE KEYS */;
INSERT INTO `jobTitle` VALUES (1,'Local Admin','XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
/*!40000 ALTER TABLE `jobTitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `loginId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` char(40) NOT NULL,
  `root` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loginId`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'administrator','b3aca92c793ee0e9b1a9b0a5f5fc044e05140df3',1);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measure` (
  `measureId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `measureName` varchar(24) NOT NULL,
  `measureDescription` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`measureId`),
  UNIQUE KEY `measureName` (`measureName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure`
--

--
-- Table structure for table `paperSize`
--

DROP TABLE IF EXISTS `paperSize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paperSize` (
  `paperSizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paperSizeName` varchar(8) NOT NULL,
  `paperSizeDetails` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`paperSizeId`),
  UNIQUE KEY `paperSizeName` (`paperSizeName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paperSize`
--

LOCK TABLES `paperSize` WRITE;
/*!40000 ALTER TABLE `paperSize` DISABLE KEYS */;
INSERT INTO `paperSize` VALUES (1,'A0',NULL),(2,'A1',NULL),(3,'A2',NULL),(4,'A3',NULL),(5,'A4',NULL),(6,'A5',NULL),(7,'A6',NULL),(8,'A7',NULL);
/*!40000 ALTER TABLE `paperSize` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priceQuote`
--

DROP TABLE IF EXISTS `priceQuote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priceQuote` (
  `quoteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quoteNumber` varchar(36) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `quoteDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  PRIMARY KEY (`quoteId`),
  UNIQUE KEY `quoteNumber` (`quoteNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `priceQuote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priceQuote`
--

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `productId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productName` varchar(32) NOT NULL,
  `productDescription` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`productId`),
  UNIQUE KEY `productName` (`productName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

--
-- Table structure for table `purchasingOrder`
--

DROP TABLE IF EXISTS `purchasingOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchasingOrder` (
  `orderId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `supplierName` varchar(32) DEFAULT NULL,
  `supplierAddress` varchar(64) DEFAULT NULL,
  `supplierPhone` varchar(14) DEFAULT NULL,
  `orderDate` varchar(19) DEFAULT NULL,
  `currencyId` int(10) unsigned DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `code` char(40) NOT NULL,
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `orderNumber` (`orderNumber`),
  KEY `documentId` (`documentId`),
  KEY `currencyId` (`currencyId`),
  CONSTRAINT `purchasingOrder_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `purchasingOrder_ibfk_2` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchasingOrder`
--


--
-- Table structure for table `stockRequisition`
--

DROP TABLE IF EXISTS `stockRequisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockRequisition` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqNo` varchar(24) NOT NULL,
  `sheetId` int(10) unsigned DEFAULT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `groupId` int(10) unsigned NOT NULL,
  `debtAccount` varchar(32) NOT NULL,
  `reqDate` varchar(19) NOT NULL,
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `issueDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `reqNo` (`reqNo`),
  KEY `sheetId` (`sheetId`),
  KEY `documentId` (`documentId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `stockRequisition_ibfk_1` FOREIGN KEY (`sheetId`) REFERENCES `jobCostSheet` (`jobCostId`),
  CONSTRAINT `stockRequisition_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `stockRequisition_ibfk_3` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stockRequisition`
--

--
-- Table structure for table `systemlogs`
--

DROP TABLE IF EXISTS `systemlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemlogs` (
  `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logName` varchar(108) NOT NULL,
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemlogs`
--

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `testId` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `testName` varchar(12) NOT NULL,
  PRIMARY KEY (`testId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loginId` int(10) unsigned NOT NULL,
  `groupId` int(10) unsigned NOT NULL,
  `fullName` varchar(32) NOT NULL,
  `userDetails` varchar(64) DEFAULT NULL,
  `context` char(128) NOT NULL DEFAULT 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
  `jobId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `fullName` (`fullName`),
  KEY `loginId` (`loginId`),
  KEY `groupId` (`groupId`),
  KEY `jobId` (`jobId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`loginId`) REFERENCES `login` (`loginId`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`jobId`) REFERENCES `jobTitle` (`jobId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,1,'Local User TAP','Tanzania Adventist Press','XXXXXXXXXX0X10XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX0XXXXXXXXXXXXXXXXXXXXXXXXXXX0X0XXXXXXXXXXXXX0X0XXXXX0XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificationData`
--

DROP TABLE IF EXISTS `verificationData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificationData` (
  `verificationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schemaId` int(10) unsigned NOT NULL,
  `docRefNumber` varchar(24) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userId` int(10) unsigned DEFAULT NULL,
  `dt` varchar(19) DEFAULT NULL,
  PRIMARY KEY (`verificationId`),
  UNIQUE KEY `schemaId_2` (`schemaId`,`docRefNumber`),
  KEY `schemaId` (`schemaId`),
  KEY `userId` (`userId`),
  CONSTRAINT `verificationData_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`),
  CONSTRAINT `verificationData_ibfk_2` FOREIGN KEY (`schemaId`) REFERENCES `verificationSchema` (`verificationId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificationData`
--

--
-- Table structure for table `verificationSchema`
--

DROP TABLE IF EXISTS `verificationSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificationSchema` (
  `verificationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `captionText` varchar(32) NOT NULL,
  `captionError` varchar(32) NOT NULL,
  `jobId` int(10) unsigned NOT NULL,
  `docId` int(10) unsigned NOT NULL,
  `seqno` int(4) unsigned NOT NULL,
  PRIMARY KEY (`verificationId`),
  UNIQUE KEY `docId` (`docId`,`seqno`),
  KEY `jobId` (`jobId`),
  KEY `docId_2` (`docId`),
  CONSTRAINT `verificationSchema_ibfk_1` FOREIGN KEY (`jobId`) REFERENCES `jobTitle` (`jobId`),
  CONSTRAINT `verificationSchema_ibfk_2` FOREIGN KEY (`docId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificationSchema`
--

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouse` (
  `warehouseId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(17) unsigned NOT NULL DEFAULT '0',
  `productValue` int(17) unsigned NOT NULL DEFAULT '0',
  `productSellingPrice` int(17) unsigned NOT NULL DEFAULT '0',
  `margin` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`warehouseId`),
  UNIQUE KEY `productId` (`productId`),
  KEY `productId_2` (`productId`),
  CONSTRAINT `warehouse_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-02 14:08:28
INSERT INTO contextPosition(cId, cName, cPosition, caption) VALUES('391', 'managecontextmanager', '391', 'managecontextmanager');
