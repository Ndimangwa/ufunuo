use dbufunuo;
alter table document add approvalSchemaList varchar(32) default null after imagename;
alter table document modify documentName varchar(64) not null;
delete from document;
alter table document add className varchar(32) default null after documentCode;
insert into document (documentName, documentCode, className, pagename) values('Delivery Note', '1', 'DeliveryNote', '1');
insert into document (documentName, documentCode, className, pagename) values('Finished Good Report', '2', 'FinishedGoodReport', '2');
insert into document (documentName, documentCode, className, pagename) values('Good Received Note (Raw Material)', '3', 'GoodReceivedNote', '3');
insert into document (documentName, documentCode, className, pagename) values('Good Returned Note', '4', 'GoodReturnedNote', '4');
insert into document (documentName, documentCode, className, pagename) values('Invoice', '5', 'Invoice', '5');
insert into document (documentName, documentCode, className, pagename) values('Issue Note', '6', 'IssueNote', '6');
insert into document (documentName, documentCode, className, pagename) values('Job Cost Sheet', '7', 'JobCostSheet', '7');
insert into document (documentName, documentCode, className, pagename) values('Price Quote', '8', 'PriceQuote', '8');
insert into document (documentName, documentCode, className, pagename) values('Purchasing Order (Product)', '9', 'ProductPurchasingOrder', '9');
insert into document (documentName, documentCode, className, pagename) values('Good Received Note (Product)', '10', 'ProductGoodReceivedNote', '10');
insert into document (documentName, documentCode, className, pagename) values('Purchasing Order (Raw Material)', '11', 'PurchasingOrder', '11');
insert into document (documentName, documentCode, className, pagename) values('Stock Requisition', '12', 'StockRequisition', '12');
insert into document (documentName, documentCode, className, pagename) values('[Local] Store Requisition', '13', 'LocalStoreRequisition', '13');
insert into document (documentName, documentCode, className, pagename) values('[Local Raw Material] Delivery Note', '14', 'LocalItemDeliveryNote', '14');
insert into document (documentName, documentCode, className, pagename) values('[Local Raw Material] Good Received Note', '15', 'LocalGoodReceivedNote', '15');
insert into document (documentName, documentCode, className, pagename) values('[Local] Warehouse Requisition', '16', 'LocalWarehouseRequisition', '16');
insert into document (documentName, documentCode, className, pagename) values('[Local Product] Delivery Note', '17', 'LocalProductDeliveryNote', '17');
insert into document (documentName, documentCode, className, pagename) values('[Local Product] Good Received Note', '18', 'LocalProductReceivedNote', '18');
create table documentApprovalSchema (
	schemaId int(10) unsigned not null auto_increment,
	captionText varchar(48) not null,
	captionError varchar(48) not null,
	jobId int(10) unsigned not null,
	 `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
	primary key (schemaId)
) engine=InnoDB;
alter table deliveryNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table deliveryNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table deliveryNote add constraint next_schema_to_approve_job_id_1 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table finishedGoodReport add alreadyApprovedList varchar(32) default null after filenameHash;
alter table finishedGoodReport add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table finishedGoodReport add constraint next_schema_to_approve_job_id_2 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table goodReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table goodReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table goodReceivedNote add constraint next_schema_to_approve_job_id_3 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table goodReturnedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table goodReturnedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table goodReturnedNote add constraint next_schema_to_approve_job_id_4 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table invoice add alreadyApprovedList varchar(32) default null after filenameHash;
alter table invoice add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table invoice add constraint next_schema_to_approve_job_id_5 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table issueNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table issueNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table issueNote add constraint next_schema_to_approve_job_id_6 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table jobCostSheet add alreadyApprovedList varchar(32) default null after filenameHash;
alter table jobCostSheet add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table jobCostSheet add constraint next_schema_to_approve_job_id_7 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table priceQuote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table priceQuote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table priceQuote add constraint next_schema_to_approve_job_id_8 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table productPurchasingOrder add alreadyApprovedList varchar(32) default null after filenameHash;
alter table productPurchasingOrder add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table productPurchasingOrder add constraint next_schema_to_approve_job_id_9 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table productGoodReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table productGoodReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table productGoodReceivedNote add constraint next_schema_to_approve_job_id_10 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table purchasingOrder add alreadyApprovedList varchar(32) default null after filenameHash;
alter table purchasingOrder add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table purchasingOrder add constraint next_schema_to_approve_job_id_11 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table stockRequisition add alreadyApprovedList varchar(32) default null after filenameHash;
alter table stockRequisition add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table stockRequisition add constraint next_schema_to_approve_job_id_12 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localStoreRequisition add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localStoreRequisition add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localStoreRequisition add constraint next_schema_to_approve_job_id_13 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localItemDeliveryNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localItemDeliveryNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localItemDeliveryNote add constraint next_schema_to_approve_job_id_14 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localGoodReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localGoodReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localGoodReceivedNote add constraint next_schema_to_approve_job_id_15 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localWarehouseRequisition add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localWarehouseRequisition add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localWarehouseRequisition add constraint next_schema_to_approve_job_id_16 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localProductDeliveryNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localProductDeliveryNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localProductDeliveryNote add constraint next_schema_to_approve_job_id_17 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localProductReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localProductReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localProductReceivedNote add constraint next_schema_to_approve_job_id_18 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
insert into contextPosition (cId, cName, cPosition, caption) values('395', 'managedocumentapprovalschema', '395', 'managedocumentapprovalschema');
insert into contextPosition (cId, cName, cPosition, caption) values('396', 'managedocumentapprovalschema_add', '396', 'managedocumentapprovalschema_add');
insert into contextPosition (cId, cName, cPosition, caption) values('397', 'managedocumentapprovalschema_detail', '397', 'managedocumentapprovalschema_detail');
insert into contextPosition (cId, cName, cPosition, caption) values('398', 'managedocumentapprovalschema_edit', '398', 'managedocumentapprovalschema_edit');
insert into contextPosition (cId, cName, cPosition, caption) values('399', 'managedocumentapprovalschema_delete', '399', 'managedocumentapprovalschema_delete');
insert into contextPosition (cId, cName, cPosition, caption) values('400', 'managedocumentapprovalschema_csv', '400', 'managedocumentapprovalschema_csv');
alter table profile add tinNumber varchar(32) default null after revisionTime;
create table notification (
	notificationId int(10) unsigned not null auto_increment,
	notificationText varchar(108) not null,
	buttonText varchar(48) not null,
	contextName varchar(56) not null,
	serverPage varchar(108) not null,
	serverArguments varchar(255) not null,
	postedTime varchar(19) not null default '0000:00:00:00:00:00',
	 `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
	primary key (notificationId)
) engine=InnoDB;