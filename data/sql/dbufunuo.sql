-- MySQL dump 10.15  Distrib 10.0.23-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: dbufunuo
-- ------------------------------------------------------
-- Server version	10.0.23-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `dbufunuo`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbufunuo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbufunuo`;

--
-- Table structure for table `AntanaNarivo`
--

DROP TABLE IF EXISTS `AntanaNarivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AntanaNarivo` (
  `antanarivoId` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AntanaNarivo`
--

LOCK TABLES `AntanaNarivo` WRITE;
/*!40000 ALTER TABLE `AntanaNarivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `AntanaNarivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `accountId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `overheadAbsorptionRate` varchar(16) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'1.091',NULL,NULL,0);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `approvalSequenceData`
--

DROP TABLE IF EXISTS `approvalSequenceData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `approvalSequenceData` (
  `dataId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schemaId` int(10) unsigned NOT NULL,
  `requestedBy` int(10) unsigned NOT NULL,
  `nextJobToApprove` int(10) unsigned DEFAULT NULL,
  `alreadyApprovedList` varchar(108) DEFAULT NULL,
  `timeOfRegistration` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `specialInstruction` varchar(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(108) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dataId`),
  KEY `schemaId` (`schemaId`),
  KEY `requestedBy` (`requestedBy`),
  KEY `nextJobToApprove` (`nextJobToApprove`),
  CONSTRAINT `approvalSequenceData_ibfk_1` FOREIGN KEY (`schemaId`) REFERENCES `approvalSequenceSchema` (`schemaId`),
  CONSTRAINT `approvalSequenceData_ibfk_2` FOREIGN KEY (`requestedBy`) REFERENCES `login` (`loginId`),
  CONSTRAINT `approvalSequenceData_ibfk_3` FOREIGN KEY (`nextJobToApprove`) REFERENCES `jobTitle` (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `approvalSequenceData`
--

LOCK TABLES `approvalSequenceData` WRITE;
/*!40000 ALTER TABLE `approvalSequenceData` DISABLE KEYS */;
/*!40000 ALTER TABLE `approvalSequenceData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `approvalSequenceSchema`
--

DROP TABLE IF EXISTS `approvalSequenceSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `approvalSequenceSchema` (
  `schemaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schemaName` varchar(48) NOT NULL,
  `operationCode` int(4) unsigned NOT NULL,
  `approvedJobList` varchar(108) DEFAULT NULL,
  `approvingJobList` varchar(108) DEFAULT NULL,
  `validity` int(4) unsigned NOT NULL DEFAULT '7',
  `defaultApproveText` varchar(48) NOT NULL DEFAULT 'Approved By',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`schemaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `approvalSequenceSchema`
--

LOCK TABLES `approvalSequenceSchema` WRITE;
/*!40000 ALTER TABLE `approvalSequenceSchema` DISABLE KEYS */;
/*!40000 ALTER TABLE `approvalSequenceSchema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bindingType`
--

DROP TABLE IF EXISTS `bindingType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bindingType` (
  `bindingTypeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bindingTypeName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bindingTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bindingType`
--

LOCK TABLES `bindingType` WRITE;
/*!40000 ALTER TABLE `bindingType` DISABLE KEYS */;
/*!40000 ALTER TABLE `bindingType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference`
--

DROP TABLE IF EXISTS `conference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference` (
  `conferenceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conferenceName` varchar(64) NOT NULL,
  `conferenceAbbreviation` varchar(16) DEFAULT NULL,
  `locationList` varchar(108) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`conferenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference`
--

LOCK TABLES `conference` WRITE;
/*!40000 ALTER TABLE `conference` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextDefinition`
--

DROP TABLE IF EXISTS `contextDefinition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextDefinition` (
  `cId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cChar` char(1) NOT NULL,
  `cVal` int(2) NOT NULL,
  PRIMARY KEY (`cId`),
  UNIQUE KEY `cChar` (`cChar`),
  UNIQUE KEY `cVal` (`cVal`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextDefinition`
--

LOCK TABLES `contextDefinition` WRITE;
/*!40000 ALTER TABLE `contextDefinition` DISABLE KEYS */;
INSERT INTO `contextDefinition` VALUES (1,'0',0),(2,'1',1),(3,'2',2),(4,'3',3),(5,'4',4),(6,'5',5),(7,'6',6),(8,'7',7),(9,'8',8),(10,'9',9),(11,'A',10),(12,'B',11),(13,'C',12),(14,'D',13),(15,'E',14),(16,'F',15),(17,'G',16),(18,'H',17),(19,'I',18),(20,'J',19),(21,'K',20),(22,'L',21),(23,'M',22),(24,'N',23),(25,'O',24),(26,'P',25),(27,'Q',26),(28,'R',27),(29,'S',28),(30,'T',29),(31,'U',30),(32,'X',31);
/*!40000 ALTER TABLE `contextDefinition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextLookup`
--

DROP TABLE IF EXISTS `contextLookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextLookup` (
  `contextId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `symbol` char(1) NOT NULL,
  `value` char(4) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`contextId`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextLookup`
--

LOCK TABLES `contextLookup` WRITE;
/*!40000 ALTER TABLE `contextLookup` DISABLE KEYS */;
INSERT INTO `contextLookup` VALUES (1,'A','0000',NULL,NULL,15),(2,'B','0001',NULL,NULL,15),(3,'C','0002',NULL,NULL,15),(4,'D','0010',NULL,NULL,15),(5,'E','0011',NULL,NULL,15),(6,'F','0012',NULL,NULL,15),(7,'G','0020',NULL,NULL,15),(8,'H','0021',NULL,NULL,15),(9,'I','0022',NULL,NULL,15),(10,'J','0100',NULL,NULL,15),(11,'K','0101',NULL,NULL,15),(12,'L','0102',NULL,NULL,15),(13,'M','0110',NULL,NULL,15),(14,'N','0111',NULL,NULL,15),(15,'O','0112',NULL,NULL,15),(16,'P','0120',NULL,NULL,15),(17,'Q','0121',NULL,NULL,15),(18,'R','0122',NULL,NULL,15),(19,'S','0200',NULL,NULL,15),(20,'T','0201',NULL,NULL,15),(21,'U','0202',NULL,NULL,15),(22,'V','0210',NULL,NULL,15),(23,'W','0211',NULL,NULL,15),(24,'X','0212',NULL,NULL,15),(25,'Y','0220',NULL,NULL,15),(26,'Z','0221',NULL,NULL,15),(27,'a','0222',NULL,NULL,15),(28,'b','1000',NULL,NULL,15),(29,'c','1001',NULL,NULL,15),(30,'d','1002',NULL,NULL,15),(31,'e','1010',NULL,NULL,15),(32,'f','1011',NULL,NULL,15),(33,'g','1012',NULL,NULL,15),(34,'h','1020',NULL,NULL,15),(35,'i','1021',NULL,NULL,15),(36,'j','1022',NULL,NULL,15),(37,'k','1100',NULL,NULL,15),(38,'l','1101',NULL,NULL,15),(39,'m','1102',NULL,NULL,15),(40,'n','1110',NULL,NULL,15),(41,'o','1111',NULL,NULL,15),(42,'p','1112',NULL,NULL,15),(43,'q','1120',NULL,NULL,15),(44,'r','1121',NULL,NULL,15),(45,'s','1122',NULL,NULL,15),(46,'t','1200',NULL,NULL,15),(47,'u','1201',NULL,NULL,15),(48,'v','1202',NULL,NULL,15),(49,'w','1210',NULL,NULL,15),(50,'x','1211',NULL,NULL,15),(51,'y','1212',NULL,NULL,15),(52,'z','1220',NULL,NULL,15),(53,'0','1221',NULL,NULL,15),(54,'1','1222',NULL,NULL,15),(55,'2','2000',NULL,NULL,15),(56,'3','2001',NULL,NULL,15),(57,'4','2002',NULL,NULL,15),(58,'5','2010',NULL,NULL,15),(59,'6','2011',NULL,NULL,15),(60,'7','2012',NULL,NULL,15),(61,'8','2020',NULL,NULL,15),(62,'9','2021',NULL,NULL,15),(63,'!','2022',NULL,NULL,15),(64,'@','2100',NULL,NULL,15),(65,'#','2101',NULL,NULL,15),(66,'%','2102',NULL,NULL,15),(67,'&','2110',NULL,NULL,15),(68,'(','2111',NULL,NULL,15),(69,')','2112',NULL,NULL,15),(70,'{','2120',NULL,NULL,15),(71,'}','2121',NULL,NULL,15),(72,'[','2122',NULL,NULL,15),(73,']','2200',NULL,NULL,15),(74,'<','2201',NULL,NULL,15),(75,'>','2202',NULL,NULL,15),(76,'?','2210',NULL,NULL,15),(77,'+','2211',NULL,NULL,15),(78,'*','2212',NULL,NULL,15),(79,'|','2220',NULL,NULL,15),(80,':','2221',NULL,NULL,15),(81,';','2222',NULL,NULL,15);
/*!40000 ALTER TABLE `contextLookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextManager`
--

DROP TABLE IF EXISTS `contextManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextManager` (
  `defaultXValue` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`defaultXValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextManager`
--

LOCK TABLES `contextManager` WRITE;
/*!40000 ALTER TABLE `contextManager` DISABLE KEYS */;
INSERT INTO `contextManager` VALUES (1);
/*!40000 ALTER TABLE `contextManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextPosition`
--

DROP TABLE IF EXISTS `contextPosition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextPosition` (
  `cId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cName` varchar(56) NOT NULL,
  `cPosition` int(4) NOT NULL,
  `caption` varchar(96) NOT NULL,
  PRIMARY KEY (`cId`),
  UNIQUE KEY `cName` (`cName`)
) ENGINE=InnoDB AUTO_INCREMENT=395 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextPosition`
--

LOCK TABLES `contextPosition` WRITE;
/*!40000 ALTER TABLE `contextPosition` DISABLE KEYS */;
INSERT INTO `contextPosition` VALUES (1,'managephptimezone',1,'managephptimezone'),(2,'managephptimezone_add',2,'managephptimezone_add'),(3,'managephptimezone_detail',3,'managephptimezone_detail'),(4,'managephptimezone_edit',4,'managephptimezone_edit'),(5,'managephptimezone_delete',5,'managephptimezone_delete'),(6,'managephptimezone_csv',6,'managephptimezone_csv'),(7,'managedaysofaweek',7,'managedaysofaweek'),(8,'managedaysofaweek_add',8,'managedaysofaweek_add'),(9,'managedaysofaweek_detail',9,'managedaysofaweek_detail'),(10,'managedaysofaweek_edit',10,'managedaysofaweek_edit'),(11,'managedaysofaweek_delete',11,'managedaysofaweek_delete'),(12,'managedaysofaweek_csv',12,'managedaysofaweek_csv'),(13,'managemonthofayear',13,'managemonthofayear'),(14,'managemonthofayear_add',14,'managemonthofayear_add'),(15,'managemonthofayear_detail',15,'managemonthofayear_detail'),(16,'managemonthofayear_edit',16,'managemonthofayear_edit'),(17,'managemonthofayear_delete',17,'managemonthofayear_delete'),(18,'managemonthofayear_csv',18,'managemonthofayear_csv'),(19,'managetheme',19,'managetheme'),(20,'managetheme_add',20,'managetheme_add'),(21,'managetheme_detail',21,'managetheme_detail'),(22,'managetheme_edit',22,'managetheme_edit'),(23,'managetheme_delete',23,'managetheme_delete'),(24,'managetheme_csv',24,'managetheme_csv'),(25,'managecountry',25,'managecountry'),(26,'managecountry_add',26,'managecountry_add'),(27,'managecountry_detail',27,'managecountry_detail'),(28,'managecountry_edit',28,'managecountry_edit'),(29,'managecountry_delete',29,'managecountry_delete'),(30,'managecountry_csv',30,'managecountry_csv'),(31,'manageprofile',31,'manageprofile'),(32,'manageprofile_add',32,'manageprofile_add'),(33,'manageprofile_detail',33,'manageprofile_detail'),(34,'manageprofile_edit',34,'manageprofile_edit'),(35,'manageprofile_delete',35,'manageprofile_delete'),(36,'manageprofile_csv',36,'manageprofile_csv'),(37,'managelocation',37,'managelocation'),(38,'managelocation_add',38,'managelocation_add'),(39,'managelocation_detail',39,'managelocation_detail'),(40,'managelocation_edit',40,'managelocation_edit'),(41,'managelocation_delete',41,'managelocation_delete'),(42,'managelocation_csv',42,'managelocation_csv'),(43,'manageconference',43,'manageconference'),(44,'manageconference_add',44,'manageconference_add'),(45,'manageconference_detail',45,'manageconference_detail'),(46,'manageconference_edit',46,'manageconference_edit'),(47,'manageconference_delete',47,'manageconference_delete'),(48,'manageconference_csv',48,'manageconference_csv'),(49,'manageaccount',49,'manageaccount'),(50,'manageaccount_add',50,'manageaccount_add'),(51,'manageaccount_detail',51,'manageaccount_detail'),(52,'manageaccount_edit',52,'manageaccount_edit'),(53,'manageaccount_delete',53,'manageaccount_delete'),(54,'manageaccount_csv',54,'manageaccount_csv'),(55,'managebindingtype',55,'managebindingtype'),(56,'managebindingtype_add',56,'managebindingtype_add'),(57,'managebindingtype_detail',57,'managebindingtype_detail'),(58,'managebindingtype_edit',58,'managebindingtype_edit'),(59,'managebindingtype_delete',59,'managebindingtype_delete'),(60,'managebindingtype_csv',60,'managebindingtype_csv'),(61,'managecurrency',61,'managecurrency'),(62,'managecurrency_add',62,'managecurrency_add'),(63,'managecurrency_detail',63,'managecurrency_detail'),(64,'managecurrency_edit',64,'managecurrency_edit'),(65,'managecurrency_delete',65,'managecurrency_delete'),(66,'managecurrency_csv',66,'managecurrency_csv'),(67,'managecurrencyconversion',67,'managecurrencyconversion'),(68,'managecurrencyconversion_add',68,'managecurrencyconversion_add'),(69,'managecurrencyconversion_detail',69,'managecurrencyconversion_detail'),(70,'managecurrencyconversion_edit',70,'managecurrencyconversion_edit'),(71,'managecurrencyconversion_delete',71,'managecurrencyconversion_delete'),(72,'managecurrencyconversion_csv',72,'managecurrencyconversion_csv'),(73,'managedocument',73,'managedocument'),(74,'managedocument_add',74,'managedocument_add'),(75,'managedocument_detail',75,'managedocument_detail'),(76,'managedocument_edit',76,'managedocument_edit'),(77,'managedocument_delete',77,'managedocument_delete'),(78,'managedocument_csv',78,'managedocument_csv'),(79,'managedocumenttransaction',79,'managedocumenttransaction'),(80,'managedocumenttransaction_add',80,'managedocumenttransaction_add'),(81,'managedocumenttransaction_detail',81,'managedocumenttransaction_detail'),(82,'managedocumenttransaction_edit',82,'managedocumenttransaction_edit'),(83,'managedocumenttransaction_delete',83,'managedocumenttransaction_delete'),(84,'managedocumenttransaction_csv',84,'managedocumenttransaction_csv'),(85,'managedeliverynote',85,'managedeliverynote'),(86,'managedeliverynote_add',86,'managedeliverynote_add'),(87,'managedeliverynote_detail',87,'managedeliverynote_detail'),(88,'managedeliverynote_edit',88,'managedeliverynote_edit'),(89,'managedeliverynote_delete',89,'managedeliverynote_delete'),(90,'managedeliverynote_csv',90,'managedeliverynote_csv'),(91,'managefinishedgoodreport',91,'managefinishedgoodreport'),(92,'managefinishedgoodreport_add',92,'managefinishedgoodreport_add'),(93,'managefinishedgoodreport_detail',93,'managefinishedgoodreport_detail'),(94,'managefinishedgoodreport_edit',94,'managefinishedgoodreport_edit'),(95,'managefinishedgoodreport_delete',95,'managefinishedgoodreport_delete'),(96,'managefinishedgoodreport_csv',96,'managefinishedgoodreport_csv'),(97,'managegoodreceivednote',97,'managegoodreceivednote'),(98,'managegoodreceivednote_add',98,'managegoodreceivednote_add'),(99,'managegoodreceivednote_detail',99,'managegoodreceivednote_detail'),(100,'managegoodreceivednote_edit',100,'managegoodreceivednote_edit'),(101,'managegoodreceivednote_delete',101,'managegoodreceivednote_delete'),(102,'managegoodreceivednote_csv',102,'managegoodreceivednote_csv'),(103,'managegoodreturnednote',103,'managegoodreturnednote'),(104,'managegoodreturnednote_add',104,'managegoodreturnednote_add'),(105,'managegoodreturnednote_detail',105,'managegoodreturnednote_detail'),(106,'managegoodreturnednote_edit',106,'managegoodreturnednote_edit'),(107,'managegoodreturnednote_delete',107,'managegoodreturnednote_delete'),(108,'managegoodreturnednote_csv',108,'managegoodreturnednote_csv'),(109,'manageinvoice',109,'manageinvoice'),(110,'manageinvoice_add',110,'manageinvoice_add'),(111,'manageinvoice_detail',111,'manageinvoice_detail'),(112,'manageinvoice_edit',112,'manageinvoice_edit'),(113,'manageinvoice_delete',113,'manageinvoice_delete'),(114,'manageinvoice_csv',114,'manageinvoice_csv'),(115,'managepaymentreceipt',115,'managepaymentreceipt'),(116,'managepaymentreceipt_add',116,'managepaymentreceipt_add'),(117,'managepaymentreceipt_detail',117,'managepaymentreceipt_detail'),(118,'managepaymentreceipt_edit',118,'managepaymentreceipt_edit'),(119,'managepaymentreceipt_delete',119,'managepaymentreceipt_delete'),(120,'managepaymentreceipt_csv',120,'managepaymentreceipt_csv'),(121,'manageissuenote',121,'manageissuenote'),(122,'manageissuenote_add',122,'manageissuenote_add'),(123,'manageissuenote_detail',123,'manageissuenote_detail'),(124,'manageissuenote_edit',124,'manageissuenote_edit'),(125,'manageissuenote_delete',125,'manageissuenote_delete'),(126,'manageissuenote_csv',126,'manageissuenote_csv'),(127,'managejobcostsheet',127,'managejobcostsheet'),(128,'managejobcostsheet_add',128,'managejobcostsheet_add'),(129,'managejobcostsheet_detail',129,'managejobcostsheet_detail'),(130,'managejobcostsheet_edit',130,'managejobcostsheet_edit'),(131,'managejobcostsheet_delete',131,'managejobcostsheet_delete'),(132,'managejobcostsheet_csv',132,'managejobcostsheet_csv'),(133,'managepricequote',133,'managepricequote'),(134,'managepricequote_add',134,'managepricequote_add'),(135,'managepricequote_detail',135,'managepricequote_detail'),(136,'managepricequote_edit',136,'managepricequote_edit'),(137,'managepricequote_delete',137,'managepricequote_delete'),(138,'managepricequote_csv',138,'managepricequote_csv'),(139,'manageproductpurchasingorder',139,'manageproductpurchasingorder'),(140,'manageproductpurchasingorder_add',140,'manageproductpurchasingorder_add'),(141,'manageproductpurchasingorder_detail',141,'manageproductpurchasingorder_detail'),(142,'manageproductpurchasingorder_edit',142,'manageproductpurchasingorder_edit'),(143,'manageproductpurchasingorder_delete',143,'manageproductpurchasingorder_delete'),(144,'manageproductpurchasingorder_csv',144,'manageproductpurchasingorder_csv'),(145,'manageproductgoodreceivednote',145,'manageproductgoodreceivednote'),(146,'manageproductgoodreceivednote_add',146,'manageproductgoodreceivednote_add'),(147,'manageproductgoodreceivednote_detail',147,'manageproductgoodreceivednote_detail'),(148,'manageproductgoodreceivednote_edit',148,'manageproductgoodreceivednote_edit'),(149,'manageproductgoodreceivednote_delete',149,'manageproductgoodreceivednote_delete'),(150,'manageproductgoodreceivednote_csv',150,'manageproductgoodreceivednote_csv'),(151,'managepurchasingorder',151,'managepurchasingorder'),(152,'managepurchasingorder_add',152,'managepurchasingorder_add'),(153,'managepurchasingorder_detail',153,'managepurchasingorder_detail'),(154,'managepurchasingorder_edit',154,'managepurchasingorder_edit'),(155,'managepurchasingorder_delete',155,'managepurchasingorder_delete'),(156,'managepurchasingorder_csv',156,'managepurchasingorder_csv'),(157,'managestockrequisition',157,'managestockrequisition'),(158,'managestockrequisition_add',158,'managestockrequisition_add'),(159,'managestockrequisition_detail',159,'managestockrequisition_detail'),(160,'managestockrequisition_edit',160,'managestockrequisition_edit'),(161,'managestockrequisition_delete',161,'managestockrequisition_delete'),(162,'managestockrequisition_csv',162,'managestockrequisition_csv'),(163,'managesalesreport',163,'managesalesreport'),(164,'managesalesreport_add',164,'managesalesreport_add'),(165,'managesalesreport_detail',165,'managesalesreport_detail'),(166,'managesalesreport_edit',166,'managesalesreport_edit'),(167,'managesalesreport_delete',167,'managesalesreport_delete'),(168,'managesalesreport_csv',168,'managesalesreport_csv'),(169,'managestockitemrecordcard',169,'managestockitemrecordcard'),(170,'managestockitemrecordcard_add',170,'managestockitemrecordcard_add'),(171,'managestockitemrecordcard_detail',171,'managestockitemrecordcard_detail'),(172,'managestockitemrecordcard_edit',172,'managestockitemrecordcard_edit'),(173,'managestockitemrecordcard_delete',173,'managestockitemrecordcard_delete'),(174,'managestockitemrecordcard_csv',174,'managestockitemrecordcard_csv'),(175,'managewarehouseproductrecordcard',175,'managewarehouseproductrecordcard'),(176,'managewarehouseproductrecordcard_add',176,'managewarehouseproductrecordcard_add'),(177,'managewarehouseproductrecordcard_detail',177,'managewarehouseproductrecordcard_detail'),(178,'managewarehouseproductrecordcard_edit',178,'managewarehouseproductrecordcard_edit'),(179,'managewarehouseproductrecordcard_delete',179,'managewarehouseproductrecordcard_delete'),(180,'managewarehouseproductrecordcard_csv',180,'managewarehouseproductrecordcard_csv'),(181,'manageitemcategory',181,'manageitemcategory'),(182,'manageitemcategory_add',182,'manageitemcategory_add'),(183,'manageitemcategory_detail',183,'manageitemcategory_detail'),(184,'manageitemcategory_edit',184,'manageitemcategory_edit'),(185,'manageitemcategory_delete',185,'manageitemcategory_delete'),(186,'manageitemcategory_csv',186,'manageitemcategory_csv'),(187,'managemeasure',187,'managemeasure'),(188,'managemeasure_add',188,'managemeasure_add'),(189,'managemeasure_detail',189,'managemeasure_detail'),(190,'managemeasure_edit',190,'managemeasure_edit'),(191,'managemeasure_delete',191,'managemeasure_delete'),(192,'managemeasure_csv',192,'managemeasure_csv'),(193,'manageitem',193,'manageitem'),(194,'manageitem_add',194,'manageitem_add'),(195,'manageitem_detail',195,'manageitem_detail'),(196,'manageitem_edit',196,'manageitem_edit'),(197,'manageitem_delete',197,'manageitem_delete'),(198,'manageitem_csv',198,'manageitem_csv'),(199,'manageproductcategory',199,'manageproductcategory'),(200,'manageproductcategory_add',200,'manageproductcategory_add'),(201,'manageproductcategory_detail',201,'manageproductcategory_detail'),(202,'manageproductcategory_edit',202,'manageproductcategory_edit'),(203,'manageproductcategory_delete',203,'manageproductcategory_delete'),(204,'manageproductcategory_csv',204,'manageproductcategory_csv'),(205,'manageproduct',205,'manageproduct'),(206,'manageproduct_add',206,'manageproduct_add'),(207,'manageproduct_detail',207,'manageproduct_detail'),(208,'manageproduct_edit',208,'manageproduct_edit'),(209,'manageproduct_delete',209,'manageproduct_delete'),(210,'manageproduct_csv',210,'manageproduct_csv'),(211,'manageitemdemandstore',211,'manageitemdemandstore'),(212,'manageitemdemandstore_add',212,'manageitemdemandstore_add'),(213,'manageitemdemandstore_detail',213,'manageitemdemandstore_detail'),(214,'manageitemdemandstore_edit',214,'manageitemdemandstore_edit'),(215,'manageitemdemandstore_delete',215,'manageitemdemandstore_delete'),(216,'manageitemdemandstore_csv',216,'manageitemdemandstore_csv'),(217,'manageitemrequisitionstore',217,'manageitemrequisitionstore'),(218,'manageitemrequisitionstore_add',218,'manageitemrequisitionstore_add'),(219,'manageitemrequisitionstore_detail',219,'manageitemrequisitionstore_detail'),(220,'manageitemrequisitionstore_edit',220,'manageitemrequisitionstore_edit'),(221,'manageitemrequisitionstore_delete',221,'manageitemrequisitionstore_delete'),(222,'manageitemrequisitionstore_csv',222,'manageitemrequisitionstore_csv'),(223,'managephysicalstore',223,'managephysicalstore'),(224,'managephysicalstore_add',224,'managephysicalstore_add'),(225,'managephysicalstore_detail',225,'managephysicalstore_detail'),(226,'managephysicalstore_edit',226,'managephysicalstore_edit'),(227,'managephysicalstore_delete',227,'managephysicalstore_delete'),(228,'managephysicalstore_csv',228,'managephysicalstore_csv'),(229,'manageitemstore',229,'manageitemstore'),(230,'manageitemstore_add',230,'manageitemstore_add'),(231,'manageitemstore_detail',231,'manageitemstore_detail'),(232,'manageitemstore_edit',232,'manageitemstore_edit'),(233,'manageitemstore_delete',233,'manageitemstore_delete'),(234,'manageitemstore_csv',234,'manageitemstore_csv'),(235,'managephysicalwarehouse',235,'managephysicalwarehouse'),(236,'managephysicalwarehouse_add',236,'managephysicalwarehouse_add'),(237,'managephysicalwarehouse_detail',237,'managephysicalwarehouse_detail'),(238,'managephysicalwarehouse_edit',238,'managephysicalwarehouse_edit'),(239,'managephysicalwarehouse_delete',239,'managephysicalwarehouse_delete'),(240,'managephysicalwarehouse_csv',240,'managephysicalwarehouse_csv'),(241,'manageproductdemandstore',241,'manageproductdemandstore'),(242,'manageproductdemandstore_add',242,'manageproductdemandstore_add'),(243,'manageproductdemandstore_detail',243,'manageproductdemandstore_detail'),(244,'manageproductdemandstore_edit',244,'manageproductdemandstore_edit'),(245,'manageproductdemandstore_delete',245,'manageproductdemandstore_delete'),(246,'manageproductdemandstore_csv',246,'manageproductdemandstore_csv'),(247,'managewarehouse',247,'managewarehouse'),(248,'managewarehouse_add',248,'managewarehouse_add'),(249,'managewarehouse_detail',249,'managewarehouse_detail'),(250,'managewarehouse_edit',250,'managewarehouse_edit'),(251,'managewarehouse_delete',251,'managewarehouse_delete'),(252,'managewarehouse_csv',252,'managewarehouse_csv'),(253,'manageinvoicedemandbank',253,'manageinvoicedemandbank'),(254,'manageinvoicedemandbank_add',254,'manageinvoicedemandbank_add'),(255,'manageinvoicedemandbank_detail',255,'manageinvoicedemandbank_detail'),(256,'manageinvoicedemandbank_edit',256,'manageinvoicedemandbank_edit'),(257,'manageinvoicedemandbank_delete',257,'manageinvoicedemandbank_delete'),(258,'manageinvoicedemandbank_csv',258,'manageinvoicedemandbank_csv'),(259,'managelocalstorerequisition',259,'managelocalstorerequisition'),(260,'managelocalstorerequisition_add',260,'managelocalstorerequisition_add'),(261,'managelocalstorerequisition_detail',261,'managelocalstorerequisition_detail'),(262,'managelocalstorerequisition_edit',262,'managelocalstorerequisition_edit'),(263,'managelocalstorerequisition_delete',263,'managelocalstorerequisition_delete'),(264,'managelocalstorerequisition_csv',264,'managelocalstorerequisition_csv'),(265,'managelocalitemdeliverynote',265,'managelocalitemdeliverynote'),(266,'managelocalitemdeliverynote_add',266,'managelocalitemdeliverynote_add'),(267,'managelocalitemdeliverynote_detail',267,'managelocalitemdeliverynote_detail'),(268,'managelocalitemdeliverynote_edit',268,'managelocalitemdeliverynote_edit'),(269,'managelocalitemdeliverynote_delete',269,'managelocalitemdeliverynote_delete'),(270,'managelocalitemdeliverynote_csv',270,'managelocalitemdeliverynote_csv'),(271,'managelocalgoodreceivednote',271,'managelocalgoodreceivednote'),(272,'managelocalgoodreceivednote_add',272,'managelocalgoodreceivednote_add'),(273,'managelocalgoodreceivednote_detail',273,'managelocalgoodreceivednote_detail'),(274,'managelocalgoodreceivednote_edit',274,'managelocalgoodreceivednote_edit'),(275,'managelocalgoodreceivednote_delete',275,'managelocalgoodreceivednote_delete'),(276,'managelocalgoodreceivednote_csv',276,'managelocalgoodreceivednote_csv'),(277,'managelocalitemdemandstore',277,'managelocalitemdemandstore'),(278,'managelocalitemdemandstore_add',278,'managelocalitemdemandstore_add'),(279,'managelocalitemdemandstore_detail',279,'managelocalitemdemandstore_detail'),(280,'managelocalitemdemandstore_edit',280,'managelocalitemdemandstore_edit'),(281,'managelocalitemdemandstore_delete',281,'managelocalitemdemandstore_delete'),(282,'managelocalitemdemandstore_csv',282,'managelocalitemdemandstore_csv'),(283,'managelocalwarehouserequisition',283,'managelocalwarehouserequisition'),(284,'managelocalwarehouserequisition_add',284,'managelocalwarehouserequisition_add'),(285,'managelocalwarehouserequisition_detail',285,'managelocalwarehouserequisition_detail'),(286,'managelocalwarehouserequisition_edit',286,'managelocalwarehouserequisition_edit'),(287,'managelocalwarehouserequisition_delete',287,'managelocalwarehouserequisition_delete'),(288,'managelocalwarehouserequisition_csv',288,'managelocalwarehouserequisition_csv'),(289,'managelocalproductdeliverynote',289,'managelocalproductdeliverynote'),(290,'managelocalproductdeliverynote_add',290,'managelocalproductdeliverynote_add'),(291,'managelocalproductdeliverynote_detail',291,'managelocalproductdeliverynote_detail'),(292,'managelocalproductdeliverynote_edit',292,'managelocalproductdeliverynote_edit'),(293,'managelocalproductdeliverynote_delete',293,'managelocalproductdeliverynote_delete'),(294,'managelocalproductdeliverynote_csv',294,'managelocalproductdeliverynote_csv'),(295,'managelocalproductreceivednote',295,'managelocalproductreceivednote'),(296,'managelocalproductreceivednote_add',296,'managelocalproductreceivednote_add'),(297,'managelocalproductreceivednote_detail',297,'managelocalproductreceivednote_detail'),(298,'managelocalproductreceivednote_edit',298,'managelocalproductreceivednote_edit'),(299,'managelocalproductreceivednote_delete',299,'managelocalproductreceivednote_delete'),(300,'managelocalproductreceivednote_csv',300,'managelocalproductreceivednote_csv'),(301,'managelocalproductdemand',301,'managelocalproductdemand'),(302,'managelocalproductdemand_add',302,'managelocalproductdemand_add'),(303,'managelocalproductdemand_detail',303,'managelocalproductdemand_detail'),(304,'managelocalproductdemand_edit',304,'managelocalproductdemand_edit'),(305,'managelocalproductdemand_delete',305,'managelocalproductdemand_delete'),(306,'managelocalproductdemand_csv',306,'managelocalproductdemand_csv'),(307,'managepaper',307,'managepaper'),(308,'managepaper_add',308,'managepaper_add'),(309,'managepaper_detail',309,'managepaper_detail'),(310,'managepaper_edit',310,'managepaper_edit'),(311,'managepaper_delete',311,'managepaper_delete'),(312,'managepaper_csv',312,'managepaper_csv'),(313,'managegroup',313,'managegroup'),(314,'managegroup_add',314,'managegroup_add'),(315,'managegroup_detail',315,'managegroup_detail'),(316,'managegroup_edit',316,'managegroup_edit'),(317,'managegroup_delete',317,'managegroup_delete'),(318,'managegroup_csv',318,'managegroup_csv'),(319,'managedepartment',319,'managedepartment'),(320,'managedepartment_add',320,'managedepartment_add'),(321,'managedepartment_detail',321,'managedepartment_detail'),(322,'managedepartment_edit',322,'managedepartment_edit'),(323,'managedepartment_delete',323,'managedepartment_delete'),(324,'managedepartment_csv',324,'managedepartment_csv'),(325,'managejobtitle',325,'managejobtitle'),(326,'managejobtitle_add',326,'managejobtitle_add'),(327,'managejobtitle_detail',327,'managejobtitle_detail'),(328,'managejobtitle_edit',328,'managejobtitle_edit'),(329,'managejobtitle_delete',329,'managejobtitle_delete'),(330,'managejobtitle_csv',330,'managejobtitle_csv'),(331,'managesex',331,'managesex'),(332,'managesex_add',332,'managesex_add'),(333,'managesex_detail',333,'managesex_detail'),(334,'managesex_edit',334,'managesex_edit'),(335,'managesex_delete',335,'managesex_delete'),(336,'managesex_csv',336,'managesex_csv'),(337,'managemarital',337,'managemarital'),(338,'managemarital_add',338,'managemarital_add'),(339,'managemarital_detail',339,'managemarital_detail'),(340,'managemarital_edit',340,'managemarital_edit'),(341,'managemarital_delete',341,'managemarital_delete'),(342,'managemarital_csv',342,'managemarital_csv'),(343,'managelogin',343,'managelogin'),(344,'managelogin_add',344,'managelogin_add'),(345,'managelogin_detail',345,'managelogin_detail'),(346,'managelogin_edit',346,'managelogin_edit'),(347,'managelogin_delete',347,'managelogin_delete'),(348,'managelogin_csv',348,'managelogin_csv'),(349,'manageuser',349,'manageuser'),(350,'manageuser_add',350,'manageuser_add'),(351,'manageuser_detail',351,'manageuser_detail'),(352,'manageuser_edit',352,'manageuser_edit'),(353,'manageuser_delete',353,'manageuser_delete'),(354,'manageuser_csv',354,'manageuser_csv'),(355,'managedocumentverificationschema',355,'managedocumentverificationschema'),(356,'managedocumentverificationschema_add',356,'managedocumentverificationschema_add'),(357,'managedocumentverificationschema_detail',357,'managedocumentverificationschema_detail'),(358,'managedocumentverificationschema_edit',358,'managedocumentverificationschema_edit'),(359,'managedocumentverificationschema_delete',359,'managedocumentverificationschema_delete'),(360,'managedocumentverificationschema_csv',360,'managedocumentverificationschema_csv'),(361,'managedocumentverificationdata',361,'managedocumentverificationdata'),(362,'managedocumentverificationdata_add',362,'managedocumentverificationdata_add'),(363,'managedocumentverificationdata_detail',363,'managedocumentverificationdata_detail'),(364,'managedocumentverificationdata_edit',364,'managedocumentverificationdata_edit'),(365,'managedocumentverificationdata_delete',365,'managedocumentverificationdata_delete'),(366,'managedocumentverificationdata_csv',366,'managedocumentverificationdata_csv'),(367,'managemessagetype',367,'managemessagetype'),(368,'managemessagetype_add',368,'managemessagetype_add'),(369,'managemessagetype_detail',369,'managemessagetype_detail'),(370,'managemessagetype_edit',370,'managemessagetype_edit'),(371,'managemessagetype_delete',371,'managemessagetype_delete'),(372,'managemessagetype_csv',372,'managemessagetype_csv'),(373,'managemessagepolicytype',373,'managemessagepolicytype'),(374,'managemessagepolicytype_add',374,'managemessagepolicytype_add'),(375,'managemessagepolicytype_detail',375,'managemessagepolicytype_detail'),(376,'managemessagepolicytype_edit',376,'managemessagepolicytype_edit'),(377,'managemessagepolicytype_delete',377,'managemessagepolicytype_delete'),(378,'managemessagepolicytype_csv',378,'managemessagepolicytype_csv'),(379,'manageapprovalsequenceschema',379,'manageapprovalsequenceschema'),(380,'manageapprovalsequenceschema_add',380,'manageapprovalsequenceschema_add'),(381,'manageapprovalsequenceschema_detail',381,'manageapprovalsequenceschema_detail'),(382,'manageapprovalsequenceschema_edit',382,'manageapprovalsequenceschema_edit'),(383,'manageapprovalsequenceschema_delete',383,'manageapprovalsequenceschema_delete'),(384,'manageapprovalsequenceschema_csv',384,'manageapprovalsequenceschema_csv'),(385,'manageapprovalsequencedata',385,'manageapprovalsequencedata'),(386,'manageapprovalsequencedata_add',386,'manageapprovalsequencedata_add'),(387,'manageapprovalsequencedata_detail',387,'manageapprovalsequencedata_detail'),(388,'manageapprovalsequencedata_edit',388,'manageapprovalsequencedata_edit'),(389,'manageapprovalsequencedata_delete',389,'manageapprovalsequencedata_delete'),(390,'manageapprovalsequencedata_csv',390,'manageapprovalsequencedata_csv'),(391,'managecontextmanager',391,'managecontextmanager'),(392,'managesystemlogs',392,'managesystemlogs'),(393,'managesystemfirewall',393,'managesystemfirewall'),(394,'managesystemfirewall_graph',394,'managesystemfirewall_graph');
/*!40000 ALTER TABLE `contextPosition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `countryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryName` varchar(48) NOT NULL,
  `abbreviation` varchar(4) NOT NULL,
  `code` int(3) NOT NULL,
  `timezone` varchar(5) NOT NULL DEFAULT '03:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`countryId`),
  UNIQUE KEY `countryName` (`countryName`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Afghanistan','AF',255,'0300',NULL,NULL,15),(2,'Akrotiri','00',255,'0300',NULL,NULL,15),(3,'Albania','AL',255,'0300',NULL,NULL,15),(4,'Algeria','DZ',255,'0300',NULL,NULL,15),(5,'American Samoa','AS',255,'0300',NULL,NULL,15),(6,'Andorra','AD',255,'0300',NULL,NULL,15),(7,'Angola','AO',255,'0300',NULL,NULL,15),(8,'Anguilla','AI',255,'0300',NULL,NULL,15),(9,'Antarctica','AQ',255,'0300',NULL,NULL,15),(10,'Antigua and Barbuda','AG',255,'0300',NULL,NULL,15),(11,'Argentina','AR',255,'0300',NULL,NULL,15),(12,'Armenia','AM',255,'0300',NULL,NULL,15),(13,'Aruba','AW',255,'0300',NULL,NULL,15),(14,'Ashmore and Cartier Islands','00',255,'0300',NULL,NULL,15),(15,'Australia','AU',255,'0300',NULL,NULL,15),(16,'Austria','AT',255,'0300',NULL,NULL,15),(17,'Azerbaijan','00',255,'0300',NULL,NULL,15),(18,'Bahamas, The','BS',255,'0300',NULL,NULL,15),(19,'Bahrain','BH',255,'0300',NULL,NULL,15),(20,'Bangladesh','BD',255,'0300',NULL,NULL,15),(21,'Barbados','BB',255,'0300',NULL,NULL,15),(22,'Bassas da India','00',255,'0300',NULL,NULL,15),(23,'Belarus','BY',255,'0300',NULL,NULL,15),(24,'Belgium','BE',255,'0300',NULL,NULL,15),(25,'Belize','BZ',255,'0300',NULL,NULL,15),(26,'Benin','BJ',255,'0300',NULL,NULL,15),(27,'Bermuda','BM',255,'0300',NULL,NULL,15),(28,'Bhutan','BT',255,'0300',NULL,NULL,15),(29,'Bolivia','BO',255,'0300',NULL,NULL,15),(30,'Bosnia and Herzegovina','BA',255,'0300',NULL,NULL,15),(31,'Botswana','BW',255,'0300',NULL,NULL,15),(32,'Bouvet Island','BV',255,'0300',NULL,NULL,15),(33,'Brazil','BR',255,'0300',NULL,NULL,15),(34,'British Indian Ocean Territory','IO',255,'0300',NULL,NULL,15),(35,'British Virgin Islands','00',255,'0300',NULL,NULL,15),(36,'Brunei','BN',255,'0300',NULL,NULL,15),(37,'Bulgaria','BG',255,'0300',NULL,NULL,15),(38,'Burkina Faso','BF',255,'0300',NULL,NULL,15),(39,'Burma','MM',255,'0300',NULL,NULL,15),(40,'Burundi','BI',255,'0300',NULL,NULL,15),(41,'Cambodia','KH',255,'0300',NULL,NULL,15),(42,'Cameroon','CM',255,'0300',NULL,NULL,15),(43,'Canada','CA',255,'0300',NULL,NULL,15),(44,'Cape Verde','CV',255,'0300',NULL,NULL,15),(45,'Cayman Islands','KY',255,'0300',NULL,NULL,15),(46,'Central African Republic','CF',255,'0300',NULL,NULL,15),(47,'Chad','TD',255,'0300',NULL,NULL,15),(48,'Chile','CL',255,'0300',NULL,NULL,15),(49,'China','CN',255,'0300',NULL,NULL,15),(50,'Christmas Island','CX',255,'0300',NULL,NULL,15),(51,'Clipperton Island','00',255,'0300',NULL,NULL,15),(52,'Cocos (Keeling) Islands','CC',255,'0300',NULL,NULL,15),(53,'Colombia','CO',255,'0300',NULL,NULL,15),(54,'Comoros','KM',255,'0300',NULL,NULL,15),(55,'Congo, Democratic Republic of th','CD',255,'0300',NULL,NULL,15),(56,'Congo, Republic of the','CG',255,'0300',NULL,NULL,15),(57,'Cook Islands','CK',255,'0300',NULL,NULL,15),(58,'Coral Sea Islands','00',255,'0300',NULL,NULL,15),(59,'Costa Rica','CR',255,'0300',NULL,NULL,15),(60,'Cote d\'Ivoire','CI',255,'0300',NULL,NULL,15),(61,'Croatia','HR',255,'0300',NULL,NULL,15),(62,'Cuba','CU',255,'0300',NULL,NULL,15),(63,'Cyprus','CY',255,'0300',NULL,NULL,15),(64,'Czech Republic','CZ',255,'0300',NULL,NULL,15),(65,'Denmark','DK',255,'0300',NULL,NULL,15),(66,'Dhekelia','00',255,'0300',NULL,NULL,15),(67,'Djibouti','DJ',255,'0300',NULL,NULL,15),(68,'Dominica','DM',255,'0300',NULL,NULL,15),(69,'Dominican Republic','DO',255,'0300',NULL,NULL,15),(70,'Ecuador','EC',255,'0300',NULL,NULL,15),(71,'Egypt','EG',255,'0300',NULL,NULL,15),(72,'El Salvador','SV',255,'0300',NULL,NULL,15),(73,'Equatorial Guinea','GQ',255,'0300',NULL,NULL,15),(74,'Eritrea','ER',255,'0300',NULL,NULL,15),(75,'Estonia','EE',255,'0300',NULL,NULL,15),(76,'Ethiopia','ET',255,'0300',NULL,NULL,15),(77,'Europa Island','00',255,'0300',NULL,NULL,15),(78,'Falkland Islands (Islas Malvinas','FK',255,'0300',NULL,NULL,15),(79,'Faroe Islands','FO',255,'0300',NULL,NULL,15),(80,'Fiji','FJ',255,'0300',NULL,NULL,15),(81,'Finland','FI',255,'0300',NULL,NULL,15),(82,'France','FR',255,'0300',NULL,NULL,15),(83,'French Guiana','GF',255,'0300',NULL,NULL,15),(84,'French Polynesia','PF',255,'0300',NULL,NULL,15),(85,'French Southern and Antarctic La','TF',255,'0300',NULL,NULL,15),(86,'Gabon','GA',255,'0300',NULL,NULL,15),(87,'Gambia, The','GM',255,'0300',NULL,NULL,15),(88,'Gaza Strip','00',255,'0300',NULL,NULL,15),(89,'Georgia','GE',255,'0300',NULL,NULL,15),(90,'Germany','DE',255,'0300',NULL,NULL,15),(91,'Ghana','GH',255,'0300',NULL,NULL,15),(92,'Gibraltar','GI',255,'0300',NULL,NULL,15),(93,'Glorioso Islands','00',255,'0300',NULL,NULL,15),(94,'Greece','GR',255,'0300',NULL,NULL,15),(95,'Greenland','GL',255,'0300',NULL,NULL,15),(96,'Grenada','GD',255,'0300',NULL,NULL,15),(97,'Guadeloupe','GP',255,'0300',NULL,NULL,15),(98,'Guam','GU',255,'0300',NULL,NULL,15),(99,'Guatemala','GT',255,'0300',NULL,NULL,15),(100,'Guernsey','00',255,'0300',NULL,NULL,15),(101,'Guinea','GN',255,'0300',NULL,NULL,15),(102,'Guinea-Bissau','GW',255,'0300',NULL,NULL,15),(103,'Guyana','GY',255,'0300',NULL,NULL,15),(104,'Haiti','HT',255,'0300',NULL,NULL,15),(105,'Heard Island and McDonald Island','HM',255,'0300',NULL,NULL,15),(106,'Holy See (Vatican City)','VA',255,'0300',NULL,NULL,15),(107,'Honduras','HN',255,'0300',NULL,NULL,15),(108,'Hong Kong','HK',255,'0300',NULL,NULL,15),(109,'Hungary','HU',255,'0300',NULL,NULL,15),(110,'Iceland','IS',255,'0300',NULL,NULL,15),(111,'India','IN',255,'0300',NULL,NULL,15),(112,'Indonesia','ID',255,'0300',NULL,NULL,15),(113,'Iran','IR',255,'0300',NULL,NULL,15),(114,'Iraq','IQ',255,'0300',NULL,NULL,15),(115,'Ireland','IE',255,'0300',NULL,NULL,15),(116,'Isle of Man','00',255,'0300',NULL,NULL,15),(117,'Israel','IL',255,'0300',NULL,NULL,15),(118,'Italy','IT',255,'0300',NULL,NULL,15),(119,'Jamaica','JM',255,'0300',NULL,NULL,15),(120,'Jan Mayen','00',255,'0300',NULL,NULL,15),(121,'Japan','JP',255,'0300',NULL,NULL,15),(122,'Jersey','00',255,'0300',NULL,NULL,15),(123,'Jordan','JO',255,'0300',NULL,NULL,15),(124,'Juan de Nova Island','00',255,'0300',NULL,NULL,15),(125,'Kazakhstan','KZ',255,'0300',NULL,NULL,15),(126,'Kenya','KE',255,'0300',NULL,NULL,15),(127,'Kiribati','KI',255,'0300',NULL,NULL,15),(128,'Korea, North','00',255,'0300',NULL,NULL,15),(129,'Korea, South','00',255,'0300',NULL,NULL,15),(130,'Kuwait','KP',255,'0300',NULL,NULL,15),(131,'Kyrgyzstan','KG',255,'0300',NULL,NULL,15),(132,'Laos','LA',255,'0300',NULL,NULL,15),(133,'Latvia','LV',255,'0300',NULL,NULL,15),(134,'Lebanon','LB',255,'0300',NULL,NULL,15),(135,'Lesotho','LS',255,'0300',NULL,NULL,15),(136,'Liberia','LR',255,'0300',NULL,NULL,15),(137,'Libya','LY',255,'0300',NULL,NULL,15),(138,'Liechtenstein','LI',255,'0300',NULL,NULL,15),(139,'Lithuania','LT',255,'0300',NULL,NULL,15),(140,'Luxembourg','LU',255,'0300',NULL,NULL,15),(141,'Macau','MO',255,'0300',NULL,NULL,15),(142,'Macedonia','MK',255,'0300',NULL,NULL,15),(143,'Madagascar','MG',255,'0300',NULL,NULL,15),(144,'Malawi','MW',255,'0300',NULL,NULL,15),(145,'Malaysia','MY',255,'0300',NULL,NULL,15),(146,'Maldives','MV',255,'0300',NULL,NULL,15),(147,'Mali','ML',255,'0300',NULL,NULL,15),(148,'Malta','MT',255,'0300',NULL,NULL,15),(149,'Marshall Islands','MH',255,'0300',NULL,NULL,15),(150,'Martinique','MQ',255,'0300',NULL,NULL,15),(151,'Mauritania','MR',255,'0300',NULL,NULL,15),(152,'Mauritius','MU',255,'0300',NULL,NULL,15),(153,'Mayotte','YT',255,'0300',NULL,NULL,15),(154,'Mexico','MX',255,'0300',NULL,NULL,15),(155,'Micronesia, Federated States of','FM',255,'0300',NULL,NULL,15),(156,'Moldova','MD',255,'0300',NULL,NULL,15),(157,'Monaco','MC',255,'0300',NULL,NULL,15),(158,'Mongolia','MN',255,'0300',NULL,NULL,15),(159,'Montserrat','MS',255,'0300',NULL,NULL,15),(160,'Morocco','MA',255,'0300',NULL,NULL,15),(161,'Mozambique','MZ',255,'0300',NULL,NULL,15),(162,'Namibia','NA',255,'0300',NULL,NULL,15),(163,'Nauru','NR',255,'0300',NULL,NULL,15),(164,'Navassa Island','00',255,'0300',NULL,NULL,15),(165,'Nepal','NP',255,'0300',NULL,NULL,15),(166,'Netherlands','NL',255,'0300',NULL,NULL,15),(167,'Netherlands Antilles','AN',255,'0300',NULL,NULL,15),(168,'New Caledonia','NC',255,'0300',NULL,NULL,15),(169,'New Zealand','NZ',255,'0300',NULL,NULL,15),(170,'Nicaragua','NI',255,'0300',NULL,NULL,15),(171,'Niger','NE',255,'0300',NULL,NULL,15),(172,'Nigeria','NG',255,'0300',NULL,NULL,15),(173,'Niue','NU',255,'0300',NULL,NULL,15),(174,'Norfolk Island','NF',255,'0300',NULL,NULL,15),(175,'Northern Mariana Islands','MP',255,'0300',NULL,NULL,15),(176,'Norway','NO',255,'0300',NULL,NULL,15),(177,'Oman','OM',255,'0300',NULL,NULL,15),(178,'Pakistan','PK',255,'0300',NULL,NULL,15),(179,'Palau','PW',255,'0300',NULL,NULL,15),(180,'Panama','PA',255,'0300',NULL,NULL,15),(181,'Papua New Guinea','PG',255,'0300',NULL,NULL,15),(182,'Paracel Islands','00',255,'0300',NULL,NULL,15),(183,'Paraguay','PY',255,'0300',NULL,NULL,15),(184,'Peru','PE',255,'0300',NULL,NULL,15),(185,'Philippines','PH',255,'0300',NULL,NULL,15),(186,'Pitcairn Islands','PN',255,'0300',NULL,NULL,15),(187,'Poland','PL',255,'0300',NULL,NULL,15),(188,'Portugal','PT',255,'0300',NULL,NULL,15),(189,'Puerto Rico','PR',255,'0300',NULL,NULL,15),(190,'Qatar','QA',255,'0300',NULL,NULL,15),(191,'Reunion','RE',255,'0300',NULL,NULL,15),(192,'Romania','RO',255,'0300',NULL,NULL,15),(193,'Russia','RU',255,'0300',NULL,NULL,15),(194,'Rwanda','RW',255,'0300',NULL,NULL,15),(195,'Saint Helena','00',255,'0300',NULL,NULL,15),(196,'Saint Kitts and Nevis','KN',255,'0300',NULL,NULL,15),(197,'Saint Lucia','LC',255,'0300',NULL,NULL,15),(198,'Saint Pierre and Miquelon','00',255,'0300',NULL,NULL,15),(199,'Saint Vincent and the Grenadines','VC',255,'0300',NULL,NULL,15),(200,'Samoa','WS',255,'0300',NULL,NULL,15),(201,'San Marino','SM',255,'0300',NULL,NULL,15),(202,'Sao Tome and Principe','ST',255,'0300',NULL,NULL,15),(203,'Saudi Arabia','SA',255,'0300',NULL,NULL,15),(204,'Senegal','SN',255,'0300',NULL,NULL,15),(205,'Serbia and Montenegro','RS',255,'0300',NULL,NULL,15),(206,'Seychelles','SC',255,'0300',NULL,NULL,15),(207,'Sierra Leone','SL',255,'0300',NULL,NULL,15),(208,'Singapore','SG',255,'0300',NULL,NULL,15),(209,'Slovakia','SK',255,'0300',NULL,NULL,15),(210,'Slovenia','SI',255,'0300',NULL,NULL,15),(211,'Solomon Islands','SB',255,'0300',NULL,NULL,15),(212,'Somalia','SO',255,'0300',NULL,NULL,15),(213,'South Africa','ZA',255,'0300',NULL,NULL,15),(214,'South Georgia and the South Sand','GS',255,'0300',NULL,NULL,15),(215,'Spain','ES',255,'0300',NULL,NULL,15),(216,'Spratly Islands','00',255,'0300',NULL,NULL,15),(217,'Sri Lanka','LK',255,'0300',NULL,NULL,15),(218,'Sudan','SD',255,'0300',NULL,NULL,15),(219,'Suriname','SR',255,'0300',NULL,NULL,15),(220,'Svalbard','SJ',255,'0300',NULL,NULL,15),(221,'Swaziland','SZ',255,'0300',NULL,NULL,15),(222,'Sweden','SE',255,'0300',NULL,NULL,15),(223,'Switzerland','CH',255,'0300',NULL,NULL,15),(224,'Syria','SY',255,'0300',NULL,NULL,15),(225,'Taiwan','TW',255,'0300',NULL,NULL,15),(226,'Tajikistan','TJ',255,'0300',NULL,NULL,15),(227,'Tanzania','TZ',255,'0300',NULL,NULL,15),(228,'Thailand','TH',255,'0300',NULL,NULL,15),(229,'Timor-Leste','00',255,'0300',NULL,NULL,15),(230,'Togo','TG',255,'0300',NULL,NULL,15),(231,'Tokelau','TK',255,'0300',NULL,NULL,15),(232,'Tonga','TO',255,'0300',NULL,NULL,15),(233,'Trinidad and Tobago','TT',255,'0300',NULL,NULL,15),(234,'Tromelin Island','00',255,'0300',NULL,NULL,15),(235,'Tunisia','TN',255,'0300',NULL,NULL,15),(236,'Turkey','TR',255,'0300',NULL,NULL,15),(237,'Turkmenistan','TM',255,'0300',NULL,NULL,15),(238,'Turks and Caicos Islands','TC',255,'0300',NULL,NULL,15),(239,'Tuvalu','TV',255,'0300',NULL,NULL,15),(240,'Uganda','UG',255,'0300',NULL,NULL,15),(241,'Ukraine','UA',255,'0300',NULL,NULL,15),(242,'United Arab Emirates','AE',255,'0300',NULL,NULL,15),(243,'United Kingdom','GB',255,'0300',NULL,NULL,15),(244,'United States','US',255,'0300',NULL,NULL,15),(245,'Uruguay','UY',255,'0300',NULL,NULL,15),(246,'Uzbekistan','UZ',255,'0300',NULL,NULL,15),(247,'Vanuatu','VU',255,'0300',NULL,NULL,15),(248,'Venezuela','VE',255,'0300',NULL,NULL,15),(249,'Vietnam','VN',255,'0300',NULL,NULL,15),(250,'Virgin Islands','VI',255,'0300',NULL,NULL,15),(251,'Wake Island','00',255,'0300',NULL,NULL,15),(252,'Wallis and Futuna','WF',255,'0300',NULL,NULL,15),(253,'West Bank','00',255,'0300',NULL,NULL,15),(254,'Western Sahara','EH',255,'0300',NULL,NULL,15),(255,'Yemen','YE',255,'0300',NULL,NULL,15),(256,'Zambia','ZM',255,'0300',NULL,NULL,15),(257,'Zimbabwe','ZW',255,'0300',NULL,NULL,15),(258,'South Sudan','SS',255,'0300',NULL,NULL,15);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `currencyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currencyName` varchar(32) NOT NULL,
  `currencyCode` varchar(4) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`currencyId`),
  UNIQUE KEY `currencyName` (`currencyName`),
  UNIQUE KEY `currencyCode` (`currencyCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencyConversion`
--

DROP TABLE IF EXISTS `currencyConversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencyConversion` (
  `conversionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currencyId` int(10) unsigned NOT NULL,
  `value` varchar(32) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`conversionId`),
  KEY `currencyId` (`currencyId`),
  CONSTRAINT `currencyConversion_ibfk_1` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencyConversion`
--

LOCK TABLES `currencyConversion` WRITE;
/*!40000 ALTER TABLE `currencyConversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencyConversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daysOfAWeek`
--

DROP TABLE IF EXISTS `daysOfAWeek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daysOfAWeek` (
  `dayId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dayName` varchar(9) NOT NULL,
  `dayAbbreviation` varchar(4) NOT NULL,
  `offsetValue` int(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dayId`),
  UNIQUE KEY `dayName` (`dayName`),
  UNIQUE KEY `dayAbbreviation` (`dayAbbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daysOfAWeek`
--

LOCK TABLES `daysOfAWeek` WRITE;
/*!40000 ALTER TABLE `daysOfAWeek` DISABLE KEYS */;
INSERT INTO `daysOfAWeek` VALUES (1,'Sunday','Sun',0,NULL,NULL,15),(2,'Monday','Mon',1,NULL,NULL,15),(3,'Tuesday','Tue',2,NULL,NULL,15),(4,'Wednesday','Wed',3,NULL,NULL,15),(5,'Thursday','Thur',4,NULL,NULL,15),(6,'Friday','Fri',5,NULL,NULL,15),(7,'Saturday','Sat',6,NULL,NULL,15);
/*!40000 ALTER TABLE `daysOfAWeek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveryNote`
--

DROP TABLE IF EXISTS `deliveryNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveryNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `deliveryNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveryNote`
--

LOCK TABLES `deliveryNote` WRITE;
/*!40000 ALTER TABLE `deliveryNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveryNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `departmentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`departmentId`),
  UNIQUE KEY `departmentName` (`departmentName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Installation Department',NULL,NULL,0);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `documentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentName` varchar(32) NOT NULL,
  `documentCode` int(4) unsigned NOT NULL DEFAULT '0',
  `pagename` int(4) unsigned NOT NULL DEFAULT '0',
  `imagename` varchar(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (1,'Purchasing Order',0,0,NULL,NULL,NULL,0),(2,'Good Received Note',0,0,NULL,NULL,NULL,0),(3,'Job Ticket',0,0,NULL,NULL,NULL,0),(4,'Stock Requisition',0,0,NULL,NULL,NULL,0),(5,'Store Requisition',0,0,NULL,NULL,NULL,0),(6,'Issue Note',0,0,NULL,NULL,NULL,0),(7,'Finished Good Report',0,0,NULL,NULL,NULL,0),(8,'Price Quote',0,0,NULL,NULL,NULL,0),(9,'Delivery Note',0,0,NULL,NULL,NULL,0),(10,'Invoice',0,0,NULL,NULL,NULL,0),(11,'Job Cost Sheet',0,0,NULL,NULL,NULL,0),(12,'Good Returned Note',0,0,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentTransaction`
--

DROP TABLE IF EXISTS `documentTransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentTransaction` (
  `transactionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction` varchar(128) NOT NULL,
  `docRefNumber` varchar(24) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transactionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentTransaction`
--

LOCK TABLES `documentTransaction` WRITE;
/*!40000 ALTER TABLE `documentTransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentTransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finishedGoodReport`
--

DROP TABLE IF EXISTS `finishedGoodReport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finishedGoodReport` (
  `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportNumber` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reportId`),
  UNIQUE KEY `reportNumber` (`reportNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `finishedGoodReport_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finishedGoodReport`
--

LOCK TABLES `finishedGoodReport` WRITE;
/*!40000 ALTER TABLE `finishedGoodReport` DISABLE KEYS */;
/*!40000 ALTER TABLE `finishedGoodReport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goodReceivedNote`
--

DROP TABLE IF EXISTS `goodReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goodReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `orderId` int(10) unsigned DEFAULT NULL,
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  KEY `orderId` (`orderId`),
  CONSTRAINT `goodReceivedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `goodReceivedNote_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `purchasingOrder` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goodReceivedNote`
--

LOCK TABLES `goodReceivedNote` WRITE;
/*!40000 ALTER TABLE `goodReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `goodReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goodReturnedNote`
--

DROP TABLE IF EXISTS `goodReturnedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goodReturnedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `documentId` (`documentId`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  CONSTRAINT `goodReturnedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `goodReturnedNote_ibfk_2` FOREIGN KEY (`deliveryNoteId`) REFERENCES `deliveryNote` (`noteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goodReturnedNote`
--

LOCK TABLES `goodReturnedNote` WRITE;
/*!40000 ALTER TABLE `goodReturnedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `goodReturnedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `groupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(32) NOT NULL,
  `pId` int(10) unsigned DEFAULT NULL,
  `context` char(255) NOT NULL DEFAULT '222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',
  `rootGroup` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupId`),
  KEY `pId` (`pId`),
  CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`pId`) REFERENCES `groups` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Local Group',NULL,'222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',0,NULL,NULL,0);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoiceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `amount` varchar(32) NOT NULL DEFAULT '0.0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invoiceId`),
  UNIQUE KEY `invoiceNumber` (`invoiceNumber`),
  KEY `documentId` (`documentId`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`deliveryNoteId`) REFERENCES `deliveryNote` (`noteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoiceDemandBank`
--

DROP TABLE IF EXISTS `invoiceDemandBank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoiceDemandBank` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `invoiceId` (`invoiceId`),
  CONSTRAINT `invoiceDemandBank_ibfk_1` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoiceDemandBank`
--

LOCK TABLES `invoiceDemandBank` WRITE;
/*!40000 ALTER TABLE `invoiceDemandBank` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoiceDemandBank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issueNote`
--

DROP TABLE IF EXISTS `issueNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issueNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `requisitionId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `documentId` (`documentId`),
  KEY `requisitionId` (`requisitionId`),
  CONSTRAINT `issueNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `issueNote_ibfk_2` FOREIGN KEY (`requisitionId`) REFERENCES `stockRequisition` (`reqId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issueNote`
--

LOCK TABLES `issueNote` WRITE;
/*!40000 ALTER TABLE `issueNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `issueNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `itemId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemName` varchar(128) NOT NULL,
  `measureId` int(10) unsigned NOT NULL,
  `categoryId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemId`),
  UNIQUE KEY `itemName` (`itemName`),
  KEY `measureId` (`measureId`),
  KEY `fbk_item_category` (`categoryId`),
  CONSTRAINT `fbk_item_category` FOREIGN KEY (`categoryId`) REFERENCES `itemCategory` (`categoryId`),
  CONSTRAINT `item_ibfk_1` FOREIGN KEY (`measureId`) REFERENCES `measure` (`measureId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemCategory`
--

DROP TABLE IF EXISTS `itemCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemCategory` (
  `categoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemCategory`
--

LOCK TABLES `itemCategory` WRITE;
/*!40000 ALTER TABLE `itemCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemDemandStore`
--

DROP TABLE IF EXISTS `itemDemandStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemDemandStore` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeId` int(10) unsigned NOT NULL,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `itemDemandStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`),
  CONSTRAINT `itemDemandStore_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemDemandStore`
--

LOCK TABLES `itemDemandStore` WRITE;
/*!40000 ALTER TABLE `itemDemandStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemDemandStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemRequisitionStore`
--

DROP TABLE IF EXISTS `itemRequisitionStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemRequisitionStore` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeId` int(10) unsigned NOT NULL,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `itemRequisitionStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`),
  CONSTRAINT `itemRequisitionStore_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemRequisitionStore`
--

LOCK TABLES `itemRequisitionStore` WRITE;
/*!40000 ALTER TABLE `itemRequisitionStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemRequisitionStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemStore`
--

DROP TABLE IF EXISTS `itemStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemStore` (
  `itemStoreId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `itemValue` varchar(32) NOT NULL DEFAULT '0.0',
  `minStockLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `orderingLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `itemPrice` varchar(32) NOT NULL DEFAULT '0.0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemStoreId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `itemStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`),
  CONSTRAINT `itemStore_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemStore`
--

LOCK TABLES `itemStore` WRITE;
/*!40000 ALTER TABLE `itemStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobCostSheet`
--

DROP TABLE IF EXISTS `jobCostSheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobCostSheet` (
  `jobCostId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobCostNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `jobCostOpeningDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `jobCostWantedDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `jobCostClosingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `productId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jobCostId`),
  KEY `documentId` (`documentId`),
  KEY `productId` (`productId`),
  CONSTRAINT `jobCostSheet_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `jobCostSheet_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobCostSheet`
--

LOCK TABLES `jobCostSheet` WRITE;
/*!40000 ALTER TABLE `jobCostSheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobCostSheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobTicket`
--

DROP TABLE IF EXISTS `jobTicket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobTicket` (
  `ticketId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticketNumber` varchar(24) NOT NULL,
  `ticketDetails` varchar(64) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `openingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `wantedDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(40) DEFAULT NULL,
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ticketId`),
  UNIQUE KEY `ticketNumber` (`ticketNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `jobTicket_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobTicket`
--

LOCK TABLES `jobTicket` WRITE;
/*!40000 ALTER TABLE `jobTicket` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobTicket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobTitle`
--

DROP TABLE IF EXISTS `jobTitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobTitle` (
  `jobId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobName` varchar(64) NOT NULL,
  `context` char(255) NOT NULL DEFAULT '222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jobId`),
  UNIQUE KEY `jobName` (`jobName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobTitle`
--

LOCK TABLES `jobTitle` WRITE;
/*!40000 ALTER TABLE `jobTitle` DISABLE KEYS */;
INSERT INTO `jobTitle` VALUES (1,'Local Admin','222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',NULL,NULL,0);
/*!40000 ALTER TABLE `jobTitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localGoodReceivedNote`
--

DROP TABLE IF EXISTS `localGoodReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localGoodReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localGoodReceivedNote_ibfk_1` FOREIGN KEY (`deliveryNoteId`) REFERENCES `localItemDeliveryNote` (`noteId`),
  CONSTRAINT `localGoodReceivedNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localGoodReceivedNote`
--

LOCK TABLES `localGoodReceivedNote` WRITE;
/*!40000 ALTER TABLE `localGoodReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localGoodReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localItemDeliveryNote`
--

DROP TABLE IF EXISTS `localItemDeliveryNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localItemDeliveryNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `reqId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `reqId` (`reqId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localItemDeliveryNote_ibfk_1` FOREIGN KEY (`reqId`) REFERENCES `localStoreRequisition` (`reqId`),
  CONSTRAINT `localItemDeliveryNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localItemDeliveryNote`
--

LOCK TABLES `localItemDeliveryNote` WRITE;
/*!40000 ALTER TABLE `localItemDeliveryNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localItemDeliveryNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localItemDemandStore`
--

DROP TABLE IF EXISTS `localItemDemandStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localItemDemandStore` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeId` int(10) unsigned NOT NULL,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `storeId` (`storeId`,`itemId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `localitemDemandStore_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `localitemDemandStore_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localItemDemandStore`
--

LOCK TABLES `localItemDemandStore` WRITE;
/*!40000 ALTER TABLE `localItemDemandStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `localItemDemandStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localProductDeliveryNote`
--

DROP TABLE IF EXISTS `localProductDeliveryNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localProductDeliveryNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `reqId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `reqId` (`reqId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localProductDeliveryNote_ibfk_1` FOREIGN KEY (`reqId`) REFERENCES `localWarehouseRequisition` (`reqId`),
  CONSTRAINT `localProductDeliveryNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localProductDeliveryNote`
--

LOCK TABLES `localProductDeliveryNote` WRITE;
/*!40000 ALTER TABLE `localProductDeliveryNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localProductDeliveryNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localProductDemand`
--

DROP TABLE IF EXISTS `localProductDemand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localProductDemand` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouseId` int(10) unsigned NOT NULL,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `warehouseId` (`warehouseId`,`productId`),
  KEY `productId` (`productId`),
  CONSTRAINT `localProductDemand_ibfk_1` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `localProductDemand_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localProductDemand`
--

LOCK TABLES `localProductDemand` WRITE;
/*!40000 ALTER TABLE `localProductDemand` DISABLE KEYS */;
/*!40000 ALTER TABLE `localProductDemand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localProductReceivedNote`
--

DROP TABLE IF EXISTS `localProductReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localProductReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localProductReceivedNote_ibfk_1` FOREIGN KEY (`deliveryNoteId`) REFERENCES `localProductDeliveryNote` (`noteId`),
  CONSTRAINT `localProductReceivedNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localProductReceivedNote`
--

LOCK TABLES `localProductReceivedNote` WRITE;
/*!40000 ALTER TABLE `localProductReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localProductReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localStoreRequisition`
--

DROP TABLE IF EXISTS `localStoreRequisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localStoreRequisition` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqNo` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `sourceStoreId` int(10) unsigned NOT NULL,
  `destinationStoreId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `reqNo` (`reqNo`),
  KEY `documentId` (`documentId`),
  KEY `sourceStoreId` (`sourceStoreId`),
  KEY `destinationStoreId` (`destinationStoreId`),
  CONSTRAINT `localStoreRequisition_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `localStoreRequisition_ibfk_2` FOREIGN KEY (`sourceStoreId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `localStoreRequisition_ibfk_3` FOREIGN KEY (`destinationStoreId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localStoreRequisition`
--

LOCK TABLES `localStoreRequisition` WRITE;
/*!40000 ALTER TABLE `localStoreRequisition` DISABLE KEYS */;
/*!40000 ALTER TABLE `localStoreRequisition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localWarehouseRequisition`
--

DROP TABLE IF EXISTS `localWarehouseRequisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localWarehouseRequisition` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqNo` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `sourceWarehouseId` int(10) unsigned NOT NULL,
  `destinationWarehouseId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `reqNo` (`reqNo`),
  KEY `documentId` (`documentId`),
  KEY `sourceWarehouseId` (`sourceWarehouseId`),
  KEY `destinationWarehouseId` (`destinationWarehouseId`),
  CONSTRAINT `localWarehouseRequisition_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `localWarehouseRequisition_ibfk_2` FOREIGN KEY (`sourceWarehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `localWarehouseRequisition_ibfk_3` FOREIGN KEY (`destinationWarehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localWarehouseRequisition`
--

LOCK TABLES `localWarehouseRequisition` WRITE;
/*!40000 ALTER TABLE `localWarehouseRequisition` DISABLE KEYS */;
/*!40000 ALTER TABLE `localWarehouseRequisition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `locationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locationName` varchar(64) NOT NULL,
  `conferenceId` int(10) unsigned DEFAULT NULL,
  `physicalAddress` varchar(64) NOT NULL,
  `postalAddress` varchar(64) DEFAULT NULL,
  `gpsCoordinate` varchar(72) DEFAULT NULL,
  `countryId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`locationId`),
  KEY `conferenceId` (`conferenceId`),
  KEY `countryId` (`countryId`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`conferenceId`) REFERENCES `conference` (`conferenceId`),
  CONSTRAINT `location_ibfk_2` FOREIGN KEY (`countryId`) REFERENCES `country` (`countryId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Installation Location',NULL,'Installation Address',NULL,NULL,227,NULL,NULL,0);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `loginId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loginName` varchar(32) DEFAULT NULL,
  `password` char(40) NOT NULL,
  `fullName` varchar(48) DEFAULT NULL,
  `groupId` int(10) unsigned DEFAULT NULL,
  `sexId` int(10) unsigned DEFAULT NULL,
  `maritalId` int(10) unsigned DEFAULT NULL,
  `context` char(255) NOT NULL DEFAULT '222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',
  `jobId` int(10) unsigned DEFAULT NULL,
  `root` tinyint(1) NOT NULL DEFAULT '0',
  `locationId` int(10) unsigned DEFAULT NULL,
  `departmentId` int(10) unsigned DEFAULT NULL,
  `initializationAccount` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `statusId` int(10) unsigned DEFAULT NULL,
  `dob` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `photo` varchar(64) DEFAULT NULL,
  `qnSecurity1` int(10) unsigned DEFAULT NULL,
  `ansSecurity1` varchar(32) DEFAULT NULL,
  `qnSecurity2` int(10) unsigned DEFAULT NULL,
  `ansSecurity2` varchar(32) DEFAULT NULL,
  `themeId` int(10) unsigned DEFAULT NULL,
  `firstDayOfAWeekId` int(10) unsigned DEFAULT NULL,
  `randomNumber` char(32) DEFAULT NULL,
  `virtualizationList` varchar(108) DEFAULT NULL,
  `admissionTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `lastLoginTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loginId`),
  KEY `fk_group` (`groupId`),
  KEY `fk_sex` (`sexId`),
  KEY `fk_marital` (`maritalId`),
  KEY `fk_department` (`departmentId`),
  KEY `fk_location` (`locationId`),
  KEY `fk_jobTitle` (`jobId`),
  KEY `login_firstDayOfAWeek` (`firstDayOfAWeekId`),
  KEY `login_theme` (`themeId`),
  KEY `login_security_qn_2` (`qnSecurity2`),
  KEY `login_security_qn_1` (`qnSecurity1`),
  KEY `login_status` (`statusId`),
  CONSTRAINT `fk_department` FOREIGN KEY (`departmentId`) REFERENCES `department` (`departmentId`),
  CONSTRAINT `fk_group` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`),
  CONSTRAINT `fk_jobTitle` FOREIGN KEY (`jobId`) REFERENCES `jobTitle` (`jobId`),
  CONSTRAINT `fk_location` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`),
  CONSTRAINT `fk_marital` FOREIGN KEY (`maritalId`) REFERENCES `marital` (`maritalId`),
  CONSTRAINT `fk_sex` FOREIGN KEY (`sexId`) REFERENCES `sex` (`sexId`),
  CONSTRAINT `login_firstDayOfAWeek` FOREIGN KEY (`firstDayOfAWeekId`) REFERENCES `daysOfAWeek` (`dayId`),
  CONSTRAINT `login_security_qn_1` FOREIGN KEY (`qnSecurity1`) REFERENCES `securityQuestion` (`questionId`),
  CONSTRAINT `login_security_qn_2` FOREIGN KEY (`qnSecurity2`) REFERENCES `securityQuestion` (`questionId`),
  CONSTRAINT `login_status` FOREIGN KEY (`statusId`) REFERENCES `userStatus` (`statusId`),
  CONSTRAINT `login_theme` FOREIGN KEY (`themeId`) REFERENCES `themes` (`themeId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marital`
--

DROP TABLE IF EXISTS `marital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marital` (
  `maritalId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `maritalName` varchar(24) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`maritalId`),
  UNIQUE KEY `maritalName` (`maritalName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marital`
--

LOCK TABLES `marital` WRITE;
/*!40000 ALTER TABLE `marital` DISABLE KEYS */;
INSERT INTO `marital` VALUES (1,'Single',NULL,NULL,15),(2,'Married',NULL,NULL,15),(3,'Divorced',NULL,NULL,15),(4,'Widowed',NULL,NULL,15),(5,'Cohabiting',NULL,NULL,15),(6,'Civil Union',NULL,NULL,15),(7,'Domestic Partner',NULL,NULL,15),(8,'Unmarried Partner',NULL,NULL,15);
/*!40000 ALTER TABLE `marital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measure` (
  `measureId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `measureName` varchar(24) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`measureId`),
  UNIQUE KEY `measureName` (`measureName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure`
--

LOCK TABLES `measure` WRITE;
/*!40000 ALTER TABLE `measure` DISABLE KEYS */;
/*!40000 ALTER TABLE `measure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messagePolicyType`
--

DROP TABLE IF EXISTS `messagePolicyType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messagePolicyType` (
  `typeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeName` varchar(48) NOT NULL,
  `typeCode` int(2) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`typeId`),
  UNIQUE KEY `typeName` (`typeName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messagePolicyType`
--

LOCK TABLES `messagePolicyType` WRITE;
/*!40000 ALTER TABLE `messagePolicyType` DISABLE KEYS */;
/*!40000 ALTER TABLE `messagePolicyType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messageType`
--

DROP TABLE IF EXISTS `messageType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messageType` (
  `typeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeName` varchar(48) NOT NULL,
  `typeCode` int(2) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`typeId`),
  UNIQUE KEY `typeName` (`typeName`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messageType`
--

LOCK TABLES `messageType` WRITE;
/*!40000 ALTER TABLE `messageType` DISABLE KEYS */;
INSERT INTO `messageType` VALUES (1,'Local',1,NULL,NULL,15),(2,'SMS',2,NULL,NULL,15),(3,'Email',3,NULL,NULL,15);
/*!40000 ALTER TABLE `messageType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monthOfAYear`
--

DROP TABLE IF EXISTS `monthOfAYear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthOfAYear` (
  `monthId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `monthName` varchar(9) NOT NULL,
  `monthAbbreviation` varchar(3) NOT NULL,
  `monthNumber` int(2) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`monthId`),
  UNIQUE KEY `monthName` (`monthName`),
  UNIQUE KEY `monthAbbreviation` (`monthAbbreviation`),
  UNIQUE KEY `monthNumber` (`monthNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monthOfAYear`
--

LOCK TABLES `monthOfAYear` WRITE;
/*!40000 ALTER TABLE `monthOfAYear` DISABLE KEYS */;
INSERT INTO `monthOfAYear` VALUES (1,'January','Jan',1,NULL,NULL,15),(2,'February','Feb',2,NULL,NULL,15),(3,'March','Mar',3,NULL,NULL,15),(4,'April','Apr',4,NULL,NULL,15),(5,'May','May',5,NULL,NULL,15),(6,'June','Jun',6,NULL,NULL,15),(7,'July','Jul',7,NULL,NULL,15),(8,'August','Aug',8,NULL,NULL,15),(9,'September','Sep',9,NULL,NULL,15),(10,'October','Oct',10,NULL,NULL,15),(11,'November','Nov',11,NULL,NULL,15),(12,'December','Dec',12,NULL,NULL,15);
/*!40000 ALTER TABLE `monthOfAYear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pHPTimezone`
--

DROP TABLE IF EXISTS `pHPTimezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pHPTimezone` (
  `zoneId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zoneName` varchar(48) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`zoneId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pHPTimezone`
--

LOCK TABLES `pHPTimezone` WRITE;
/*!40000 ALTER TABLE `pHPTimezone` DISABLE KEYS */;
INSERT INTO `pHPTimezone` VALUES (1,'Africa/Dar_es_Salaam',NULL,NULL,15);
/*!40000 ALTER TABLE `pHPTimezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper`
--

DROP TABLE IF EXISTS `paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper` (
  `paperId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paperName` varchar(32) NOT NULL,
  `width` int(4) unsigned NOT NULL DEFAULT '0',
  `height` int(4) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paperId`),
  UNIQUE KEY `paperName` (`paperName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper`
--

LOCK TABLES `paper` WRITE;
/*!40000 ALTER TABLE `paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentReceipt`
--

DROP TABLE IF EXISTS `paymentReceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentReceipt` (
  `receiptId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receiptNumber` varchar(32) NOT NULL,
  `invoiceId` int(10) unsigned NOT NULL,
  `amount` varchar(32) NOT NULL DEFAULT '0.0',
  `paymentTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiptId`),
  KEY `invoiceId` (`invoiceId`),
  CONSTRAINT `paymentReceipt_ibfk_1` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentReceipt`
--

LOCK TABLES `paymentReceipt` WRITE;
/*!40000 ALTER TABLE `paymentReceipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentReceipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentReceiptBank`
--

DROP TABLE IF EXISTS `paymentReceiptBank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentReceiptBank` (
  `bankId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receiptId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bankId`),
  KEY `receiptId` (`receiptId`),
  CONSTRAINT `paymentReceiptBank_ibfk_1` FOREIGN KEY (`receiptId`) REFERENCES `paymentReceipt` (`receiptId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentReceiptBank`
--

LOCK TABLES `paymentReceiptBank` WRITE;
/*!40000 ALTER TABLE `paymentReceiptBank` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentReceiptBank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physicalStore`
--

DROP TABLE IF EXISTS `physicalStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physicalStore` (
  `storeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeName` varchar(32) NOT NULL,
  `locationId` int(10) unsigned NOT NULL,
  `openingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`storeId`),
  KEY `locationId` (`locationId`),
  CONSTRAINT `physicalStore_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physicalStore`
--

LOCK TABLES `physicalStore` WRITE;
/*!40000 ALTER TABLE `physicalStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `physicalStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physicalWarehouse`
--

DROP TABLE IF EXISTS `physicalWarehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physicalWarehouse` (
  `warehouseId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouseName` varchar(32) NOT NULL,
  `locationId` int(10) unsigned NOT NULL,
  `openingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`warehouseId`),
  KEY `locationId` (`locationId`),
  CONSTRAINT `physicalWarehouse_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physicalWarehouse`
--

LOCK TABLES `physicalWarehouse` WRITE;
/*!40000 ALTER TABLE `physicalWarehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `physicalWarehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priceQuote`
--

DROP TABLE IF EXISTS `priceQuote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priceQuote` (
  `quoteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quoteNumber` varchar(36) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`quoteId`),
  UNIQUE KEY `quoteNumber` (`quoteNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `priceQuote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priceQuote`
--

LOCK TABLES `priceQuote` WRITE;
/*!40000 ALTER TABLE `priceQuote` DISABLE KEYS */;
/*!40000 ALTER TABLE `priceQuote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `productId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productName` varchar(32) NOT NULL,
  `categoryId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`productId`),
  UNIQUE KEY `productName` (`productName`),
  KEY `fbk_product_category` (`categoryId`),
  CONSTRAINT `fbk_product_category` FOREIGN KEY (`categoryId`) REFERENCES `productCategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productCategory`
--

DROP TABLE IF EXISTS `productCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productCategory` (
  `categoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productCategory`
--

LOCK TABLES `productCategory` WRITE;
/*!40000 ALTER TABLE `productCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `productCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productDemandStore`
--

DROP TABLE IF EXISTS `productDemandStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productDemandStore` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouseId` int(10) unsigned NOT NULL,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `productId` (`productId`),
  KEY `warehouseId` (`warehouseId`),
  CONSTRAINT `productDemandStore_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `productDemandStore_ibfk_2` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productDemandStore`
--

LOCK TABLES `productDemandStore` WRITE;
/*!40000 ALTER TABLE `productDemandStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `productDemandStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productGoodReceivedNote`
--

DROP TABLE IF EXISTS `productGoodReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productGoodReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `orderId` int(10) unsigned DEFAULT NULL,
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  KEY `orderId` (`orderId`),
  CONSTRAINT `productGoodReceivedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `productGoodReceivedNote_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `productPurchasingOrder` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productGoodReceivedNote`
--

LOCK TABLES `productGoodReceivedNote` WRITE;
/*!40000 ALTER TABLE `productGoodReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `productGoodReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productPurchasingOrder`
--

DROP TABLE IF EXISTS `productPurchasingOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productPurchasingOrder` (
  `orderId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `warehouseId` int(10) unsigned NOT NULL,
  `supplierName` varchar(32) DEFAULT NULL,
  `supplierAddress` varchar(64) DEFAULT NULL,
  `supplierPhone` varchar(14) DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `currencyId` int(10) unsigned DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) NOT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `orderNumber` (`orderNumber`),
  KEY `currencyId` (`currencyId`),
  KEY `warehouseId` (`warehouseId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `productPurchasingOrder_ibfk_1` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`),
  CONSTRAINT `productPurchasingOrder_ibfk_2` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `productPurchasingOrder_ibfk_3` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productPurchasingOrder`
--

LOCK TABLES `productPurchasingOrder` WRITE;
/*!40000 ALTER TABLE `productPurchasingOrder` DISABLE KEYS */;
/*!40000 ALTER TABLE `productPurchasingOrder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `profileId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profileName` varchar(96) NOT NULL DEFAULT 'Default Profile',
  `systemName` varchar(108) NOT NULL DEFAULT 'Default SYS',
  `logo` varchar(64) DEFAULT NULL,
  `telephoneList` varchar(108) DEFAULT NULL,
  `emailList` varchar(108) DEFAULT NULL,
  `otherCommunication` varchar(108) DEFAULT NULL,
  `locationId` int(10) unsigned DEFAULT NULL,
  `website` varchar(64) DEFAULT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `pHPTimezoneId` int(10) unsigned DEFAULT NULL,
  `dob` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `themeId` int(10) unsigned DEFAULT NULL,
  `firstDayOfAWeekId` int(10) unsigned DEFAULT NULL,
  `xmlFile` varchar(12) NOT NULL DEFAULT 'profile.xml',
  `xmlFileChecksum` char(32) DEFAULT NULL,
  `serverIpAddress` varchar(15) NOT NULL DEFAULT '192.168.1.1',
  `localAreaNetworkMask` varchar(15) NOT NULL DEFAULT '255.255.255.0',
  `maxNumberOfReturnedSearchRecords` int(4) NOT NULL DEFAULT '512',
  `maxNumberOfDisplayedRowsPerPage` int(4) NOT NULL DEFAULT '64',
  `minAgeCriteriaForUsers` int(4) NOT NULL DEFAULT '12',
  `applicationCounter` int(2) unsigned NOT NULL DEFAULT '0',
  `revisionNumber` int(8) unsigned NOT NULL DEFAULT '1',
  `revisionTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `serverMACAddress` varchar(24) DEFAULT NULL,
  `systemHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`profileId`),
  KEY `locationId` (`locationId`),
  KEY `pHPTimezoneId` (`pHPTimezoneId`),
  KEY `themeId` (`themeId`),
  KEY `firstDayOfAWeekId` (`firstDayOfAWeekId`),
  CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`pHPTimezoneId`) REFERENCES `pHPTimezone` (`zoneId`),
  CONSTRAINT `profile_ibfk_3` FOREIGN KEY (`themeId`) REFERENCES `themes` (`themeId`),
  CONSTRAINT `profile_ibfk_4` FOREIGN KEY (`firstDayOfAWeekId`) REFERENCES `daysOfAWeek` (`dayId`),
  CONSTRAINT `profile_ibfk_5` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'Default System Profile','Default SYS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000:00:00:00:00:00',NULL,NULL,'profile.xml',NULL,'192.168.1.1','255.255.255.0',512,64,12,0,1,'0000:00:00:00:00:00',NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchasingOrder`
--

DROP TABLE IF EXISTS `purchasingOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchasingOrder` (
  `orderId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `supplierName` varchar(32) DEFAULT NULL,
  `supplierAddress` varchar(64) DEFAULT NULL,
  `supplierPhone` varchar(14) DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `currencyId` int(10) unsigned DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `code` char(40) NOT NULL,
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `orderNumber` (`orderNumber`),
  KEY `documentId` (`documentId`),
  KEY `currencyId` (`currencyId`),
  KEY `fk_constraint_purchasing_order` (`storeId`),
  CONSTRAINT `fk_constraint_purchasing_order` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `purchasingOrder_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `purchasingOrder_ibfk_2` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchasingOrder`
--

LOCK TABLES `purchasingOrder` WRITE;
/*!40000 ALTER TABLE `purchasingOrder` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchasingOrder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesReport`
--

DROP TABLE IF EXISTS `salesReport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesReport` (
  `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportName` varchar(32) NOT NULL,
  `dateTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `invoiceId` int(10) unsigned NOT NULL,
  `conferenceId` int(10) unsigned DEFAULT NULL,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(14) unsigned NOT NULL,
  `productValue` int(17) unsigned NOT NULL,
  `productSellingPrice` int(17) unsigned NOT NULL,
  `customerName` varchar(32) DEFAULT NULL,
  `locationId` int(10) unsigned DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reportId`),
  KEY `locationId` (`locationId`),
  KEY `conferenceId` (`conferenceId`),
  KEY `productId` (`productId`),
  KEY `invoiceId` (`invoiceId`),
  CONSTRAINT `salesReport_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`),
  CONSTRAINT `salesReport_ibfk_2` FOREIGN KEY (`conferenceId`) REFERENCES `conference` (`conferenceId`),
  CONSTRAINT `salesReport_ibfk_3` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `salesReport_ibfk_4` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesReport`
--

LOCK TABLES `salesReport` WRITE;
/*!40000 ALTER TABLE `salesReport` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesReport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `securityQuestion`
--

DROP TABLE IF EXISTS `securityQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securityQuestion` (
  `questionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questionName` varchar(96) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`questionId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `securityQuestion`
--

LOCK TABLES `securityQuestion` WRITE;
/*!40000 ALTER TABLE `securityQuestion` DISABLE KEYS */;
INSERT INTO `securityQuestion` VALUES (1,'What was your childhood nickname?',NULL,NULL,15),(2,'In what city did you meet your spouse/significant other?',NULL,NULL,15),(3,'What is the name of your favorite childhood friend?',NULL,NULL,15),(4,'What street did you live on in third grade?',NULL,NULL,15),(5,'What is your oldest sibling\'s birthday month and year? (e.g., January 1900)',NULL,NULL,15),(6,'What is the middle name of your oldest child?',NULL,NULL,15),(7,'What is your oldest sibling\'s middle name?',NULL,NULL,15),(8,'What school did you attend for sixth grade?',NULL,NULL,15),(9,'What was your childhood phone number including area code? (e.g., 000-000-0000)',NULL,NULL,15),(10,'What is your oldest cousin\'s first and last name?',NULL,NULL,15),(11,'What was the name of your first stuffed animal?',NULL,NULL,15),(12,'In what city or town did your mother and father meet?',NULL,NULL,15),(13,'Where were you when you had your first kiss?',NULL,NULL,15),(14,'What is the first name of the boy or girl that you first kissed?',NULL,NULL,15),(15,'What was the last name of your third grade teacher?',NULL,NULL,15),(16,'In what city does your nearest sibling live?',NULL,NULL,15),(17,'What is your oldest brother\'s birthday month and year? (e.g., January 1900)',NULL,NULL,15),(18,'What is your maternal grandmother\'s maiden name?',NULL,NULL,15),(19,'In what city or town was your first job?',NULL,NULL,15),(20,'What is the name of the place your wedding reception was held?',NULL,NULL,15),(21,'What is the name of a college you applied to but didn\'t attend?',NULL,NULL,15),(22,'Where were you when you first heard about 9/11?',NULL,NULL,15);
/*!40000 ALTER TABLE `securityQuestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sex`
--

DROP TABLE IF EXISTS `sex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sex` (
  `sexId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sexName` varchar(8) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`sexId`),
  UNIQUE KEY `sexName` (`sexName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sex`
--

LOCK TABLES `sex` WRITE;
/*!40000 ALTER TABLE `sex` DISABLE KEYS */;
INSERT INTO `sex` VALUES (1,'Male',NULL,NULL,15),(2,'Female',NULL,NULL,15);
/*!40000 ALTER TABLE `sex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stockItemRecordCard`
--

DROP TABLE IF EXISTS `stockItemRecordCard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockItemRecordCard` (
  `recordCardId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `recordDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `refNumber` varchar(32) NOT NULL,
  `titleText` varchar(32) DEFAULT NULL,
  `quantity` int(16) NOT NULL DEFAULT '0',
  `unitValue` varchar(32) NOT NULL DEFAULT '0.0',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recordCardId`),
  KEY `storeId` (`storeId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `stockItemRecordCard_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `stockItemRecordCard_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stockItemRecordCard`
--

LOCK TABLES `stockItemRecordCard` WRITE;
/*!40000 ALTER TABLE `stockItemRecordCard` DISABLE KEYS */;
/*!40000 ALTER TABLE `stockItemRecordCard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stockRequisition`
--

DROP TABLE IF EXISTS `stockRequisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockRequisition` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqNo` varchar(24) NOT NULL,
  `sheetId` int(10) unsigned DEFAULT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `departmentId` int(10) unsigned DEFAULT NULL,
  `debtAccount` varchar(32) NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `reqNo` (`reqNo`),
  KEY `sheetId` (`sheetId`),
  KEY `documentId` (`documentId`),
  KEY `fk_constraint_department` (`departmentId`),
  KEY `fk_constraint_physical_store` (`storeId`),
  CONSTRAINT `fk_constraint_department` FOREIGN KEY (`departmentId`) REFERENCES `department` (`departmentId`),
  CONSTRAINT `fk_constraint_physical_store` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `stockRequisition_ibfk_1` FOREIGN KEY (`sheetId`) REFERENCES `jobCostSheet` (`jobCostId`),
  CONSTRAINT `stockRequisition_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stockRequisition`
--

LOCK TABLES `stockRequisition` WRITE;
/*!40000 ALTER TABLE `stockRequisition` DISABLE KEYS */;
/*!40000 ALTER TABLE `stockRequisition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemPolicy`
--

DROP TABLE IF EXISTS `systemPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemPolicy` (
  `policyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `className` varchar(64) NOT NULL,
  `policyCaption` varchar(96) NOT NULL,
  `root` tinyint(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`policyId`),
  UNIQUE KEY `className` (`className`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemPolicy`
--

LOCK TABLES `systemPolicy` WRITE;
/*!40000 ALTER TABLE `systemPolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemlogs`
--

DROP TABLE IF EXISTS `systemlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemlogs` (
  `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logName` varchar(108) NOT NULL,
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemlogs`
--

LOCK TABLES `systemlogs` WRITE;
/*!40000 ALTER TABLE `systemlogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemlogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `testId` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `testName` varchar(12) NOT NULL,
  PRIMARY KEY (`testId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes` (
  `themeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `themeName` varchar(24) NOT NULL,
  `themeFolder` varchar(16) NOT NULL,
  `themeBackgroundColor` varchar(6) NOT NULL DEFAULT 'ffffff',
  `themeBackgroundImage` varchar(16) DEFAULT NULL,
  `themeFontFace` varchar(108) DEFAULT NULL,
  `themeFontSize` varchar(8) DEFAULT NULL,
  `themeFontColor` varchar(6) NOT NULL DEFAULT '000000',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`themeId`),
  UNIQUE KEY `themeName` (`themeName`),
  UNIQUE KEY `themeFolder` (`themeFolder`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (1,'Black Tie','black-tie','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(2,'Blitzer','blitzer','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(3,'Cupertino','cupertino','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(4,'Dark Hive','dark-hive','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(5,'Dot Luv','dot-luv','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(6,'Egg Plant','eggplant','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(7,'Excite Bike','excite-bike','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(8,'Flick','flick','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(9,'Hot Sneaks','hot-sneaks','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(10,'Humanity','humanity','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(11,'Le Frog','le-frog','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(12,'Mint Choc','mint-choc','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(13,'Overcast','overcast','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(14,'Pepper Grinder','pepper-grinder','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(15,'Redmond','redmond','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(16,'Smoothness','smoothness','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(17,'South Street','south-street','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(18,'Start','start','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(19,'Sunny','sunny','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(20,'Swanky Purse','swanky-purse','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(21,'Trontastic','trontastic','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(22,'UI Darkness','ui-darkness','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(23,'UI Lightness','ui-lightness','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(24,'Vader','vader','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15);
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userStatus`
--

DROP TABLE IF EXISTS `userStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userStatus` (
  `statusId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `statusName` varchar(32) NOT NULL,
  `statusCode` int(2) unsigned NOT NULL,
  `alive` tinyint(1) NOT NULL DEFAULT '1',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`statusId`),
  UNIQUE KEY `statusName` (`statusName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userStatus`
--

LOCK TABLES `userStatus` WRITE;
/*!40000 ALTER TABLE `userStatus` DISABLE KEYS */;
INSERT INTO `userStatus` VALUES (1,'Alive',1,1,NULL,NULL,15),(2,'Dead',2,0,NULL,NULL,15),(3,'Disqualified',3,0,NULL,NULL,15),(4,'Leave',4,1,NULL,NULL,15),(5,'Martenity',5,1,NULL,NULL,15);
/*!40000 ALTER TABLE `userStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loginId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`),
  KEY `loginId` (`loginId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`loginId`) REFERENCES `login` (`loginId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificationData`
--

DROP TABLE IF EXISTS `verificationData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificationData` (
  `verificationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schemaId` int(10) unsigned NOT NULL,
  `docRefNumber` varchar(24) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `loginId` int(10) unsigned DEFAULT NULL,
  `dt` varchar(19) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`verificationId`),
  UNIQUE KEY `schemaId_2` (`schemaId`,`docRefNumber`),
  KEY `fk_login` (`loginId`),
  CONSTRAINT `fk_login` FOREIGN KEY (`loginId`) REFERENCES `login` (`loginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificationData`
--

LOCK TABLES `verificationData` WRITE;
/*!40000 ALTER TABLE `verificationData` DISABLE KEYS */;
/*!40000 ALTER TABLE `verificationData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificationSchema`
--

DROP TABLE IF EXISTS `verificationSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificationSchema` (
  `verificationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `captionText` varchar(32) NOT NULL,
  `captionError` varchar(32) NOT NULL,
  `jobId` int(10) unsigned NOT NULL,
  `docId` int(10) unsigned NOT NULL,
  `seqno` int(4) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`verificationId`),
  UNIQUE KEY `docId` (`docId`,`seqno`),
  KEY `jobId` (`jobId`),
  CONSTRAINT `verificationSchema_ibfk_1` FOREIGN KEY (`docId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `verificationschema_ibfk_2` FOREIGN KEY (`jobId`) REFERENCES `jobTitle` (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificationSchema`
--

LOCK TABLES `verificationSchema` WRITE;
/*!40000 ALTER TABLE `verificationSchema` DISABLE KEYS */;
/*!40000 ALTER TABLE `verificationSchema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouse` (
  `warehouseId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `physicalWarehouseId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `productValue` varchar(32) NOT NULL DEFAULT '0.0',
  `minStockLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `productionLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `productSellingPrice` varchar(32) NOT NULL DEFAULT '0.0',
  `margin` varchar(16) NOT NULL DEFAULT '1.0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`warehouseId`),
  UNIQUE KEY `productId` (`productId`),
  KEY `physicalWarehouseId` (`physicalWarehouseId`),
  CONSTRAINT `warehouse_ibfk_1` FOREIGN KEY (`physicalWarehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `warehouse_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse`
--

LOCK TABLES `warehouse` WRITE;
/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouseProductRecordCard`
--

DROP TABLE IF EXISTS `warehouseProductRecordCard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouseProductRecordCard` (
  `recordCardId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `warehouseId` int(10) unsigned NOT NULL,
  `recordDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `refNumber` varchar(32) NOT NULL,
  `titleText` varchar(32) DEFAULT NULL,
  `quantity` int(16) NOT NULL DEFAULT '0',
  `unitValue` varchar(32) NOT NULL DEFAULT '0.0',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recordCardId`),
  KEY `warehouseId` (`warehouseId`),
  KEY `productId` (`productId`),
  CONSTRAINT `warehouseProductRecordCard_ibfk_1` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `warehouseProductRecordCard_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouseProductRecordCard`
--

LOCK TABLES `warehouseProductRecordCard` WRITE;
/*!40000 ALTER TABLE `warehouseProductRecordCard` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouseProductRecordCard` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-24  9:36:27
alter table login modify `context` char(255) NOT NULL DEFAULT ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;';
alter table jobTitle modify context char(255) not null default ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;';
alter table groups modify context char(255) not null default ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;';
alter table document add approvalSchemaList varchar(32) default null after imagename;
alter table document modify documentName varchar(64) not null;
delete from document;
alter table document add className varchar(32) default null after documentCode;
insert into document (documentName, documentCode, className, pagename) values('Delivery Note', '1', 'DeliveryNote', '1');
insert into document (documentName, documentCode, className, pagename) values('Finished Good Report', '2', 'FinishedGoodReport', '2');
insert into document (documentName, documentCode, className, pagename) values('Good Received Note (Raw Material)', '3', 'GoodReceivedNote', '3');
insert into document (documentName, documentCode, className, pagename) values('Good Returned Note', '4', 'GoodReturnedNote', '4');
insert into document (documentName, documentCode, className, pagename) values('Invoice', '5', 'Invoice', '5');
insert into document (documentName, documentCode, className, pagename) values('Issue Note', '6', 'IssueNote', '6');
insert into document (documentName, documentCode, className, pagename) values('Job Cost Sheet', '7', 'JobCostSheet', '7');
insert into document (documentName, documentCode, className, pagename) values('Price Quote', '8', 'PriceQuote', '8');
insert into document (documentName, documentCode, className, pagename) values('Purchasing Order (Product)', '9', 'ProductPurchasingOrder', '9');
insert into document (documentName, documentCode, className, pagename) values('Good Received Note (Product)', '10', 'ProductGoodReceivedNote', '10');
insert into document (documentName, documentCode, className, pagename) values('Purchasing Order (Raw Material)', '11', 'PurchasingOrder', '11');
insert into document (documentName, documentCode, className, pagename) values('Stock Requisition', '12', 'StockRequisition', '12');
insert into document (documentName, documentCode, className, pagename) values('[Local] Store Requisition', '13', 'LocalStoreRequisition', '13');
insert into document (documentName, documentCode, className, pagename) values('[Local Raw Material] Delivery Note', '14', 'LocalItemDeliveryNote', '14');
insert into document (documentName, documentCode, className, pagename) values('[Local Raw Material] Good Received Note', '15', 'LocalGoodReceivedNote', '15');
insert into document (documentName, documentCode, className, pagename) values('[Local] Warehouse Requisition', '16', 'LocalWarehouseRequisition', '16');
insert into document (documentName, documentCode, className, pagename) values('[Local Product] Delivery Note', '17', 'LocalProductDeliveryNote', '17');
insert into document (documentName, documentCode, className, pagename) values('[Local Product] Good Received Note', '18', 'LocalProductReceivedNote', '18');

create table documentApprovalSchema (
	schemaId int(10) unsigned not null auto_increment,
	captionText varchar(48) not null,
	captionError varchar(48) not null,
	jobId int(10) unsigned not null,
	 `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
	primary key (schemaId)
) engine=InnoDB;
alter table deliveryNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table deliveryNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table deliveryNote add constraint next_schema_to_approve_job_id_1 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table finishedGoodReport add alreadyApprovedList varchar(32) default null after filenameHash;
alter table finishedGoodReport add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table finishedGoodReport add constraint next_schema_to_approve_job_id_2 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table goodReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table goodReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table goodReceivedNote add constraint next_schema_to_approve_job_id_3 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table goodReturnedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table goodReturnedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table goodReturnedNote add constraint next_schema_to_approve_job_id_4 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table invoice add alreadyApprovedList varchar(32) default null after filenameHash;
alter table invoice add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table invoice add constraint next_schema_to_approve_job_id_5 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table issueNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table issueNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table issueNote add constraint next_schema_to_approve_job_id_6 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table jobCostSheet add alreadyApprovedList varchar(32) default null after filenameHash;
alter table jobCostSheet add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table jobCostSheet add constraint next_schema_to_approve_job_id_7 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table priceQuote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table priceQuote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table priceQuote add constraint next_schema_to_approve_job_id_8 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table productPurchasingOrder add alreadyApprovedList varchar(32) default null after filenameHash;
alter table productPurchasingOrder add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table productPurchasingOrder add constraint next_schema_to_approve_job_id_9 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table productGoodReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table productGoodReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table productGoodReceivedNote add constraint next_schema_to_approve_job_id_10 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table purchasingOrder add alreadyApprovedList varchar(32) default null after filenameHash;
alter table purchasingOrder add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table purchasingOrder add constraint next_schema_to_approve_job_id_11 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table stockRequisition add alreadyApprovedList varchar(32) default null after filenameHash;
alter table stockRequisition add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table stockRequisition add constraint next_schema_to_approve_job_id_12 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localStoreRequisition add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localStoreRequisition add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localStoreRequisition add constraint next_schema_to_approve_job_id_13 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localItemDeliveryNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localItemDeliveryNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localItemDeliveryNote add constraint next_schema_to_approve_job_id_14 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localGoodReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localGoodReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localGoodReceivedNote add constraint next_schema_to_approve_job_id_15 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localWarehouseRequisition add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localWarehouseRequisition add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localWarehouseRequisition add constraint next_schema_to_approve_job_id_16 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localProductDeliveryNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localProductDeliveryNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localProductDeliveryNote add constraint next_schema_to_approve_job_id_17 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
alter table localProductReceivedNote add alreadyApprovedList varchar(32) default null after filenameHash;
alter table localProductReceivedNote add nextSchemaToApprove int(10) unsigned default null after filenameHash;
alter table localProductReceivedNote add constraint next_schema_to_approve_job_id_18 foreign key (nextSchemaToApprove) references documentApprovalSchema(schemaId);
insert into contextPosition (cId, cName, cPosition, caption) values('395', 'managedocumentapprovalschema', '395', 'managedocumentapprovalschema');
insert into contextPosition (cId, cName, cPosition, caption) values('396', 'managedocumentapprovalschema_add', '396', 'managedocumentapprovalschema_add');
insert into contextPosition (cId, cName, cPosition, caption) values('397', 'managedocumentapprovalschema_detail', '397', 'managedocumentapprovalschema_detail');
insert into contextPosition (cId, cName, cPosition, caption) values('398', 'managedocumentapprovalschema_edit', '398', 'managedocumentapprovalschema_edit');
insert into contextPosition (cId, cName, cPosition, caption) values('399', 'managedocumentapprovalschema_delete', '399', 'managedocumentapprovalschema_delete');
insert into contextPosition (cId, cName, cPosition, caption) values('400', 'managedocumentapprovalschema_csv', '400', 'managedocumentapprovalschema_csv');
alter table profile add tinNumber varchar(32) default null after revisionTime;
create table notification (
	notificationId int(10) unsigned not null auto_increment,
	notificationText varchar(108) not null,
	buttonText varchar(48) not null,
	contextName varchar(56) not null,
	serverPage varchar(108) not null,
	serverArguments varchar(255) not null,
	postedTime varchar(19) not null default '0000:00:00:00:00:00',
	 `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
	primary key (notificationId)
) engine=InnoDB;