<?php 
class FileFactory {
	/*
	These are statically defined file Manager
	If not supposed to return data, they should return a promise object with data 
	A function which does file editing should create a backup file with 
	Append with .backup extension 
	The promise object should have the position of the next element, this means the length of returned records
		since this is a zero based
	*/
	public final static function addDOMListToTree($doc, $parentNodeName, $domListArray)	{
		/*
		parentNodeName : ie product 
		domListArray : key val
		*/
		$__parentNode1 = $doc->createElement($parentNodeName);
		foreach ($domListArray as $__key => $__val)	{
			$__itemHolder1 = $doc->createElement($__key);
			$__itemHolder1->appendChild($doc->createTextNode($__val));
			$__parentNode1->appendChild($__itemHolder1);
		}
		return $__parentNode1;
	}
	public final static function updateExistingFile($filename, $doc)	{
		//Create a backup first
		//Input, the Updated DOMDocument 
		//This applies during deleting and editing 
		/*checksum begins here*/
		$doc = self::createDOMChecksum($doc);
		$promise1 = new Promise();
		$promise1->setPromise(true);
		if (is_null($doc))	{
			$promise1->setPromise(false);
			$promise1->setReason("Duplicate Checksum keys were found");
			return $promise1;
		}
		if ($promise1->isPromising())	{
			//Reset the promise Object 
			$promise1->setPromise(false);
			$doc->formatOutput = true;
			if ($doc->save($filename))	{
				$promise1->setPromise(true);
			} else {
				$promise1->setReason("Could not create update a file, perpahs the server policy or timeout has occured");
			}
		}
		return $promise1;
	}
	public final static function createBackupFile($filename)	{
		$promise1 = new Promise();
		if (copy($filename, $filename.".backup"))	{
			$promise1->setPromise(true); //successful
		} else {
			$promise1->setReason("Could not make a backup file");
		}
		return $promise1;
	}
	public final static function createFile($filename, $rootnodename)	{
		$doc=new DOMDocument(Object::$xmlVersion);
		$doc->formatOutput = true;
		$doc->appendChild($doc->createElement($rootnodename));
		$promise1 = new Promise();
		if ($doc->save($filename))	{
			$promise1->setPromise(true);
		} else {
			$promise1->setReason("Could not create a file, perhaps you do not have permission to this server");
		}
		return $promise1;
	}
	public final static function readFile($filename)	{
		//return xmlDocumenRoot or null 
		$doc = null;
		if (file_exists($filename))	{
			$doc = new DOMDocument();
			$doc->load($filename);
		}
		return $doc;
	}
	public final static function findNodes($doc, $nodename, $pos)	{
		/*
		doc refer to the outer node 
		if pos = *, you will get all matched nodes from the outer node 
		else specific position
		*/
		$pos = trim("".$pos);
		$nodeList = array();
		$tempList = $doc->getElementsByTagName($nodename);
		if ($pos == "*")	{
			//All records 
			foreach ($tempList as $alist)	{
				$nodeList[sizeof($nodeList)] = $alist;
			}
		} else {
			//Specific position 
			$pos = intval($pos);
			if ($pos < intval($tempList->length))	{
				$nodeList[sizeof($nodeList)] = $tempList->item($pos);
			}
		}
		if (sizeof($nodeList) == 0) $nodeList = null;
		return $nodeList;
	}
	public final static function deleteWithoutSavingNodeCollections($filename, $doc, $collectionToDelete)	{
		/*
		INPUT filename, DOMDocument and collectionToBe Deleted 
		OUTPUT promise Object 
		*/
		$promise1 = new Promise();
		$enableUpdate = false;
		if (is_null($collectionToDelete))	{
			$promise1->setReason("Collection to delete were not found");
			return $promise1;
		}
		foreach ($collectionToDelete as $anode1)	{
			$parent1 = $anode1->parentNode;
			if (! is_null($parent1))	{
				$parent1->removeChild($anode1); $enableUpdate=true;
			}
		}
		if ($enableUpdate)	{
			$promise1->setPromise(true);
		} else {
			$promise1->setReason("There were Nothing to Delete from the Collection");
		}
		return $promise1;
	}
	public final static function findResultsWhichMatches($doc, $dataArray, $pos)	{
		/*
		INPUT: active doc , dataArray[nodeName] = value, empty is presented as "_@32767@_"
							return list[nodeName] = value or null
							if all in dataArray are empty then use position to return 
						
							All of the returned nodes should be siblings , must have a common parent 
									
		*/
		if (is_null($doc)) return null;
		if (is_null($dataArray)) return null;
		$list = array();
		//User the firstIndex just to begin with 
		reset($dataArray);
		$tagName = key($dataArray);
		$nodeList = $doc->getElementsByTagName($tagName);
		if (is_null($nodeList)) return null;
		$allAreEmpty = true;
		foreach ($nodeList as $anode1)	{
			$listsize = sizeof($list);
			$list[$listsize] = array();
			$parent1 = $anode1->parentNode;
			if (is_null($parent1)) return null;
			$blnmatch = true;
			$isempty = true;
			$isvalid = true;
			foreach ($dataArray as $nodeName => $suppliedValue)	{
				$valueFound = "";
				$blnNodeFound = false;
				/*foreach ($parent1->childNodes as $child1)	{
					//Now All siblings are here 
					$domNodeName = $child1->nodeName;
					$domNodeValue = $child1->nodeValue;
					//echo "<br/>Supplied [Name, Value] = [$nodeName , $suppliedValue]; Stored [Name, Value] = [$domNodeName , $domNodeValue]";
					if ($nodeName == $domNodeName)	{
						if ($suppliedValue != "_@32767@_")	$isempty = false; //User filled nothing
						if (($suppliedValue == "_@32767@_") || (trim($suppliedValue) == trim($domNodeValue)))	{
							$valueFound = $domNodeValue;
							$blnNodeFound = true;
						}
						break;
					}
				}*/
				//Try Alternative -- Begin 
				$tempNode = $parent1->getElementsByTagName($nodeName);
				if ($tempNode->length > 0)	{
					$domNodeValue = $tempNode->item(0)->nodeValue;
					if ($suppliedValue != "_@32767@_")	$isempty = false; //User filled nothing
					if (($suppliedValue == "_@32767@_") || (trim($suppliedValue) == trim($domNodeValue)))	{
						$valueFound = $domNodeValue;
						$blnNodeFound = true;
					}
				}
				//Try Alternative -- Ends
				if ($blnNodeFound) $list[$listsize][$nodeName] = $valueFound;
				$isvalid = $isvalid && $blnNodeFound;
			}
			$list[$listsize]['empty'] = $isempty;
			$allAreEmpty = $allAreEmpty && $isempty;
			$list[$listsize]['valid'] = $isvalid;
		}
		//Now we have 
		/*
		list in the form of list[i][nodename] = value 
		Now we need to pick the best one 
		*/
		$listToReturn = array();
		if ($allAreEmpty)	{
			//use position 
			if ($pos < sizeof($list))	{
				$listToReturn = $list[$pos];
			}
		}	else {
			//Pick the first one which is valid 
			foreach ($list as $alist)	{
				if ($alist['valid']) $listToReturn = $alist;
			}
		}
		$list = $listToReturn;
		if (sizeof($list) == 0) $list = null;
		if (! is_null($list)) {
			$listToReturn = array(); 
			//Write Only columns available in the original dataArray 
			foreach ($dataArray as $nodeName => $suppliedValue)	{
				if (isset($list[$nodeName]))	{
					$listToReturn[$nodeName] = $list[$nodeName];
				}
			}
		}
		$list = $listToReturn;
		if (sizeof($list) == 0) $list = null;
		return $list;
	}
	public final static function deleteNodeCollections($filename, $doc, $collectionToDelete)	{
		/*
		INPUT filename, DOMDocument and collectionToBe Deleted 
		OUTPUT promise Object 
		*/
		$promise1 = new Promise();
		$enableUpdate = false;
		if (is_null($collectionToDelete))	{
			$promise1->setReason("Collection to delete were not found");
			return $promise1;
		}
		foreach ($collectionToDelete as $anode1)	{
			$parent1 = $anode1->parentNode;
			if (! is_null($parent1))	{
				$parent1->removeChild($anode1); $enableUpdate=true;
			}
		}
		if ($enableUpdate)	{
			$promise1 = self::updateExistingFile($filename, $doc);
		} else {
			$promise1->setReason("There is nothing to delete from the collection");
		}
		return $promise1;
	}
	public final static function addNodeAfterCollections($filename, $doc, $referenceCollection, $nodeToAdd)	{
		/*
		The node should have been created with the same doc Object
		*/
		$promise1 = new Promise();
		if (is_null($referenceCollection))	{
			$promise1->setReason("Reference Collection is Empty");
			return $promise1;
		}
 		$enableUpdate = false;
		foreach ($referenceCollection as $anode1)	{
			$parent1 = $anode1->parentNode;
			if (! is_null($parent1))	{
				$clonedNode = $nodeToAdd->cloneNode(true);
				$parent1->appendChild($clonedNode); $enableUpdate = true;
			}
		}
		if ($enableUpdate)	{
			$promise1 = self::updateExistingFile($filename, $doc);
		} else {
			$promise1->setReason("There is nothing to Append in the NodeTree");
		}
		return $promise1;
	}
	public final static function addNodeBeforeCollections($filename, $doc, $referenceCollection, $nodeToAdd)	{
		/*
		The node should have been created with the same doc Object
		*/
		$promise1 = new Promise();
		if (is_null($referenceCollection))	{
			$promise1->setReason("Reference Collection is Empty");
			return $promise1;
		}
		$enableUpdate = false;
		foreach ($referenceCollection as $anode1)	{
			$parent1 = $anode1->parentNode;
			if (! is_null($parent1))	{
				$clonedNode = $nodeToAdd->cloneNode(true);
				$parent1->insertBefore($clonedNode, $anode1); $enableUpdate = true;
			}
		}
		if ($enableUpdate)	{
			$promise1 = self::updateExistingFile($filename, $doc);
		} else {
			$promise1->setReason("There is nothing to Append in the NodeTree");
		}
		return $promise1;
	}
	public final static function appendNode($filename, $doc, $referenceCollection, $nodeToAdd)	{
		/*
		if pos is * append to All Matched nodes 
		nodeToAppend is an xml node, create a backup 
		*/
		$promise1 = new Promise();
		if (is_null($referenceCollection))	{
			$promise1->setReason("Reference Collection is Empty");
			return $promise1;
		}
 		$enableUpdate = false;
		foreach ($referenceCollection as $anode1)	{
			$clonedNode = $nodeToAdd->cloneNode(true);
			$anode1->appendChild($clonedNode); $enableUpdate = true;
			
		}
		if ($enableUpdate)	{
			$promise1 = self::updateExistingFile($filename, $doc);
		} else {
			$promise1->setReason("There is nothing to Append in the NodeTree");
		}
		return $promise1;
	}
	public final static function replaceContentOfNodeCollectionsWithoutSaving($filename, $doc, $referenceCollection, $contentText)	{
		/*
		if pos is * replace all matched nodes
		newNode is a new XML Node , create a backup
		NodeToReplace should be a member created by the same DOMDocument ($doc)
		*/
		$promise1 = new Promise();
		if (is_null($referenceCollection))	{
			$promise1->setReason("Reference Collection is Empty");
			return $promise1;
		}
		$enableUpdate = false;
		foreach ($referenceCollection as $anode1)	{
			if ($anode1->hasChildNodes())	{
				//Clear All Childres 
				foreach ($anode1->childNodes as $child1)	{
					$anode1->removeChild($child1);
				}
			}
			//Now Add the content Node 
			$anode1->appendChild($doc->createTextNode($contentText)); $enableUpdate=true;
		}
		if (! $enableUpdate) $promise1->setReason("There is nothing to Replace in the Collection");
		return $promise1;
	}
	public final static function replaceContentOfNodeCollections($filename, $doc, $referenceCollection, $contentText)	{
		/*
		if pos is * replace all matched nodes
		newNode is a new XML Node , create a backup
		NodeToReplace should be a member created by the same DOMDocument ($doc)
		*/
		$promise1 = new Promise();
		if (is_null($referenceCollection))	{
			$promise1->setReason("Reference Collection is Empty");
			return $promise1;
		}
		$enableUpdate = false;
		foreach ($referenceCollection as $anode1)	{
			if ($anode1->hasChildNodes())	{
				//Clear All Childres 
				foreach ($anode1->childNodes as $child1)	{
					$anode1->removeChild($child1);
				}
			}
			//Now Add the content Node 
			$anode1->appendChild($doc->createTextNode($contentText)); $enableUpdate=true;
		}
		if ($enableUpdate)	{
			$promise1 = self::updateExistingFile($filename, $doc);
		} else {
			$promise1->setReason("There is nothing to Replace in the Collection");
		}
		return $promise1;
	}
	public final static function replaceNodeCollections($filename, $doc, $referenceCollection, $nodeToReplace)	{
		/*
		if pos is * replace all matched nodes
		newNode is a new XML Node , create a backup
		NodeToReplace should be a member created by the same DOMDocument ($doc)
		*/
		$promise1 = new Promise();
		if (is_null($referenceCollection))	{
			$promise1->setReason("Reference Collection is Empty");
			return $promise1;
		}
		$enableUpdate = false;
		foreach ($referenceCollection as $anode1)	{
			$parent1 = $anode1->parentNode;
			if (! is_null($parent1))	{
				$clonedNode = $nodeToReplace->cloneNode(true);
				$parent1->replaceChild($clonedNode, $anode1);
				$enableUpdate = true;
			}
		}
		if ($enableUpdate)	{
			$promise1 = self::updateExistingFile($filename, $doc);
		} else {
			$promise1->setReason("There is nothing to Replace in the Collection");
		}
		return $promise1;
	}
	public final static function getListOfNodesWithValueFromCollection($listOfAllNodes, $pos, $value, $iscasesensitive)	{
		if (is_null($listOfAllNodes)) return null;
		$pos = trim("".$pos);
		$listOfMatchedNodes = array();
		foreach ($listOfAllNodes as $anode1)	{
			//anode1 is doc 
			$nodeValue = $anode1->nodeValue;
			//Lower value equivalent
			$ivalue = strtolower($value);
			$inodeValue = strtolower($nodeValue);
			//Expression B + !A!BC 
			$logic_A = $iscasesensitive;
			$logic_B = ($value == $nodeValue);
			$logic_C = ($ivalue == $inodeValue);
			if ($logic_B || (! $logic_A)&& (! $logic_B) && $logic_C)	{
				//Add to matched list 
				$listOfMatchedNodes[sizeof($listOfMatchedNodes)] = $anode1;
			}
		}
		/*
		We need to filter Accoring to the matched Nodes 
		*/
		if ($pos == "*")	{
			//What is matched is Alrite 
		} else {
			$pos = intval($pos);
			if ($pos < sizeof($listOfMatchedNodes))	{
				$tempVal = $listOfMatchedNodes[$pos];
				$listOfMatchedNodes = array();
				$listOfMatchedNodes[0] = $tempVal;
			} else {
				$listOfMatchedNodes = array(); //clear array
			}
		}
		/* Return now the results */
		if (sizeof($listOfMatchedNodes) == 0) $listOfMatchedNodes = null;
		return $listOfMatchedNodes;
	}
	private final static function convertCustomStringToArray($customString1)	{
		/*
		INPUT 0:nodename1;3:nodename2;*:nodename3 
		OUTPUT array[0][0]=0
				array[0][1]=nodename1 
				array[1][0]=3
				
				so nodename1 is/are a parent of nodename2 which is/are parent of nodename3 
		*/
		$customArray = array();
		$nodeListArray = explode(";", $customString1);
		foreach ($nodeListArray as $aListArray)	{
			//0:nodename1 
			$listsize = sizeof($customArray);
			$customArray[$listsize] = array();
			$splitNodesArray = explode(":", $aListArray);
			if (sizeof($splitNodesArray) != 2) return null;
			$customArray[$listsize][0] = $splitNodesArray[0]; 
			$customArray[$listsize][1] = $splitNodesArray[1]; 
		}
		return $customArray;
	}
	private final static function packAllCustomNodes($doc, $nodeCollectionArray, $customArray, $currentArrayPosition)	{
		//Terminating Condition
		if (is_null($customArray)) return null;
		if (($currentArrayPosition + 1)> sizeof($customArray)) return $nodeCollectionArray;
		if (($currentArrayPosition != 0) && (sizeof($nodeCollectionArray) == 0)) return null;
		//Fetch the position and the nodename 
		$pos = trim("".$customArray[$currentArrayPosition][0]);
		$nodename = trim($customArray[$currentArrayPosition][1]);
		//Initially the nodeCollectionArray would be null
		if ($currentArrayPosition == 0)	{
			$nodeCollectionArray = array();
			$tempList = $doc->getElementsByTagName($nodename);
			if ($tempList->length == 0) return null;
			if ($pos == "*")	{
				//All records 
				foreach ($tempList as $alist)	{
					$nodeCollectionArray[sizeof($nodeCollectionArray)] = $alist;
				}
			} else {
				//Specific position 
				$pos = intval($pos);
				if ($pos < intval($tempList->length))	{
					$nodeCollectionArray[sizeof($nodeCollectionArray)] = $tempList->item($pos);
				}
			}
			return self::packAllCustomNodes($doc, $nodeCollectionArray, $customArray, $currentArrayPosition + 1);
		} //end-if-pos-zero
		/*Proceed Now with the list */
		$tempNodeCollection = array();
		foreach ($nodeCollectionArray as $node1)	{
			$tempList = $node1->getElementsByTagName($nodename);
			if ($tempList->length == 0) return null;
			if ($pos == "*")	{
				//All records 
				foreach ($tempList as $alist)	{
					$tempNodeCollection[sizeof($tempNodeCollection)] = $alist;
				}
			} else {
				//Specific position 
				$pos = intval($pos);
				if ($pos < intval($tempList->length))	{
					$tempNodeCollection[sizeof($tempNodeCollection)] = $tempList->item($pos);
				}
			}
		}//end-foreach-anode 
		return self::packAllCustomNodes($doc, $tempNodeCollection, $customArray, $currentArrayPosition + 1);
	}
	public final static function findCustomNodes($doc, $customExpression)	{
		/*
		Input: pos:nodename;pos:nodename.....;pos:nodename
		*/
		$nodeListArray = self::convertCustomStringToArray($customExpression);
		$nodeListArray = self::packAllCustomNodes($doc, null, $nodeListArray, 0);
		if (sizeof($nodeListArray) == 0) $nodeListArray = null;
		return $nodeListArray;
	}
	public final static function findParentOfNode($doc, $refnodename, $pos)	{
		/*
			return xml parent or null 
			parent of pos-th node 
			pack this in an array too
		*/
		$pos = trim("".$pos);
		if ($pos == "*") return null; //does not allow all, should be specific 
		$nodeList = self::findNodes($doc, $refnodename, $pos);
		if (is_null($nodeList)) return null;
		// For compliance , pack in array
		$parentArray = array();
		$parentArray[0] = $nodeList[0]->parentNode;
		return $parentArray;
	}
	public final static function findParentOfCustomNode($doc, $customExpression)	{
		$nodeList = self::findCustomNodes($doc, $customExpression);
		if (is_null($nodeList)) return null;
		//Return ONLY the parent of the first march 
		$parentArray = array();
		$parentArray[0] = $nodeList[0]->parentNode;
		return $parentArray;
	}
	public final static function calculateFileChecksum($filename)	{
		/*
		isaacompletechecksum
		*/
		$doc = new DOMDocument(Object::$xmlVersion);
		$doc->load($filename);
		return self::calculateDOMChecksum($doc);
	}
	public final static function calculateDOMChecksum($doc)	{
		$referenceHash = md5(Object::$hashText);
		return self::documentHash($doc, $referenceHash);
	}
	public final static function createDOMChecksum($doc)	{
		/*
		INPUT A New DOMDocument, can have checksum or not 
		OUTPUT Return a DOMDocument with a valid checksum , same original DOM 
		*/
		$checksumvalue = self::calculateDOMChecksum($doc);
		$rootElement = $doc->documentElement;
		$checksum = $rootElement->getElementsByTagName('isaacompletechecksum');
		if (intval($checksum->length) == 0)	{
			//Not Existing
			$checksum = $doc->createElement('isaacompletechecksum');
			$checksum->appendChild($doc->createTextNode($checksumvalue));
			$rootElement->appendChild($checksum);
		} else if (intval($checksum->length) == 1)	{
			$checksum->item(0)->nodeValue = $checksumvalue;
		} else {
			//Incase of duplicate destroy the DOM 
			$doc = null;
		}
		return $doc;
	}
	public final static function getFileChecksum($filename)	{
		/*
		return null or read from isaacompletechecksum
		*/
		if (! file_exists($filename)) return null;
		$doc = new DOMDocument(Object::$xmlVersion);
		$doc->load($filename);
		return self::getDOMChecksum($doc);
	}
	public final static function getDOMChecksum($doc)	{
		$checksumvalue = null;
		$checksum = $doc->getElementsByTagName('isaacompletechecksum');
		if (intval($checksum->length) == 1)	{
			//Must be a unique existence 
			$checksumvalue = $checksum->item(0)->nodeValue; 
		}
		return $checksumvalue;
	}
	public final static function isDOMIntegrityPassed($doc)	{
		$storedDocumentHashValue = trim(self::getDOMChecksum($doc));
		$calculatedDocumentHashValue = trim(self::calculateDOMChecksum($doc));
		return (strcmp($calculatedDocumentHashValue, $storedDocumentHashValue) == 0);
	}
	public final static function isFileIntegrityPassed($filename)	{
		/*
		return true or false compare calculated and from get 
		*/
		//Load DOM Once 
		if (! file_exists($filename)) return false;
		$doc = new DOMDocument(Object::$xmlVersion);
		$doc->load($filename);
		return self::isDOMIntegrityPassed($doc);
	}
	private static final function documentHash($node1, $referenceHash)	{
		$hashCode = $referenceHash;
		$applyLock = false;
		//Pre Order for Non Binary Tree 
		//Deal with this Node first 
		if ($node1->nodeType == XML_ELEMENT_NODE)	{
			if (ctype_space($node1->tagName))	{
				//retain the value of hashCode, same change not 
			} else if ($node1->tagName == 'isaacompletechecksum')	{
				//Make sure the value of a checksum is not part of the calculation 
				//This is to avoid recursive logic which will not end, logic error avoidance
				$applyLock = true;
			} else	{
				//Other Nodes 
				$val = trim($node1->tagName);
				$hashCode = $hashCode.$val;
				$hashCode=md5($hashCode);
			}
		} else if ($node1->nodeType == XML_TEXT_NODE)	{
			$val = trim($node1->nodeValue);
			$hashCode = $hashCode.$val;
			$hashCode = md5($hashCode);
		}
		//We are done with this Node, Check if child nodes present 
		if (! $applyLock && $node1->hasChildNodes())	{
			foreach ($node1->childNodes as $child1)	{
				$hashCode = self::documentHash($child1, $hashCode);
			}
		}
		return $hashCode;
	}
	public final static function restoreFile($filename)	{
		/*
		restore from a .backup file
		*/
		$promise1 = new Promise();
		$backupfile = $filename.".backup";
		if (file_exists($backupfile))	{
			if (copy($backupfile, $filename))	{
				$promise1->setPromise(true);
			} else {
				$promise1->setReason("Could not create a backup file");
			}			
		} else {
			$promise1->setReason("There is no backup file for supplied file");
		}		
		return $promise1;
	}
}
?>