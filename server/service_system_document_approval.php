<?php 
/*
INPUT: param1 [loginId], param2 [jobId], param3 [schemaId]

OUTPUT: json code, message 
*/
if (session_status() == PHP_SESSION_NONE)	{
	session_start();
}
if (! isset($_SESSION['login'][0]['id']))	{
	die(json_encode(array("code"=>"1","message"=>"You are not Logged In to the System")));
}
require_once("../class/system.php");
require_once("../server/authorization.php");
require_once("../server/accounting.php");
$config="../config.php";
include($config);
if (! (isset($_POST['param1']) && isset($_POST['param2']) && isset($_POST['param3']) && isset($_POST['param4']) && isset($_POST['param6']) && isset($_POST['param7']) &&  isset($_POST['param8']) && isset($_POST['param9']) && isset($_POST['param10']) && isset($_POST['param11']))) die(json_encode(array("code"=>"1","message"=>"Perhaps data loss in transit")));
$tLoginId = $_POST['param1'];
$tJobId = $_POST['param2'];
$tSchemaId = $_POST['param3'];
$__time = $_POST['param4'];
$dataFolder = "../data/document/"; //Was supposed to be carried by param5 but we think this is more save 
$documentClassName = $_POST['param6']; //className ie PurchasingOrder
$documentReferenceId = $_POST['param7'];
$optionalStoreToAdd = null; if (trim($_POST['param8']) != Document::$__NULL_STORE) $optionalStoreToAdd = $_POST['param8'];
$optionalStoreToRemove = null; if (trim($_POST['param9']) != Document::$__NULL_STORE) $optionalStoreToRemove = $_POST['param9'];
$documentFolder = $dataFolder.$_POST['param10']."/";  //results ../data/document/purchasingOrder/
$storageId = $_POST['param11'];
$conn = mysql_connect($hostname, $user, $pass) or die(json_encode(array("code"=>"1","message"=>"Connection to data services failed")));
$ulogin1 = null;
try {
	$ulogin1 = new Login($database, $_SESSION['login'][0]['id'], $conn);
	if ($ulogin1->getLoginId() != $tLoginId) Object::shootException("You are not the Authorized Account to perform Approval");
	$document1 = ClassRegistry::getObjectReference($database, $conn, $documentClassName, $documentReferenceId); //ie PurchasingOrder
	$document1->putData('storageId', $storageId);
	//We can only sign non-stocked documents
	if ($document1->isStocked()) Object::shootException("Document stocking had already completed");
	//Check if Elligible for Approval 
	if (is_null($ulogin1->getJobTitle())) Object::shootException("Could not get the reference Job Title for the current logged user");
	if (is_null($ulogin1->getGroup())) Object::shootException("Could not get the reference Group for the current logged user");
	if (is_null($document1->getNextSchemaToApprove())) Object::shootException("Could not get the Next Schema to Approve");
	if (is_null($document1->getNextSchemaToApprove()->getJobTitle())) Object::shootException("Could not get the Next Schema Job reference to Approve");
	if ($document1->getNextSchemaToApprove()->getJobTitle()->getJobId() != $ulogin1->getJobTitle()->getJobId()) Object::shootException("You are not Authorized to Approve at this time");
	//Make Tree  and fill necessary data
	$filename = $documentFolder.$document1->getFilename();
	$tree1 = NodeTree::loadFromFile($filename);
	//create document you need document type and reference 
	$blockArray1 = array();
	$blockArray1['signedBy'] = array();
	$blockArray1['signedBy']['loginId'] = $ulogin1->getLoginId();
	$blockArray1['signedBy']['fullname'] = $ulogin1->getFullname();
	$blockArray1['signedBy']['approvalTime'] = $__time;
	$blockArray1['signedBy']['jobId'] = $ulogin1->getJobTitle()->getJobId();
	$blockArray1['signedBy']['jobName'] = $ulogin1->getJobTitle()->getJobName();
	$blockArray1['signedBy']['groupId'] = $ulogin1->getGroup()->getGroupId();
	$blockArray1['signedBy']['groupName'] = $ulogin1->getGroup()->getGroupName();
	$blockArray1['signedBy']['schema'] = array();
	$blockArray1['signedBy']['schema']['schemaId'] = $document1->getNextSchemaToApprove()->getSchemaId();
	$blockArray1['signedBy']['schema']['schemaName'] = $document1->getNextSchemaToApprove()->getCaptionText();
	$blockArray1['signedBy']['schema']['captionText'] = $document1->getNextSchemaToApprove()->getCaptionText();
	$tree1 = $tree1->appendNodeBlock(NodeTree::findCustomNodes($tree1, "0:documentApproval"), $blockArray1);
	//Update nextSchemaToApproval and Add this loginAccount to AlreadyApproved List 
	$loginIdToAttach = array();
	$loginIdToAttach[0] = $ulogin1->getLoginId();
	$listToUpdate = Object::attachIdListToObjectList($document1->getAlreadyApprovedList(), $loginIdToAttach);
	$document1->setAlreadyApprovedList($listToUpdate);
	if ($document1->isDocumentApprovalDone())	$document1->setStocked("1"); //You have completed you need to stock 
	if (! $document1->isStocked())	{
		//Still we have a room 
		$nextSchemaToApproval1 = $document1->getANewSchemaToApprove();
		if (is_null($nextSchemaToApproval1)) Object::shootException("Could not identify the next schema to Approve");
		$document1->setNextSchemaToApprove($nextSchemaToApproval1->getSchemaId());
	}
	//Perform necessary store de-stock and stock  -- already saved
	if ($document1->isStocked())	{
		//Perform this only if status has changed to stocked -- And Permanently have been changed
		if (! is_null($optionalStoreToAdd)) ClassRegistry::stock($database, $conn, $optionalStoreToAdd, $document1, $tree1);
		if (! is_null($optionalStoreToRemove)) ClassRegistry::destock($database, $conn, $optionalStoreToRemove, $document1, $tree1);
		//We need to record time too 
		$document1->setClosingTime($__time);
	}
	//Commit Tree 
	$document1->setFilenameHash($tree1->save());
	//Commit Document 
	$document1->commitUpdate();
} catch (Exception $e)	{
	$message = $e->getMessage();
	mysql_close($conn);
	die(json_encode(array("code"=>"1","message"=>$message)));
}
mysql_close($conn);
echo json_encode(array("code"=>"0","message"=>"You have successful approved the document"));
?>