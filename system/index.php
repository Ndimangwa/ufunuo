<?php 
if (session_status() == PHP_SESSION_NONE)	{
	session_start();
}
if (! isset($_SESSION['login'][0]['id']))	{
	header("Location: ../");
	exit();
}
$config="../config.php";
include($config);
require_once("../common/validation.php");
require_once("../class/system.php");
require_once("../server/authorization.php");
require_once("../server/accounting.php");
$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$profile1 = null;
$login1 = null;
$registeredUser1 = null;
$__profileId = Profile::getProfileReference($database, $conn);
try {
	$profile1 = new Profile($database, $__profileId , $conn);
	$login1 = new Login($database, $_SESSION['login'][0]['id'], $conn);
	$profile1->loadXMLFolder("../data/profile");
	$registeredUser1 = Login::getUserWithThisLogin($database, $conn, $login1);
	if (is_null($registeredUser1)) Object::shootException("Could not pull this user type");
} catch (Exception $e)	{
	//Clear Session 
	$_SESSION = array();
	session_destroy();
	$logoutLink = "<a class=\"ui-sys-logout-control bypass-data-error-control\" data-error-control=\"__ui_common_errors\" data-next-page=\"../\" data-server-directory=\"../server\" title=\"Logout\" href=\"../\">Proceed to Logout</a>";
	echo "<br/>User Pulling failed ".$e->getMessage();
	die("<br/>$logoutLink");
}
mysql_close($conn);
//Will redirect Automatically If Necessary
System::systemSSLTLSCertificateVerification($profile1);
$__login_extra_filter = $login1->getExtraFilter();
if (! $profile1->isInstallationComplete())	{
	header("Location: ../installation/");
	exit();
}
$thispage = $_SERVER['PHP_SELF'];
$page = null;
if (isset($_REQUEST['page'])) $page = $_REQUEST['page'];
//Awaiting for my Approval
$__request_awaiting_my_approval_searchtext = "________!____@atr_____j_____i____";
if (($page == "request_awaiting_my_approval") && isset($_REQUEST['searchtext'])) $__request_awaiting_my_approval_searchtext = $_REQUEST['searchtext'];
//You may use extraFilter to store path of the photo , user the setExtraFilter of login
$__user_data_folder = "../data/unknown/";
$__user_photo_prefix = "default";
//The user type we have is only one , the users 
$login1->setExtraFilter("../data/users/photo/".$login1->getPhoto());
$__user_data_folder = "../data/users/";
$__user_photo_prefix = "user";
$dataFolder = "data";
$themeFolder = "sunny"; //--set this as a default 
if (! is_null($profile1->getTheme())) $themeFolder = $profile1->getTheme()->getThemeFolder();
if (! is_null($login1->getTheme())) $themeFolder = $login1->getTheme()->getThemeFolder();
//We Just need to get A Default Timezone so we can extra a year
$timezone="Africa/Dar_es_Salaam";
if (! is_null($profile1->getPHPTimezone())) $timezone = $profile1->getPHPTimezone()->getZoneName();
date_default_timezone_set($timezone);
$date=date("Y:m:d:H:i:s");
$__systemDayOffset = 0;
if (! is_null($profile1->getFirstDayOfAWeek()))	{
	$__systemDayOffset = $profile1->getFirstDayOfAWeek()->getOffsetValue();
}
if (! is_null($login1->getFirstDayOfAWeek()))	{
	$__systemDayOffset = $login1->getFirstDayOfAWeek()->getOffsetValue();
}
$systemDate1 = new DateAndTime("Ndimangwa", $date, "Fadhili");
$systemTime1 = $systemDate1;
//systemMenus 
$sysmenu = SystemMenu::$__DASHBOARD;
if (isset($_REQUEST['sysmenu'])) $sysmenu = $_REQUEST['sysmenu'];
?>
<! DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must
come *after* these tags -->
<title><?= $profile1->getProfileName() ?></title>
<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800"
rel="stylesheet">-->
<link rel="stylesheet" type="text/css" media="all" href="../client/jquery-ui-1.11.3/themes/<?= $themeFolder ?>/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/plugin/twbsPagination/twbsPagination.min.css"/>
<link href="../client/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="../client/assets/css/linearicons.css" rel="stylesheet">
<link href="../client/assets/css/simple-line-icons.css" rel="stylesheet">
<link href="../client/assets/css/ionicons.css" rel="stylesheet">
<link href="../client/assets/css/fakeLoader.css" rel="stylesheet">
<link href="../client/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="../client/assets/css/scoop-vertical.min.css" rel="stylesheet">
<link href="../client/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="../client/css/purecss/pure-min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/purecss/grids-responsive-min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/site.css"/>
<script src="../client/assets/js/jquery.1.11.3.min.js"></script>
<script src="../client/jquery-ui-1.11.3/jquery-ui.js"></script>
<script type="text/javascript" src="../client/plugin/twbsPagination/jquery.twbsPagination.min.js"></script>
<script src="../client/js/jvalidation.js"></script> 
<script src="../client/assets/js/lib/fakeLoader.js"></script>
<script src="../client/assets/js/bootstrap.min.js"></script>
<script src="../client/assets/js/scoop.min.js"></script>
<script src="../client/assets/js/demo-17.js"></script>
<script src="../client/assets/js/lib/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="../client/assets/js/lib/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="../client/js/page.js"></script>
<script>
	$(function() {
		/*Date Handling*/
		$('.datepicker').datepicker({
			dateFormat: 'dd/mm/yy',
			firstDay: <?= $__systemDayOffset ?>,
			changeYear: true,
			yearRange:'1932:2099'
		});
		//Now Proceed with menus adjustment 
		$('li.sysmenu').removeClass('active');
		$('li.menu_<?= $sysmenu ?>').addClass('active');
		//Initiate scoop fakeloader
		$("#fakeLoader").fakeLoader();
	});
</script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
	<div id="__id_general_dialog_holder">
		<!--Holding Popup dialogs, they shoud use absolute positioning -->
	</div>
	<div id="fakeLoader"></div>
	<div id="scoop" class="scoop">
		<div class="scoop-overlay-box"></div>
		<div class="scoop-container">
			<header class="scoop-header">
				<div class="scoop-wrapper">
					<div class="scoop-left-header">
						<div class="scoop-logo">
							<a href="#">
								<span class="logo-icon"><i class="ion-stats-bars"></i></span>
								<span class="logo-text"><?= $profile1->getProfileName() ?><span class="hide-in-smallsize"></span></span>
							</a>
						</div><!--End .scoop-logo-->
					</div> <!--End .scoop-left-header-->
					<div class="scoop-right-header">
						<div class="sidebar_toggle"><a href="javascript:void(0)"><i class="icon-menu"></i></a></div>	<!--End .sidebar_toggle-->
						<div class="scoop-rl-header">
							<ul>
								<li class="icons">
									<a href="javascript:void(0)"><i class="fa fa-envelope" aria-hidden="true"></i>
										<span class="scoop-badge badge-success">2</span>
									</a>
								</li>
								<li class="icons">
									<a href="javascript:void(0)"><i class="fa fa-bell" aria-hidden="true"></i>
										<span class="scoop-badge badge-danger">8</span>
									</a>
								</li>
								<li class="icons">
									<a href="javascript:void(0)"><i class="fa fa-tasks" aria-hidden="true"></i>
										<span class="scoop-badge badge-warning">8</span>
									</a>
								</li> 
								<li class="icons hide-small-device">
									<a href="javascript:void(0)">
										<i class="fa fa-rss" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>	<!-- End .scoop-rl-header-->
						<div class="scoop-rr-header">
							<ul>
								<li class="icons">
									<a href="javascript:void(0)">
										<i class="fa fa-user" aria-hidden="true"></i>
									</a>
								</li>
								<li class="icons">
									<a class="ui-sys-logout-control" data-error-control="__ui_common_errors" data-next-page="../" data-server-directory="../server" title="Logout <?= $login1->getFullname() ?>">
										<i class="fa fa-sign-out" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>	<!--End .scoop-rr-header-->
					</div>	<!--End .scoop-right-header-->
				</div> <!--End .scoop-wrapper-->
			</header> <!--.scoop-header-->
			<div class="scoop-main-container">
				<div class="scoop-wrapper">
					<nav class="scoop-navbar">
						<div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
						<div class="scoop-inner-navbar">
							<div class="scoop-search">
								<span class="searchbar-toggle">  </span>
								<div class="scoop-search-box "> 
									<input type="text" placeholder="Search">
									<span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
								</div>
							</div>	<!--End .scoop-search-->
							<ul class="scoop-item scoop-brand">
								<li class="sysmenu menu_<?= SystemMenu::$__DASHBOARD ?>">
									<a href="<?= $thispage ?>">
										<span class="scoop-micon"><i class="icon-speedometer"></i></span>
										<span class="scoop-mtext">DashBoard</span>
										<span class="scoop-mcaret"></span>
									</a>
								</li>
							</ul>
							<ul class="scoop-item scoop-left-item">
								<li class="sysmenu menu_<?= SystemMenu::$__MYPROFILE ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">My Profile</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-puzzle"></i></span>
												<span class="scoop-mtext">View My Profile</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class="scoop-hasmenu">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Edit My Profile</span>
												<span class="scoop-mcaret"></span>
											</a>
											<ul class="scoop-submenu">
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">My Name(s)</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Sex</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Marital Status</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Date of Birth</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Contacts</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Profile Photo</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
												<li class="scoop-hasmenu">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-puzzle"></i></span>
														<span class="scoop-mtext">Security</span>
														<span class="scoop-mcaret"></span>
													</a>
													<ul class="scoop-submenu">
														<li class=" ">
															<a href="javascript:void(0)">
																<span class="scoop-micon"><i class="icon-link"></i></span>
																<span class="scoop-mtext">Change Password</span>
																<span class="scoop-mcaret"></span>
															</a>
														</li>
														<li class=" ">
															<a href="javascript:void(0)">
																<span class="scoop-micon"><i class="icon-link"></i></span>
																<span class="scoop-mtext">Change Security Qn & Ans</span>
																<span class="scoop-mcaret"></span>
															</a>
														</li>
													</ul>
												</li>
												<li class="scoop-hasmenu">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-puzzle"></i></span>
														<span class="scoop-mtext">Personalize</span>
														<span class="scoop-mcaret"></span>
													</a>
													<ul class="scoop-submenu">
														<li class=" ">
															<a href="javascript:void(0)">
																<span class="scoop-micon"><i class="icon-link"></i></span>
																<span class="scoop-mtext">Change Theme</span>
																<span class="scoop-mcaret"></span>
															</a>
														</li>
														<li class=" ">
															<a href="javascript:void(0)">
																<span class="scoop-micon"><i class="icon-link"></i></span>
																<span class="scoop-mtext">Change Week Start</span>
																<span class="scoop-mcaret"></span>
															</a>
														</li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="scoop-item scoop-left-item">
								<li class="sysmenu menu_<?= SystemMenu::$__SETTINGS ?> scoop-hasmenu active">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Settings</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
<?php 
									if ($login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=system_reinstall&sysmenu=<?= SystemMenu::$__SETTINGS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Reinstall System</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="<?= $thispage ?>?page=system_certificate&sysmenu=<?= SystemMenu::$__SETTINGS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">SSL/TLS Certificate</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
									}
									if ($login1->isRoot() && Authorize::isAllowable($config, "managecontextmanager", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managecontextmanager&sysmenu=<?= SystemMenu::$__SETTINGS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Default Allow/Deny</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
									}
									if ($login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=system_record_limits&sysmenu=<?= SystemMenu::$__SETTINGS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Record Limits</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
									}
									if (Authorize::isAllowable($config, "managedocument", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managedocument&sysmenu=<?= SystemMenu::$__SETTINGS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">System Documents</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
									}
?>
										<li class="scoop-hasmenu">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-puzzle"></i></span>
												<span class="scoop-mtext">Document Approval</span>
												<span class="scoop-mcaret"></span>
											</a>
											<ul class="scoop-submenu">
<?php 
										if (Authorize::isAllowable($config, "managedocumentapprovalschema", "normal", "donotsetlog", "-1", "-1"))	{
?>
												<li class=" ">
													<a href="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Approval Schema</span>
														<span class="scoop-mcaret"></span>
													</a>						
												</li>
<?php 
										}
?>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Approval Progress</span>
														<span class="scoop-mcaret"></span>
													</a>						
												</li>
											</ul>
										</li>
										<li class="scoop-hasmenu">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-puzzle"></i></span>
												<span class="scoop-mtext">General Approval</span>
												<span class="scoop-mcaret"></span>
											</a>
											<ul class="scoop-submenu">
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Approval Schema</span>
														<span class="scoop-mcaret"></span>
													</a>						
												</li>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Approval Progress</span>
														<span class="scoop-mcaret"></span>
													</a>						
												</li>
											</ul>
										</li>
<?php 
									if (Authorize::isAllowable($config, "managesystemlogs", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managesystemlogs&sysmenu=<?= SystemMenu::$__SETTINGS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">System Logs</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
									}
?>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__USERS_GROUPS ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Users & Groups</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
<?php 
								if	(Authorize::isAllowable($config, "managegroup", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Groups</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "managejobtitle", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Job Title</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "managelogin", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">User Accounts</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
?>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__INVENTORYRESOURCES ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Inventory Resources</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
<?php 
								if (Authorize::isAllowable($config, "managemeasure", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Unit of Measurement</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "manageitemcategory", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Category of Raw Materials</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "manageitem", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Raw Materials</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "manageproductcategory", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Category of Finished Goods</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "manageproduct", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Finished Goods</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "managepaper", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Paper Management</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
?>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__LOCATION ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Location</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
<?php 
								if (Authorize::isAllowable($config, "managelocation", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Location</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "manageconference", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Conference</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "managedepartment", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Department</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
?>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__PROCUREMENT ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Procurement</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
<?php 
								if (Authorize::isAllowable($config, "managepurchasingorder", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Purchasing Order (Raw Material)</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
								if (Authorize::isAllowable($config, "managepurchasingorder", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Good Received Note (Raw Material)</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
?>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Purchasing Order (Product)</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Good Received Note (Product)</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__STORE ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Store</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Store Requisition</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Stock Requisition</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Issue Note</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Record Card</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								if (Authorize::isAllowable($config, "managephysicalstore", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Physical Store(s)</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
?>
										<li class="scoop-hasmenu">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-puzzle"></i></span>
												<span class="scoop-mtext">Actual Store Content</span>
												<span class="scoop-mcaret"></span>
											</a>
											<ul class="scoop-submenu">
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">Initialize Content</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
												<li class=" ">
													<a href="javascript:void(0)">
														<span class="scoop-micon"><i class="icon-link"></i></span>
														<span class="scoop-mtext">View Content</span>
														<span class="scoop-mcaret"></span>
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__PRODUCTION ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Production</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
<?php 
								if (Authorize::isAllowable($config, "managejobcostsheet", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Job Cost Sheet</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
?>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__WAREHOUSE ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Warehouse</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Record Card</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Finished Good Report</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Good Returned Note</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
									</ul>
								</li>
								<li class="sysmenu menu_<?= SystemMenu::$__FINANCE ?> scoop-hasmenu">
									<a href="javascript:void(0)">
										<span class="scoop-micon"><i class="icon-puzzle"></i></span>
										<span class="scoop-mtext">Finance</span>
										<!-- <span class="scoop-badge badge-success">2</span> -->
										<span class="scoop-mcaret"></span>
									</a>
									<ul class="scoop-submenu">
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Overhead Absorption Rate</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								if (Authorize::isAllowable($config, "managecurrency", "normal", "donotsetlog", "-1", "-1"))	{
?>
										<li class=" ">
											<a href="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Currency</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
<?php 
								}
?>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Currency Conversion</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Price Quote</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Invoice</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
										<li class=" ">
											<a href="javascript:void(0)">
												<span class="scoop-micon"><i class="icon-link"></i></span>
												<span class="scoop-mtext">Receipt</span>
												<span class="scoop-mcaret"></span>
											</a>
										</li>
									</ul>
								</li>
								</ul>
							<div class="scoop-navigatio-lavel">Multi Lavel</div>
							<ul class="scoop-item scoop-right-item"></ul>
						</div> <!-- .scoop-inner-navbar-->
					</nav>	<!--End .scoop-navbar-->
					<div class="scoop-content">
						<div class="scoop-inner-content">
<!--BEGIN: Business Logic will begin at this point -->
<?php 
/*BEGIN: JobCostSheetManagement*/if ($page == "managejobcostsheet_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managejobcostsheet_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobCostSheet CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Management</a></div>
					Rule: managejobcostsheet_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managejobcostsheet_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managejobcostsheet_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobCostSheet CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = JobCostSheet::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("JobCostSheet[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$job1 = null;
			try	{
				$job1 = new JobCostSheet($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($job1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managejobcostsheet_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managejobcostsheet_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managejobcostsheet_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managejobcostsheet_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobCostSheet CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managejobcostsheet_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PRODUCTION ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managejobcostsheet_csv&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managejobcostsheet_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managejobcostsheet_csv" && Authorize::isAllowable($config, "managejobcostsheet_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobCostSheet CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managejobcostsheet_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PRODUCTION ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, JobCostSheet::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Managegement</a>
					</div>
					Rule: managejobcostsheet_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managejobcostsheet_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobcostsheet_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$job1 = null;
	$captionText = "Unknown";
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		 $job1 = new JobCostSheet($database, $_REQUEST['id'], $conn);
		 $captionText = $job1->getJobName();
		 $job1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a JobCostSheet which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The JobCostSheet <?= $job1->getJobName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the JobCostSheet <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Management</a>
					</div>
					Rule: managejobcostsheet_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managejobcostsheet_delete", "Deleted ".$job1->getJobName());
} else if ($page == "managejobcostsheet_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobcostsheet_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$job1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$job1 = new JobCostSheet($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		mysql_close($conn);
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $job1->getJobName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a JobCostSheet <b><?= $job1->getJobName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the JobCostSheet <?= $job1->getJobName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managejobcostsheet_delete&id=<?= $job1->getJobId() ?>&report=yes&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managejobcostsheet_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managejobcostsheet_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managejobcostsheet_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobcostsheet_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$job1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$job1 = new JobCostSheet($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	
	if ($extraInformation != $job1->getExtraInformation())	{
		$job1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $job1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$job1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$job1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for JobCostSheet Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit JobCostSheet (Report) <i><?= $job1->getJobName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			JobCostSheet <?= $job1->getJobName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the job <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobcostsheet_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Details</a></div>
					Rule: managejobcostsheet_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managejobcostsheet_edit", "Edited ".$job1->getJobName());
} else if ($page == "managejobcostsheet_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobcostsheet_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$job1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$job1 = new JobCostSheet($database, $_REQUEST['id'], $conn);
		$job1->setExtraFilter($extraFilter);
		$job1->commitUpdate();
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit JobCostSheet <i><?= $job1->getJobName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managejobcostsheet_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $job1->getJobId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $job1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PRODUCTION ?>"/>
						<div class="pure-control-group">
							<label for="jobName">JobCostSheet Name </label>
							<input value="<?= $job1->getJobName() ?>" type="text" name="jobName" id="jobName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="JobCostSheet Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="pId">Parent JobCostSheet</label>
							<select id="pId" name="pId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Parent JobCostSheet">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = JobCostSheet::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected="";
		if ($job1->getJobId() == $alist['id']) continue;
		if ($alist['id'] == $job1->getParentJobCostSheet()->getJobId()) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $job1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobcostsheet_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Details</a></div>
					Rule: managejobcostsheet_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managejobcostsheet_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobcostsheet_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$job1 = null;
	try {
		$job1 = new JobCostSheet($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for job : <i><?= $job1->getJobNumber() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$documentToDisplay = "";
	try {
		$job1->putData('logoFolder', '../data/profile/logo/');
		$job1->putData('dataFolder', '../data/document/jobCostSheet/');
		$job1->putData('currentLoginInId', $login1->getLoginId());
		$job1->putData('systemTime', $systemTime1->getDateAndTimeString());
		$job1->putData('optionalStoreToAdd', 'ItemDemandStore');
		$job1->putData('storageId', $job1->getStore()->getStoreId());
		$documentToDisplay = $job1->getDocumentUI();
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	mysql_close($conn);
	if ($promise1->isPromising())	{
?>
					<div class="display-background-black">
						<!--START DOCUMENT DETAILS-->
<?php 
	echo $documentToDisplay;
?>						
						<!--STOP DOCUMENT DETAILS-->
					</div>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managejobcostsheet_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $job1->getJobNumber() ?>" href="<?= $thispage ?>?page=managejobcostsheet_edit&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if ( Authorize::isAllowable($config, "managejobcostsheet_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $job1->getJobNumber() ?>" href="<?= $thispage ?>?page=managejobcostsheet_delete&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
<?php 
	} else {
?>
		<div class="ui-sys-warning">
			There were problems in displaying document <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Management</a>
					</div>
					Rule: managejobcostsheet_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('button.button-sign-document').on('click', function(event)	{
		var pathOnSuccess = '<?= $thispage ?>?page=<?= $page ?>&id=<?= $job1->getId() ?>&sysmenu=<?= $_REQUEST['sysmenu'] ?>';
		window.approveDocument(this, 'jobCostSheet', pathOnSuccess, '__approval_error_control');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managejobcostsheet_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managejobcostsheet_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$orderNumber = mysql_real_escape_string($_REQUEST['orderNumber']);
		$customerName = mysql_real_escape_string($_REQUEST['customerName']);
		$customerEmail = mysql_real_escape_string($_REQUEST['customerEmail']);
		$customerAddress = mysql_real_escape_string($_REQUEST['customerAddress']);
		$customerPhone = mysql_real_escape_string($_REQUEST['customerPhone']);
		$productId = mysql_real_escape_string($_REQUEST['productId']);
		$quantity = mysql_real_escape_string($_REQUEST['quantity']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Create A New Job Cost Sheet (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$job1 = null;
	try {
		var_dump($_REQUEST);
		Object::shootException("It is just a testing time");
		$jobNumber = rand(1000, 999999999);
		$documentId = Document::getDocumentIdFromDocumentCode($database, $conn, Document::$__DOC_JOB_COST_SHEET);
		$jobId = JobCostSheet::addRecord($database, $conn, $jobNumber, $documentId, $storeId, $supplierName, $supplierAddress, $supplierPhone, $systemTime1, $currencyId, $extraFilter, $extraInformation, 0);
		//Create Now Object 
		$job1 = new JobCostSheet($database, $jobId, $conn);
		$jobNumber = Document::createReferenceNumber("PO-", 6, $jobId);
		$job1->setJobNumber($jobNumber);
		$document1 = $job1->getDocument();
		//First Approving Schema 
		$schema1 = $document1->getFirstSchemaToApprove();
		if (! is_null($schema1)) $job1->setNextSchemaToApprove($schema1->getSchemaId());
		if (is_null($document1)) Object::shootException("Failed to get a reference Document ");
		$filename = $document1->getDocumentClass()."".$job1->getJobId().".xml";
		$job1->setFilename($filename);
		$job1->setCompleted("1");
		//Approval Schema 
		$folderPath = "../data/document/jobCostSheet/";
		$filename = $folderPath.$filename;
		//Now create Job Cost Sheet 
		$listType = "Item";
		$controlFlags = 0;
		$tree1 = JobCostSheet::createJobCostSheet($database, $conn, $profile1, $job1->getJobId(), $itemArray1, $listType, $controlFlags);
		$tree1->setFilename($filename);
		$job1->setFilenameHash($tree1->save());
		$job1->commitUpdate();
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
		if (! is_null($job1))	{
			try {
				$job1->commitDelete();
			} catch (Exception $e)	{
				$promise1->setReason("Delete Resource ".$e->getMessage());
			}
		}
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The JobCostSheet <?= $job1->getJobNumber() ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the job<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another JobCostSheet";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managejobcostsheet_add&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Manager</a>
					</div>
					Rule: managejobcostsheet_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managejobcostsheet_add", "Added ".$job1->getJobNumber());
} else if ($page == "managejobcostsheet_add" && Authorize::isAllowable($config, "managejobcostsheet_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Creating A Job Cost Sheet</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="text-center ui-sys-warning">
						NOTE: You can not attach any Requisition after signing this document 
					</div>
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managejobcostsheet_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PRODUCTION ?>"/>
						<div class="pure-control-group">
							<label for="orderNumber">Customer Order Number </label>
							<input type="text" name="orderNumber" id="orderNumber" size="48" required pattern="<?= $exprL48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL48Name ?>" validate_message="Customer Order Number : <?= $msgL48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="customerName">Name of the Customer </label>
							<input type="text" name="customerName" id="customerName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Name of the Customer : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="customerEmail">Email of the Customer </label>
							<input type="text" name="customerEmail" id="customerEmail" size="48" required pattern="<?= $exprEmail ?>" validate="true" validate_control="text" validate_expression="<?= $exprEmail ?>" validate_message="Email of the Customer : <?= $msgEmail ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="customerAddress">Address of the Customer </label>
							<input type="text" name="customerAddress" id="customerAddress" size="48" required pattern="<?= $exprL64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Address of the Customer : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="customerPhone">Phone of the Customer </label>
							<input type="text" name="customerPhone" id="customerPhone" size="48" required pattern="<?= $exprPhone ?>" validate="true" validate_control="text" validate_expression="<?= $exprPhone ?>" validate_message="Phone of the Customer : <?= $msgPhone ?>"/>
						</div>
						<div title="This is the deadline of completing this Job" class="pure-control-group">
							<label for="wantedDate">Wanted Date </label>
							<input class="datepicker" type="text" name="wantedDate" id="wantedDate" size="16" required pattern="<?= $exprDate ?>" validate="true" validate_control="text" validate_expression="<?= $exprDate ?>" validate_message="Wanted Date : <?= $msgDate ?>"/>
						</div>
						<div  title="Product you want to produce" class="pure-control-group">
							<label for="productId">Type of Product</label>
							<select id="productId" name="productId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Product you want to produce">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Product::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div title="Quantity of Products you want to produce" class="pure-control-group">
							<label for="quantity">Production Quantity </label>
							<input type="text" name="quantity" id="quantity" size="16" required pattern="<?= $expr8Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr8Number ?>" validate_message="Production Quantity : <?= $msg8Number ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>">Back to JobCostSheet Management</a></div>
					Rule: managejobcostsheet_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managejobcostsheet" && Authorize::isAllowable($config, "managejobcostsheet", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobCostSheet Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-job-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managejobcostsheet&sysmenu=<?= SystemMenu::$__PRODUCTION ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managejobcostsheet_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th></th>
				<th>Job Number</th>
				<th>Product</th>
				<th>Created:Date</th>
				<th>Wanted:Date</th>
				<th>Closed:Date</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = JobCostSheet::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch job data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$job1 = null;
		try {
			$job1 = new JobCostSheet($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($job1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
			$creationTime = "";
			if (! is_null($job1->getCreationTime())) $creationTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($job1->getCreationTime());
			$wantedTime = "";
			if (! is_null($job1->getWantedTime())) $wantedTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($job1->getWantedTime());
			$closingTime = "";
			if (! is_null($job1->getClosingTime())) $closingTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($job1->getClosingTime());
			$productName = "";
			if (! is_null($job1->getProduct())) $productName = $job1->getProduct()->getProductName();
			$bgcolor = "blue";
			if ($job1->isStocked()) $bgcolor = "gold";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><span style="background-color: <?= $bgcolor ?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
				<td><?= $job1->getJobNumber() ?></td>
				<td><?= $productName ?></td>
				<td><?= $creationTime ?></td>
				<td><?= $wantedTime ?></td>
				<td><?= $closingTime ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managejobcostsheet_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__PRODUCTION ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managejobcostsheet_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managejobcostsheet_csv&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managejobcostsheet_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Create a New Job Cost Sheet" href="<?= $thispage ?>?page=managejobcostsheet_add&sysmenu=<?= SystemMenu::$__PRODUCTION ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managejobcostsheet</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: JobCostSheetManagement*/ else  /*BEGIN: GoodReceivedNoteManagement*/if ($page == "managegoodreceivednote_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a></div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managegoodreceivednote_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = GoodReceivedNote::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("GoodReceivedNote[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$note1 = null;
			try	{
				$note1 = new GoodReceivedNote($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($note1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managegoodreceivednote_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managegoodreceivednote_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managegoodreceivednote_csv&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegoodreceivednote_csv" && Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegoodreceivednote_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, GoodReceivedNote::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Managegement</a>
					</div>
					Rule: managegoodreceivednote_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegoodreceivednote_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$note1 = null;
	$captionText = "Unknown";
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		 $note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
		 $captionText = $note1->getNoteName();
		 $note1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a GoodReceivedNote which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The GoodReceivedNote <?= $note1->getNoteName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the GoodReceivedNote <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a>
					</div>
					Rule: managegoodreceivednote_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegoodreceivednote_delete", "Deleted ".$note1->getNoteName());
} else if ($page == "managegoodreceivednote_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$note1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		mysql_close($conn);
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $note1->getNoteName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a GoodReceivedNote <b><?= $note1->getNoteName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the GoodReceivedNote <?= $note1->getNoteName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegoodreceivednote_delete&id=<?= $note1->getNoteId() ?>&report=yes&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managegoodreceivednote_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$note1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	
	if ($extraInformation != $note1->getExtraInformation())	{
		$note1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $note1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$note1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$note1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for GoodReceivedNote Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit GoodReceivedNote (Report) <i><?= $note1->getNoteName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			GoodReceivedNote <?= $note1->getNoteName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the order <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Details</a></div>
					Rule: managegoodreceivednote_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegoodreceivednote_edit", "Edited ".$note1->getNoteName());
} else if ($page == "managegoodreceivednote_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$note1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
		$note1->setExtraFilter($extraFilter);
		$note1->commitUpdate();
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit GoodReceivedNote <i><?= $note1->getNoteName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegoodreceivednote_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $note1->getNoteId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $note1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
						<div class="pure-control-group">
							<label for="orderName">GoodReceivedNote Name </label>
							<input value="<?= $note1->getNoteName() ?>" type="text" name="orderName" id="orderName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="GoodReceivedNote Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="pId">Parent GoodReceivedNote</label>
							<select id="pId" name="pId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Parent GoodReceivedNote">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = GoodReceivedNote::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected="";
		if ($note1->getNoteId() == $alist['id']) continue;
		if ($alist['id'] == $note1->getParentGoodReceivedNote()->getNoteId()) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $note1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Details</a></div>
					Rule: managegoodreceivednote_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegoodreceivednote_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$note1 = null;
	try {
		$note1 = new GoodReceivedNote($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for GRN : <i><?= $note1->getNoteNumber() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$documentToDisplay = "";
	try {
		$note1->putData('logoFolder', '../data/profile/logo/');
		$note1->putData('dataFolder', '../data/document/goodReceivedNote/');
		$note1->putData('currentLoginInId', $login1->getLoginId());
		$note1->putData('systemTime', $systemTime1->getDateAndTimeString());
		$note1->putData('optionalStoreToRemove', 'ItemDemandStore');
		$note1->putData('optionalStoreToAdd', 'ItemStore');
		$note1->putData('storageId', $note1->getPurchasingOrder()->getStore()->getStoreId());
		$note1->putData('referencePurchasingOrder', $note1->getPurchasingOrder()->getOrderNumber());
		$documentToDisplay = $note1->getDocumentUI();
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	mysql_close($conn);
	if ($promise1->isPromising())	{
?>
					<div class="display-background-black">
						<!--START DOCUMENT DETAILS-->
<?php 
	echo $documentToDisplay;
?>						
						<!--STOP DOCUMENT DETAILS-->
					</div>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managegoodreceivednote_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $note1->getNoteNumber() ?>" href="<?= $thispage ?>?page=managegoodreceivednote_edit&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if ( Authorize::isAllowable($config, "managegoodreceivednote_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $note1->getNoteNumber() ?>" href="<?= $thispage ?>?page=managegoodreceivednote_delete&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
<?php 
	} else {
?>
		<div class="ui-sys-warning">
			There were problems in displaying document <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a>
					</div>
					Rule: managegoodreceivednote_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('button.button-sign-document').on('click', function(event)	{
		var pathOnSuccess = '<?= $thispage ?>?page=<?= $page ?>&id=<?= $note1->getId() ?>&sysmenu=<?= $_REQUEST['sysmenu'] ?>';
		window.approveDocument(this, 'goodReceivedNote', pathOnSuccess, '__approval_error_control');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegoodreceivednote_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managegoodreceivednote_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$handlingCharges = mysql_real_escape_string($_REQUEST['hcharges']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Create A New Good Received Note (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$itemArray1 = Document::mergeUserDataBasic($_REQUEST['item'], $_REQUEST['quantity'], null, null, 0);
	$extraFilter = System::getCodeString(8);
	$note1 = null;
	try {
		//You need to fetch actual 
		//We need some information from the reference order 
		$order1 = new PurchasingOrder($database, $_REQUEST['orderId'], $conn);
		$dataFolder = "../data/document/purchasingOrder/";
		$filename = $dataFolder.$order1->getFilename();
		$purchasingOrderTree1 = NodeTree::loadFromFile($filename);
		$dataFolder = "../data/document/goodReceivedNote/";
		$itemArray1 = PurchasingOrder::getNewItemArray($database, $conn, $itemArray1, $dataFolder, $order1, $purchasingOrderTree1, 0);
		$noteNumber = rand(1000, 999999999);
		$documentId = Document::getDocumentIdFromDocumentCode($database, $conn, Document::$__DOC_RAW_GOOD_RECEIVED_NOTE);
		$noteId = GoodReceivedNote::addRecord($database, $conn, $noteNumber, $documentId, $systemTime1, $order1->getId(), $handlingCharges, $extraFilter, $extraInformation, 0);
		//Create Now Object 
		$note1 = new GoodReceivedNote($database, $noteId, $conn);
		$noteNumber = Document::createReferenceNumber("GRN-", 6, $noteId);
		$note1->setNoteNumber($noteNumber);
		$document1 = $note1->getDocument();
		//First Approving Schema 
		$schema1 = $document1->getFirstSchemaToApprove();
		if (! is_null($schema1)) $note1->setNextSchemaToApprove($schema1->getSchemaId());
		if (is_null($document1)) Object::shootException("Failed to get a reference Document ");
		$filename = $document1->getDocumentClass()."".$note1->getNoteId().".xml";
		$note1->setFilename($filename);
		$note1->setCompleted("1");
		//Approval Schema 
		$folderPath = "../data/document/goodReceivedNote/";
		$filename = $folderPath.$filename;
		//Now create Good Received Note 
		$listType = "Item";
		$controlFlags = 0;
		$tree1 = GoodReceivedNote::createGoodReceivedNote($database, $conn, $profile1, $note1->getNoteId(), $itemArray1, $listType, $controlFlags);
		$tree1->setFilename($filename);
		$note1->setFilenameHash($tree1->save());
		$note1->commitUpdate();
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
		if (! is_null($note1))	{
			try {
				$note1->commitDelete();
			} catch (Exception $e)	{
				$promise1->setReason("Delete Resource ".$e->getMessage());
			}
		}
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The GoodReceivedNote <?= $note1->getNoteNumber() ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the order<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another GoodReceivedNote";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegoodreceivednote_add&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Manager</a>
					</div>
					Rule: managegoodreceivednote_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegoodreceivednote_add", "Added ".$note1->getNoteNumber());
} else if ($page == "managegoodreceivednote_add" && Authorize::isAllowable($config, "managegoodreceivednote_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Creating A Good Received Note</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (isset($_REQUEST['count']))	{
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$count = intval($_REQUEST['count']);
		$order1 = null;
		$listNodes = null;
		try {
			$order1 = new PurchasingOrder($database, $_REQUEST['orderId'], $conn);
			$dataFolder = "../data/document/purchasingOrder/";
			$filename = $dataFolder.$order1->getFilename();
			$tree1 = NodeTree::loadFromFile($filename);
			if ($tree1->getChecksum() != $order1->getFilenameHash()) Object::shootException("Reference Order has been compromised");
			$listNodes = NodeTree::findCustomNodes($tree1, "0:itemList;*:item");
			if (is_null($listNodes)) Object::shootException("Reference Items were not found");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
		if ($promise1->isPromising())	{
			$count = min($count, sizeof($listNodes));
?>
					<div class="text-center">Reference Order is <b><?= $order1->getOrderNumber() ?></b></div>
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegoodreceivednote_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
						<input type="hidden" name="orderId" value="<?= $order1->getId() ?>"/>
										
<?php 
	if ($count > 0)	{ 
?>
		<br/><table style="font-size: 0.9em;" class="pure-table">
			<thead><tr><th>S/N</th><th>Item</th><th>Quantity</th></tr></thead>
			<tbody>
<?php 
	//Preparing ItemList 
	$itemList = array();
	foreach ($listNodes as $nd)	{
		try {
			$node1 = new NodeTree($nd);
			$itemId = null; $temp = $node1->getNodeContent(NodeTree::findCustomNodes($node1, "0:itemId")); if (! is_null($temp)) $itemId = $temp[0];
			if (is_null($itemId)) continue;
			$itemName = null; $temp = $node1->getNodeContent(NodeTree::findCustomNodes($node1, "0:itemName")); if (! is_null($temp)) $itemName = $temp[0];
			$measure = null; $temp = $node1->getNodeContent(NodeTree::findCustomNodes($node1, "0:measure")); if (! is_null($temp)) $measure = $temp[0];
			$quantity = null; $temp = $node1->getNodeContent(NodeTree::findCustomNodes($node1, "0:quantity")); if (! is_null($temp)) $quantity = $temp[0];
			$listsize = sizeof($itemList);
			$itemList[$listsize] = array();
			$itemList[$listsize]['id'] = $itemId;
			$itemList[$listsize]['val'] = $itemName;
			$itemList[$listsize]['measure'] = $measure;
			$itemList[$listsize]['quantity'] = $quantity;
		} catch (Exception $e)	{
			continue;
		}
	}
	for ($i = 0; $i < $count; $i++)	{
?>
		<tr class="data-row">
			<td><?= $i + 1 ?></td>
			<td><select class="select-item" name="item[<?= $i ?>]" validate="true" validate_control="select" validate_expression="true" validate_message="Kindly Select Item at Row <?= $i + 1 ?>">
				<option value="_@32767@_" data-measure="">--select--</option>
<?php 
	foreach ($itemList as $alist)	{
?>
		<option value="<?= $alist['id'] ?>" data-item-name="<?= $alist['val'] ?>" data-measure="<?= $alist['measure'] ?>" data-max-quantity=<?= $alist['quantity'] ?>><?=  $alist['val'] ?></option>
<?php
	}
?>
			</select></td>
			<td><input class="text-quantity" data-index="<?= $i ?>" type="text" name="quantity[<?= $i ?>]" size="4" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Fill Quantity at Row <?= $i + 1 ?>"/>&nbsp;&nbsp;<label class="label-measure"></label></td>
		</tr>
<?php
	} //end.for.i .. count
?>		
			</tbody>
		</table><br/>
<?php
	} //end if.count>0
?>
						<div class="pure-control-group">
							<label for="hcharges">Handling Charges </label>
							<input type="text" name="hcharges" id="hcharges" size="32" required="true" pattern="<?= $exprDecimalNumber ?>" validate="true" validate_control="text" validate_expression="<?= $exprDecimalNumber ?>" validate_message="Handling Charges : <?= $msgDecimalNumber ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
<?php 
		} else {
?>
			<div class="ui-state-error">
				There were problems in processing this page <br/>
				Details : <?= $promise1->getReason() ?>
			</div>
<?php
		}
	} else {
		//Default Landing Page 
?>
		<div class="ui-sys-data-capture">
			<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
				<input type="hidden" name="page" value="managegoodreceivednote_add"/>
				<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
				<div class="pure-control-group">
					<label for="count">Enter Number of Raw Materials : </label>
					<input type="text" name="count" id="count" size="16" required pattern="<?= $expr2Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr2Number ?>" validate_message="Raw Materials Count : <?= $msg2Number ?>"/>
				</div>
				<div class="pure-control-group">
					<label for="orderId">Select Reference Order</label>
					<select id="orderId" name="orderId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Reference Order">
						<option value="_@32767@_">--select--</option>
<?php 
	try {
		$list = Document::getListOfStockedDocuments($database, $conn, "purchasingOrder", "orderId");
		if (is_null($list)) Object::shootException("There is no available order to make reference");
		foreach ($list as $id)	{
			$order1 = new PurchasingOrder($database, $id, $conn);
			if ($order1->isDocumentSatisfied()) continue;
			// I need only valid one and which are not satisfied Only
			$captionText = $order1->getOrderNumber().", from ".$order1->getSupplierName();
?>
			<option value="<?= $order1->getOrderId() ?>"><?= $captionText ?></option>
<?php
		}
	} catch (Exception $e)	{
		$message = $e->getMessage();
		echo "<option>$message</option>"; 
	}
?>
					</select>
				</div>
				<div class="pure-controls">
					<br/><span id="perror" class="ui-sys-error-message"></span>
				</div>
				<div class="pure-controls">
					<br/><input id="__add_record" type="button" value="Continue"/>
				</div>
			</form>
		</div>
<?php
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to GoodReceivedNote Management</a></div>
					Rule: managegoodreceivednote_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('select.select-item').on('change', function(event)	{
		var $select1 = $(this).closest('select'); 
		var $option1 = $select1.find('option:selected');
		if (! $option1.length) return; 
		var $tr1 = $select1.closest('tr');
		if (! $tr1.length) return;
		var measureName = $option1.attr('data-measure');
		var $label1 = $tr1.find('label.label-measure');
		if (! $label1.length) return;
		$label1.html(measureName);
	});
	$('#__add_record').on('click', function(event)	{
		$target1 = $('#perror');
		$target1.empty();
		var allowSubmission = true;
		$('tr.data-row').each(function(i, rw)	{
			if (allowSubmission)	{
				var $tr1 = $(rw); //Housing row 
				var $select1 = $tr1.find('select.select-item');
				if (! $select1.length) return;
				var $option1 = $select1.find('option:selected');
				if (! $option1.length) return;
				var maxQuantity = $option1.attr('data-max-quantity');
				if (! maxQuantity) return;
				var itemName = $option1.attr('data-item-name');
				if (! itemName) return;
				var $text1 = $tr1.find('input.text-quantity');
				if (! $text1.length) return;
				if (parseInt($text1.val()) > maxQuantity) {
					$target1.html('Row [' + (i + 1) +  '] : You have exceed maximum order quantity for ' + itemName);
					allowSubmission = false;
				}
			}
		});
		if (allowSubmission)	{
			generalFormSubmission(this, 'form1', 'perror');
		}
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managegoodreceivednote" && Authorize::isAllowable($config, "managegoodreceivednote", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">GoodReceivedNote Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-order-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managegoodreceivednote&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managegoodreceivednote_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th></th>
				<th>Note Number</th>
				<th>Reference Order</th>
				<th>Created:Date</th>
				<th>Closed:Date</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = GoodReceivedNote::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch order data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$note1 = null;
		try {
			$note1 = new GoodReceivedNote($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($note1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
			$creationTime = "";
			if (! is_null($note1->getCreationTime())) $creationTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($note1->getCreationTime());
			$closingTime = "";
			if (! is_null($note1->getClosingTime())) $closingTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($note1->getClosingTime());
			$referenceOrderNumber = "";
			if (! is_null($note1->getPurchasingOrder())) $referenceOrderNumber = $note1->getPurchasingOrder()->getOrderNumber();
			$bgcolor = "blue";
			if ($note1->isStocked()) $bgcolor = "gold";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><span style="background-color: <?= $bgcolor ?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
				<td><?= $note1->getNoteNumber() ?></td>
				<td><?= $referenceOrderNumber ?></td>
				<td><?= $creationTime ?></td>
				<td><?= $closingTime ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managegoodreceivednote_detail&id=<?= $note1->getNoteId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managegoodreceivednote_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managegoodreceivednote_csv&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managegoodreceivednote_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Create a New Good Received Note" href="<?= $thispage ?>?page=managegoodreceivednote_add&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managegoodreceivednote</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: GoodReceivedNoteManagement*/ else /*BEGIN: PurchasingOrderManagement*/if ($page == "managepurchasingorder_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managepurchasingorder_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PurchasingOrder CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Management</a></div>
					Rule: managepurchasingorder_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managepurchasingorder_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managepurchasingorder_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PurchasingOrder CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = PurchasingOrder::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("PurchasingOrder[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$order1 = null;
			try	{
				$order1 = new PurchasingOrder($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($order1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managepurchasingorder_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managepurchasingorder_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managepurchasingorder_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managepurchasingorder_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PurchasingOrder CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managepurchasingorder_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managepurchasingorder_csv&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managepurchasingorder_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managepurchasingorder_csv" && Authorize::isAllowable($config, "managepurchasingorder_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PurchasingOrder CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managepurchasingorder_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, PurchasingOrder::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Managegement</a>
					</div>
					Rule: managepurchasingorder_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managepurchasingorder_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepurchasingorder_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$order1 = null;
	$captionText = "Unknown";
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		 $order1 = new PurchasingOrder($database, $_REQUEST['id'], $conn);
		 $captionText = $order1->getOrderName();
		 $order1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a PurchasingOrder which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The PurchasingOrder <?= $order1->getOrderName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the PurchasingOrder <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Management</a>
					</div>
					Rule: managepurchasingorder_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managepurchasingorder_delete", "Deleted ".$order1->getOrderName());
} else if ($page == "managepurchasingorder_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepurchasingorder_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$order1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$order1 = new PurchasingOrder($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		mysql_close($conn);
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $order1->getOrderName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a PurchasingOrder <b><?= $order1->getOrderName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the PurchasingOrder <?= $order1->getOrderName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managepurchasingorder_delete&id=<?= $order1->getOrderId() ?>&report=yes&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managepurchasingorder_detail&id=<?= $order1->getOrderId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managepurchasingorder_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managepurchasingorder_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepurchasingorder_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$order1 = null;
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$order1 = new PurchasingOrder($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	
	if ($extraInformation != $order1->getExtraInformation())	{
		$order1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $order1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$order1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$order1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for PurchasingOrder Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit PurchasingOrder (Report) <i><?= $order1->getOrderName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			PurchasingOrder <?= $order1->getOrderName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the order <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepurchasingorder_detail&id=<?= $order1->getOrderId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Details</a></div>
					Rule: managepurchasingorder_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managepurchasingorder_edit", "Edited ".$order1->getOrderName());
} else if ($page == "managepurchasingorder_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepurchasingorder_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$order1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		Object::shootException("Under Construction, Event Tracking System not installed");
		$order1 = new PurchasingOrder($database, $_REQUEST['id'], $conn);
		$order1->setExtraFilter($extraFilter);
		$order1->commitUpdate();
	} catch (Exception $e)	{ 
		mysql_close($conn);
		die($e->getMessage()); 
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit PurchasingOrder <i><?= $order1->getOrderName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managepurchasingorder_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $order1->getOrderId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $order1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
						<div class="pure-control-group">
							<label for="orderName">PurchasingOrder Name </label>
							<input value="<?= $order1->getOrderName() ?>" type="text" name="orderName" id="orderName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="PurchasingOrder Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="pId">Parent PurchasingOrder</label>
							<select id="pId" name="pId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Parent PurchasingOrder">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = PurchasingOrder::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected="";
		if ($order1->getOrderId() == $alist['id']) continue;
		if ($alist['id'] == $order1->getParentPurchasingOrder()->getOrderId()) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $order1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepurchasingorder_detail&id=<?= $order1->getOrderId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Details</a></div>
					Rule: managepurchasingorder_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managepurchasingorder_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepurchasingorder_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$order1 = null;
	try {
		$order1 = new PurchasingOrder($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for order : <i><?= $order1->getOrderNumber() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$documentToDisplay = "";
	try {
		$order1->putData('logoFolder', '../data/profile/logo/');
		$order1->putData('dataFolder', '../data/document/purchasingOrder/');
		$order1->putData('currentLoginInId', $login1->getLoginId());
		$order1->putData('systemTime', $systemTime1->getDateAndTimeString());
		$order1->putData('optionalStoreToAdd', 'ItemDemandStore');
		$order1->putData('storageId', $order1->getStore()->getStoreId());
		$documentToDisplay = $order1->getDocumentUI();
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	mysql_close($conn);
	if ($promise1->isPromising())	{
?>
					<div class="display-background-black">
						<!--START DOCUMENT DETAILS-->
<?php 
	echo $documentToDisplay;
?>						
						<!--STOP DOCUMENT DETAILS-->
					</div>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managepurchasingorder_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $order1->getOrderNumber() ?>" href="<?= $thispage ?>?page=managepurchasingorder_edit&id=<?= $order1->getOrderId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if ( Authorize::isAllowable($config, "managepurchasingorder_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $order1->getOrderNumber() ?>" href="<?= $thispage ?>?page=managepurchasingorder_delete&id=<?= $order1->getOrderId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
<?php 
	} else {
?>
		<div class="ui-sys-warning">
			There were problems in displaying document <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Management</a>
					</div>
					Rule: managepurchasingorder_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('button.button-sign-document').on('click', function(event)	{
		var pathOnSuccess = '<?= $thispage ?>?page=<?= $page ?>&id=<?= $order1->getId() ?>&sysmenu=<?= $_REQUEST['sysmenu'] ?>';
		window.approveDocument(this, 'purchasingOrder', pathOnSuccess, '__approval_error_control');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managepurchasingorder_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managepurchasingorder_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$storeId = mysql_real_escape_string($_REQUEST['storeId']);
		$supplierName = mysql_real_escape_string($_REQUEST['supplierName']);
		$supplierAddress = mysql_real_escape_string($_REQUEST['supplierAddress']);
		$supplierPhone = mysql_real_escape_string($_REQUEST['supplierPhone']);
		$currencyId = mysql_real_escape_string($_REQUEST['currencyId']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Create A New Purchasing Order (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$itemArray1 = Document::mergeUserData($_REQUEST['item'], $_REQUEST['quantity'], $_REQUEST['price'], null, null, 0);
	$extraFilter = System::getCodeString(8);
	$order1 = null;
	try {
		$orderNumber = rand(1000, 999999999);
		$documentId = Document::getDocumentIdFromDocumentCode($database, $conn, Document::$__DOC_RAW_PURCHASING_ORDER);
		$orderId = PurchasingOrder::addRecord($database, $conn, $orderNumber, $documentId, $storeId, $supplierName, $supplierAddress, $supplierPhone, $systemTime1, $currencyId, $extraFilter, $extraInformation, 0);
		//Create Now Object 
		$order1 = new PurchasingOrder($database, $orderId, $conn);
		$orderNumber = Document::createReferenceNumber("PO-", 6, $orderId);
		$order1->setOrderNumber($orderNumber);
		$document1 = $order1->getDocument();
		//First Approving Schema 
		$schema1 = $document1->getFirstSchemaToApprove();
		if (! is_null($schema1)) $order1->setNextSchemaToApprove($schema1->getSchemaId());
		if (is_null($document1)) Object::shootException("Failed to get a reference Document ");
		$filename = $document1->getDocumentClass()."".$order1->getOrderId().".xml";
		$order1->setFilename($filename);
		$order1->setCompleted("1");
		//Approval Schema 
		$folderPath = "../data/document/purchasingOrder/";
		$filename = $folderPath.$filename;
		//Now create Purchasing Order 
		$listType = "Item";
		$controlFlags = 0;
		$tree1 = PurchasingOrder::createPurchasingOrder($database, $conn, $profile1, $order1->getOrderId(), $itemArray1, $listType, $controlFlags);
		$tree1->setFilename($filename);
		$order1->setFilenameHash($tree1->save());
		$order1->commitUpdate();
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
		if (! is_null($order1))	{
			try {
				$order1->commitDelete();
			} catch (Exception $e)	{
				$promise1->setReason("Delete Resource ".$e->getMessage());
			}
		}
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The PurchasingOrder <?= $order1->getOrderNumber() ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the order<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another PurchasingOrder";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managepurchasingorder_add&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Manager</a>
					</div>
					Rule: managepurchasingorder_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managepurchasingorder_add", "Added ".$order1->getOrderNumber());
} else if ($page == "managepurchasingorder_add" && Authorize::isAllowable($config, "managepurchasingorder_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Creating A Purchasing Order</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (isset($_REQUEST['count']))	{
		$count = intval($_REQUEST['count']);
?>
					<div class="text-center ui-sys-warning">
						NOTE: If an item is added more than once the highest price will be considered!!!
					</div>
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managepurchasingorder_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
						<div class="pure-control-group">
							<label for="storeId">Physical Store</label>
							<select id="storeId" name="storeId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Physical Store">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = PhysicalStore::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="supplierName">Name of Supplier </label>
							<input type="text" name="supplierName" id="supplierName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Name of Supplier : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="supplierAddress">Address of Supplier </label>
							<input type="text" name="supplierAddress" id="supplierAddress" size="48" required pattern="<?= $exprL64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Address of Supplier : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="supplierPhone">Phone of Supplier </label>
							<input type="text" name="supplierPhone" id="supplierPhone" size="48" required pattern="<?= $exprPhone ?>" validate="true" validate_control="text" validate_expression="<?= $exprPhone ?>" validate_message="Phone of Supplier : <?= $msgPhone ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="currencyId">Currency</label>
							<select id="currencyId" name="currencyId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Currency">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Currency::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
<?php 
	if ($count > 0)	{
?>
		<br/><table style="font-size: 0.9em;" class="pure-table">
			<thead><tr><th>S/N</th><th>Item</th><th>Quantity</th><th>Unit Price</th><th>Amount</th></tr></thead>
			<tbody>
<?php 
	$list = Item::loadAllData($database, $conn);
	//Preparing ItemList 
	$itemList = array();
	foreach ($list as $alist)	{
		try {
			$item1 = new Item($database, $alist['id'], $conn);
			$listsize = sizeof($itemList);
			$itemList[$listsize] = array();
			$itemList[$listsize]['id'] = $item1->getItemId();
			$itemList[$listsize]['val'] = $item1->getItemName();
			$itemList[$listsize]['measure'] = $item1->getMeasure()->getMeasureName();
		} catch (Exception $e)	{
			continue;
		}
	}
	for ($i = 0; $i < $count; $i++)	{
?>
		<tr class="data-row">
			<td><?= $i + 1 ?></td>
			<td><select class="select-item" name="item[<?= $i ?>]" validate="true" validate_control="select" validate_expression="true" validate_message="Kindly Select Item at Row <?= $i + 1 ?>">
				<option value="_@32767@_" data-measure="">--select--</option>
<?php 
	foreach ($itemList as $alist)	{
?>
		<option value="<?= $alist['id'] ?>" data-measure="<?= $alist['measure'] ?>"><?=  $alist['val'] ?></option>
<?php
	}
?>
			</select></td>
			<td><input data-index="<?= $i ?>" class="change-amount" type="text" name="quantity[<?= $i ?>]" size="4" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Fill Quantity at Row <?= $i + 1 ?>"/>&nbsp;&nbsp;<label class="label-measure"></label></td>
			<td><input data-index="<?= $i ?>" class="change-amount" type="text" name="price[<?= $i ?>]" size="8" required pattern="<?= $expr8Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr8Number ?>" validate_message="Fill Unit Price at Row <?= $i + 1 ?>"/></td>
			<td class="text-right"><label class="label-amount"></label></td>
		</tr>
<?php
	} //end.for.i .. count
?>		
				<tr><td colspan="4"><b>Estimated Grand Total : </b></td><td class="text-right"><label class="grand-total"></label></td></tr>
			</tbody>
		</table><br/>
<?php
	} //end if.count>0
?>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
<?php 
	} else {
		//Default Landing Page 
?>
		<div class="text-center">
			<form id="form1" method="POST" action="<?= $thispage ?>">
				<input type="hidden" name="page" value="managepurchasingorder_add"/>
				<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__PROCUREMENT ?>"/>
				<label>Enter Number of Raw Materials : <input type="text" name="count" id="count" size="32" required pattern="<?= $expr2Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr2Number ?>" validate_message="Raw Materials Count : <?= $msg2Number ?>"/></label>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="__add_record" value="Continue"/><br/>
				<span id="perror" class="ui-sys-error-message"></span>
			</form>
		</div>
<?php
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>">Back to PurchasingOrder Management</a></div>
					Rule: managepurchasingorder_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('input.change-amount').on('keyup', function(event)	{
		var $tr1 = $(this).closest('tr');
		if (! $tr1.length) return;
		var $numberBoxes = $tr1.find('input.change-amount');
		if (! $numberBoxes.length) return;
		var product = 1;
		$numberBoxes.each(function(index, val)	{
			var $box1 = $(val);
			var value = 0;
			if ($box1.val()) value = parseInt($box1.val()) 	;
			product = product * value;
		});
		var $amount1 = $tr1.find('label.label-amount');
		if (! $amount1.length) return;
		$amount1.html(getCommaSeparatorFormat(product));
		//We also need to get the grandTotal 
		var $tbody1 = $tr1.closest('tbody');
		if (! $tbody1.length) return;
		var $trList = $tbody1.find('tr.data-row');
		if (! $trList.length) return;
		var total = 0;
		$trList.each(function(index, val)	{
			$tr1 = $(val);
			product = 1;
			$tr1.find('input.change-amount').each(function(index, val)	{
				$box1 = $(val);
				value = 0;
				if ($box1.val()) value = parseInt($box1.val());
				product = product * value;
			});
			total = total + product;
		});
		var $grandTotal1 = $('label.grand-total');
		if (! $grandTotal1.length) return;
		$grandTotal1.html(getCommaSeparatorFormat(total));
	});
	$('select.select-item').on('change', function(event)	{
		var $select1 = $(this).closest('select'); 
		var $option1 = $select1.find('option:selected');
		if (! $option1.length) return; 
		var $tr1 = $select1.closest('tr');
		if (! $tr1.length) return;
		var measureName = $option1.attr('data-measure');
		var $label1 = $tr1.find('label.label-measure');
		if (! $label1.length) return;
		$label1.html(measureName);
	});
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managepurchasingorder" && Authorize::isAllowable($config, "managepurchasingorder", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PurchasingOrder Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-order-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managepurchasingorder&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managepurchasingorder_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th></th>
				<th>Order Number</th>
				<th>Supplier</th>
				<th>Created:Date</th>
				<th>Closed:Date</th>
				<th>Currency</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = PurchasingOrder::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch order data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$order1 = null;
		try {
			$order1 = new PurchasingOrder($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($order1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
			$creationTime = "";
			if (! is_null($order1->getCreationTime())) $creationTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($order1->getCreationTime());
			$closingTime = "";
			if (! is_null($order1->getClosingTime())) $closingTime = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($order1->getClosingTime());
			$currencyCode = "";
			if (! is_null($order1->getCurrency())) $currencyCode = $order1->getCurrency()->getCurrencyCode();
			$bgcolor = "blue";
			if ($order1->isStocked()) $bgcolor = "gold";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><span style="background-color: <?= $bgcolor ?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
				<td><?= $order1->getOrderNumber() ?></td>
				<td><?= $order1->getSupplierName() ?></td>
				<td><?= $creationTime ?></td>
				<td><?= $closingTime ?></td>
				<td><?= $currencyCode ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managepurchasingorder_detail&id=<?= $order1->getOrderId() ?>&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managepurchasingorder_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managepurchasingorder_csv&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managepurchasingorder_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Create a New Purchasing Order" href="<?= $thispage ?>?page=managepurchasingorder_add&sysmenu=<?= SystemMenu::$__PROCUREMENT ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managepurchasingorder</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: PurchasingOrderManagement*/ else /*BEGIN: CurrencyManagement*/if ($page == "managecurrency_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managecurrency_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Currency CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Management</a></div>
					Rule: managecurrency_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managecurrency_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managecurrency_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Currency CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Currency::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Currency[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$currency1 = null;
			try	{
				$currency1 = new Currency($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($currency1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managecurrency_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__FINANCE ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managecurrency_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managecurrency_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managecurrency_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Currency CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managecurrency_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__FINANCE ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managecurrency_csv&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managecurrency_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managecurrency_csv" && Authorize::isAllowable($config, "managecurrency_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Currency CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managecurrency_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__FINANCE ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Currency::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Managegement</a>
					</div>
					Rule: managecurrency_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managecurrency_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managecurrency_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$currency1 = null;
	$captionText = "Unknown";
	try {
		 $currency1 = new Currency($database, $_REQUEST['id'], $conn);
		 $captionText = $currency1->getCurrencyName();
		 $currency1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Currency which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Currency <?= $currency1->getCurrencyName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Currency <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Management</a>
					</div>
					Rule: managecurrency_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managecurrency_delete", "Deleted ".$currency1->getCurrencyName());
} else if ($page == "managecurrency_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managecurrency_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$currency1 = null;
	try {
		$currency1 = new Currency($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $currency1->getCurrencyName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Currency <b><?= $currency1->getCurrencyName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Currency <?= $currency1->getCurrencyName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managecurrency_delete&id=<?= $currency1->getCurrencyId() ?>&report=yes&sysmenu=<?= SystemMenu::$__FINANCE ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managecurrency_detail&id=<?= $currency1->getCurrencyId() ?>&sysmenu=<?= SystemMenu::$__FINANCE ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managecurrency_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managecurrency_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managecurrency_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$currency1 = null;
	try {
		$currency1 = new Currency($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$currencyName = mysql_real_escape_string($_REQUEST['currencyName']);
	$currencyCode = mysql_real_escape_string($_REQUEST['currencyCode']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($currencyName != $currency1->getCurrencyName())	{
		$currency1->setCurrencyName($currencyName); $enableUpdate = true;
	}
	if ($currencyCode != $currency1->getCurrencyCode())	{
		$currency1->setCurrencyCode($currencyCode); $enableUpdate = true;
	}
	if ($extraInformation != $currency1->getExtraInformation())	{
		$currency1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $currency1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$currency1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$currency1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Currency Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Currency (Report) <i><?= $currency1->getCurrencyName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Currency <?= $currency1->getCurrencyName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the currency <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managecurrency_detail&id=<?= $currency1->getCurrencyId() ?>&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Details</a></div>
					Rule: managecurrency_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managecurrency_edit", "Edited ".$currency1->getCurrencyName());
} else if ($page == "managecurrency_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managecurrency_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$currency1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$currency1 = new Currency($database, $_REQUEST['id'], $conn);
		$currency1->setExtraFilter($extraFilter);
		$currency1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Currency <i><?= $currency1->getCurrencyName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managecurrency_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $currency1->getCurrencyId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $currency1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__FINANCE ?>"/>
						<div class="pure-control-group">
							<label for="currencyName">Currency Name </label>
							<input value="<?= $currency1->getCurrencyName() ?>" type="text" name="currencyName" id="currencyName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Currency Name : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="currencyCode">Currency Code </label>
							<input value="<?= $currency1->getCurrencyCode() ?>" type="text" name="currencyCode" id="currencyCode" size="4" required pattern="<?= $exprL4Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL4Name ?>" validate_message="Currency Code : <?= $msgL4Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $currency1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managecurrency_detail&id=<?= $currency1->getCurrencyId() ?>&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Details</a></div>
					Rule: managecurrency_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managecurrency_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managecurrency_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$currency1 = null;
	try {
		$currency1 = new Currency($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for currency : <i><?= $currency1->getCurrencyName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Currency Name</td>
								<td><?= $currency1->getCurrencyName() ?></td>
							</tr>
							<tr>
								<td>Currency Code</td>
								<td><?= $currency1->getCurrencyCode() ?></td>
							</tr>
<?php 
	if (! is_null($currency1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $currency1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managecurrency_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $currency1->getCurrencyCode() ?>" href="<?= $thispage ?>?page=managecurrency_edit&id=<?= $currency1->getCurrencyId() ?>&sysmenu=<?= SystemMenu::$__FINANCE ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managecurrency_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $currency1->getCurrencyCode() ?>" href="<?= $thispage ?>?page=managecurrency_delete&id=<?= $currency1->getCurrencyId() ?>&sysmenu=<?= SystemMenu::$__FINANCE ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Management</a> 
					</div>
					Rule: managecurrency_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managecurrency_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managecurrency_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$currencyName = mysql_real_escape_string($_REQUEST['currencyName']);
		$currencyCode = mysql_real_escape_string($_REQUEST['currencyCode']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Currency (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO currency (currencyName, currencyCode, extraFilter, extraInformation) VALUES('$currencyName', '$currencyCode', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Currency[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Currency <?= $currencyName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the currency<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Currency";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managecurrency_add&sysmenu=<?= SystemMenu::$__FINANCE ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Manager</a>
					</div>
					Rule: managecurrency_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managecurrency_add", "Added $currencyName");
} else if ($page == "managecurrency_add" && Authorize::isAllowable($config, "managecurrency_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Currency</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managecurrency_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__FINANCE ?>"/>
						<div class="pure-control-group">
							<label for="currencyName">Currency Name </label>
							<input type="text" name="currencyName" id="currencyName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Currency Name : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="currencyCode">Currency Code </label>
							<input type="text" name="currencyCode" id="currencyCode" size="4" required pattern="<?= $exprL4Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL4Name ?>" validate_message="Currency Code : <?= $msgL4Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>">Back to Currency Management</a></div>
					Rule: managecurrency_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managecurrency" && Authorize::isAllowable($config, "managecurrency", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Currency Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-currency-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managecurrency&sysmenu=<?= SystemMenu::$__FINANCE ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managecurrency_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Currency Name</th>
				<th>Currency Code</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Currency::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch currency data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$currency1 = null;
		try {
			$currency1 = new Currency($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($currency1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $currency1->getCurrencyName() ?></td>
				<td><?= $currency1->getCurrencyCode() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managecurrency_detail&id=<?= $currency1->getCurrencyId() ?>&sysmenu=<?= SystemMenu::$__FINANCE ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managecurrency_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managecurrency_csv&sysmenu=<?= SystemMenu::$__FINANCE ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managecurrency_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Currency" href="<?= $thispage ?>?page=managecurrency_add&sysmenu=<?= SystemMenu::$__FINANCE ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managecurrency</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: CurrencyManagement*/ else/*BEGIN: PhysicalStoreManagement*/if ($page == "managephysicalstore_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managephysicalstore_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PhysicalStore CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Management</a></div>
					Rule: managephysicalstore_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managephysicalstore_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managephysicalstore_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PhysicalStore CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = PhysicalStore::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("PhysicalStore[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$store1 = null;
			try	{
				$store1 = new PhysicalStore($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($store1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managephysicalstore_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__STORE ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managephysicalstore_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managephysicalstore_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managephysicalstore_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PhysicalStore CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managephysicalstore_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__STORE ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managephysicalstore_csv&sysmenu=<?= SystemMenu::$__STORE ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managephysicalstore_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managephysicalstore_csv" && Authorize::isAllowable($config, "managephysicalstore_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PhysicalStore CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managephysicalstore_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__STORE ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, PhysicalStore::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Managegement</a>
					</div>
					Rule: managephysicalstore_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managephysicalstore_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managephysicalstore_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$store1 = null;
	$captionText = "Unknown";
	try {
		 $store1 = new PhysicalStore($database, $_REQUEST['id'], $conn);
		 $captionText = $store1->getStoreName();
		 $store1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a PhysicalStore which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The PhysicalStore <?= $store1->getStoreName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the PhysicalStore <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Management</a>
					</div>
					Rule: managephysicalstore_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managephysicalstore_delete", "Deleted ".$store1->getStoreName());
} else if ($page == "managephysicalstore_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managephysicalstore_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$store1 = null;
	try {
		$store1 = new PhysicalStore($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $store1->getStoreName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a PhysicalStore <b><?= $store1->getStoreName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the PhysicalStore <?= $store1->getStoreName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managephysicalstore_delete&id=<?= $store1->getStoreId() ?>&report=yes&sysmenu=<?= SystemMenu::$__STORE ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managephysicalstore_detail&id=<?= $store1->getStoreId() ?>&sysmenu=<?= SystemMenu::$__STORE ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managephysicalstore_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managephysicalstore_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managephysicalstore_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$store1 = null;
	try {
		$store1 = new PhysicalStore($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$storeName = mysql_real_escape_string($_REQUEST['storeName']);
	$locationId = mysql_real_escape_string($_REQUEST['locationId']);
	$openingDate = DateAndTime::convertFromGUIDateFormatToSystemDateAndTimeFormat($_REQUEST['openingDate']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($storeName != $store1->getStoreName())	{
		$store1->setStoreName($storeName); $enableUpdate = true;
	}
	if (is_null($store1->getLocation()) || ($store1->getLocation()->getLocationId() != $locationId))	{
		$store1->setLocation($locationId); $enableUpdate = true;
	}
	if (is_null($store1->getTimeSinceOpened()) || ($store1->getTimeSinceOpened()->getDateAndTimeString() != $openingDate))	{
		try {
			if ((new DateAndTime("ndimangwa", $openingDate, "fadhili"))->compareDateAndTime(new DateAndTime("fadhili", Object::getCurrentTimestamp(),"ngoya")) > 0) Object::shootException("Store can not be created on future");
			$store1->setTimeSinceOpened($openingDate); $enableUpdate = true;
		} catch (Exception $e)	{
			$isLoopDetected = true;
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	}
	if ($extraInformation != $store1->getExtraInformation())	{
		$store1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $store1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$store1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$store1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for PhysicalStore Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit PhysicalStore (Report) <i><?= $store1->getStoreName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			PhysicalStore <?= $store1->getStoreName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the store <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managephysicalstore_detail&id=<?= $store1->getStoreId() ?>&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Details</a></div>
					Rule: managephysicalstore_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managephysicalstore_edit", "Edited ".$store1->getStoreName());
} else if ($page == "managephysicalstore_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managephysicalstore_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$store1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$store1 = new PhysicalStore($database, $_REQUEST['id'], $conn);
		$store1->setExtraFilter($extraFilter);
		$store1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit PhysicalStore <i><?= $store1->getStoreName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managephysicalstore_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $store1->getStoreId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $store1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__STORE ?>"/>
						<div class="pure-control-group">
							<label for="storeName">Name of the Store </label>
							<input value="<?= $store1->getStoreName() ?>" type="text" name="storeName" id="storeName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Name of the Store : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="locationId">Location</label>
							<select id="locationId" name="locationId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select location of the store">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Location::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected = "";
		if (! is_null($store1->getLocation()) && ($store1->getLocation()->getLocationId() == $alist['id'])) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php 
	}
?>
							</select>
						</div>
<?php 
	$openingDate = "";
	if (! is_null($store1->getTimeSinceOpened())) $openingDate = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($store1->getTimeSinceOpened());
?>
						<div class="pure-control-group">
							<label for="openingDate">Date of Opening </label>
							<input value="<?= $openingDate ?>" class="datepicker" type="text" name="openingDate" id="openingDate" size="16" required pattern="<?= $exprDate ?>" validate="true" validate_control="text" validate_expression="<?= $exprDate ?>" validate_message="Date of Opening: <?= $msgDate ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $store1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managephysicalstore_detail&id=<?= $store1->getStoreId() ?>&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Details</a></div>
					Rule: managephysicalstore_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managephysicalstore_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managephysicalstore_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$store1 = null;
	try {
		$store1 = new PhysicalStore($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for store : <i><?= $store1->getStoreName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Store Name</td>
								<td><?= $store1->getStoreName() ?></td>
							</tr>
<?php if (! is_null($store1->getLocation()))	{?><tr><td>Location </td><td><?= $store1->getLocation()->getLocationName() ?></td></tr><?php } ?>
<?php if (! is_null($store1->getTimeSinceOpened()))	{?><tr><td>Opening Date </td><td><?= DateAndTime::convertFromDateTimeObjectToGUIDateFormat($store1->getTimeSinceOpened()) ?></td></tr><?php } ?>
<?php 
	if (! is_null($store1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $store1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managephysicalstore_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $store1->getStoreName() ?>" href="<?= $thispage ?>?page=managephysicalstore_edit&id=<?= $store1->getStoreId() ?>&sysmenu=<?= SystemMenu::$__STORE ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managephysicalstore_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $store1->getStoreName() ?>" href="<?= $thispage ?>?page=managephysicalstore_delete&id=<?= $store1->getStoreId() ?>&sysmenu=<?= SystemMenu::$__STORE ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Management</a>
					</div>
					Rule: managephysicalstore_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managephysicalstore_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managephysicalstore_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$storeName = mysql_real_escape_string($_REQUEST['storeName']);
		$locationId = mysql_real_escape_string($_REQUEST['locationId']);
		$openingDate = DateAndTime::convertFromGUIDateFormatToSystemDateAndTimeFormat($_REQUEST['openingDate']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New PhysicalStore (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO physicalStore (storeName, locationId, openingDate, extraFilter, extraInformation) VALUES('$storeName', '$locationId', '$openingDate', '$extraFilter', '$extraInformation')";
	try {
		if ((new DateAndTime("ndimangwa", $openingDate, "fadhili"))->compareDateAndTime(new DateAndTime("fadhili", Object::getCurrentTimestamp(),"ngoya")) > 0) Object::shootException("Store can not be created on future");
		mysql_db_query($database, $query, $conn) or Object::shootException("PhysicalStore[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The PhysicalStore <?= $storeName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the store<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another PhysicalStore";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managephysicalstore_add&sysmenu=<?= SystemMenu::$__STORE ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Manager</a>
					</div>
					Rule: managephysicalstore_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managephysicalstore_add", "Added $storeName");
} else if ($page == "managephysicalstore_add" && Authorize::isAllowable($config, "managephysicalstore_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New PhysicalStore</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managephysicalstore_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__STORE ?>"/>
						<div class="pure-control-group">
							<label for="storeName">Name of the Store </label>
							<input type="text" name="storeName" id="storeName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Name of the Store : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="locationId">Location</label>
							<select id="locationId" name="locationId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select location of the store">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Location::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="openingDate">Date of Opening </label>
							<input class="datepicker" type="text" name="openingDate" id="openingDate" size="16" required pattern="<?= $exprDate ?>" validate="true" validate_control="text" validate_expression="<?= $exprDate ?>" validate_message="Date of Opening: <?= $msgDate ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>">Back to PhysicalStore Management</a></div>
					Rule: managephysicalstore_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managephysicalstore" && Authorize::isAllowable($config, "managephysicalstore", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">PhysicalStore Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-store-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managephysicalstore&sysmenu=<?= SystemMenu::$__STORE ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managephysicalstore_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Store Name</th>
				<th>Location</th>
				<th>Date: Age</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = PhysicalStore::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch store data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$store1 = null;
		try {
			$store1 = new PhysicalStore($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($store1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
			$locationName = "";
			if (! is_null($store1->getLocation())) $locationName = $store1->getLocation()->getLocationName();
			$openingDate = "";
			if (! is_null($store1->getTimeSinceOpened())) $openingDate = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($store1->getTimeSinceOpened());
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $store1->getStoreName() ?></td>
				<td><?= $locationName ?></td>
				<td><?= $openingDate ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managephysicalstore_detail&id=<?= $store1->getStoreId() ?>&sysmenu=<?= SystemMenu::$__STORE ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managephysicalstore_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managephysicalstore_csv&sysmenu=<?= SystemMenu::$__STORE ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managephysicalstore_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System PhysicalStore" href="<?= $thispage ?>?page=managephysicalstore_add&sysmenu=<?= SystemMenu::$__STORE ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managephysicalstore</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: PhysicalStoreManagement*/ else/*Document BEGIN Attach vs Dettach*/if ($page == "managedocument_edit" && isset($_REQUEST['from']) && isset($_REQUEST['id']) && isset($_REQUEST['report']) && isset($_REQUEST['detach_document']) && Authorize::isAllowable($config, "managedocument_edit", "normal", "setlog", "-1", "-1"))	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$document1 = null;
		$headerText = "UNKNOWN";
		try {
			$document1 = new Document($database, $_REQUEST['id'], $conn);
			$headerText = "De-Assign Document Approval Schema from ".$document1->getDocumentName();
			if (! isset($_REQUEST['did'])) Object::shootException("There were nothing to Attach");
			$listToUpdate = Object::detachIdListFromObjectList($document1->getListOfApprovalSchema(), $_REQUEST['did']);
			$document1->setListOfApprovalSchema($listToUpdate);
			$document1->commitUpdate();
			$enableUpdate = true;
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
?>
			<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Detach Report</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Document Approval Schema List Attachment were removed successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in De-Attaching Document Approval Schema List <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a class="ui-sys-back" href="<?= $thispage ?>?page=managedocument_detail&from=true&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Details Page</a></div>
					Rule: <?= $page ?>
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
		mysql_close($conn);
		if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedocument_edit", "Detach Document Approval Schema for ".$document1->getDocumentName());
	} else if ($page == "managedocument_edit" && isset($_REQUEST['from']) && isset($_REQUEST['id']) && isset($_REQUEST['report']) && isset($_REQUEST['attach_document']) && Authorize::isAllowable($config, "managedocument_edit", "normal", "setlog", "-1", "-1"))	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$document1 = null;
		$headerText = "UNKNOWN";
		try {
			$document1 = new Document($database, $_REQUEST['id'], $conn);
			$headerText = "Assign Document Approval Schema to ".$document1->getDocumentName();
			if (! isset($_REQUEST['did'])) Object::shootException("There were nothing to Attach");
			$listToUpdate = Object::attachIdListToObjectList($document1->getListOfApprovalSchema(), $_REQUEST['did']);
			$document1->setListOfApprovalSchema($listToUpdate);
			$document1->commitUpdate();
			$enableUpdate = true;
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Attachment Report</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">			
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Document Approval Schema List Attachment were done successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Attaching Document Approval Schema List <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>			
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a class="ui-sys-back" href="<?= $thispage ?>?page=managedocument_detail&from=true&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Details Page</a></div>
					Rule: <?= $page ?>
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
		mysql_close($conn);
		if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedocument_edit", "Attach Document Approval Schema for ".$document1->getDocumentName());
	} else if ($page == "managedocument_edit" && isset($_REQUEST['from']) && isset($_REQUEST['id']) && isset($_REQUEST['attach_document']) && Authorize::isAllowable($config, "managedocument_edit", "normal", "setlog", "-1", "-1")) {
		$conn  = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$document1 = null;
		$headerText = "UNKNOWN";
		try {
			$document1 = new Document($database, $_REQUEST['id'], $conn);
			$headerText = "Assign Document Approval Schema to ".$document1->getDocumentName();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
?>
		<div class="row ui-sys-body">
			<div class="col-md-12">
<?php 
				if ($promise1->isPromising())	{
					if (! is_null($document1->getListOfApprovalSchema()) && sizeof($document1->getListOfApprovalSchema()) > 0)	{
						//Starting Block One 
						$objectList1 = $document1->getListOfApprovalSchema();
?>
						<div class="panel panel-default">
							<div class="panel-heading"><h3 class="panel-title">Attached List of Document Approval Schemas</h3></div> <!--End div.panel-heading-->
							<div class="panel-body">
						<form method="POST" action="<?= $thispage ?>">
							<input type="hidden" name="page" value="managedocument_edit"/>
							<input type="hidden" name="id" value="<?= $document1->getDocumentId() ?>"/>
							<input type="hidden" name="report" value="true"/>
							<input type="hidden" name="detach_document" value="true"/>
							<input type="hidden" name="from" value="true"/>
							<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
							<table class="pure-table pure-table-aligned ui-sys-search-results-1">
								<thead>
									<tr><th colspan="5">Document Approval Schema Attached To</th></tr>
									<tr><th></th><th></th><th>Before Text</th><th>After Text</th><th>Job Title</th></tr>
								</thead>
<?php 
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	foreach ($objectList1 as $obj1)	{
		if (is_null($obj1)) continue;
		if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
			$numberOfTBodies++;
			$tbodyClosed = false;
		} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
			$numberOfTBodies++;
			$perTbodyCounter = 0;
		}
		//In All Cases we need to display tr
		$pureTableOddClass = "";
		if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
		<tr class="<?= $pureTableOddClass ?>">
			<td><input type="checkbox" name="did[<?= $sqlReturnedRowCount ?>]" value="<?= $obj1->getSchemaId() ?>"/></td>
			<td><?= $sqlReturnedRowCount + 1 ?></td>
			<td><?= $obj1->getCaptionError() ?></td>
			<td><?= $obj1->getCaptionText() ?></td>
			<td>
<?php 
	if (! is_null($obj1->getJobTitle()))	{
?>
		<?= $obj1->getJobTitle()->getJobName() ?>
<?php
	}
?>			
			</td>
		</tr>
<?php
		$perTbodyCounter++;
		$sqlReturnedRowCount++;
	}
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
?>
							</table>
							<div class="ui-sys-right"><input type="submit" value="Detach Document Approval Schema"/></div>
						</form>
						<ul class="ui-sys-pagination-1"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination-1').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results-1 tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
							</div>	<!--End div.panel-body-->
							<div class="panel-footer">
								<div class="text-center"><a class="ui-sys-back" href="<?= $thispage ?>?page=managedocument_detail&from=true&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Details Page</a></div>
								Rule: <?= $page ?>
							</div>	<!--End div.panel-footer-->
						</div> <!--End div.panel.panel-default-->
<?php	
						//Stopping Block One
					} 
						//Starting Block Two 
?>
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">List of Document Approval Schemas</h3></div> <!--End div.panel-heading-->
						<div class="panel-body">
							<div style="padding: 1px; border: 1px gold dotted;">
								<!--Content of Block Two Extra BEgins-->
		<!--Block ONE controls for searching-->
				<div class="ui-sys-search-container pure-form pure-approval-control">
					<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
					<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managedocument_edit&from=true&id=<?= $document1->getDocumentId() ?>&attach_document=true&sysmenu=<?= SystemMenu::$__SETTINGS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
				</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<form method="POST" action="<?= $thispage ?>">
		<input type="hidden" name="page" value="managedocument_edit"/>
		<input type="hidden" name="id" value="<?= $document1->getDocumentId() ?>"/>
		<input type="hidden" name="report" value="true"/>
		<input type="hidden" name="attach_document" value="true"/>
		<input type="hidden" name="from" value="true"/>
		<input type="hidden" name="sysmenu" value="SystemMenu::$__SETTINGS"/>
		<table class="pure-table ui-sys-table-search-results-2">
			<thead>
				<tr>
					<th></th>
					<th>S/N</th>
					<th>Before Text</th>
					<th>After Text</th>
					<th>Job Title</th>
				</tr>
			</thead>
<?php 
	$query = DocumentApprovalSchema::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch approval data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$schema1 = null;
		try {
			$schema1 = new DocumentApprovalSchema($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($schema1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><input type="checkbox" name="did[<?= $sqlReturnedRowCount ?>]" value="<?= $schema1->getSchemaId() ?>"/></td>
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $schema1->getCaptionError() ?></td>
				<td><?= $schema1->getCaptionText() ?></td>
				<td>
<?php 
	if (! is_null($schema1->getJobTitle()))	{
?>
		<?= $schema1->getJobTitle()->getJobName() ?>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
?>
		</table>
		<div class="ui-sys-right">
			<input type="submit" value="Attach Document Approval Schema"/>
		</div>
	</form>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination-2"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination-2').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results-2 tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
		<!--Content of Block Two Extra Ends-->
							</div>
						</div>	<!--End div.panel-body-->
						<div class="panel-footer">
							<div class="text-center"><a class="ui-sys-back" href="<?= $thispage ?>?page=managedocument_detail&from=true&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Details Page</a></div>
							Rule: <?= $page ?>
						</div>	<!--End div.panel-footer-->
					</div> <!--End div.panel.panel-default-->
<?php
						//Stopping Block Two			
				} else {
					//Non-Promising Alternative 
?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Problem(s) Found</h3>
						</div> <!--End div.panel-heading-->
						<div class="panel-body">
							<div class="ui-state-error">
								There were problems in the initial setup of Assignment <br/>
								Reason : <?= $promise1->getReason() ?>
							</div>
						</div>	<!--End div.panel-body-->
						<div class="panel-footer">
							<div class="text-center"><a class="ui-sys-back" href="<?= $thispage ?>?page=managedocument_detail&from=true&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Details Page</a></div>
							Rule: <?= $page ?>
						</div>	<!--End div.panel-footer-->
					</div> <!--End div.panel.panel-default-->
<?php
				}
?>
			</div>	<!--End div.col-md-12-->
		</div><!--End div.row-->
<?php
		mysql_close($conn);
	} /*Document Ends Attach vs Detach*/ else /*BEGIN: DocumentManagement*/if ($page == "managedocument_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managedocument_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Document CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedocument&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Management</a></div>
					Rule: managedocument_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managedocument_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managedocument_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Document CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Document::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Document[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$document1 = null;
			try	{
				$document1 = new Document($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($document1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managedocument_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managedocument_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managedocument_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managedocument_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Document CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managedocument_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managedocument_csv&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managedocument_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managedocument_csv" && Authorize::isAllowable($config, "managedocument_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Document CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managedocument_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Document::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managedocument&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Managegement</a>
					</div>
					Rule: managedocument_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managedocument_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedocument_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$document1 = null;
	try {
		$document1 = new Document($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for document : <i><?= $document1->getDocumentName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Document Name</td>
								<td><?= $document1->getDocumentName() ?></td>
							</tr>
							<tr><td>Document Code</td><td><?= $document1->getDocumentCode() ?></td></tr>
							<tr><td>Page</td><td><?= $document1->getPagename() ?></td></tr>
							<tr><td>Class Name</td><td><?= $document1->getDocumentClass() ?></td></tr>
<?php 
	if (! is_null($document1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $document1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table><br/>
<?php 
	if (! is_null($document1->getListOfApprovalSchema()) && sizeof($document1->getListOfApprovalSchema()) > 0)	{
		$objectList1 = $document1->getListOfApprovalSchema();
?>
			<table class="pure-table pure-table-aligned ui-sys-search-results">
				<thead>
					<tr>
						<th colspan="4">List of Approval Schema</th>
					</tr>
					<tr>
						<th></th>
						<th>Before Text</th>
						<th>After Text</th>
						<th>Job Title</th>
					</tr>
				</thead>
<?php 
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	foreach ($objectList1 as $obj1)	{
		if (is_null($obj1)) continue;
		if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
			$numberOfTBodies++;
			$tbodyClosed = false;
		} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
			$numberOfTBodies++;
			$perTbodyCounter = 0;
		}
		//In All Cases we need to display tr
		$pureTableOddClass = "";
		if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
		<tr class="<?= $pureTableOddClass ?>">
			<td><?= $sqlReturnedRowCount + 1 ?></td>
			<td><?= $obj1->getCaptionError() ?></td>
			<td><?= $obj1->getCaptionText() ?></td>
			<td>
<?php 
	if (! is_null($obj1->getJobTitle()))	{
?>
		<?= $obj1->getJobTitle()->getJobName() ?>
<?php
	}
?>			
			</td>
		</tr>
<?php
		$perTbodyCounter++;
		$sqlReturnedRowCount++;
	}
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
?>
			</table>
			<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>

<?php
	}
?>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managedocument_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Attach Document Approval Schemas" href="<?= $thispage ?>?page=managedocument_edit&id=<?= $document1->getDocumentId() ?>&attach_document=true&sysmenu=<?= SystemMenu::$__SETTINGS ?>&from=managedocument_detail"><img alt="DAT" src="../sysimage/buttonattach.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managedocument&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Document Management</a>
					</div>
					Rule: managedocument_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managedocument" && Authorize::isAllowable($config, "managedocument", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Document Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-document-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managedocument&sysmenu=<?= SystemMenu::$__SETTINGS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managedocument_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Document Name</th>
				<th>Document Code</th>
				<th>Details</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Document::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch document data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$document1 = null;
		try {
			$document1 = new Document($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($document1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $document1->getDocumentName() ?></td>
				<td><?= $document1->getDocumentCode() ?></td>
				<td><?= Object::summarizeString($document1->getExtraInformation(), 16) ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managedocument_detail&id=<?= $document1->getDocumentId() ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managedocument_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managedocument_csv&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managedocument</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: DocumentManagement*/ else /*BEGIN: DocumentApprovalSchemaManagement*/if ($page == "managedocumentapprovalschema_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managedocumentapprovalschema_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">DocumentApprovalSchema CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Management</a></div>
					Rule: managedocumentapprovalschema_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managedocumentapprovalschema_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managedocumentapprovalschema_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">DocumentApprovalSchema CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = DocumentApprovalSchema::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("DocumentApprovalSchema[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$schema1 = null;
			try	{
				$schema1 = new DocumentApprovalSchema($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($schema1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managedocumentapprovalschema_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managedocumentapprovalschema_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managedocumentapprovalschema_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managedocumentapprovalschema_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">DocumentApprovalSchema CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managedocumentapprovalschema_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managedocumentapprovalschema_csv&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managedocumentapprovalschema_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managedocumentapprovalschema_csv" && Authorize::isAllowable($config, "managedocumentapprovalschema_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">DocumentApprovalSchema CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managedocumentapprovalschema_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, DocumentApprovalSchema::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Managegement</a>
					</div>
					Rule: managedocumentapprovalschema_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managedocumentapprovalschema_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedocumentapprovalschema_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$schema1 = null;
	$captionText = "Unknown";
	try {
		 $schema1 = new DocumentApprovalSchema($database, $_REQUEST['id'], $conn);
		 $captionText = $schema1->getSchemaName();
		 $schema1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a DocumentApprovalSchema which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The DocumentApprovalSchema <?= $schema1->getSchemaName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the DocumentApprovalSchema <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Management</a>
					</div>
					Rule: managedocumentapprovalschema_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedocumentapprovalschema_delete", "Deleted ".$schema1->getSchemaName());
} else if ($page == "managedocumentapprovalschema_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedocumentapprovalschema_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$schema1 = null;
	try {
		$schema1 = new DocumentApprovalSchema($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $schema1->getSchemaName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a DocumentApprovalSchema <b><?= $schema1->getSchemaName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the DocumentApprovalSchema <?= $schema1->getSchemaName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managedocumentapprovalschema_delete&id=<?= $schema1->getSchemaId() ?>&report=yes&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managedocumentapprovalschema_detail&id=<?= $schema1->getSchemaId() ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managedocumentapprovalschema_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managedocumentapprovalschema_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedocumentapprovalschema_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$schema1 = null;
	try {
		$schema1 = new DocumentApprovalSchema($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$captionText = mysql_real_escape_string($_REQUEST['captionText']);
	$captionError = mysql_real_escape_string($_REQUEST['captionError']);
	$jobId = mysql_real_escape_string($_REQUEST['jobId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	$schemaName = $captionText;
	if ($captionText != $schema1->getCaptionText())	{
		$schema1->setCaptionText($captionText); $enableUpdate = true;
	}
	if ($captionError != $schema1->getCaptionError())	{
		$schema1->setCaptionError($captionError); $enableUpdate = true;
	}
	if (is_null($schema1->getJobTitle()) || ($schema1->getJobTitle()->getJobId() != $jobId))	{
		$schema1->setJobTitle($jobId); $enableUpdate = true;
	}
	if ($extraInformation != $schema1->getExtraInformation())	{
		$schema1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $schema1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$schema1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$schema1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for DocumentApprovalSchema Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit DocumentApprovalSchema (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			DocumentApprovalSchema has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the schema <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedocumentapprovalschema_detail&id=<?= $schema1->getSchemaId() ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Details</a></div>
					Rule: managedocumentapprovalschema_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedocumentapprovalschema_edit", "Edited Schema ($schemaName)");
} else if ($page == "managedocumentapprovalschema_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedocumentapprovalschema_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$schema1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$schema1 = new DocumentApprovalSchema($database, $_REQUEST['id'], $conn);
		$schema1->setExtraFilter($extraFilter);
		$schema1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit DocumentApprovalSchema</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managedocumentapprovalschema_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $schema1->getSchemaId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $schema1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
						<div class="pure-control-group">
							<label for="captionError">Caption Text Before Signed </label>
							<input value="<?= $schema1->getCaptionError() ?>" type="text" name="captionError" id="captionError" placeholder="Not Yet Approved" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Caption Text (Before Signing) : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="captionText">Caption Text After Signed </label>
							<input value="<?= $schema1->getCaptionText() ?>" type="text" name="captionText" id="captionText" placeholder="Approved" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Caption Text (After Signing) : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="jobId">Job Title</label>
							<select id="jobId" name="jobId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Job Title">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = JobTitle::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected = "";
		if (! is_null($schema1->getJobTitle()) && ($schema1->getJobTitle()->getJobId() == $alist['id'])) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $schema1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedocumentapprovalschema_detail&id=<?= $schema1->getSchemaId() ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Details</a></div>
					Rule: managedocumentapprovalschema_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managedocumentapprovalschema_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedocumentapprovalschema_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$schema1 = null;
	try {
		$schema1 = new DocumentApprovalSchema($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for schema</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr><td>Caption Text (Before Signing)</td><td><?= $schema1->getCaptionError() ?></td></tr>
							<tr><td>Caption Text (After Signing)</td><td><?= $schema1->getCaptionText() ?></td></tr>
<?php 
	if (! is_null($schema1->getJobTitle()))	{
?>
					<tr>
						<td>Job Title</td>
						<td><?= $schema1->getJobTitle()->getJobName() ?></td>
					</tr>
<?php	
	}
	if (! is_null($schema1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $schema1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managedocumentapprovalschema_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit Schema" href="<?= $thispage ?>?page=managedocumentapprovalschema_edit&id=<?= $schema1->getSchemaId() ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managedocumentapprovalschema_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete Schema" href="<?= $thispage ?>?page=managedocumentapprovalschema_delete&id=<?= $schema1->getSchemaId() ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Management</a>
					</div>
					Rule: managedocumentapprovalschema_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managedocumentapprovalschema_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managedocumentapprovalschema_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$captionError = mysql_real_escape_string($_REQUEST['captionError']);
	$captionText = mysql_real_escape_string($_REQUEST['captionText']);
	$jobId = mysql_real_escape_string($_REQUEST['jobId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	$schemaName = $captionText;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New DocumentApprovalSchema (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO documentApprovalSchema (captionText, captionError, jobId, extraFilter, extraInformation) VALUES('$captionText', '$captionError', '$jobId', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("DocumentApprovalSchema[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The DocumentApprovalSchema <?= $schemaName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the schema<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another DocumentApprovalSchema";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managedocumentapprovalschema_add&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Manager</a>
					</div>
					Rule: managedocumentapprovalschema_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedocumentapprovalschema_add", "Added $schemaName");
} else if ($page == "managedocumentapprovalschema_add" && Authorize::isAllowable($config, "managedocumentapprovalschema_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New DocumentApprovalSchema</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managedocumentapprovalschema_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
						<div class="pure-control-group">
							<label for="captionError">Caption Text Before Signed </label>
							<input type="text" name="captionError" id="captionError" placeholder="Not Yet Approved" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Caption Text (Before Signing) : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="captionText">Caption Text After Signed </label>
							<input type="text" name="captionText" id="captionText" placeholder="Approved" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Caption Text (After Signing) : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="jobId">Job Title</label>
							<select id="jobId" name="jobId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Job Title">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = JobTitle::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>">Back to DocumentApprovalSchema Management</a></div>
					Rule: managedocumentapprovalschema_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managedocumentapprovalschema" && Authorize::isAllowable($config, "managedocumentapprovalschema", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">DocumentApprovalSchema Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-schema-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managedocumentapprovalschema&sysmenu=<?= SystemMenu::$__SETTINGS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managedocumentapprovalschema_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Before Text</th>
				<th>After Text</th>
				<th>Job Title</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = DocumentApprovalSchema::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch schema data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$schema1 = null;
		try {
			$schema1 = new DocumentApprovalSchema($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($schema1->searchMatrix($matrix1)->evaluateResult())	{
			$jobName = ""; if (! is_null($schema1->getJobTitle())) $jobName = $schema1->getJobTitle()->getJobName();
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= Object::summarizeString($schema1->getCaptionError(), 16) ?></td>
				<td><?= Object::summarizeString($schema1->getCaptionText(), 16) ?></td>
				<td><?= $jobName ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managedocumentapprovalschema_detail&id=<?= $schema1->getSchemaId() ?>&sysmenu=<?= SystemMenu::$__SETTINGS ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managedocumentapprovalschema_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managedocumentapprovalschema_csv&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managedocumentapprovalschema_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System DocumentApprovalSchema" href="<?= $thispage ?>?page=managedocumentapprovalschema_add&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managedocumentapprovalschema</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: DocumentApprovalSchemaManagement*/ else /*BEGIN: PaperManagement*/if ($page == "managepaper_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managepaper_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Paper CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Management</a></div>
					Rule: managepaper_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managepaper_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managepaper_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Paper CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Paper::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Paper[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$paper1 = null;
			try	{
				$paper1 = new Paper($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($paper1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managepaper_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managepaper_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managepaper_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managepaper_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Paper CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managepaper_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managepaper_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managepaper_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managepaper_csv" && Authorize::isAllowable($config, "managepaper_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Paper CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managepaper_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Paper::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Managegement</a>
					</div>
					Rule: managepaper_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managepaper_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepaper_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$paper1 = null;
	$captionText = "Unknown";
	try {
		 $paper1 = new Paper($database, $_REQUEST['id'], $conn);
		 $captionText = $paper1->getPaperName();
		 $paper1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Paper which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Paper <?= $paper1->getPaperName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Paper <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Management</a>
					</div>
					Rule: managepaper_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managepaper_delete", "Deleted ".$paper1->getPaperName());
} else if ($page == "managepaper_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepaper_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$paper1 = null;
	try {
		$paper1 = new Paper($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $paper1->getPaperName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Paper <b><?= $paper1->getPaperName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Paper <?= $paper1->getPaperName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managepaper_delete&id=<?= $paper1->getPaperId() ?>&report=yes&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managepaper_detail&id=<?= $paper1->getPaperId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managepaper_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managepaper_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepaper_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$paper1 = null;
	try {
		$paper1 = new Paper($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$paperName = mysql_real_escape_string($_REQUEST['paperName']);
	$width = mysql_real_escape_string($_REQUEST['width']);
	$height = mysql_real_escape_string($_REQUEST['height']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($paperName != $paper1->getPaperName())	{
		$paper1->setPaperName($paperName); $enableUpdate = true;
	}
	if ($width != $paper1->getWidth())	{
		$paper1->setWidth($width); $enableUpdate = true;
	}
	if ($height != $paper1->getHeight())	{
		$paper1->setHeight($height); $enableUpdate = true;
	}
	if ($extraInformation != $paper1->getExtraInformation())	{
		$paper1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $paper1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$paper1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$paper1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Paper Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Paper (Report) <i><?= $paper1->getPaperName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Paper <?= $paper1->getPaperName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the paper <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepaper_detail&id=<?= $paper1->getPaperId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Details</a></div>
					Rule: managepaper_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managepaper_edit", "Edited ".$paper1->getPaperName());
} else if ($page == "managepaper_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepaper_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$paper1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$paper1 = new Paper($database, $_REQUEST['id'], $conn);
		$paper1->setExtraFilter($extraFilter);
		$paper1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Paper <i><?= $paper1->getPaperName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managepaper_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $paper1->getPaperId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $paper1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="paperName">Paper Name </label>
							<input value="<?= $paper1->getPaperName() ?>" type="text" name="paperName" id="paperName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Paper Name : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="width">Width (mm) </label>
							<input value="<?= $paper1->getWidth() ?>" type="text" name="width" id="width" size="8" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Width: <?= $msg4Number ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="height">Height (mm) </label>
							<input value="<?= $paper1->getHeight() ?>" type="text" name="height" id="height" size="8" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Height: <?= $msg4Number ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $paper1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepaper_detail&id=<?= $paper1->getPaperId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Details</a></div>
					Rule: managepaper_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managepaper_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managepaper_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$paper1 = null;
	try {
		$paper1 = new Paper($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for paper : <i><?= $paper1->getPaperName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Paper Name</td>
								<td><?= $paper1->getPaperName() ?></td>
							</tr>
							<tr><td>Width (mm) : </td><td><?= $paper1->getWidth() ?></td></tr>
							<tr><td>Height (mm) : </td><td><?= $paper1->getHeight() ?></td></tr>
<?php 
	if (! is_null($paper1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $paper1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managepaper_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $paper1->getPaperName() ?>" href="<?= $thispage ?>?page=managepaper_edit&id=<?= $paper1->getPaperId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managepaper_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $paper1->getPaperName() ?>" href="<?= $thispage ?>?page=managepaper_delete&id=<?= $paper1->getPaperId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Management</a>
					</div>
					Rule: managepaper_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managepaper_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managepaper_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$paperName = mysql_real_escape_string($_REQUEST['paperName']);
		$width = mysql_real_escape_string($_REQUEST['width']);
		$height = mysql_real_escape_string($_REQUEST['height']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Paper (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO paper (paperName, width, height, extraFilter, extraInformation) VALUES('$paperName', '$width', '$height', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Paper[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Paper <?= $paperName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the paper<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Paper";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managepaper_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Manager</a>
					</div>
					Rule: managepaper_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managepaper_add", "Added $paperName");
} else if ($page == "managepaper_add" && Authorize::isAllowable($config, "managepaper_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Paper</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managepaper_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="paperName">Paper Name </label>
							<input type="text" name="paperName" id="paperName" size="48" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Paper Name : <?= $msgA32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="width">Width (mm) </label>
							<input type="text" name="width" id="width" size="8" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Width: <?= $msg4Number ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="height">Height (mm) </label>
							<input type="text" name="height" id="height" size="8" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Height: <?= $msg4Number ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Paper Management</a></div>
					Rule: managepaper_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managepaper" && Authorize::isAllowable($config, "managepaper", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Paper Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-paper-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managepaper&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managepaper_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Paper Name</th>
				<th>Width (mm)</th>
				<th>Height (mm)</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Paper::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch paper data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$paper1 = null;
		try {
			$paper1 = new Paper($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($paper1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $paper1->getPaperName() ?></td>
				<td><?= $paper1->getWidth() ?></td>
				<td><?= $paper1->getHeight() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managepaper_detail&id=<?= $paper1->getPaperId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managepaper_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managepaper_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managepaper_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Paper" href="<?= $thispage ?>?page=managepaper_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managepaper</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: PaperManagement*/ else /*BEGIN: LocationManagement*/if ($page == "managelocation_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managelocation_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Location CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Management</a></div>
					Rule: managelocation_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managelocation_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managelocation_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Location CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Location::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Location[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$location1 = null;
			try	{
				$location1 = new Location($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($location1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managelocation_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managelocation_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managelocation_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managelocation_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Location CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managelocation_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managelocation_csv&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managelocation_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managelocation_csv" && Authorize::isAllowable($config, "managelocation_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Location CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managelocation_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Location::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Managegement</a>
					</div>
					Rule: managelocation_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managelocation_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelocation_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$location1 = null;
	$captionText = "Unknown";
	try {
		 $location1 = new Location($database, $_REQUEST['id'], $conn);
		 $captionText = $location1->getLocationName();
		 $location1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Location which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Location <?= $location1->getLocationName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Location <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Management</a>
					</div>
					Rule: managelocation_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managelocation_delete", "Deleted ".$location1->getLocationName());
} else if ($page == "managelocation_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelocation_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$location1 = null;
	try {
		$location1 = new Location($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $location1->getLocationName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Location <b><?= $location1->getLocationName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Location <?= $location1->getLocationName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managelocation_delete&id=<?= $location1->getLocationId() ?>&report=yes&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managelocation_detail&id=<?= $location1->getLocationId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managelocation_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managelocation_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelocation_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$location1 = null;
	try {
		$location1 = new Location($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$locationName = mysql_real_escape_string($_REQUEST['locationName']);
	$conferenceId = mysql_real_escape_string($_REQUEST['conferenceId']);
	$physicalAddress = mysql_real_escape_string($_REQUEST['physicalAddress']);
	$postalAddress = mysql_real_escape_string($_REQUEST['postalAddress']);
	$gpsCoordinate = mysql_real_escape_string($_REQUEST['gpsCoordinate']);
	$countryId = mysql_real_escape_string($_REQUEST['countryId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	//Dealing with GPS Coordinate 
	$coordinate1 = null;
	try {
		$coordinate1 = new GPSCoordinate("Ndimangwa", $gpsCoordinate, "Fadhili");
	} catch (Exception $e)	{ $coordinate1 = null; }
	if ($locationName != $location1->getLocationName())	{
		$location1->setLocationName($locationName); $enableUpdate = true;
	}
	if (is_null($location1->getConference()) || ($conferenceId != $location1->getConference()->getConferenceId()))	{
		$location1->setConference($conferenceId); $enableUpdate = true;
	}
	if ($physicalAddress != $location1->getPhysicalAddress())	{
		$location1->setPhysicalAddress($physicalAddress); $enableUpdate = true;
	}
	if ($postalAddress != $location1->getPostalAddress())	{
		$location1->setPostalAddress($postalAddress); $enableUpdate = true;
	}
	if (! is_null($coordinate1) && (is_null($location1->getGPSCoordinate()) || (trim($location1->getGPSCoordinate()->getSystemCoordinate()) != trim($coordinate1->getSystemCoordinate()))))	{
		$location1->setGPSCoordinate($coordinate1->getSystemCoordinate()); $enableUpdate = true;
	}
	if (is_null($location1->getCountry()) || ($location1->getCountry()->getCountryId() != $countryId))	{
		$location1->setCountry($countryId); $enableUpdate = true;
	}
	if ($extraInformation != $location1->getExtraInformation())	{
		$location1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $location1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$location1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$location1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Location Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Location (Report) <i><?= $location1->getLocationName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Location <?= $location1->getLocationName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the location <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelocation_detail&id=<?= $location1->getLocationId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Details</a></div>
					Rule: managelocation_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managelocation_edit", "Edited ".$location1->getLocationName());
} else if ($page == "managelocation_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelocation_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$location1 = null;
	$extraFilter = System::getCodeString(8);
	$gpsCoordinate = "";
	try {
		$location1 = new Location($database, $_REQUEST['id'], $conn);
		if (! is_null($location1->getGPSCoordinate())) $gpsCoordinate = $location1->getGPSCoordinate()->getSystemCoordinate();
		$location1->setExtraFilter($extraFilter);
		$location1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Location <i><?= $location1->getLocationName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managelocation_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $location1->getLocationId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $location1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
						<div class="pure-control-group">
							<label for="locationName">Location Name </label>
							<input value="<?= $location1->getLocationName() ?>" type="text" name="locationName" id="locationName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Location Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="conferenceId">Conference</label>
							<select id="conferenceId" name="conferenceId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Conference">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Conference::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected = "";
		if (! is_null($location1->getConference()) && ($location1->getConference()->getConferenceId() == $alist['id'])) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="physicalAddress">Physical Address </label>
							<input value="<?= $location1->getPhysicalAddress() ?>" type="text" name="physicalAddress" id="physicalAddress" size="48" required pattern="<?= $exprA64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Physical Address : <?= $msgA64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="postalAddress">Postal Address </label>
							<input value="<?= $location1->getPostalAddress() ?>" type="text" name="postalAddress" id="postalAddress" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Postal Address : <?= $msgA64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="gpsCoordinate">GPS Coordinate </label>
							<input value="<?= $gpsCoordinate ?>" type="text" name="gpsCoordinate" id="gpsCoordinate" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprGPSCoordinate ?>%<?= $exprL72Name ?>" validate_message="<?= $msgGPSCoordinate ?>%GPS Coordinate [Length] : <?= $msgL72Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="countryId">Country</label>
							<select id="countryId" name="countryId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Country">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Country::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected = "";
		if (! is_null($location1->getCountry()) && ($location1->getCountry()->getCountryId() == $alist['id'])) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $location1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelocation_detail&id=<?= $location1->getLocationId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Details</a></div>
					Rule: managelocation_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managelocation_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelocation_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$location1 = null;
	try {
		$location1 = new Location($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for location : <i><?= $location1->getLocationName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Location Name</td>
								<td><?= $location1->getLocationName() ?></td>
							</tr>
<?php 
	if (! is_null($location1->getConference()))	{  
?>
		<tr><td>Conference</td><td><?= $location1->getConference()->getConferenceName() ?></td></tr>
<?php	
	}
	if (! is_null($location1->getPhysicalAddress()))	{
?>
		<tr><td>Physical Address</td><td><?= $location1->getPhysicalAddress() ?></td></tr>
<?php
	}
	if (! is_null($location1->getPostalAddress()))	{
?>
		<tr><td>Postal Address</td><td><?= $location1->getPostalAddress() ?></td></tr>
<?php
	}
	if (! is_null($location1->getGPSCoordinate()))	{
?>
		<tr><td>GPS Coordinate</td><td><?= $location1->getGPSCoordinate()->getSystemCoordinate() ?></td></tr>
<?php
	}
	if (! is_null($location1->getCountry()))	{
?>
		<tr><td>Country</td><td><?= $location1->getCountry()->getCountryName() ?></td></tr>
<?php
	}
	if (! is_null($location1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $location1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managelocation_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $location1->getLocationName() ?>" href="<?= $thispage ?>?page=managelocation_edit&id=<?= $location1->getLocationId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managelocation_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $location1->getLocationName() ?>" href="<?= $thispage ?>?page=managelocation_delete&id=<?= $location1->getLocationId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Management</a>
					</div>
					Rule: managelocation_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managelocation_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managelocation_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$locationName = mysql_real_escape_string($_REQUEST['locationName']);
	$conferenceId = mysql_real_escape_string($_REQUEST['conferenceId']);
	$physicalAddress = mysql_real_escape_string($_REQUEST['physicalAddress']);
	$postalAddress = mysql_real_escape_string($_REQUEST['postalAddress']);
	$gpsCoordinate = mysql_real_escape_string($_REQUEST['gpsCoordinate']);
	$countryId = mysql_real_escape_string($_REQUEST['countryId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Location (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO location (locationName, conferenceId, physicalAddress, postalAddress, gpsCoordinate, countryId, extraFilter, extraInformation) VALUES('$locationName', '$conferenceId', '$physicalAddress', '$postalAddress', '$gpsCoordinate', '$countryId', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Location[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Location <?= $locationName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the location<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Location";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managelocation_add&sysmenu=<?= SystemMenu::$__LOCATION ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Manager</a>
					</div>
					Rule: managelocation_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managelocation_add", "Added $locationName");
} else if ($page == "managelocation_add" && Authorize::isAllowable($config, "managelocation_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Location</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managelocation_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
						<div class="pure-control-group">
							<label for="locationName">Location Name </label>
							<input type="text" name="locationName" id="locationName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Location Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="conferenceId">Conference</label>
							<select id="conferenceId" name="conferenceId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Conference">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Conference::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="physicalAddress">Physical Address </label>
							<input type="text" name="physicalAddress" id="physicalAddress" size="48" required pattern="<?= $exprA64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Physical Address : <?= $msgA64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="postalAddress">Postal Address </label>
							<input type="text" name="postalAddress" id="postalAddress" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Postal Address : <?= $msgA64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="gpsCoordinate">GPS Coordinate </label>
							<input type="text" name="gpsCoordinate" id="gpsCoordinate" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprGPSCoordinate ?>%<?= $exprL72Name ?>" validate_message="<?= $msgGPSCoordinate ?>%GPS Coordinate [Length] : <?= $msgL72Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="countryId">Country</label>
							<select id="countryId" name="countryId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Country">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Country::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Location Management</a></div>
					Rule: managelocation_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managelocation" && Authorize::isAllowable($config, "managelocation", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Location Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-location-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managelocation&sysmenu=<?= SystemMenu::$__LOCATION ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managelocation_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Location</th>
				<th>Conference</th>
				<th>Phy Address</th>
				<th>Country</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Location::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch location data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$location1 = null;
		try {
			$location1 = new Location($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($location1->searchMatrix($matrix1)->evaluateResult())	{
			$conferenceName = ""; if (! is_null($location1->getConference())) $conferenceName = Object::summarizeString($location1->getConference()->getConferenceName(), 16);
			$countryName = ""; if (! is_null($location1->getCountry())) $countryName = Object::summarizeString($location1->getCountry()->getCountryName(), 16);
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $location1->getLocationName() ?></td>
				<td><?= $conferenceName ?></td>
				<td><?= Object::summarizeString($location1->getPhysicalAddress(), 12) ?></td>
				<td><?= $countryName ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managelocation_detail&id=<?= $location1->getLocationId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managelocation_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managelocation_csv&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managelocation_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Location" href="<?= $thispage ?>?page=managelocation_add&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managelocation</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: LocationManagement*/ else /*BEGIN: DepartmentManagement*/if ($page == "managedepartment_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managedepartment_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Department CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Management</a></div>
					Rule: managedepartment_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managedepartment_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managedepartment_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Department CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Department::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Department[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$department1 = null;
			try	{
				$department1 = new Department($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($department1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managedepartment_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managedepartment_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managedepartment_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managedepartment_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Department CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managedepartment_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managedepartment_csv&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managedepartment_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managedepartment_csv" && Authorize::isAllowable($config, "managedepartment_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Department CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managedepartment_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Department::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Managegement</a>
					</div>
					Rule: managedepartment_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managedepartment_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedepartment_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$department1 = null;
	$captionText = "Unknown";
	try {
		 $department1 = new Department($database, $_REQUEST['id'], $conn);
		 $captionText = $department1->getDepartmentName();
		 $department1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Department which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Department <?= $department1->getDepartmentName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Department <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Management</a>
					</div>
					Rule: managedepartment_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedepartment_delete", "Deleted ".$department1->getDepartmentName());
} else if ($page == "managedepartment_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedepartment_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$department1 = null;
	try {
		$department1 = new Department($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $department1->getDepartmentName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Department <b><?= $department1->getDepartmentName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Department <?= $department1->getDepartmentName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managedepartment_delete&id=<?= $department1->getDepartmentId() ?>&report=yes&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managedepartment_detail&id=<?= $department1->getDepartmentId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managedepartment_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managedepartment_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedepartment_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$department1 = null;
	try {
		$department1 = new Department($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$departmentName = mysql_real_escape_string($_REQUEST['departmentName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($departmentName != $department1->getDepartmentName())	{
		$department1->setDepartmentName($departmentName); $enableUpdate = true;
	}
	if ($extraInformation != $department1->getExtraInformation())	{
		$department1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $department1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$department1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$department1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Department Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Department (Report) <i><?= $department1->getDepartmentName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Department <?= $department1->getDepartmentName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the department <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedepartment_detail&id=<?= $department1->getDepartmentId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Details</a></div>
					Rule: managedepartment_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedepartment_edit", "Edited ".$department1->getDepartmentName());
} else if ($page == "managedepartment_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedepartment_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$department1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$department1 = new Department($database, $_REQUEST['id'], $conn);
		$department1->setExtraFilter($extraFilter);
		$department1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Department <i><?= $department1->getDepartmentName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managedepartment_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $department1->getDepartmentId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $department1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
						<div class="pure-control-group">
							<label for="departmentName">Department Name </label>
							<input value="<?= $department1->getDepartmentName() ?>" type="text" name="departmentName" id="departmentName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Department Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $department1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedepartment_detail&id=<?= $department1->getDepartmentId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Details</a></div>
					Rule: managedepartment_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managedepartment_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managedepartment_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$department1 = null;
	try {
		$department1 = new Department($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for department : <i><?= $department1->getDepartmentName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Department Name</td>
								<td><?= $department1->getDepartmentName() ?></td>
							</tr>
<?php 
	if (! is_null($department1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $department1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managedepartment_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $department1->getDepartmentName() ?>" href="<?= $thispage ?>?page=managedepartment_edit&id=<?= $department1->getDepartmentId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managedepartment_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $department1->getDepartmentName() ?>" href="<?= $thispage ?>?page=managedepartment_delete&id=<?= $department1->getDepartmentId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Management</a>
					</div>
					Rule: managedepartment_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managedepartment_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managedepartment_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$departmentName = mysql_real_escape_string($_REQUEST['departmentName']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Department (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO department (departmentName, extraFilter, extraInformation) VALUES('$departmentName', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Department[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Department <?= $departmentName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the department<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Department";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managedepartment_add&sysmenu=<?= SystemMenu::$__LOCATION ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Manager</a>
					</div>
					Rule: managedepartment_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managedepartment_add", "Added $departmentName");
} else if ($page == "managedepartment_add" && Authorize::isAllowable($config, "managedepartment_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Department</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managedepartment_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
						<div class="pure-control-group">
							<label for="departmentName">Department Name </label>
							<input type="text" name="departmentName" id="departmentName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Department Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Department Management</a></div>
					Rule: managedepartment_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managedepartment" && Authorize::isAllowable($config, "managedepartment", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Department Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-department-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managedepartment&sysmenu=<?= SystemMenu::$__LOCATION ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managedepartment_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Department Name</th>
				<th>Details</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Department::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch department data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$department1 = null;
		try {
			$department1 = new Department($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($department1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $department1->getDepartmentName() ?></td>
				<td><?= $department1->getExtraInformation() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managedepartment_detail&id=<?= $department1->getDepartmentId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managedepartment_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managedepartment_csv&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managedepartment_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Department" href="<?= $thispage ?>?page=managedepartment_add&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managedepartment</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: DepartmentManagement*/ else /*BEGIN: ConferenceManagement*/if ($page == "manageconference_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageconference_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Conference CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Management</a></div>
					Rule: manageconference_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageconference_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageconference_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Conference CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Conference::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Conference[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$conference1 = null;
			try	{
				$conference1 = new Conference($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($conference1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=manageconference_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: manageconference_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageconference_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "manageconference_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Conference CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="manageconference_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=manageconference_csv&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: manageconference_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageconference_csv" && Authorize::isAllowable($config, "manageconference_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Conference CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageconference_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Conference::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Managegement</a>
					</div>
					Rule: manageconference_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageconference_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageconference_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$conference1 = null;
	$captionText = "Unknown";
	try {
		 $conference1 = new Conference($database, $_REQUEST['id'], $conn);
		 $captionText = $conference1->getConferenceName();
		 $conference1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Conference which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Conference <?= $conference1->getConferenceName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Conference <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Management</a>
					</div>
					Rule: manageconference_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageconference_delete", "Deleted ".$conference1->getConferenceName());
} else if ($page == "manageconference_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageconference_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$conference1 = null;
	try {
		$conference1 = new Conference($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $conference1->getConferenceName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Conference <b><?= $conference1->getConferenceName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Conference <?= $conference1->getConferenceName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageconference_delete&id=<?= $conference1->getConferenceId() ?>&report=yes&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=manageconference_detail&id=<?= $conference1->getConferenceId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: manageconference_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageconference_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageconference_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$conference1 = null;
	try {
		$conference1 = new Conference($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$conferenceName = mysql_real_escape_string($_REQUEST['conferenceName']);
	$conferenceAbbreviation = mysql_real_escape_string($_REQUEST['conferenceAbbreviation']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($conferenceName != $conference1->getConferenceName())	{
		$conference1->setConferenceName($conferenceName); $enableUpdate = true;
	}
	if ($conferenceAbbreviation != $conference1->getConferenceAbbreviation())	{
		$conference1->setConferenceAbbreviation($conferenceAbbreviation); $enableUpdate = true;
	}
	if ($extraInformation != $conference1->getExtraInformation())	{
		$conference1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $conference1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$conference1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$conference1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Conference Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Conference (Report) <i><?= $conference1->getConferenceName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Conference <?= $conference1->getConferenceName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the conference <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageconference_detail&id=<?= $conference1->getConferenceId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Details</a></div>
					Rule: manageconference_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageconference_edit", "Edited ".$conference1->getConferenceName());
} else if ($page == "manageconference_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageconference_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$conference1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$conference1 = new Conference($database, $_REQUEST['id'], $conn);
		$conference1->setExtraFilter($extraFilter);
		$conference1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Conference <i><?= $conference1->getConferenceName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageconference_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $conference1->getConferenceId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $conference1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
						<div class="pure-control-group">
							<label for="conferenceName">Conference Name </label>
							<input value="<?= $conference1->getConferenceName() ?>" type="text" name="conferenceName" id="conferenceName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Conference Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="conferenceAbbreviation">Abbreviation </label>
							<input value="<?= $conference1->getConferenceAbbreviation() ?>" type="text" name="conferenceAbbreviation" id="conferenceAbbreviation" size="48" required pattern="<?= $exprA16Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA16Name ?>" validate_message="Conference Abbreviation : <?= $msgA16Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $conference1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageconference_detail&id=<?= $conference1->getConferenceId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Details</a></div>
					Rule: manageconference_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageconference_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageconference_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$conference1 = null;
	try {
		$conference1 = new Conference($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for conference : <i><?= $conference1->getConferenceName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Conference Name</td>
								<td><?= $conference1->getConferenceName() ?></td>
							</tr>
							<tr><td>Abbreviation</td><td><?= $conference1->getConferenceAbbreviation() ?></td></tr>
<?php 
	if (! is_null($conference1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $conference1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageconference_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $conference1->getConferenceName() ?>" href="<?= $thispage ?>?page=manageconference_edit&id=<?= $conference1->getConferenceId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageconference_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $conference1->getConferenceName() ?>" href="<?= $thispage ?>?page=manageconference_delete&id=<?= $conference1->getConferenceId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Management</a>
					</div>
					Rule: manageconference_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageconference_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "manageconference_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$conferenceName = mysql_real_escape_string($_REQUEST['conferenceName']);
	$conferenceAbbreviation = mysql_real_escape_string($_REQUEST['conferenceAbbreviation']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Conference (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO conference (conferenceName, conferenceAbbreviation, extraFilter, extraInformation) VALUES('$conferenceName', '$conferenceAbbreviation', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Conference[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Conference <?= $conferenceName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the conference<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Conference";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageconference_add&sysmenu=<?= SystemMenu::$__LOCATION ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Manager</a>
					</div>
					Rule: manageconference_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageconference_add", "Added $conferenceName");
} else if ($page == "manageconference_add" && Authorize::isAllowable($config, "manageconference_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Conference</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageconference_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__LOCATION ?>"/>
						<div class="pure-control-group">
							<label for="conferenceName">Conference Name </label>
							<input type="text" name="conferenceName" id="conferenceName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Conference Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="conferenceAbbreviation">Abbreviation </label>
							<input type="text" name="conferenceAbbreviation" id="conferenceAbbreviation" size="48" required pattern="<?= $exprA16Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA16Name ?>" validate_message="Conference Abbreviation : <?= $msgA16Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>">Back to Conference Management</a></div>
					Rule: manageconference_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageconference" && Authorize::isAllowable($config, "manageconference", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Conference Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-conference-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=manageconference&sysmenu=<?= SystemMenu::$__LOCATION ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "manageconference_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Conference Name</th>
				<th>Abbreviation</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Conference::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch conference data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$conference1 = null;
		try {
			$conference1 = new Conference($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($conference1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $conference1->getConferenceName() ?></td>
				<td><?= $conference1->getConferenceAbbreviation() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=manageconference_detail&id=<?= $conference1->getConferenceId() ?>&sysmenu=<?= SystemMenu::$__LOCATION ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageconference_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=manageconference_csv&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageconference_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Conference" href="<?= $thispage ?>?page=manageconference_add&sysmenu=<?= SystemMenu::$__LOCATION ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: manageconference</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: ConferenceManagement*/ else/*BEGIN: ProductManagement*/if ($page == "manageproduct_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageproduct_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Product CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Management</a></div>
					Rule: manageproduct_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageproduct_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageproduct_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Product CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Product::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Product[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$product1 = null;
			try	{
				$product1 = new Product($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($product1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=manageproduct_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: manageproduct_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageproduct_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "manageproduct_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Product CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="manageproduct_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=manageproduct_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: manageproduct_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageproduct_csv" && Authorize::isAllowable($config, "manageproduct_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Product CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageproduct_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Product::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Managegement</a>
					</div>
					Rule: manageproduct_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageproduct_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproduct_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$product1 = null;
	$captionText = "Unknown";
	try {
		 $product1 = new Product($database, $_REQUEST['id'], $conn);
		 $captionText = $product1->getProductName();
		 $product1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Product which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Product <?= $product1->getProductName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Product <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Management</a>
					</div>
					Rule: manageproduct_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageproduct_delete", "Deleted ".$product1->getProductName());
} else if ($page == "manageproduct_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproduct_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$product1 = null;
	try {
		$product1 = new Product($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $product1->getProductName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Product <b><?= $product1->getProductName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Product <?= $product1->getProductName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageproduct_delete&id=<?= $product1->getProductId() ?>&report=yes&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=manageproduct_detail&id=<?= $product1->getProductId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: manageproduct_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageproduct_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproduct_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$product1 = null;
	try {
		$product1 = new Product($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$productName = mysql_real_escape_string($_REQUEST['productName']);
	$categoryId = mysql_real_escape_string($_REQUEST['categoryId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($productName != $product1->getProductName())	{
		$product1->setProductName($productName); $enableUpdate = true;
	}
	if (is_null($product1->getCategory()) || ($product1->getCategory()->getCategoryId() != $categoryId))	{
		$product1->setCategory($categoryId); $enableUpdate = true;
	}
	if ($extraInformation != $product1->getExtraInformation())	{
		$product1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $product1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$product1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$product1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Product Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Product (Report) <i><?= $product1->getProductName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Product <?= $product1->getProductName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the product <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproduct_detail&id=<?= $product1->getProductId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Details</a></div>
					Rule: manageproduct_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageproduct_edit", "Edited ".$product1->getProductName());
} else if ($page == "manageproduct_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproduct_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$product1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$product1 = new Product($database, $_REQUEST['id'], $conn);
		$product1->setExtraFilter($extraFilter);
		$product1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Product <i><?= $product1->getProductName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageproduct_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $product1->getProductId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $product1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="productName">Product Name </label>
							<input value="<?= $product1->getProductName() ?>" type="text" name="productName" id="productName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Product Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="categoryId">Category</label>
							<select id="categoryId" name="categoryId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select an Product Category">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = ProductCategory::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected = "";
		if (! is_null($product1->getCategory()) && ($product1->getCategory()->getCategoryId() == $alist['id'])) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $product1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproduct_detail&id=<?= $product1->getProductId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Details</a></div>
					Rule: manageproduct_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageproduct_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproduct_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$product1 = null;
	try {
		$product1 = new Product($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for product : <i><?= $product1->getProductName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Product Name</td>
								<td><?= $product1->getProductName() ?></td>
							</tr>
<?php
	if (! is_null($product1->getCategory()))	{
?>
		<tr><td>Category</td><td><?= $product1->getCategory()->getCategoryName() ?></td></tr>
<?php
	}
	if (! is_null($product1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $product1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageproduct_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $product1->getProductName() ?>" href="<?= $thispage ?>?page=manageproduct_edit&id=<?= $product1->getProductId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageproduct_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $product1->getProductName() ?>" href="<?= $thispage ?>?page=manageproduct_delete&id=<?= $product1->getProductId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Management</a>
					</div>
					Rule: manageproduct_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageproduct_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "manageproduct_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$productName = mysql_real_escape_string($_REQUEST['productName']);
	$categoryId = mysql_real_escape_string($_REQUEST['categoryId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Product (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO product (productName, categoryId, extraFilter, extraInformation) VALUES('$productName', '$categoryId', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Product[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Product <?= $productName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the product<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Product";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageproduct_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Manager</a>
					</div>
					Rule: manageproduct_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageproduct_add", "Added $productName");
} else if ($page == "manageproduct_add" && Authorize::isAllowable($config, "manageproduct_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Product</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageproduct_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="productName">Product Name </label>
							<input type="text" name="productName" id="productName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Product Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="categoryId">Category</label>
							<select id="categoryId" name="categoryId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select an Product Category">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = ProductCategory::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Product Management</a></div>
					Rule: manageproduct_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageproduct" && Authorize::isAllowable($config, "manageproduct", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Product Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-product-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=manageproduct&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "manageproduct_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Product Name</th>
				<th>Category</th>
				<th>Details</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Product::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch product data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$product1 = null;
		try {
			$product1 = new Product($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($product1->searchMatrix($matrix1)->evaluateResult())	{
			$categoryName = ""; if (! is_null($product1->getCategory())) $categoryName = $product1->getCategory()->getCategoryName();
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $product1->getProductName() ?></td>
				<td><?= $categoryName ?></td>
				<td><?= $product1->getExtraInformation() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=manageproduct_detail&id=<?= $product1->getProductId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageproduct_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=manageproduct_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageproduct_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Product" href="<?= $thispage ?>?page=manageproduct_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: manageproduct</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: ProductManagement*/ else /*BEGIN: ProductCategoryManagement*/if ($page == "manageproductcategory_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageproductcategory_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ProductCategory CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Management</a></div>
					Rule: manageproductcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageproductcategory_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageproductcategory_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ProductCategory CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = ProductCategory::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("ProductCategory[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$category1 = null;
			try	{
				$category1 = new ProductCategory($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($category1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=manageproductcategory_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: manageproductcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageproductcategory_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "manageproductcategory_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ProductCategory CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="manageproductcategory_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=manageproductcategory_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: manageproductcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageproductcategory_csv" && Authorize::isAllowable($config, "manageproductcategory_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ProductCategory CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageproductcategory_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, ProductCategory::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Managegement</a>
					</div>
					Rule: manageproductcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageproductcategory_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproductcategory_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$category1 = null;
	$captionText = "Unknown";
	try {
		 $category1 = new ProductCategory($database, $_REQUEST['id'], $conn);
		 $captionText = $category1->getcategoryName();
		 $category1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a ProductCategory which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The ProductCategory <?= $category1->getcategoryName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the ProductCategory <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Management</a>
					</div>
					Rule: manageproductcategory_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageproductcategory_delete", "Deleted ".$category1->getcategoryName());
} else if ($page == "manageproductcategory_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproductcategory_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$category1 = null;
	try {
		$category1 = new ProductCategory($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a ProductCategory <b><?= $category1->getcategoryName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the ProductCategory <?= $category1->getcategoryName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageproductcategory_delete&id=<?= $category1->getCategoryId() ?>&report=yes&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=manageproductcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: manageproductcategory_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageproductcategory_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproductcategory_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$category1 = null;
	try {
		$category1 = new ProductCategory($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$categoryName = mysql_real_escape_string($_REQUEST['categoryName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($categoryName != $category1->getcategoryName())	{
		$category1->setcategoryName($categoryName); $enableUpdate = true;
	}
	if ($extraInformation != $category1->getExtraInformation())	{
		$category1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $category1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$category1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$category1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for ProductCategory Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit ProductCategory (Report) <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			ProductCategory <?= $category1->getcategoryName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the category <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproductcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Details</a></div>
					Rule: manageproductcategory_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageproductcategory_edit", "Edited ".$category1->getcategoryName());
} else if ($page == "manageproductcategory_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproductcategory_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$category1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$category1 = new ProductCategory($database, $_REQUEST['id'], $conn);
		$category1->setExtraFilter($extraFilter);
		$category1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit ProductCategory <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageproductcategory_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $category1->getCategoryId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $category1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="categoryName">ProductCategory Name </label>
							<input value="<?= $category1->getcategoryName() ?>" type="text" name="categoryName" id="categoryName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="ProductCategory Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $category1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproductcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Details</a></div>
					Rule: manageproductcategory_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageproductcategory_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageproductcategory_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$category1 = null;
	try {
		$category1 = new ProductCategory($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for category : <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>ProductCategory Name</td>
								<td><?= $category1->getcategoryName() ?></td>
							</tr>
<?php 
	if (! is_null($category1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $category1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageproductcategory_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $category1->getCategoryName() ?>" href="<?= $thispage ?>?page=manageproductcategory_edit&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageproductcategory_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $category1->getcategoryName() ?>" href="<?= $thispage ?>?page=manageproductcategory_delete&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Management</a>
					</div>
					Rule: manageproductcategory_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageproductcategory_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "manageproductcategory_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$categoryName = mysql_real_escape_string($_REQUEST['categoryName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New ProductCategory (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO productCategory (categoryName, extraFilter, extraInformation) VALUES('$categoryName', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("ProductCategory[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The ProductCategory <?= $categoryName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the category<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another ProductCategory";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageproductcategory_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Manager</a>
					</div>
					Rule: manageproductcategory_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageproductcategory_add", "Added $categoryName");
} else if ($page == "manageproductcategory_add" && Authorize::isAllowable($config, "manageproductcategory_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New ProductCategory</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageproductcategory_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="categoryName">Category Name </label>
							<input type="text" name="categoryName" id="categoryName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="ProductCategory Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ProductCategory Management</a></div>
					Rule: manageproductcategory_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageproductcategory" && Authorize::isAllowable($config, "manageproductcategory", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ProductCategory Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-category-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=manageproductcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "manageproductcategory_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Category Name</th>
				<th>Extra Information</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = ProductCategory::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch category data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$category1 = null;
		try {
			$category1 = new ProductCategory($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($category1->searchMatrix($matrix1)->evaluateResult())	{
				if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $category1->getCategoryName() ?></td>
				<td><?= $category1->getExtraInformation() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=manageproductcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageproductcategory_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=manageproductcategory_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageproductcategory_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System ProductCategory" href="<?= $thispage ?>?page=manageproductcategory_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: manageproductcategory</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: ProductCategoryManagement*/ else /*BEGIN: ItemManagement*/if ($page == "manageitem_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageitem_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Item CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Management</a></div>
					Rule: manageitem_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageitem_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageitem_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Item CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Item::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Item[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$item1 = null;
			try	{
				$item1 = new Item($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($item1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=manageitem_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: manageitem_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageitem_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "manageitem_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Item CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="manageitem_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=manageitem_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: manageitem_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageitem_csv" && Authorize::isAllowable($config, "manageitem_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Item CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageitem_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Item::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Managegement</a>
					</div>
					Rule: manageitem_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageitem_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitem_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$item1 = null;
	$captionText = "Unknown";
	try {
		 $item1 = new Item($database, $_REQUEST['id'], $conn);
		 $captionText = $item1->getItemName();
		 $item1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Item which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Item <?= $item1->getItemName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Item <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Management</a>
					</div>
					Rule: manageitem_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageitem_delete", "Deleted ".$item1->getItemName());
} else if ($page == "manageitem_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitem_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$item1 = null;
	try {
		$item1 = new Item($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $item1->getItemName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Item <b><?= $item1->getItemName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Item <?= $item1->getItemName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageitem_delete&id=<?= $item1->getItemId() ?>&report=yes&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=manageitem_detail&id=<?= $item1->getItemId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: manageitem_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageitem_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitem_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$item1 = null;
	try {
		$item1 = new Item($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$itemName = mysql_real_escape_string($_REQUEST['itemName']);
	$categoryId = mysql_real_escape_string($_REQUEST['categoryId']);
	$measureId = mysql_real_escape_string($_REQUEST['measureId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($itemName != $item1->getItemName())	{
		$item1->setItemName($itemName); $enableUpdate = true;
	}
	if (is_null($item1->getCategory()) || ($categoryId != $item1->getCategory()->getCategoryId()))	{
		$item1->setCategory($categoryId); $enableUpdate = true;
	}
	if (is_null($item1->getMeasure()) || ($measureId != $item1->getMeasure()->getMeasureId()))	{
		$item1->setMeasure($measureId); $enableUpdate = true;
	}
	if ($extraInformation != $item1->getExtraInformation())	{
		$item1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $item1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$item1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$item1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Item Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Item (Report) <i><?= $item1->getItemName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Item <?= $item1->getItemName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the item <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitem_detail&id=<?= $item1->getItemId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Details</a></div>
					Rule: manageitem_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageitem_edit", "Edited ".$item1->getItemName());
} else if ($page == "manageitem_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitem_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$item1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$item1 = new Item($database, $_REQUEST['id'], $conn);
		$item1->setExtraFilter($extraFilter);
		$item1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Item <i><?= $item1->getItemName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageitem_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $item1->getItemId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $item1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="itemName">Item Name </label>
							<input value="<?= $item1->getItemName() ?>" type="text" name="itemName" id="itemName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Item Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="categoryId">Category</label>
							<select id="categoryId" name="categoryId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select an Item Category">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = ItemCategory::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected = "";
		if (! is_null($item1->getCategory()) && ($item1->getCategory()->getCategoryId() == $alist['id'])) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="measureId">Unit of Measurement</label>
							<select id="measureId" name="measureId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Unit of Measurement">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Measure::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected = "";
		if (! is_null($item1->getMeasure()) && ($item1->getMeasure()->getMeasureId() == $alist['id'])) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $item1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitem_detail&id=<?= $item1->getItemId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Details</a></div>
					Rule: manageitem_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageitem_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitem_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$item1 = null;
	try {
		$item1 = new Item($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for item : <i><?= $item1->getItemName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Item Name</td>
								<td><?= $item1->getItemName() ?></td>
							</tr>
<?php 
	if (! is_null($item1->getCategory()))	{
?>
					<tr>
						<td>Category</td>
						<td><?= $item1->getCategory()->getCategoryName() ?></td>
					</tr>
<?php	
	}
	if (! is_null($item1->getMeasure()))	{
?>
					<tr>
						<td>Unit of Measure</td>
						<td><?= $item1->getMeasure()->getMeasureName() ?></td>
					</tr>
<?php
	}
	if (! is_null($item1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $item1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageitem_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $item1->getItemName() ?>" href="<?= $thispage ?>?page=manageitem_edit&id=<?= $item1->getItemId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageitem_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $item1->getItemName() ?>" href="<?= $thispage ?>?page=manageitem_delete&id=<?= $item1->getItemId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Management</a>
					</div>
					Rule: manageitem_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageitem_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "manageitem_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$itemName = mysql_real_escape_string($_REQUEST['itemName']);
		$categoryId = mysql_real_escape_string($_REQUEST['categoryId']);
		$measureId = mysql_real_escape_string($_REQUEST['measureId']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Item (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO item (itemName, measureId, categoryId, extraFilter, extraInformation) VALUES('$itemName', '$measureId', '$categoryId', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Item[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Item <?= $itemName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the item<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Item";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageitem_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Manager</a>
					</div>
					Rule: manageitem_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageitem_add", "Added $itemName");
} else if ($page == "manageitem_add" && Authorize::isAllowable($config, "manageitem_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Item</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageitem_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="itemName">Item Name </label>
							<input type="text" name="itemName" id="itemName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Item Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="categoryId">Category</label>
							<select id="categoryId" name="categoryId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select an Item Category">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = ItemCategory::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="measureId">Unit of Measurement</label>
							<select id="measureId" name="measureId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Unit of Measurement">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Measure::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Item Management</a></div>
					Rule: manageitem_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageitem" && Authorize::isAllowable($config, "manageitem", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Item Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-item-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=manageitem&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "manageitem_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Item Name</th>
				<th>Category</th>
				<th>Unit of Measure</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Item::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch item data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$item1 = null;
		try {
			$item1 = new Item($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($item1->searchMatrix($matrix1)->evaluateResult())	{
			$categoryName = ""; if (! is_null($item1->getCategory())) $categoryName = $item1->getCategory()->getCategoryName();
			$measureName = ""; if (! is_null($item1->getMeasure())) $measureName = $item1->getMeasure()->getMeasureName();
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $item1->getItemName() ?></td>
				<td><?= $categoryName ?></td>
				<td><?= $measureName ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=manageitem_detail&id=<?= $item1->getItemId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageitem_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=manageitem_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageitem_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Item" href="<?= $thispage ?>?page=manageitem_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: manageitem</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: ItemManagement*/ else /*BEGIN: ItemCategoryManagement*/if ($page == "manageitemcategory_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageitemcategory_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ItemCategory CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Management</a></div>
					Rule: manageitemcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageitemcategory_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "manageitemcategory_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ItemCategory CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = ItemCategory::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("ItemCategory[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$category1 = null;
			try	{
				$category1 = new ItemCategory($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($category1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=manageitemcategory_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: manageitemcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageitemcategory_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "manageitemcategory_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ItemCategory CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="manageitemcategory_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=manageitemcategory_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: manageitemcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageitemcategory_csv" && Authorize::isAllowable($config, "manageitemcategory_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ItemCategory CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageitemcategory_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, ItemCategory::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Managegement</a>
					</div>
					Rule: manageitemcategory_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "manageitemcategory_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitemcategory_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$category1 = null;
	$captionText = "Unknown";
	try {
		 $category1 = new ItemCategory($database, $_REQUEST['id'], $conn);
		 $captionText = $category1->getcategoryName();
		 $category1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a ItemCategory which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The ItemCategory <?= $category1->getcategoryName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the ItemCategory <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Management</a>
					</div>
					Rule: manageitemcategory_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageitemcategory_delete", "Deleted ".$category1->getcategoryName());
} else if ($page == "manageitemcategory_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitemcategory_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$category1 = null;
	try {
		$category1 = new ItemCategory($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a ItemCategory <b><?= $category1->getcategoryName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the ItemCategory <?= $category1->getcategoryName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageitemcategory_delete&id=<?= $category1->getCategoryId() ?>&report=yes&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=manageitemcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: manageitemcategory_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "manageitemcategory_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitemcategory_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$category1 = null;
	try {
		$category1 = new ItemCategory($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$categoryName = mysql_real_escape_string($_REQUEST['categoryName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($categoryName != $category1->getcategoryName())	{
		$category1->setcategoryName($categoryName); $enableUpdate = true;
	}
	if ($extraInformation != $category1->getExtraInformation())	{
		$category1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $category1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$category1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$category1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for ItemCategory Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit ItemCategory (Report) <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			ItemCategory <?= $category1->getcategoryName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the category <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitemcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Details</a></div>
					Rule: manageitemcategory_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageitemcategory_edit", "Edited ".$category1->getcategoryName());
} else if ($page == "manageitemcategory_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitemcategory_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$category1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$category1 = new ItemCategory($database, $_REQUEST['id'], $conn);
		$category1->setExtraFilter($extraFilter);
		$category1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit ItemCategory <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageitemcategory_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $category1->getCategoryId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $category1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="categoryName">ItemCategory Name </label>
							<input value="<?= $category1->getcategoryName() ?>" type="text" name="categoryName" id="categoryName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="ItemCategory Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $category1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitemcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Details</a></div>
					Rule: manageitemcategory_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageitemcategory_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "manageitemcategory_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$category1 = null;
	try {
		$category1 = new ItemCategory($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for category : <i><?= $category1->getcategoryName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>ItemCategory Name</td>
								<td><?= $category1->getcategoryName() ?></td>
							</tr>
<?php 
	if (! is_null($category1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $category1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageitemcategory_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $category1->getCategoryName() ?>" href="<?= $thispage ?>?page=manageitemcategory_edit&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageitemcategory_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $category1->getcategoryName() ?>" href="<?= $thispage ?>?page=manageitemcategory_delete&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Management</a>
					</div>
					Rule: manageitemcategory_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "manageitemcategory_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "manageitemcategory_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$categoryName = mysql_real_escape_string($_REQUEST['categoryName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New ItemCategory (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO itemCategory (categoryName, extraFilter, extraInformation) VALUES('$categoryName', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("ItemCategory[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The ItemCategory <?= $categoryName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the category<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another ItemCategory";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageitemcategory_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Manager</a>
					</div>
					Rule: manageitemcategory_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageitemcategory_add", "Added $categoryName");
} else if ($page == "manageitemcategory_add" && Authorize::isAllowable($config, "manageitemcategory_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New ItemCategory</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="manageitemcategory_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="categoryName">Category Name </label>
							<input type="text" name="categoryName" id="categoryName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="ItemCategory Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to ItemCategory Management</a></div>
					Rule: manageitemcategory_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "manageitemcategory" && Authorize::isAllowable($config, "manageitemcategory", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">ItemCategory Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-category-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=manageitemcategory&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "manageitemcategory_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Category Name</th>
				<th>Extra Information</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = ItemCategory::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch category data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$category1 = null;
		try {
			$category1 = new ItemCategory($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($category1->searchMatrix($matrix1)->evaluateResult())	{
				if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $category1->getCategoryName() ?></td>
				<td><?= $category1->getExtraInformation() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=manageitemcategory_detail&id=<?= $category1->getCategoryId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "manageitemcategory_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=manageitemcategory_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "manageitemcategory_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System ItemCategory" href="<?= $thispage ?>?page=manageitemcategory_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: manageitemcategory</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: ItemCategoryManagement*/ else /*BEGIN: MeasureManagement*/if ($page == "managemeasure_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managemeasure_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Measure CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Management</a></div>
					Rule: managemeasure_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managemeasure_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managemeasure_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Measure CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Measure::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Measure[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$measure1 = null;
			try	{
				$measure1 = new Measure($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($measure1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managemeasure_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managemeasure_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managemeasure_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managemeasure_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Measure CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managemeasure_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managemeasure_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managemeasure_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managemeasure_csv" && Authorize::isAllowable($config, "managemeasure_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Measure CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managemeasure_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Measure::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Managegement</a>
					</div>
					Rule: managemeasure_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managemeasure_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managemeasure_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$measure1 = null;
	$captionText = "Unknown";
	try {
		 $measure1 = new Measure($database, $_REQUEST['id'], $conn);
		 $captionText = $measure1->getMeasureName();
		 $measure1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Measure which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Measure <?= $measure1->getMeasureName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Measure <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Management</a>
					</div>
					Rule: managemeasure_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managemeasure_delete", "Deleted ".$measure1->getMeasureName());
} else if ($page == "managemeasure_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managemeasure_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$measure1 = null;
	try {
		$measure1 = new Measure($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $measure1->getMeasureName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Measure <b><?= $measure1->getMeasureName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Measure <?= $measure1->getMeasureName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managemeasure_delete&id=<?= $measure1->getMeasureId() ?>&report=yes&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managemeasure_detail&id=<?= $measure1->getMeasureId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managemeasure_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managemeasure_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managemeasure_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$measure1 = null;
	try {
		$measure1 = new Measure($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$measureName = mysql_real_escape_string($_REQUEST['measureName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	$isLoopDetected = false;
	if ($measureName != $measure1->getMeasureName())	{
		$measure1->setMeasureName($measureName); $enableUpdate = true;
	}
	if ($extraInformation != $measure1->getExtraInformation())	{
		$measure1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $measure1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$measure1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$measure1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Measure Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Measure (Report) <i><?= $measure1->getMeasureName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Measure <?= $measure1->getMeasureName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the measure <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managemeasure_detail&id=<?= $measure1->getMeasureId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Details</a></div>
					Rule: managemeasure_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managemeasure_edit", "Edited ".$measure1->getMeasureName());
} else if ($page == "managemeasure_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managemeasure_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$measure1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$measure1 = new Measure($database, $_REQUEST['id'], $conn);
		$measure1->setExtraFilter($extraFilter);
		$measure1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Measure <i><?= $measure1->getMeasureName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managemeasure_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $measure1->getMeasureId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $measure1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="measureName">Measure Name </label>
							<input value="<?= $measure1->getMeasureName() ?>" type="text" name="measureName" id="measureName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Measure Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $measure1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managemeasure_detail&id=<?= $measure1->getMeasureId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Details</a></div>
					Rule: managemeasure_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managemeasure_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managemeasure_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$measure1 = null;
	try {
		$measure1 = new Measure($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for measure : <i><?= $measure1->getMeasureName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Measure Name</td>
								<td><?= $measure1->getMeasureName() ?></td>
							</tr>
<?php
	if (! is_null($measure1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $measure1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managemeasure_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $measure1->getMeasureName() ?>" href="<?= $thispage ?>?page=managemeasure_edit&id=<?= $measure1->getMeasureId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managemeasure_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $measure1->getMeasureName() ?>" href="<?= $thispage ?>?page=managemeasure_delete&id=<?= $measure1->getMeasureId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Management</a>
					</div>
					Rule: managemeasure_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managemeasure_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managemeasure_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$measureName = mysql_real_escape_string($_REQUEST['measureName']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Measure (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO measure (measureName, extraFilter, extraInformation) VALUES('$measureName', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Measure[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Measure <?= $measureName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the measure<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Measure";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managemeasure_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Manager</a>
					</div>
					Rule: managemeasure_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managemeasure_add", "Added $measureName");
} else if ($page == "managemeasure_add" && Authorize::isAllowable($config, "managemeasure_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Measure</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managemeasure_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__INVENTORYRESOURCES ?>"/>
						<div class="pure-control-group">
							<label for="measureName">Measure Name </label>
							<input type="text" name="measureName" id="measureName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Measure Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>">Back to Measure Management</a></div>
					Rule: managemeasure_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managemeasure" && Authorize::isAllowable($config, "managemeasure", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Measure Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-measure-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managemeasure&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managemeasure_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Measure Name</th>
				<th>Extra Information</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Measure::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch measure data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$measure1 = null;
		try {
			$measure1 = new Measure($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($measure1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $measure1->getMeasureName() ?></td>
				<td><?= $measure1->getExtraInformation() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managemeasure_detail&id=<?= $measure1->getMeasureId() ?>&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managemeasure_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managemeasure_csv&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managemeasure_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Measure" href="<?= $thispage ?>?page=managemeasure_add&sysmenu=<?= SystemMenu::$__INVENTORYRESOURCES ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managemeasure</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: MeasureManagement*/ else/*BEGIN: SystemFirewall*/if ($page == "managesystemfirewall" && isset($_REQUEST['rndx']) && isset($_REQUEST['command']) && $login1->isRoot() && isset($_REQUEST['report']) && isset($_REQUEST['type']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managesystemfirewall", "normal", "setlog", "-1", "-1"))	{
	$type=$_REQUEST['type'];
	$object1 = null;
	$objectName = "";
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$enableUpdate = false;
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	if ($type == "login")	{
		$object1 = new Login($database, $_REQUEST['id'], $conn);
		$objectName = $object1->getFullname();
	} else if ($type == "group")	{
		$object1 = new Group($database, $_REQUEST['id'], $conn);
		$objectName = $object1->getGroupName();
	} else if ($type == "jobtitle")	{
		$object1 = new JobTitle($database, $_REQUEST['id'], $conn);
		$objectName = $object1->getJobName();
	} else {
		$promise1->setReason("Perhaps the system has received a wrong Instruction OR invalid Object Type");
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
		//Doing Actual Processing 
		$context = null;
		$command = intval($_REQUEST['command']);
		switch ($command)	{
			case ContextPosition::$__ALLOW_ALL:
				$context = Authorize::buildAllContextStringTo($database, $conn, $object1->getContext(), ContextPosition::$__ALLOW);
				break;
			case ContextPosition::$__DENY_ALL:
				$context = Authorize::buildAllContextStringTo($database, $conn, $object1->getContext(), ContextPosition::$__DENY);
				break;
			case ContextPosition::$__DONOTCARE_ALL:
				$context = Authorize::buildAllContextStringTo($database, $conn, $object1->getContext(), ContextPosition::$__DONOTCARE);
				break;
			case ContextPosition::$__CUSTOMIZE:
				$context = Authorize::buildTheEntireContextString($database, $conn, $object1->getContext(), $_REQUEST['position']);
				break;
			default: $promise1->setReason("Could not Intepret the command");
				$promise1->setPromise(false);
		}
		try {
			if (trim($_REQUEST['rndx']) != $object1->getExtraFilter()) Object::shootException("The System has Detected you are replaying data in your Browser window");
			if (! $promise1->isPromising()) Object::shootException($promise1->getReason());
			if (is_null($context)) Object::shootException("Could not calculate the Required Context Value");
			$object1->setContext($context);
			$object1->setExtraFilter(System::getCodeString(8));
			$object1->commitUpdate();
			$enableUpdate = true;
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">System Firewall for <i><?= $objectName ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Congratulations, Firewall for <i><?= $objectName ?></i> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in updating firewall for <i><?= $objectName ?></i> <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managesystemfirewall&type=<?= $_REQUEST['type'] ?>&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to System Firewall for <?= $objectName ?> (<?= $_REQUEST['type'] ?>)</a>
					</div>
					Rule: managesystemfirewall
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managesystemfirewall", "Changed for $objectName ($type)");
} else if ($page == "managesystemfirewall" && $login1->isRoot() && isset($_REQUEST['type']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managesystemfirewall", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$type = trim($_REQUEST['type']);
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$object1 = null;
	$objectName = "";
	$contextString1 = "";
	if ($type == "login")	{
		try {
			$object1 = new Login($database, $_REQUEST['id'], $conn);
			$objectName = $object1->getFullname();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	} else if ($type == "group")	{
		try {
			$object1 = new Group($database, $_REQUEST['id'], $conn);
			$objectName = $object1->getGroupName();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	} else if ($type == "jobtitle")	{
		 try {
			$object1 = new JobTitle($database, $_REQUEST['id'], $conn);
			$objectName = $object1->getJobName();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	} else {
		$promise1->setReason("The type Object could not be Identified");
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
		$contextString1 = $object1->getContext();
		$object1->setExtraFilter(System::getCodeString(8));
		try {
			$object1->commitUpdate();
		} catch (Exception $e)	{
			$promise1->setReason("Can not set filter, ".$e->getMessage());
			$promise1->setPromise(false);
		}
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">System Firewall for <?= $objectName ?></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<!--Begin Search Box-->
		<div class="pure-form pure-group-control ui-sys-search-container">
			<input title="Atleast Three Characters should be supplied" type="text" required placeholder="ABC" size="32" />
			<a title="Click To Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managesystemfirewall&type=<?= $_REQUEST['type'] ?>&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
		</div>
		<!--End Search Box-->
		<!--Begin Box with Allow/Deny Do Not Care All-->
		<div class="ui-sys-firewall-all">
			<span><a href="<?= $thispage ?>?page=managesystemfirewall&type=<?= $_REQUEST['type'] ?>&id=<?= $_REQUEST['id'] ?>&report=kdiie&command=<?= ContextPosition::$__ALLOW_ALL ?>&rndx=<?= $object1->getExtraFilter() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><input type="button" value="ALLOW ALL"/></a></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><a href="<?= $thispage ?>?page=managesystemfirewall&type=<?= $_REQUEST['type'] ?>&id=<?= $_REQUEST['id'] ?>&report=kdiie&command=<?= ContextPosition::$__DENY_ALL ?>&rndx=<?= $object1->getExtraFilter() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><input type="button" value="DENY ALL"/></a></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><a href="<?= $thispage ?>?page=managesystemfirewall&type=<?= $_REQUEST['type'] ?>&id=<?= $_REQUEST['id'] ?>&report=kdiie&command=<?= ContextPosition::$__DONOTCARE_ALL ?>&rndx=<?= $object1->getExtraFilter() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><input type="button" value="DO NOT CARE ALL"/></a>
		</div>

		<!--End Box with Allow/Deny Do Not Care All-->
		<!--Begin Another Block with Search Results-->
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		$query = ContextPosition::getQueryText();
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$result = mysql_db_query($database, $query, $conn) or die("Could not connect execute Context Query");
?>
		<!--Block Two Begins: with search results-->
		<div class="ui-sys-search-results">
			<label id="statustextlabel"></label>
				<form method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managesystemfirewall"/>
					<input type="hidden" name="command" value="<?= ContextPosition::$__CUSTOMIZE ?>"/>
					<input type="hidden" name="rndx" value="<?= $object1->getExtraFilter() ?>"/>
					<input type="hidden" name="id" value="<?= $object1->getId() ?>"/>
					<input type="hidden" name="type" value="<?= $_REQUEST['type'] ?>"/>
					<input type="hidden" name="report" value="io"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
			<table class="pure-table ui-sys-table-search-results ui-sys-context-table">
		<thead>
			<tr>
				<th></th>
				<th>Caption</th>
				<th>Allow</th>
				<th>Deny</th>
				<th>Do Not Care</th>
			</tr>
		</thead>
<?php 
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$context1 = null;
		try {
			$context1 = new ContextPosition($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$pos = $context1->getCharacterPosition();
		$char1 = Authorize::getContextCharacter($database, $conn, $contextString1, $pos);
		//Marker for display
		$action="";
		$chkAllow="";
		$chkDeny="";
		$chkDoNotCare="";
		if ($char1 == ContextPosition::$__DONOTCARE)	{
			$action="donotcare";
			$chkDoNotCare="Checked";
		} else if ($char1 == ContextPosition::$__DENY)	{
			$action="deny";
			$chkDeny="checked";
		} else if ($char1 == ContextPosition::$__ALLOW)	{
			$action="allow";
			$chkAllow="checked";
		} else {
			die("Could not interpret the Instruction");
		}//end-if-else 
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($context1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			$disabled = "";
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?> ui-sys-context-data-row" data-context-id="<?= $context1->getContextId() ?>" data-context-position="<?= $pos ?>" data-context-character="<?= $char1 ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $context1->getContextCaption() ?></td>
				<td><label><input <?= $chkAllow ?> value="<?= ContextPosition::$__ALLOW ?>" class="ui-sys-control-radio control-radio-allow" type="radio" name="position[<?= $context1->getCharacterPosition() ?>]"/>Allow</label></td>
				<td><label><input <?= $chkDeny ?> value="<?= ContextPosition::$__DENY ?>" class="ui-sys-control-radio control-radio-deny" type="radio" name="position[<?= $context1->getCharacterPosition() ?>]"/>Deny</label></td>
				<td><label><input <?= $chkDoNotCare ?> value="<?= ContextPosition::$__DONOTCARE ?>" class="ui-sys-control-radio control-radio-donotcare" type="radio" name="position[<?= $context1->getCharacterPosition() ?>]"/>Do Not Care</label></td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
?>
	</table>
			<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
			<div class="ui-sys-right">
				<input type="submit" title="Save the Customized Firewall Roles for <?= $objectName ?>" value="Save Firewall Rules"/>
			</div>
		</form>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
		</div>
		<!--Block Two Ends: with search result-->
<?php
		mysql_close($conn);
	} //end-if-search-text
?>		
		<!--End Another Block with Search Results-->
		<div id="perror" class="ui-sys-error-message"></div>
<?php
	} else {
?>
		<div class="ui-state-error">
			Could not load the reference Object, perhaps wrong Instruction<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>			
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="ui-sys-back" href="<?= $thispage ?>?page=manage<?= $_REQUEST['type'] ?>_detail&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to <?= Object::getCaptionLabelFromPropertyName($_REQUEST['type']) ?> Manager (Details)</a>
					</div>
					Rule: managesystemfirewall
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: SystemFirewall*/ else /*BEGIN: SystemFirewallGraph*/if ($page == "managesystemfirewall_graph" && $login1->isRoot() && isset($_REQUEST['searchtext']) && isset($_REQUEST['type']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managesystemfirewall_graph", "normal", "setlog", "-1", "-1"))	{
	$type = $_REQUEST['type'];
	$searchtext = $_REQUEST['searchtext'];
	$object1 = null;
	$objectText = "UNKNOWN";
	$targetObjectName = "UNKNOWN";
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	if ($type == "login")	{
		try {
			$object1 = new Login($database, $_REQUEST['id'], $conn);
			$targetObjectName = $object1->getClassName();
			$targetText = $object1->getFullname();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	} else if ($type == "group")	{
		try {
			$object1 = new Group($database, $_REQUEST['id'], $conn);
			$targetObjectName = $object1->getClassName();
			$targetText = $object1->getGroupName();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	} else {
		$promise1->setReason("The System has received an Invalid Instruction, the Object Type could not be Identified");
		$promise1->setPromise(false);
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Authorization Graph for <?= $targetText ?> (<?= $targetObjectName ?>)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising() && ($object1->getClassName() == "Login") && $object1->isRoot())	{
?>
		<div class="ui-state-highlight">
			This is a SUPER USER, This User can perform ALL Operations in the System<br/>
			This User is ABOVE All the Firewall Rules
		</div>
<?php
	} else if ($promise1->isPromising())	{
		//General Non Root 
		$ds1 = Authorize::getAuthorizationGraphDataStructure($database, $conn, $object1, $type, $searchtext);
		if (! is_null($ds1))	{
			$numberOfColumns = sizeof($ds1['header']);
?>
			<table class="pure-table ui-sys-graph-drawable ui-sys-table-search-results">
				<thead>
					<tr>
						<th></th>
<?php 
	foreach ($ds1['header'] as $aheader)	{
?>
		<th><?= $aheader ?></th>
<?php
	}
?>
					</tr>
					<tr>
						<th></th>
<?php 
	foreach ($ds1['caption'] as $acaption)	{
?>
		<th><?= $acaption ?></th>
<?php
	}
?>
					</tr>
				</thead>
<?php 
	$row = "row";
	$viewableText = "";
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$tbodyClosed=true;
	for ($i=0; $i < sizeof($ds1) -2; $i++)	{
		$sqlReturnedRowCount = $i;
		if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
			$numberOfTBodies++;
			$tbodyClosed = false;
		} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
			$numberOfTBodies++;
			$perTbodyCounter = 0;
		}
		$rowIndex = $row.$i;
		$contextId = $ds1[$rowIndex][0];
		$context1 = null;
		try {
			$context1 = new ContextPosition($database, $contextId, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$colorToDraw = "black";
		$viewableText = "";
		if ($ds1[$rowIndex][$numberOfColumns-1] == ContextPosition::$__ALLOW)	{
			$colorToDraw = "blue";
			$viewableText = "Allowed by";
		} else if ($ds1[$rowIndex][$numberOfColumns-1] == ContextPosition::$__DENY)	{
			$colorToDraw = "red";
			$viewableText = "Denied by";
		}
		$rowClassName = "";
		if (($perTbodyCounter % 2) == 1)	$rowClassName="pure-table-odd";
?>
		<tr class="<?= $rowClassName ?>">
			<td><?= $i+1 ?></td>
			<td><?= $context1->getContextCaption() ?></td>
<?php 
	$lengthToMark = 0;
	$markValue = ContextPosition::$__DONOTCARE;
	for ($lengthToMark=1; $lengthToMark < $numberOfColumns; $lengthToMark++)	{
		if (isset($ds1[$rowIndex][$lengthToMark]))	{
			$markValue=$ds1[$rowIndex][$lengthToMark];
			if ($markValue != ContextPosition::$__DONOTCARE)	{
				break;
			}
		} else {
			break;
		}
	}//end-for-lengthToMark
	$tempText = $ds1['header'][$lengthToMark]."[".$ds1['caption'][$lengthToMark]."]";
	$viewableText = $viewableText." ".$tempText;
	$remainingLength = $numberOfColumns-($lengthToMark+1);
?>
			<td colspan="<?= $lengthToMark ?>" style="background-color: <?= $colorToDraw ?>;">&nbsp;</td>
			<td colspan="<?= $remainingLength ?>" style="font-style: italic; font-size: 0.8em;">(<?= $viewableText ?>)</td>
		</tr>
<?php
		$perTbodyCounter++;
	}
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
?>				
			</table>
<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
<?php
		} else {
?>
			<div class="ui-state-error">
				There were no related Authorization Graph 
			</div>
<?php
		}
	} else {
		//Error Here 
?>
		<div class="ui-state-error">
			Could not load Graph for Authorization <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a class="ui-sys-back" href="<?= $thispage ?>?page=managesystemfirewall_graph&type=<?= $_REQUEST['type'] ?>&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to System Firewall Graph Manager for <?= $targetText ?> (<?= $_REQUEST['type'] ?>)</a></div>
					Rule: managesystemfirewall_graph
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php	
	mysql_close($conn);
} else if ($page == "managesystemfirewall_graph" && $login1->isRoot() && isset($_REQUEST['type']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managesystemfirewall_graph", "normal", "setlog", "-1", "-1"))	{
	$type = $_REQUEST['type'];
	$object1 = null;
	$targetText = "UNKNOWN";
	$targetObjectName = "UNKNOWN";
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	if ($type == "login")	{
		try {
			$object1 = new Login($database, $_REQUEST['id'], $conn);
			$targetObjectName = $object1->getClassName();
			$targetText = $object1->getFullname();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	} else if ($type == "group")	{
		try {
			$object1 = new Group($database, $_REQUEST['id'], $conn);
			$targetObjectName = $object1->getClassName();
			$targetText = $object1->getGroupName();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	} else {
		$promise1->setReason("Could not load the Object, The System received an Invalid Instruction");
		$promise1->setPromise(false);
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Authorization Graph for <?= $targetText ?> (<?= $targetObjectName ?>)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<!--Begin Search UIs now comes here-->
			<div class="ui-sys-search-container pure-form">
				<input type="text" title="Atleast Three Characters should be supplied" required placeholder="ABC" size="32"/>
				<a title="Click To Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managesystemfirewall_graph&type=<?= $_REQUEST['type'] ?>&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
			</div>
		<!--End Search UIs comes to end-->
<?php
	} else {
?>
				<div class="ui-state-error">
					The System Could not load graph <br/>
					Details: <?= $promise1->getReason() ?>
				</div>
<?php
	}
?>
				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a class="ui-sys-back" href="<?= $thispage ?>?page=manage<?= $_REQUEST['type'] ?>_detail&id=<?= $_REQUEST['id'] ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to <?= Object::getCaptionLabelFromPropertyName($_REQUEST['type']) ?> Manager (Details)</a></div>
					Rule: managesystemfirewall_graph
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php	
	mysql_close($conn);
}/*END: SystemFirewallGraph*/ else /*BEGIN: LoginManagement*/if ($page == "managelogin_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managelogin_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Login CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Management</a></div>
					Rule: managelogin_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managelogin_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managelogin_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Login CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Login::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Login[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$ulogin1 = null;
			try	{
				$ulogin1 = new Login($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($ulogin1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managelogin_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managelogin_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managelogin_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managelogin_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Login CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managelogin_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managelogin_csv&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managelogin_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managelogin_csv" && Authorize::isAllowable($config, "managelogin_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Login CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managelogin_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Login::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Managegement</a>
					</div>
					Rule: managelogin_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managelogin_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelogin_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$ulogin1 = null;
	$captionText = "Unknown";
	try {
		 $ulogin1 = new Login($database, $_REQUEST['id'], $conn);
		 $captionText = $ulogin1->getLoginName();
		 //You need to delete user table first 
		 $user1 = Login::getUserWithThisLogin($database, $conn, $ulogin1);
		 if (! is_null($user1)) {
			 //Just to test you are in the right account 
			 if ($user1->getLogin()->getLoginId() != $ulogin1->getLoginId()) Object::shootException("Failed to verify record for deletion");
			 $user1->commitDelete();
		 }
		 $ulogin1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Login which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Login <?= $ulogin1->getLoginName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Login <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Management</a>
					</div>
					Rule: managelogin_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managelogin_delete", "Deleted ".$ulogin1->getLoginName());
} else if ($page == "managelogin_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelogin_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$ulogin1 = null;
	try {
		$ulogin1 = new Login($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $ulogin1->getLoginName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Login <b><?= $ulogin1->getLoginName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Login <?= $ulogin1->getLoginName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managelogin_delete&id=<?= $ulogin1->getLoginId() ?>&report=yes&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managelogin_detail&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managelogin_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managelogin_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelogin_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$ulogin1 = null;
	try {
		$ulogin1 = new Login($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$locationId = mysql_real_escape_string($_REQUEST['locationId']);
	$departmentId = mysql_real_escape_string($_REQUEST['departmentId']);
	$jobId = mysql_real_escape_string($_REQUEST['jobId']);
	$groupId = mysql_real_escape_string($_REQUEST['groupId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	$root = 0;
	if (isset($_REQUEST['isroot'])) $root = 1;
	if (is_null($ulogin1->getLocation()) || ($ulogin1->getLocation()->getLocationId() != $locationId))	{
		$ulogin1->setLocation($locationId); $enableUpdate = true;
	}
	if (is_null($ulogin1->getDepartment()) || ($ulogin1->getDepartment()->getDepartmentId() != $departmentId))	{
		$ulogin1->setDepartment($departmentId); $enableUpdate = true;
	}
	if (is_null($ulogin1->getJobTitle()) || ($ulogin1->getJobTitle()->getJobId() != $jobId))	{
		$ulogin1->setJobTitle($jobId); $enableUpdate = true;
	}
	if (is_null($ulogin1->getGroup()) || ($ulogin1->getGroup()->getGroupId() != $groupId))	{
		$ulogin1->setGroup($groupId); $enableUpdate = true;
	}
	if (($root == 1) xor $ulogin1->isRoot())	{
		$ulogin1->setRoot($root); $enableUpdate = true;
	}
	if ($extraInformation != $ulogin1->getExtraInformation())	{
		$ulogin1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $ulogin1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$ulogin1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$ulogin1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Login Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Login (Report) <i><?= $ulogin1->getLoginName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Login <?= $ulogin1->getLoginName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the login <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelogin_detail&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Details</a></div>
					Rule: managelogin_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managelogin_edit", "Edited ".$ulogin1->getLoginName());
} else if ($page == "managelogin_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelogin_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$ulogin1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$ulogin1 = new Login($database, $_REQUEST['id'], $conn);
		$ulogin1->setExtraFilter($extraFilter);
		$ulogin1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Login <i><?= $ulogin1->getLoginName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managelogin_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $ulogin1->getLoginId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $ulogin1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
						<div class="pure-control-group">
							<label for="locationId">Location </label>
							<select id="locationId" name="locationId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Location">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Location::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($ulogin1->getLocation()) && ($ulogin1->getLocation()->getLocationId() == $alist1['id'])) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="departmentId">Department </label>
							<select id="departmentId" name="departmentId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Department">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Department::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($ulogin1->getDepartment()) && ($ulogin1->getDepartment()->getDepartmentId() == $alist1['id'])) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
						<label for="jobId">Job Title </label>
							<select id="jobId" name="jobId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Job Title">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = JobTitle::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($ulogin1->getJobTitle()) && ($ulogin1->getJobTitle()->getJobId() == $alist1['id'])) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="groupId">Group </label>
							<select id="groupId" name="groupId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Group">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Group::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($ulogin1->getGroup()) && ($ulogin1->getGroup()->getGroupId() == $alist1['id'])) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
<?php 
	if ($login1->isRoot() && ($login1->getLoginId() != $ulogin1->getLoginId()))	{
		//Only those logged in as root users are allowed to change 
		//this status
		//And as usual you can not change this for yourself even if you are the root user
		$chkEnable = "";
		if ($ulogin1->isRoot()) $chkEnable = "checked";
?>
						<div class="pure-controls">
							<label><input type="checkbox" name="isroot" value="1" <?= $chkEnable ?>/>Enable/Disable System Administrator</label>
						</div>
<?php 
	} // End-- isroot
?>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $ulogin1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelogin_detail&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Details</a></div>
					Rule: managelogin_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managelogin_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managelogin_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$ulogin1 = null;
	try {
		$ulogin1 = new Login($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for login : <i><?= $ulogin1->getLoginName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr><td>Username</td><td><?= $ulogin1->getLoginName() ?></td></tr>
							<tr><td>Fullname</td><td><?= $ulogin1->getFullname() ?></td></tr>
<?php if (! is_null($ulogin1->getSex()))	{ ?><tr><td>Sex</td><td><?= $ulogin1->getSex()->getSexName() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getDOB()))	{ ?><tr><td title="Date Of Birth">D.O.B</td><td><?= DateAndTime::convertFromDateTimeObjectToGUIDateFormat($ulogin1->getDOB()) ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getMarital()))	{ ?><tr><td>Marriage Status</td><td><?= $ulogin1->getMarital()->getMaritalName() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getPhone()))	{ ?><tr><td>Phone</td><td><?= $ulogin1->getPhone() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getEmail()))	{ ?><tr><td>Email</td><td><?= $ulogin1->getEmail() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getLocation()))	{ ?><tr><td>Location</td><td><?= $ulogin1->getLocation()->getLocationName() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getDepartment()))	{ ?><tr><td>Department</td><td><?= $ulogin1->getDepartment()->getDepartmentName() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getJobTitle()))	{ ?><tr><td>Job Title</td><td><?= $ulogin1->getJobTitle()->getJobName() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getGroup()))	{ ?><tr><td>Group</td><td><?= $ulogin1->getGroup()->getGroupName() ?></td></tr><?php }?>
<?php if (! is_null($ulogin1->getUserStatus()))	{ ?><tr><td colspan="2"><?= $ulogin1->getUserStatus()->getStatusName() ?></td></tr><?php }?>
<?php $captionText = "Normal User"; if ($ulogin1->isInitializationAccount())	{ $captionText= "System Installation Account";} else if ($ulogin1->isRoot())	{ $captionText = "System Administrator"; } ?> <tr><td colspan="2"><?= $captionText ?></td></tr>
<?php if (! is_null($ulogin1->getExtraInformation()))	{ ?><tr><td>Extra Information</td><td><?= $ulogin1->getExtraInformation() ?></td></tr><?php }?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Login::shouldAllowUpdateControls($login1, $ulogin1) && Authorize::isAllowable($config, "managelogin_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $ulogin1->getLoginName() ?>" href="<?= $thispage ?>?page=managelogin_edit&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Login::shouldAllowUpdateControls($login1, $ulogin1) && Authorize::isAllowable($config, "managelogin_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $ulogin1->getLoginName() ?>" href="<?= $thispage ?>?page=managelogin_delete&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
	if ($login1->isRoot() && Login::shouldAllowUpdateControls($login1, $ulogin1) && Authorize::isAllowable($config, "managesystemfirewall", "normal", "donsetlog", "-1", "-1"))	{
?>
		<a title="Set System Firewall for <?= $ulogin1->getLoginName() ?>" href="<?= $thispage ?>?page=managesystemfirewall&type=login&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonfirewall.png"/></a>
<?php	
	}
	if ($login1->isRoot() && Login::shouldAllowUpdateControls($login1, $ulogin1) && Authorize::isAllowable($config, "managesystemfirewall_graph", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="View Authorization Graph for <?= $ulogin1->getLoginName() ?>" href="<?= $thispage ?>?page=managesystemfirewall_graph&type=login&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttongraph.png"/></a>
<?php	
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Management</a>
					</div>
					Rule: managelogin_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managelogin_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managelogin_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$fullname = mysql_real_escape_string($_REQUEST['fullname']);
		$loginName = mysql_real_escape_string($_REQUEST['loginName']);
		$sexId = mysql_real_escape_string($_REQUEST['sexId']);
		$maritalId = mysql_real_escape_string($_REQUEST['maritalId']);
		$phone = mysql_real_escape_string($_REQUEST['phone']); //Convert to Standard Format
		$email = mysql_real_escape_string($_REQUEST['email']);
		$locationId = mysql_real_escape_string($_REQUEST['locationId']);
		$departmentId = mysql_real_escape_string($_REQUEST['departmentId']);
		$jobId = mysql_real_escape_string($_REQUEST['jobId']);
		$groupId = mysql_real_escape_string($_REQUEST['groupId']);
		$dob = $_REQUEST['dob'];
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Login (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	//$query="INSERT INTO login (loginName, pId, extraFilter, extraInformation) VALUES('$loginName', '$pId', '$extraFilter', '$extraInformation')";
	try {
		$loginId = Login::initializeLoginRecordForUser($database, $conn, $loginName, false); /* loginName, statusId, root and initializationAccount already set */
		//creating a link in user table 
		$query = "INSERT INTO users (loginId, extraFilter) VALUES ('$loginId', '$extraFilter')";
		mysql_db_query($database, $query, $conn) or Object::shootException("User [Adding]: Could not Add a Record into the System");
		//Now we will create a Login Object and use the update procedures 
		$ulogin1 = new Login($database, $loginId, $conn);
		$ulogin1->setFullname($fullname);
		$ulogin1->setSex($sexId);
		$ulogin1->setDOB(DateAndTime::convertFromGUIDateFormatToSystemDateAndTimeFormat($dob));
		$ulogin1->setMarital($maritalId);
		$ulogin1->setPhone($phone);
		$ulogin1->setEmail($email);
		$ulogin1->setLocation($locationId);
		$ulogin1->setDepartment($departmentId);
		$ulogin1->setJobTitle($jobId);
		$ulogin1->setGroup($groupId);
		$ulogin1->setExtraFilter($extraFilter);
		$ulogin1->setExtraInformation($extraInformation);
		$ulogin1->setPassword(sha1(Object::$defaultPassword));
		$ulogin1->setUsingDefaultPassword("1");
		$ulogin1->setAdmitted("1");
		//Before Commiting we need to reverify the user DOB 
		$systemTime1 = new DateAndTime("Ndimangwa", Object::getCurrentTimestamp(), "Fadhili");
		$timeDifference1 = (new DateAndTime("Ndimangwa", Object::getCurrentTimestamp(), "Fadhili"))->dateDifference($ulogin1->getDOB());
		if (is_null($timeDifference1)) Object::shootException("Date Difference could not be calculated");
		$minAgeCriteria = $profile1->getMinimumAgeCriteriaForUsers();
		if (! ($timeDifference1->isPositive() && ($timeDifference1->getYear() >= $profile1->getMinimumAgeCriteriaForUsers()))) Object::shootException("The user should have atleast $minAgeCriteria years of age");
		//End.DOB Verification		
		$ulogin1->commitUpdate();
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Login <?= $loginName ?>, has successful Added to the system<br/> The Default Login password is <b><?= Object::$defaultPassword ?></b></div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the login<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Login";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managelogin_add&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Manager</a>
					</div>
					Rule: managelogin_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managelogin_add", "Added $loginName");
} else if ($page == "managelogin_add" && Authorize::isAllowable($config, "managelogin_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Login</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managelogin_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
						<div class="pure-control-group">
							<label for="fullname">Full Name </label>
							<input type="text" name="fullname" id="fullname" size="48" required pattern="<?= $exprA64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Full Name : <?= $msgA64Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="sexId">Sex </label>
							<select id="sexId" name="sexId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Sex">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Sex::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
?>
			<option value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="dob">Date of Birth </label>
							<input class="datepicker" type="text" name="dob" id="dob" size="48" required pattern="<?= $exprDate ?>" validate="true" validate_control="text" validate_expression="<?= $exprDate ?>" validate_message="Date: <?= $msgDate ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="maritalId">Marital Status</label>
							<select id="maritalId" name="maritalId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Marital">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Marital::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
?>
			<option value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="phone">Phone/Telephone </label>
							<input type="text" name="phone" id="phone" size="32" required pattern="<?= $exprPhone ?>" validate="true" validate_control="text" validate_expression="<?= $exprPhone ?>" validate_message="Phone/Telephone : <?= $msgPhone ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="email">Email Address</label>
							<input type="text" name="email" id="email" size="48" required pattern="<?= $exprEmail ?>" validate="true" validate_control="text" validate_expression="<?= $exprEmail ?>%<?= $exprL32Name ?>" validate_message="Email Format : <?= $msgEmail ?>%Email Length: <?= $msgL32Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="loginName">Username </label>
							<input type="text" name="loginName" id="loginName" size="48" required pattern="<?= $exprUsername ?>" validate="true" validate_control="text" validate_expression="<?= $exprUsername ?>" validate_message="User Name : <?= $msgUsername ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="locationId">Location </label>
							<select id="locationId" name="locationId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Location">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Location::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
?>
			<option value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="departmentId">Department </label>
							<select id="departmentId" name="departmentId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Department">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Department::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
?>
			<option value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
						<label for="jobId">Job Title </label>
							<select id="jobId" name="jobId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Job Title">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = JobTitle::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
?>
			<option value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="groupId">Group </label>
							<select id="groupId" name="groupId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Group">
								<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Group::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
?>
			<option value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Login Management</a></div>
					Rule: managelogin_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		var $target1 = $('#perror');
		if (! $target1.length) return;
		var $loginName1 = $('#loginName');
		if (! $loginName1.length) return;
		var $email1 = $('#email');
		if (! $email1.length) return;
		var $phone1 = $('#phone');
		if (! $phone1.length) return;
		var $dob1 = $('#dob');
		if (! $dob1.length) return;
		$target1.empty();
		$.ajax({
			url: "../server/service_username_email_phone_availability.php",
			method: "POST",
			data: { param1: $loginName1.val(), param2: $email1.val(), param3: $phone1.val(), param4: 0, param5: <?= $profile1->getProfileId() ?>, param6: $dob1.val() },
			dataType: "json",
			cache: false,
			async: false
		}).done(function(data, textStatus, jqXHR)	{
			if (parseInt(data.code) === 0)	{
				generalFormSubmission(this, 'form1', 'perror');
			} else	{
				//Failed 
				$target1.html("1-- Failed " + data.message); return;
			}
		}).fail(function(jqXHR, textStatus, errorThrown)	{
			$target1.html("2-- Failed " + textStatus); return;
		}).always(function(data, textStatus, jqXHR)	{
			//Default Always
		})
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managelogin" && Authorize::isAllowable($config, "managelogin", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Login Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-login-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managelogin&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managelogin_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Username</th>
				<th>Fullname</th>
				<th>Phone</th>
				<th>Email</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Login::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch login data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$ulogin1 = null;
		try {
			$ulogin1 = new Login($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($ulogin1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $ulogin1->getLoginName() ?></td>
				<td><?= $ulogin1->getFullname() ?></td>
				<td><?= $ulogin1->getPhone() ?></td>
				<td><?= $ulogin1->getEmail() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managelogin_detail&id=<?= $ulogin1->getLoginId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managelogin_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managelogin_csv&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managelogin_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Login" href="<?= $thispage ?>?page=managelogin_add&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managelogin</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: LoginManagement*/ else/*BEGIN: JobTitleManagement*/if ($page == "managejobtitle_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managejobtitle_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobTitle CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Management</a></div>
					Rule: managejobtitle_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managejobtitle_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managejobtitle_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobTitle CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = JobTitle::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("JobTitle[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$job1 = null;
			try	{
				$job1 = new JobTitle($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($job1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managejobtitle_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managejobtitle_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managejobtitle_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managejobtitle_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobTitle CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managejobtitle_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managejobtitle_csv&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managejobtitle_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managejobtitle_csv" && Authorize::isAllowable($config, "managejobtitle_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobTitle CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managejobtitle_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, JobTitle::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Managegement</a>
					</div>
					Rule: managejobtitle_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managejobtitle_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobtitle_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$job1 = null;
	$captionText = "Unknown";
	try {
		 $job1 = new JobTitle($database, $_REQUEST['id'], $conn);
		 $captionText = $job1->getJobName();
		 $job1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a JobTitle which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The JobTitle <?= $job1->getJobName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the JobTitle <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Management</a>
					</div>
					Rule: managejobtitle_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managejobtitle_delete", "Deleted ".$job1->getJobName());
} else if ($page == "managejobtitle_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobtitle_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$job1 = null;
	try {
		$job1 = new JobTitle($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $job1->getJobName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a JobTitle <b><?= $job1->getJobName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the JobTitle <?= $job1->getJobName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managejobtitle_delete&id=<?= $job1->getJobId() ?>&report=yes&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managejobtitle_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managejobtitle_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managejobtitle_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobtitle_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$job1 = null;
	try {
		$job1 = new JobTitle($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$jobName = mysql_real_escape_string($_REQUEST['jobName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($jobName != $job1->getJobName())	{
		$job1->setJobName($jobName); $enableUpdate = true;
	}
	if ($extraInformation != $job1->getExtraInformation())	{
		$job1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $job1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$job1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$job1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for JobTitle Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit JobTitle (Report) <i><?= $job1->getJobName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			JobTitle <?= $job1->getJobName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the job <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobtitle_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Details</a></div>
					Rule: managejobtitle_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managejobtitle_edit", "Edited ".$job1->getJobName());
} else if ($page == "managejobtitle_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobtitle_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$job1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$job1 = new JobTitle($database, $_REQUEST['id'], $conn);
		$job1->setExtraFilter($extraFilter);
		$job1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit JobTitle <i><?= $job1->getJobName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managejobtitle_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $job1->getJobId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $job1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
						<div class="pure-control-group">
							<label for="jobName">JobTitle Name </label>
							<input value="<?= $job1->getJobName() ?>" type="text" name="jobName" id="jobName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="JobTitle Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $job1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobtitle_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Details</a></div>
					Rule: managejobtitle_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managejobtitle_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managejobtitle_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$job1 = null;
	try {
		$job1 = new JobTitle($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for job : <i><?= $job1->getJobName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>JobTitle Name</td>
								<td><?= $job1->getJobName() ?></td>
							</tr>
<?php 
	if (! is_null($job1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $job1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managejobtitle_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $job1->getJobName() ?>" href="<?= $thispage ?>?page=managejobtitle_edit&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managejobtitle_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $job1->getJobName() ?>" href="<?= $thispage ?>?page=managejobtitle_delete&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
	if ($login1->isRoot() && Authorize::isAllowable($config, "managesystemfirewall", "normal", "donsetlog", "-1", "-1"))	{
?>
		<a title="Set System Firewall for <?= $job1->getJobName() ?>" href="<?= $thispage ?>?page=managesystemfirewall&type=jobtitle&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonfirewall.png"/></a>
<?php	
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Management</a>
					</div>
					Rule: managejobtitle_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managejobtitle_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managejobtitle_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$jobName = mysql_real_escape_string($_REQUEST['jobName']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New JobTitle (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO jobTitle (jobName, extraFilter, extraInformation) VALUES('$jobName', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("JobTitle[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The JobTitle <?= $jobName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the job<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another JobTitle";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managejobtitle_add&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Manager</a>
					</div>
					Rule: managejobtitle_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managejobtitle_add", "Added $jobName");
} else if ($page == "managejobtitle_add" && Authorize::isAllowable($config, "managejobtitle_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New JobTitle</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managejobtitle_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
						<div class="pure-control-group">
							<label for="jobName">JobTitle Name </label>
							<input type="text" name="jobName" id="jobName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="JobTitle Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to JobTitle Management</a></div>
					Rule: managejobtitle_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managejobtitle" && Authorize::isAllowable($config, "managejobtitle", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">JobTitle Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-job-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managejobtitle&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managejobtitle_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>JobTitle Name</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = JobTitle::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch job data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$job1 = null;
		try {
			$job1 = new JobTitle($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($job1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $job1->getJobName() ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managejobtitle_detail&id=<?= $job1->getJobId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managejobtitle_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managejobtitle_csv&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managejobtitle_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System JobTitle" href="<?= $thispage ?>?page=managejobtitle_add&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managejobtitle</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: JobTitleManagement*/ else /*BEGIN: GroupManagement*/if ($page == "managegroup_csv" && isset($_REQUEST['report']) && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managegroup_csv", "normal", "setlog", "-1", "-1"))	{
	$filename=$_REQUEST['report'];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Group CSV File (Download Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if (file_exists($filename) && unlink($filename))	{
?>
		<div class="ui-state-highlight">
			The temporary file has been removed succesful from the Server
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problem on removing the temporary file from the Server
		</div>
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Management</a></div>
					Rule: managegroup_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managegroup_csv" && isset($_REQUEST['downloadable']) && Authorize::isAllowable($config, "managegroup_csv", "normal", "setlog", "-1", "-1"))	{
	$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$promise1 = new Promise();
	$promise1->setPromise(true);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Group CSV File (Downlad CSV File)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$filename="";
	$tfname=$filename.$date;
	$tfname=$tfname.rand(100,999);
	$filename=sha1($tfname);
	$filename=$filename.".csv";
	$filename="../temp/".$filename;
	$searchText = Object::getSearchTextFromUserFieldData($_REQUEST['controlIndex'], $_REQUEST['fieldname'], $_REQUEST['op'], $_REQUEST['namespaceTag'], $_REQUEST['fieldvalue']);
	$lineCounter = 0;
	$file1 = fopen($filename, "w") or die("Could not Open the File for writing");
	$csvProcessor1 = null;
	try {
		$csvProcessor1 = new CSVProcessor($searchText, $file1);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$primise1->setPromise(false);
	}
	$result = null;
	if ($promise1->isPromising())	{
		$query = Group::getQueryText();
		try {
			$result = mysql_db_query($database, $query, $conn) or Object::shootException("Group[CSV]: Could not Execute Query");
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$primise1->setPromise(false);
		}
	}
	if ($promise1->isPromising())	{
		while (list($id) = mysql_fetch_row($result))	{
			$group1 = null;
			try	{
				$group1 = new Group($database, $id, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$csvProcessor1->reload(); //Clear the Previous Object Properties 
			if ($group1->processCSV($csvProcessor1)->evaluateResult())	{
				$lineCounter++;
			}
		}//end-while 
	}//end-if-promising
	fclose($file1);
	if ($promise1->isPromising())	{
?>
		<div>
			Your file is Ready for downloading<br/>
			Number data rows it contains is <?= $lineCounter ?><br/><br/>
			<b style="font-size: 1.1em;"><a href="<?= $filename ?>">Click Here</a></b> to download your file<br/><br/>
			Once the download is complete, kindly click the proceed button 
		</div>
<?php	
	} else {
?>
		<div class="ui-state-error">
			There were problems in generating your file <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>									
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<div id="perror" class="ui-sys-error-message"></div>
<?php 
	if ($promise1->isPromising())	{
		if ($lineCounter > 0)	{
?>
			<a id="__id_csv_previewer" data-error-control="perror" data-server-forward-path="../server/getcsvpreviewer.php" data-file-to-read="<?= $filename ?>" data-dialog-container="__id_general_dialog_holder" class="button-link"><button>Preview</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php
		}
?>
		<a class="button-link" href="<?= $thispage ?>?page=managegroup_csv&report=<?= $filename ?>&downloadable=true&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>Continue</button></a>
<?php
	}
?>				
					</div>
					Rule: managegroup_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managegroup_csv" && isset($_REQUEST['sortable']) && Authorize::isAllowable($config, "managegroup_csv", "normal", "setlog", "-1", "-1"))	{
	$enableUpdate = false;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Group CSV File (Column Sorting)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
<?php 
	if (isset($_REQUEST['fieldname']) && isset($_REQUEST['fieldtype']) && isset($_REQUEST['namespaceTag']) && isset($_REQUEST['fieldvalue']))	{
		$enableUpdate = true;
?>
				<form class="pure-form" id="form1" method="POST" action="<?= $thispage ?>">
					<input type="hidden" name="page" value="managegroup_csv"/>
					<input type="hidden" name="downloadable" value="true"/>
					<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
<?php 
	Object::$iconPath = "../sysimage/";
	$sortableContainer = Object::getUICSVDivSortable(Object::getLineToSortFromUserFieldData($_REQUEST['fieldname'], $_REQUEST['fieldtype'], $_REQUEST['namespaceTag'],$_REQUEST['fieldvalue']));
	echo $sortableContainer;
?>
					<div class="pure-controls">
						<span id="perror" class="ui-sys-error-message"></span>
					</div>
				</form>
<?php 
	} else {
?>
		Perhaps you did not select any column to be included in your CSV File <br/>
		Kindly go back to Column Selection and select atleast ONE Column
<?php
	}
?>				
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a class="button-link" href="<?= $thispage ?>?page=managegroup_csv&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Column Selection</a>&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	if ($enableUpdate)	{
?>				
							<input type="button" id="__add_record" value="Proceed"/>
<?php 
	}
?>				
					</div>
					Rule: managegroup_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegroup_csv" && Authorize::isAllowable($config, "managegroup_csv", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Group CSV File (Column Selection)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegroup_csv"/>
						<input type="hidden" name="sortable" value="true"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
<?php
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$listOfAvailableColumnsInTables = Object::getUICSVTables($database, Group::getAvailableViewableColumns(), $conn);
	mysql_close($conn);
	echo $listOfAvailableColumnsInTables;
?>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<input type="button" id="__add_record" value="Proceed"/><br/>
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Managegement</a>
					</div>
					Rule: managegroup_csv
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
} else if ($page == "managegroup_delete" && isset($_REQUEST['report']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegroup_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has happened");
	$group1 = null;
	$captionText = "Unknown";
	try {
		 $group1 = new Group($database, $_REQUEST['id'], $conn);
		 $captionText = $group1->getGroupName();
		 $group1->commitDelete();
		 $promise1->setPromise(true);
		 $enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason("You might be dealing with a Group which is no longer existing in the system or a None Empty Record");
		$promise1->setPromise(false);
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Deletion report for <i><?= $captionText ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			The Group <?= $group1->getGroupName() ?> has been successful removed from the system
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Removing the Group <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Management</a>
					</div>
					Rule: managegroup_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegroup_delete", "Deleted ".$group1->getGroupName());
} else if ($page == "managegroup_delete" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegroup_delete", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to database services");
	$group1 = null;
	try {
		$group1 = new Group($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Delete Confirmation for <i><?= $group1->getGroupName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<div class="ui-sys-warning">
						You are about to remove a Group <b><?= $group1->getGroupName() ?></b> from the System <br/>
						<b>NOTE: This Operation Can not be reversed</b><br/>
						Are You Sure You want to remove the Group <?= $group1->getGroupName() ?> from the System?
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegroup_delete&id=<?= $group1->getGroupId() ?>&report=yes&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?= $thispage ?>?page=managegroup_detail&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><button>NO (Cancel)</button></a>
					</div>
					Rule: managegroup_delete
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
} else if ($page == "managegroup_edit" && isset($_REQUEST['report']) && isset($_REQUEST['rndx']) && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegroup_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$group1 = null;
	try {
		$group1 = new Group($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{ die($e->getMessage()); }
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing Has been Updated");
	$enableUpdate = false;
	$isLoopDetected = false;
	$groupName = mysql_real_escape_string($_REQUEST['groupName']);
	$pId = mysql_real_escape_string($_REQUEST['pId']);
	$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
	if ($groupName != $group1->getGroupName())	{
		$group1->setGroupName($groupName); $enableUpdate = true;
	}
	if ($pId != $group1->getParentGroup()->getGroupId())	{
		try {
			if (Group::isLoopDetected($database, $conn, $group1->getGroupId(), $pId))	Object::shootException("Loop (STP), Loop Detected");
			$group1->setParentGroup($pId); $enableUpdate = true;
		} catch (Exception $e)	{
			$isLoopDetected = true;
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
	}
	if ($extraInformation != $group1->getExtraInformation())	{
		$group1->setExtraInformation($extraInformation); $enableUpdate = true;
	}
	//This is the Last 
	if (trim($_REQUEST['rndx']) != $group1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window");
		$promise1->setPromise(false);
	}
	//Now proceed with general find a new value of extra Filter 
	$group1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate && ! $isLoopDetected)	{
		try {
			$group1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	} else {
		$enableUpdate = false; //Use this for Group Only incase loopIs Detected shut do not keep logs
	}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Group (Report) <i><?= $group1->getGroupName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Group <?= $group1->getGroupName() ?> has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were no updates for the group <br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>					
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegroup_detail&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Details</a></div>
					Rule: managegroup_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegroup_edit", "Edited ".$group1->getGroupName());
} else if ($page == "managegroup_edit" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegroup_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$group1 = null;
	$extraFilter = System::getCodeString(8);
	try {
		$group1 = new Group($database, $_REQUEST['id'], $conn);
		$group1->setExtraFilter($extraFilter);
		$group1->commitUpdate();
	} catch (Exception $e)	{ die($e->getMessage()); }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Group <i><?= $group1->getGroupName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegroup_edit"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="id" value="<?= $group1->getGroupId() ?>"/>
						<input type="hidden" name="rndx" value="<?= $group1->getExtraFilter() ?>"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
						<div class="pure-control-group">
							<label for="groupName">Group Name </label>
							<input value="<?= $group1->getGroupName() ?>" type="text" name="groupName" id="groupName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Group Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="pId">Parent Group</label>
							<select id="pId" name="pId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Parent Group">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Group::loadAllData($database, $conn);
	foreach ($list as $alist)	{
		$selected="";
		if ($group1->getGroupId() == $alist['id']) continue;
		if ($alist['id'] == $group1->getParentGroup()->getGroupId()) $selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input value="<?= $group1->getExtraInformation() ?>" type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Edit Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegroup_detail&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Details</a></div>
					Rule: managegroup_edit
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managegroup_detail" && isset($_REQUEST['id']) && Authorize::isAllowable($config, "managegroup_detail", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$group1 = null;
	try {
		$group1 = new Group($database, $_REQUEST['id'], $conn);
	} catch (Exception $e)	{die($e->getMessage());}
	mysql_close($conn);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default ui-sys-body">
				<div class="panel-heading">
					<h3 class="panel-title">Details for group : <i><?= $group1->getGroupName() ?></i></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<table class="pure-table">
						<thead>
							<tr>
								<th>Field Name</th>
								<th>Field Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Group Name</td>
								<td><?= $group1->getGroupName() ?></td>
							</tr>
<?php 
	if (! is_null($group1->getParentGroup()))	{
?>
					<tr>
						<td>Parent Group</td>
						<td><?= $group1->getParentGroup()->getGroupName() ?></td>
					</tr>
<?php	
	}
	$typeOfGroupText = "Normal Group";
	if ($group1->isRootGroup())	{
		$typeOfGroupText = "System Group";
?>
					<tr>
						<td>Narure of the Group</td>
						<td><?= $typeOfGroupText ?></td>
					</tr>
<?php
	}
	if (! is_null($group1->getExtraInformation()))	{
?>
					<tr>
						<td>Extra Information</td>
						<td><?= $group1->getExtraInformation() ?></td>
					</tr>
<?php
	}
?>
						</tbody>
					</table>
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (! $group1->isRootGroup() && Authorize::isAllowable($config, "managegroup_edit", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Edit <?= $group1->getGroupName() ?>" href="<?= $thispage ?>?page=managegroup_edit&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonedit.png"/></a>
<?php
	}
	if (! $group1->isRootGroup() && Authorize::isAllowable($config, "managegroup_delete", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Delete <?= $group1->getGroupName() ?>" href="<?= $thispage ?>?page=managegroup_delete&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttondelete.png"/></a>
<?php
	}
	if ($login1->isRoot() && Authorize::isAllowable($config, "managesystemfirewall", "normal", "donsetlog", "-1", "-1"))	{
?>
		<a title="Set System Firewall for <?= $group1->getGroupName() ?>" href="<?= $thispage ?>?page=managesystemfirewall&type=group&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonfirewall.png"/></a>
<?php	
	}
	if ($login1->isRoot() && Authorize::isAllowable($config, "managesystemfirewall_graph", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="View Authorization Graph for <?= $group1->getGroupName() ?>" href="<?= $thispage ?>?page=managesystemfirewall_graph&type=group&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttongraph.png"/></a>
<?php	
	}
?>
						<span class="ui-sys-clear-both">&nbsp;</span><br />
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
						<a href="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Management</a>
					</div>
					Rule: managegroup_detail
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
} else if ($page == "managegroup_add" && isset($_REQUEST['report']) && Authorize::isAllowable($config, "managegroup_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(false);
		$promise1->setReason("Nothing has happened");
		$groupName = mysql_real_escape_string($_REQUEST['groupName']);
		$pId = mysql_real_escape_string($_REQUEST['pId']);
		$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Group (Report)</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
<?php 
	$extraFilter = System::getCodeString(8);
	$query="INSERT INTO groups (groupName, pId, extraFilter, extraInformation) VALUES('$groupName', '$pId', '$extraFilter', '$extraInformation')";
	try {
		mysql_db_query($database, $query, $conn) or Object::shootException("Group[Adding]: Could not Add a Record into the System");
		$promise1->setPromise(true);
		$enableUpdate = true;
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">The Group <?= $groupName ?>, has successful Added to the system</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Adding the group<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php		
	}
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center">
<?php 
	$captionText = "Try Again";
	if ($enableUpdate) $captionText = "Add Another Group";
?>			
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegroup_add&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><?= $captionText ?></a>&nbsp;&nbsp;
						<a class="ui-sys-back" href="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Manager</a>
					</div>
					Rule: managegroup_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managegroup_add", "Added $groupName");
} else if ($page == "managegroup_add" && Authorize::isAllowable($config, "managegroup_add", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add New Group</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body">
					<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
						<input type="hidden" name="page" value="managegroup_add"/>
						<input type="hidden" name="report" value="io"/>
						<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__USERS_GROUPS ?>"/>
						<div class="pure-control-group">
							<label for="groupName">Group Name </label>
							<input type="text" name="groupName" id="groupName" size="48" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Group Name : <?= $msgA48Name ?>"/>
						</div>
						<div class="pure-control-group">
							<label for="pId">Parent Group</label>
							<select id="pId" name="pId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select a Parent Group">
								<option value="_@32767@_">--select--</option>
<?php 
	$list = Group::loadAllData($database, $conn);
	foreach ($list as $alist)	{
?>
		<option value="<?= $alist['id'] ?>"><?= $alist['val'] ?></option>
<?php
	}
?>
							</select>
						</div>
						<div class="pure-control-group">
							<label for="extraInformation">Extra Information </label>
							<input type="text" name="extraInformation" id="extraInformation" size="48" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
						</div>
						<div class="pure-controls">
							<span id="perror" class="ui-sys-error-message"></span>
						</div>
						<div class="pure-controls">
							<input id="__add_record" type="button" value="Add Record"/>
						</div>
					</form>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">
					<div class="text-center"><a href="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>">Back to Group Management</a></div>
					Rule: managegroup_add
				</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
} else if ($page == "managegroup" && Authorize::isAllowable($config, "managegroup", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Group Management</h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE controls for searching-->
					<div class="ui-sys-search-container pure-form pure-group-control">
						<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
						<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managegroup&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
		$blnAuthorizationDetail = Authorize::isAllowable($config, "managegroup_detail", "normal", "donotsetlog", "-1", "-1");
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<table class="pure-table ui-sys-table-search-results">
		<thead>
			<tr>
				<th>S/N</th>
				<th>Group Name</th>
				<th>Parent Group</th>
				<th>Nature</th>
				<th></th>
			</tr>
		</thead>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
	$query = Group::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch group data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$group1 = null;
		try {
			$group1 = new Group($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($group1->searchMatrix($matrix1)->evaluateResult())	{
			$parentGroupName = "";
			if (! is_null($group1->getParentGroup())) $parentGroupName = $group1->getParentGroup()->getGroupName();
			$natureOfGroup = "Normal";
			if ($group1->isRootGroup()) $natureOfGroup = "System";
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $group1->getGroupName() ?></td>
				<td><?= $parentGroupName ?></td>
				<td><?= $natureOfGroup ?></td>
				<td>
<?php 
	if ($blnAuthorizationDetail)	{
?>
		<a href="<?= $thispage ?>?page=managegroup_detail&id=<?= $group1->getGroupId() ?>&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>" class="ui-sys-search-results-controls"><img alt="DAT" src="../sysimage/buttondetail.png"/></a>
<?php
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
	</table>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
<!--Beginning of Block Three-->
					<div class="ui-sys-search-controls ui-sys-right">
<?php 
	if (Authorize::isAllowable($config, "managegroup_csv", "normal", "donotsetlog", "-1", "-1")) {
?>
		<a title="Download CSV/EXCEL Formatted Data" href="<?= $thispage ?>?page=managegroup_csv&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttoncsv.png"/></a>
<?php
	}
	if (Authorize::isAllowable($config, "managegroup_add", "normal", "donotsetlog", "-1", "-1"))	{
?>
		<a title="Add a New System Group" href="<?= $thispage ?>?page=managegroup_add&sysmenu=<?= SystemMenu::$__USERS_GROUPS ?>"><img alt="DAT" src="../sysimage/buttonadd.png"/></a>
<?php	
	}
?>				
					</div>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer">Rule: managegroup</div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: GroupManagement*/ else/*BEGIN: SystemLogs*/if ($page == "managesystemlogs" && Authorize::isAllowable($config, "managesystemlogs", "normal", "setlog", "-1", "-1"))	{
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"></h3>
				</div> <!--End div.panel-heading-->
				<div class="panel-body ui-sys-body">
					<!--Block ONE BEGIN Search box-->
					<div class="pure-form ui-sys-search-container">
						<input title="Atleast Three Characters should be supplied" type="text" required placeholder="ABC" size="32" /> 
						<a title="Click To Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managesystemlogs&sysmenu=<?= SystemMenu::$__SETTINGS ?>" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
					</div>
					<!--Block ONE ENDS-->
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		/*Block Two Begin Search Results*/
?>
		<div class="ui-sys-search-results">
			<label id="statustextlabel"></label>
			<table class="pure-table pure-table-horizontal ui-sys-table-search-results">
				<thead>
					<tr>
						<th></th>
						<th>Time</th>
						<th>Actor</th>
						<th>Action</th>
						<th>Acted To</th>
					</tr>
<?php 
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	$list=SystemLogs::searchFromSystemLogs($database, $conn, $searchtext, $maximumReturnedRows); //Does not Allowed to exceed 512, due to memory leakage 
	foreach ($list as $alist)	{
		if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
			$numberOfTBodies++;
			$tbodyClosed = false;
		} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
			$numberOfTBodies++;
			$perTbodyCounter = 0;
		}
?>
		<tr>
			<td><?= $sqlReturnedRowCount + 1 ?></td>
			<td><?= $alist['when'] ?></td>
			<td><?= $alist['who'] ?></td>
			<td><?= $alist['what'] ?></td>
			<td><?= $alist['towhom'] ?></td>
		</tr>
<?php
		$perTbodyCounter++;
		$sqlReturnedRowCount++;
	}//end-foreach
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
	mysql_close($conn);
?>
				</thead>
			</table>
			<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
			<ul class="ui-sys-pagination"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
		</div>
<?php		
		/*Block Two Ending Search Results*/
	} //end-if-searchtext
?>
				</div>	<!--End div.panel-body-->
				<div class="panel-footer"></div>	<!--End div.panel-footer-->
			</div> <!--End div.panel.panel-default-->
		</div> <!--End.col-md-12-->
	</div> <!--End div.row-->
<?php
}/*END: SystemLogs*/ else /*BEGIN: SystemRecordLimits*/if ($page == "system_record_limits" && isset($_REQUEST['rndx']) && isset($_REQUEST['report']) && $login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing has been updated");
	$maximumNumberOfReturnedSearchRecords = mysql_real_escape_string($_REQUEST['maximumNumberOfReturnedSearchRecords']);
	$maximumNumberOfDisplayedRowsPerPage = mysql_real_escape_string($_REQUEST['maximumNumberOfDisplayedRowsPerPage']);
	$uprofile1 = null;
	try {
		$uprofile1 = new profile($database, $__profileId, $conn);
		if ($uprofile1->getExtraFilter() != trim($_REQUEST['rndx'])) Object::shootException("The System has Detected you are replaying data in your Browser window or someone else is updating profile information");
		if ($maximumNumberOfReturnedSearchRecords != $uprofile1->getMaximumNumberOfReturnedSearchRecords())	{
			$uprofile1->setMaximumNumberOfReturnedSearchRecords($maximumNumberOfReturnedSearchRecords); $enableUpdate = true;
		}
		if ($maximumNumberOfDisplayedRowsPerPage != $uprofile1->getMaximumNumberOfDisplayedRowsPerPage())	{
			$uprofile1->setMaximumNumberOfDisplayedRowsPerPage($maximumNumberOfDisplayedRowsPerPage); $enableUpdate = true;
		}
		if ($enableUpdate) {
			$promise1->setPromise(true);
			$uprofile1->setExtraFilter(System::getCodeString(8));
			$uprofile1->commitUpdate();
		}
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
		$enableUpdate = false;
	}
?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Record Limits (Update Report)</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div ui-state-highlight>
			You have successful updated the records limit
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems on saving the record limits<br/>
			Reason: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>		
					</div>	<!--End div.panel-body-->
					<div class="panel-footer"><a href="<?= $thispage ?>">Back to DashBoard</a></div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageprofile_edit", "Records Count Updates");
} else if ($page == "system_record_limits" && $login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$uprofile1 = null;
	try {
		$uprofile1 = new Profile($database, $profile1->getProfileId(), $conn);
		$uprofile1->setExtraFilter(System::getCodeString(8));
		$uprofile1->commitUpdate();
	} catch (Exception $e)	{
		die($e->getMessage());
	}
	mysql_close($conn);
?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Record Limits</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
						<form class="text-center pure-form pure-form-aligned" id="form1" method="POST" action="<?= $thispage ?>">
							<input type="hidden" name="page" value="system_record_limits"/>
							<input type="hidden" name="rndx" value="<?= $uprofile1->getExtraFilter() ?>"/>
							<input type="hidden" name="report" value="true"/>
							<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
							<div class="pure-control-group">
								<label for="maximumNumberOfReturnedSearchRecords">Maximum Number of Returned Search Records </label>
								<input value="<?= $profile1->getMaximumNumberOfReturnedSearchRecords() ?>" class="text-can-not-be-zero" type="text" name="maximumNumberOfReturnedSearchRecords" id="maximumNumberOfReturnedSearchRecords" size="48" required pattern="<?= $exprRecordCount ?>" validate="true" validate_control="text" validate_expression="<?= $exprRecordCount ?>" validate_message="Maximu Number of Returned Search Records : <?= $msgRecordCount ?>"/>
							</div>
							<div class="pure-control-group">
								<label for="maximumNumberOfDisplayedRowsPerPage">Maximum Number of Displayed Rows Per Page </label>
								<input value="<?= $profile1->getMaximumNumberOfDisplayedRowsPerPage() ?>" class="text-can-not-be-zero" type="text" name="maximumNumberOfDisplayedRowsPerPage" id="maximumNumberOfDisplayedRowsPerPage" size="48" required pattern="<?= $exprRecordCount ?>" validate="true" validate_control="text" validate_expression="<?= $exprRecordCount ?>" validate_message="Maximu Number of Displayed Rows Per Page : <?= $msgRecordCount ?>"/>
							</div>
							<div class="pure-controls">
								<span id="perror" class="ui-sys-error-message"></span>
							</div>
							<div class="pure-controls">
								<input id="__add_record" type="button" value="Edit Record"/>
							</div>
						</form>
					</div>	<!--End div.panel-body-->
					<div class="panel-footer"></div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	$('#__add_record').on('click', function(event)	{
		event.preventDefault();
		var $target1 = $('#perror');
		if (! $target1.length) return;
		var enableSubmit = true;
		$('input.text-can-not-be-zero').each(function(i, v)	{
			var $text1 = $(v);
			enableSubmit = enableSubmit && (parseInt($text1.val()) != 0);
		});
		if (enableSubmit)	{
			generalFormSubmission(this, 'form1', 'perror');
		} else {
			$target1.empty();
			$('<span/>').html('Records Count Can not be Zero')
				.appendTo($target1);
		}
	});
})(jQuery);
</script>
<?php
}/*END: SystemRecordLimits*/ else/*BEGIN: ContextManager*/if ($page == "managecontextmanager" && isset($_REQUEST['rndx']) && isset($_REQUEST['report']) && $login1->isRoot() && Authorize::isAllowable($config, "managecontextmanager", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$action = $_REQUEST['report'];
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("Nothing were updated");
	$isAllowedByDefault = false;
	$enableUpdate = false;
	$contextManagerCaptionText = "";
	if (intval($action) == 1)	{
		$contextManagerCaptionText = " ALLOWED (OPEN MODE)";
	} else if (intval($action) == 0)	{
		$contextManagerCaptionText = " DENIED (CLOSED MODE)";
	}
	try {
		$isAllowedByDefault = ContextManager::isSystemDefaultAllowed($database, $conn);
		if (! ((intval($action) == 1) xor $isAllowedByDefault)) Object::shootException("No Change has been applied to the system");
		ContextManager::setSystemDefaultAllowed($database, $conn, $action);
		$enableUpdate = true;
		$promise1->setPromise(true);
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
?>
	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Last Resort for Do Not Care</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div>
			Congratulations, you have updated the Last Resort Value for Do Not Care
		</div>
		<div>
<?php 
	if (intval($action) == 1)	{
?>
		<div class="ui-state-error">
			The System Now is in an OPEN Mode. This settings pose security problems. We are not encouraging you to leave the system under these settings, if you are in a production enviroment kindly set your system in a CLOSED (DENIED) Mode
		</div>
<?php
	} else if (intval($action) == 0)	{
?>
		<div class="ui-state-highlight">
			The System Now is in a CLOSED Mode. This is the most secured settings for your system, We strongly recommend to leave the system in this state
		</div>
<?php		
	}
?>
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were some problems in updating the Last Resort Value for Do Not Care <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>								
					</div>	<!--End div.panel-body-->
					<div class="panel-footer"><a href="<?= $thispage ?>">Go to DashBoard</a></div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managecontextmanager", "Changed to $contextManagerCaptionText");
} else if ($page == "managecontextmanager" && $login1->isRoot() && Authorize::isAllowable($config, "managecontextmanager", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$promise1 = new Promise();
	$promise1->setPromise(true);
	$isAllowedByDefault = false;
	$okayButtonClass = "button-link";
	$report = 1;
	try {
		$isAllowedByDefault = ContextManager::isSystemDefaultAllowed($database, $conn);
		if ($isAllowedByDefault)	{
			$okayButtonClass = "button-link-warning";
			$report = 0;
		}
	} catch (Exception $e)	{
		$promise1->setReason($e->getMessage());
		$promise1->setPromise(false);
	}
?>
	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Last Resort for Do Not Care</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<form class="pure-form" method="POST" action="<?= $thispage ?>" id="form1">
			<input type="hidden" name="page" value="managecontextmanager"/>
			<input type="hidden" name="report" value="<?= $report ?>"/>
			<input type="hidden" name="rndx" value="4"/>
			<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
<?php 
	if ($isAllowedByDefault)	{
?>
		<div>
			<div>
				By Clicking the <b>DENY Button</b>, will cause all actions which are set to <b>DO NOT CARE</b> in all levels {users, Job Title and Groups} <b>will be DENIED</b> by default
			</div>
			<div class="text-center">
				<input type="submit" value="DENY (Default)"/>&nbsp;&nbsp;&nbsp;&nbsp;<a class="button-link" href="<?= $thispage ?>"><input type="button" value="Cancel"/></a>
			</div>
		</div>
<?php
	} else {
?>
		<div class="ui-sys-warning">
			<div>
				By Clicking the <b>ALLOW Button</b>, will cause all actions which are set to <b>DO NOT CARE</b> in all levels {users, Job Title and Groups} <b>will be ALLOWED</b> by default<br/>
				We strongly DO NOT RECOMMEND to leave the system in an ALLOW state by default. 
				<br/>Kindly think and plan careful, as this might impose security holes in the system
			</div>
			<div class="text-center">
				<input type="submit" value="ALLOW (Default)"/>&nbsp;&nbsp;&nbsp;&nbsp;<a class="button-link" href="<?= $thispage ?>"><input type="button" value="Cancel"/></a>
			</div>
		</div>
<?php
	} //--end-ifIsAllowedByDefault
?>
		</form>
<?php
	} else {
?>
		<div class="ui-state-error">
			The system has encoutered a problem <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>								
					</div>	<!--End div.panel-body-->
					<div class="panel-footer"></div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<?php
	mysql_close($conn);
}/*END: ContextManager*/ else /*BEGIN: System_Certificate*/if ($page == "system_certificate" && isset($_REQUEST['rndx']) && isset($_REQUEST['report']) && $login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$secured = 0;
	$strictSecured = 0;
	if (isset($_REQUEST['isSecured'])) $secured = 1;
	if (isset($_REQUEST['isStrictSecured'])) $strictSecured = 1;
	$promise1 = new Promise();
	$promise1->setPromise(false);
	$promise1->setReason("No Update were received");
	$enableUpdate = false;
	$uprofile1 = null;
	try {
		$uprofile1 = new Profile($database, $profile1->getProfileId(), $conn);
	} catch (Exception $e)	{
		die($e->getMessage());
	}
	$logText = "";
	if (($secured == 1) xor $uprofile1->isSecureConnectionRequired())	{
		$uprofile1->setSecureConnectionRequired($secured); $enableUpdate = true;
		$logText = "To INSECURED";
		if ($secured == 1)	{
			$logText = "To SECURED";
		} 
	}
	if (($secured == 1) && (($strictSecured == 1) xor $uprofile1->isStrictSecureConnectionRequired()))	{
		$uprofile1->setStrictSecureConnectionRequired($strictSecured); $enableUpdate = true;
		$tempText = " To Loose Secured";
		if ($strictSecured == 1) $tempText = " To Strict Secured";
		$logText .= $tempText;
	}
	if (trim($_REQUEST['rndx']) != $uprofile1->getExtraFilter())	{
		$enableUpdate = false;
		$promise1->setReason("The System has Detected you are replaying data in your Browser window or someone else is updating profile information");
		$promise1->setPromise(false);
	}
	$uprofile1->setExtraFilter(System::getCodeString(8));
	if ($enableUpdate)	{
		try {
			$uprofile1->commitUpdate();
			$promise1->setPromise(true);
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
			$enableUpdate = false;
		}
	}
?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">SSL/TLS Certificates</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Certificates has been updated successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Certificates Updates <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>
					</div>	<!--End div.panel-body-->
					<div class="panel-footer text-center"><a href="<?= $thispage ?>">Go to DashBoard</a></div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageprofile_edit", $logText);
} else if ($page == "system_certificate" && $login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$uprofile1 = null;
	try {
		$uprofile1 = new Profile($database, $profile1->getProfileId(), $conn);
		$uprofile1->setExtraFilter(System::getCodeString(8));
		$uprofile1->commitUpdate();
	} catch (Exception $e)	{
		die($e->getMessage());
	}
	$secureConnectionChecked = "";
	if ($uprofile1->isSecureConnectionRequired()) $secureConnectionChecked = "checked";
	$strictSecureConnectionChecked = "";
	if ($uprofile1->isStrictSecureConnectionRequired()) $strictSecureConnectionChecked = "checked";
?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">SSL/TLS Certificates</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
						<div class="ui-sys-warning" style="padding: 1px; margin: 1px; border: 1px dotted white;">
							Enabling <b>Secure Connection</b> will cause connection to this server to be encrypted. The Server will use certificates as configured on the server or if there is a link to a Trusted Certificate Authority (CA)<br/>
							<b>RECOMMENDATION: </b>We strongly recommend to Enable Secure Connection , If you disable this feature you are vulnerable to Man In the Middle Attack <br/><br/>
							Enabling <b>Strict Secure Connection</b> will cause both LAN (Local Area Network) and WAN (Wide Area Network) connections to be secure. However disabling this feature will cause only WAN connections to be secure and LAN to be insecure<br/>
							<b>RECOMMENDATION: </b> You can disable this feature if your network is secured, otherwise enable this feature
						</div>
					</div>	<!--End div.panel-body-->
					<div class="panel-footer text-center">
						<form id="form1" method="POST" action="<?= $thispage ?>">
							<input type="hidden" name="page" value="system_certificate"/>
							<input type="hidden" name="rndx" value="<?= $uprofile1->getExtraFilter() ?>"/>
							<input type="hidden" name="report" value="ihk"/>
							<input type="hidden" name="sysmenu" value="<?= SystemMenu::$__SETTINGS ?>"/>
							<label class="cursor_pointer" title="Check this Box for Secure Connection"><input <?= $secureConnectionChecked ?> type="checkbox" id="isSecured" name="isSecured" value="1"/>&nbsp;&nbsp;Enable/Disable Secure Connection</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="cursor_pointer" title="Check this Box for both LAN and WAN Secure Connection, Uncheck for WAN Secure Connection"><input <?= $strictSecureConnectionChecked ?> type="checkbox" id="isStrictSecured" name="isStrictSecured" value="1"/>&nbsp;&nbsp;Enable/Disable Strict Secure Connection</label><br/>
							<input type="submit" value="Enable/Disable Certificates"/>
						</form>
					</div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<script type="text/javascript">
(function($)	{
	function uiMaintainance($secureCheck1, $strictSecureCheck1)	{
		if (! $secureCheck1.length) return;
		if (! $strictSecureCheck1.length) return;
		$strictSecureCheck1.prop('disabled', ! $secureCheck1.prop('checked'));
	}
	$(function()	{
		$('#isSecured').on('change', function(event)	{
			uiMaintainance($(this), $('#isStrictSecured'));
		});
		uiMaintainance($('#isSecured'), $('#isStrictSecured'));
	});
})(jQuery);
</script>
<?php
	mysql_close($conn);
}/*END: System_Certificate*/else /*BEGIN: System_Reinstall*/if ($page == "system_reinstall" && isset($_REQUEST['rndx']) && isset($_REQUEST['reinstall']) && $login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "setlog", "-1", "-1"))	{
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	$enableUpdate = false;
	$promise1 = new Promise();
	$promise1->setReason("Nothing were done");
	$promise1->setPromise(false);
	$uprofile1 = null;
	try {
		$uprofile1 = new Profile($database, $profile1->getProfileId(), $conn);
		if ($uprofile1->getExtraFilter() != trim($_REQUEST['rndx'])) Object::shootException("The system has detected you are replaying data in your browser window");
		$uprofile1->setExtraFilter(System::getCodeString(8));
		$uprofile1->setInstallationComplete("0");
		$uprofile1->commitUpdate();
		$enableUpdate = true;
		$promise1->setPromise(true);
	} catch (Exception $e)	{
		$promise1->setPromise(false);
		$enableUpdate = false;
		$promise1->setReason($e->getMessage());
	}
?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">System Reinstallation</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		You are about to start the system reinstallation procedure<br/>
		<a href="<?= $thispage ?>">Click Here to Start</a>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in system reinstallation <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>					
					</div>	<!--End div.panel-body-->
					<div class="panel-footer">
<?php 
	if (! $promise1->isPromising())	{
?>
		<a href="<?= $thispage ?>">Back to DashBoard</a>
<?php
	}
?>
					</div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<?php
	mysql_close($conn);
	if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "manageprofile_edit", "System Reinstallation");
} else if ($page == "system_reinstall" && $login1->isRoot() && Authorize::isAllowable($config, "manageprofile_edit", "normal", "setlog", "-1", "-1"))	{
	$uprofile1 = null;
	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
	try {
		$uprofile1 = new Profile($database, $profile1->getProfileId(), $conn);
		$uprofile1->setExtraFilter(System::getCodeString(8));
		$uprofile1->commitUpdate();
	} catch (Exception $e)	{
		die($e->getMessage());
	}
	mysql_close($conn);
?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">System Reinstallation</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
						<div class="ui-sys-warning">
							You are about to reinstall the system. This process will make the system to be unavailable. We strongly 
							advice you to perform this action offline. During the process of reinstallation, other system users will not be able to access the system 
							however they can access the installation wizard. <br/>
							Are you sure you want to proceed?
						</div>
					</div>	<!--End div.panel-body-->
					<div class="panel-footer">
						<div class="text-center">
							<a href="<?= $thispage ?>?page=system_reinstall&rndx=<?= $uprofile1->getExtraFilter() ?>&reinstall=true&sysmenu=<?= SystemMenu::$__SETTINGS ?>"><button>YES (Proceed)</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="<?= $thispage ?>"><button>NO (Cancel)</button></a>
						</div>
					</div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<?php
} /*END: System_Reinstall*/ else {
	//Default Landing Page 
?>
		<div class="row">
			<div class="col-md-12">
<!--Begin: NonApprovalMessage-->
<?php
	//Begin In case you are thrown by trying to access a non approval action 
	// Contains collection of div.row
	if (Authorize::isSessionSet())	{
		$op=Authorize::getSessionValue();;
		$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
		$cid=ContextPosition::getContextIdFromName($database, $op, $conn);
		$context1=new ContextPosition($database, $cid, $conn);
		mysql_close($conn);
?>
		
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Permission Alert</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
						<div class="ui-sys-authorize-syslog ui-state-error">
							<b><i>OPERATION DENIED!!</i></b> <br />
							You do not have enough rights to perform <b><?= $context1->getContextCaption() ?></b> operation
						</div>
					</div>	<!--End div.panel-body-->
					<div class="panel-footer">
						Denied::<?= $context1->getContextName() ?>
					</div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			
<?php
		Authorize::clearSession();
	}
	//End of Accessing a non approval action
?>
<!--End NonApprovalMessage-->
<!--Begin WelcomeNote-->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Welcome Note</h3>
					</div> <!--End div.panel-heading-->
					<div class="panel-body">
						<p>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Dear <?= $login1->getFullname() ?></span>;<br/>
						You are warmly welcome to our <?= $profile1->getSystemName() ?> for <?= $profile1->getProfileName() ?>. This is the view where  you can 
						perform most of operations in this system. The Sidebar will assist you in navigating different pages of this system. In case you are lost, kindly click the <a href="<?= $thispage ?>">DashBoard</a> button on the top left hand side of the page
						</p>
					</div>	<!--End div.panel-body-->
					<div class="panel-footer"><i><?= $profile1->getSystemName() ?></i></div>	<!--End div.panel-footer-->
				</div> <!--End div.panel.panel-default-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<!--End WelcomeNote-->
			</div> <!--End.col-md-12-->
		</div> <!--End div.row-->
<?php
}
?>
<!--END: Business Logic will end at this point-->	
							<div id="__ui_common_errors" class="ui-sys-error-message"></div>
						</div> <!--End. scoop-inner-content-->
					</div>	<!--End .scoop-content-->
				</div> <!--End .scoop-wrapper-->
			</div> <!--End .scoop-main-container-->
		</div><!--End .scoop-container-->
	</div> <!--End .scoop-->
	<div id="__scoop_footer">Footer Text</div>
</body>
</html>