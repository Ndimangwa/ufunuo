-- MySQL dump 10.15  Distrib 10.0.23-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: dbufunuo
-- ------------------------------------------------------
-- Server version	10.0.23-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `dbufunuo`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbufunuo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbufunuo`;

--
-- Table structure for table `AntanaNarivo`
--

DROP TABLE IF EXISTS `AntanaNarivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AntanaNarivo` (
  `antanarivoId` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AntanaNarivo`
--

LOCK TABLES `AntanaNarivo` WRITE;
/*!40000 ALTER TABLE `AntanaNarivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `AntanaNarivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `accountId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `overheadAbsorptionRate` varchar(16) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'1.091',NULL,NULL,0);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `approvalSequenceData`
--

DROP TABLE IF EXISTS `approvalSequenceData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `approvalSequenceData` (
  `dataId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schemaId` int(10) unsigned NOT NULL,
  `requestedBy` int(10) unsigned NOT NULL,
  `nextJobToApprove` int(10) unsigned DEFAULT NULL,
  `alreadyApprovedList` varchar(108) DEFAULT NULL,
  `timeOfRegistration` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `specialInstruction` varchar(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(108) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dataId`),
  KEY `schemaId` (`schemaId`),
  KEY `requestedBy` (`requestedBy`),
  KEY `nextJobToApprove` (`nextJobToApprove`),
  CONSTRAINT `approvalSequenceData_ibfk_1` FOREIGN KEY (`schemaId`) REFERENCES `approvalSequenceSchema` (`schemaId`),
  CONSTRAINT `approvalSequenceData_ibfk_2` FOREIGN KEY (`requestedBy`) REFERENCES `login` (`loginId`),
  CONSTRAINT `approvalSequenceData_ibfk_3` FOREIGN KEY (`nextJobToApprove`) REFERENCES `jobTitle` (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `approvalSequenceData`
--

LOCK TABLES `approvalSequenceData` WRITE;
/*!40000 ALTER TABLE `approvalSequenceData` DISABLE KEYS */;
/*!40000 ALTER TABLE `approvalSequenceData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `approvalSequenceSchema`
--

DROP TABLE IF EXISTS `approvalSequenceSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `approvalSequenceSchema` (
  `schemaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schemaName` varchar(48) NOT NULL,
  `operationCode` int(4) unsigned NOT NULL,
  `approvedJobList` varchar(108) DEFAULT NULL,
  `approvingJobList` varchar(108) DEFAULT NULL,
  `validity` int(4) unsigned NOT NULL DEFAULT '7',
  `defaultApproveText` varchar(48) NOT NULL DEFAULT 'Approved By',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`schemaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `approvalSequenceSchema`
--

LOCK TABLES `approvalSequenceSchema` WRITE;
/*!40000 ALTER TABLE `approvalSequenceSchema` DISABLE KEYS */;
/*!40000 ALTER TABLE `approvalSequenceSchema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bindingType`
--

DROP TABLE IF EXISTS `bindingType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bindingType` (
  `bindingTypeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bindingTypeName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bindingTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bindingType`
--

LOCK TABLES `bindingType` WRITE;
/*!40000 ALTER TABLE `bindingType` DISABLE KEYS */;
/*!40000 ALTER TABLE `bindingType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference`
--

DROP TABLE IF EXISTS `conference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference` (
  `conferenceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conferenceName` varchar(64) NOT NULL,
  `conferenceAbbreviation` varchar(16) DEFAULT NULL,
  `locationList` varchar(108) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`conferenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference`
--

LOCK TABLES `conference` WRITE;
/*!40000 ALTER TABLE `conference` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextDefinition`
--

DROP TABLE IF EXISTS `contextDefinition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextDefinition` (
  `cId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cChar` char(1) NOT NULL,
  `cVal` int(2) NOT NULL,
  PRIMARY KEY (`cId`),
  UNIQUE KEY `cChar` (`cChar`),
  UNIQUE KEY `cVal` (`cVal`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextDefinition`
--

LOCK TABLES `contextDefinition` WRITE;
/*!40000 ALTER TABLE `contextDefinition` DISABLE KEYS */;
INSERT INTO `contextDefinition` VALUES (1,'0',0),(2,'1',1),(3,'2',2),(4,'3',3),(5,'4',4),(6,'5',5),(7,'6',6),(8,'7',7),(9,'8',8),(10,'9',9),(11,'A',10),(12,'B',11),(13,'C',12),(14,'D',13),(15,'E',14),(16,'F',15),(17,'G',16),(18,'H',17),(19,'I',18),(20,'J',19),(21,'K',20),(22,'L',21),(23,'M',22),(24,'N',23),(25,'O',24),(26,'P',25),(27,'Q',26),(28,'R',27),(29,'S',28),(30,'T',29),(31,'U',30),(32,'X',31);
/*!40000 ALTER TABLE `contextDefinition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextManager`
--

DROP TABLE IF EXISTS `contextManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextManager` (
  `defaultXValue` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`defaultXValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextManager`
--

LOCK TABLES `contextManager` WRITE;
/*!40000 ALTER TABLE `contextManager` DISABLE KEYS */;
INSERT INTO `contextManager` VALUES (1);
/*!40000 ALTER TABLE `contextManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contextPosition`
--

DROP TABLE IF EXISTS `contextPosition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contextPosition` (
  `cId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cName` varchar(56) NOT NULL,
  `cPosition` int(4) NOT NULL,
  `caption` varchar(96) NOT NULL,
  PRIMARY KEY (`cId`),
  UNIQUE KEY `cName` (`cName`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contextPosition`
--

LOCK TABLES `contextPosition` WRITE;
/*!40000 ALTER TABLE `contextPosition` DISABLE KEYS */;
INSERT INTO `contextPosition` VALUES (1,'system_login',1,'Logging into System'),(2,'my_account',2,'My Account Settings'),(3,'manage_groups',3,'General Department Management'),(4,'manage_users',4,'User Management'),(5,'control_panel',5,'Control Panel Operations'),(6,'sign_out',6,'Logout from the System'),(7,'procurement',7,'Procurement Operations'),(8,'store',8,'Store Operations'),(9,'production',9,'Production Operations'),(10,'warehouse',10,'Warehouse Operations'),(11,'depot',11,'Depot Operations'),(12,'accounts',12,'Accounts Operations'),(13,'group_add',13,'Add Department'),(14,'group_delete',14,'Delete Department'),(15,'group_edit',15,'Edit Department'),(16,'group_firewall',16,'Change/View Department Firewall'),(17,'manage_jobtitle',17,'Job Title Management'),(18,'group_view',18,'View group details'),(19,'managecurrency_view',19,'View Currency details'),(20,'managecurrency_delete',20,'Delete Currency Definition'),(21,'managecurrency_edit',21,'Edit Currency Definition'),(22,'managecurrency_add',22,'Add a new Currency Definition'),(23,'managecurrency',23,'Global Currency Management'),(24,'manageitem_view',24,'View Item details'),(25,'manageitem_delete',25,'Delete an Item Definition'),(26,'manageitem_edit',26,'Edit an Item Definition'),(27,'manageitem_add',27,'An an Item Definition'),(28,'manageitem',28,'Global Item Management'),(29,'managemeasure_view',29,'View Measurement details'),(30,'managemeasure_delete',30,'Delete Measurement Definition'),(31,'managemeasure_edit',31,'Edit Measurement Definition'),(32,'managemeasure_add',32,'Add a Measurement Definition'),(33,'managemeasure',33,'Global Measurement Definition'),(34,'files',34,'Global Files Management'),(35,'donotcarelastresort',35,'Last resort for Do not care'),(36,'managejobtitle_view',36,'View Job Title Details'),(37,'managejobtitle_delete',37,'Delete a Job Title'),(38,'managejobtitle_edit',38,'Edit a Job Title'),(39,'managejobtitle_add',39,'Add a New Job Title'),(40,'manageuser_view',40,'View User details'),(41,'manageuser_delete',41,'Delete a user'),(42,'manageuser_edit',42,'Edit a user'),(43,'manageuser_add',43,'Add a user'),(44,'controlpanel_firewall',44,'Firewall Rules'),(45,'controlpanel_firewall_jobtitle',45,'Job Title Firewall'),(46,'controlpanel_firewall_user',46,'User Firewall'),(47,'controlpanel_firewall_group',47,'Group Firewall'),(48,'managepurchasingorder',48,'Purchasing Order Management'),(49,'manageitemstore',49,'General Management of Item Store'),(50,'manageitemdemandstore',50,'General Management of Item Demand Store'),(51,'managegoodreturnednote',51,'General Management of Good Returned Note'),(52,'managedeliverynote',52,'General Management of Delivery Note'),(53,'manageinvoice',53,'General Management of Invoices'),(54,'managepricequote',54,'General Management of Price Quote'),(55,'managefinishedgoodreport',55,'General Management of Finished Good Report'),(56,'manageissuenote',56,'General Management of Issue Note'),(57,'managestockrequisition',57,'General Management of Stock and Store Requisitions'),(58,'manageitemstockrecordcard',58,'General Management of Item Stock Record Card'),(59,'managegoodreceivednote',59,'General Management of Good Received Note'),(88,'managejobticket',61,'General Management of Job Ticket'),(89,'manageproduct',62,'General Management of Products'),(90,'managejobticket_create',63,'Allow creation of Job Ticket'),(91,'managejobticket_details',64,'Allow viewing details of a Job Ticket document'),(92,'manageproduct_add',65,'Allow adding a new Product'),(93,'manageproduct_edit',66,'Allow editing an existing Product'),(94,'manageproduct_view',67,'Allow viewing an existing Product'),(95,'manageproduct_delete',68,'Allow deleting an existing Product'),(96,'managepurchasingorder_snapshotviewer',69,'Allow viewing a snapshot of a Purchasing Order'),(97,'managepurchasingorder_create',70,'Allow creattion of a Purchasing Order'),(98,'managepurchasingorder_details',71,'Allowing viewing details of a Purchasing Order document'),(99,'verificationschema',72,'General Management of Approval Definitions'),(100,'verificationschema_drop',73,'Allow dropping/deleting an existing Approval Definition'),(101,'verificationschema_create',74,'Allow creating of a new Approval Definition'),(102,'verificationschema_details',75,'Allow viewing details of an Approval Definition'),(103,'managestockrequisition_snapshotviewer',76,'Allow viewing a snapshot of a Stock or Store Requisition'),(104,'managestockrequisition_create',77,'Allow creating a New Stock or Store Requisition'),(105,'managestockrequisition_details',78,'Allow viewing details of a Stock or Store Requisition Document'),(106,'managegoodreceivednote_create',79,'Allow creation of a New Good Received Note'),(107,'managegoodreceivednote_details',80,'Allow viewing details of a Good Received Note Document'),(108,'manageissuenote_create',81,'Allow creation of a new Issue Note'),(109,'manageissuenote_details',82,'Allow viewing details of an Issue Note Document'),(110,'managefinishedgoodreport_create',83,'Allow creation of a New Finished Good Report'),(111,'managefinishedgoodreport_details',84,'Allow viewing details of a Finished Good Report Document'),(112,'managepricequote_create',85,'Allow creation of a New Price Quote'),(113,'managepricequote_details',86,'Allow viewing details of a Price Quote Document'),(114,'managefinishedgoodreport_addproduct',87,'Allow Add more product quantity to an existing Finished Good Report'),(115,'managesystemlogs',88,'Allow System Logs management'),(116,'manageitemrequisitionstore',89,'Perform Item Requisition Store management'),(117,'managedeliverynote_create',60,'Create Delivery Note'),(118,'managedeliverynote_details',90,'View Delivery Note Document'),(119,'manageinvoice_create',91,'Create a New Invoice'),(120,'manageinvoice_details',92,'View details of an Invoice Document'),(121,'managegoodreturnednote_create',93,'Create Good Returned Note'),(122,'managegoodreturnednote_details',94,'View/Details of Good Returned Note Document'),(123,'manageitemstockrecordcard_create',95,'Create Item Stock Record Card'),(124,'manageitemstockrecordcard_details',96,'View/Details of Item Stock Record Card'),(125,'managewarehouse',97,'Warehouse Management'),(126,'managejobcostsheet',98,'General Management of a Job Cost Sheet'),(127,'managejobcostsheet_create',99,'Create a New Job Cost Sheet'),(128,'managejobcostsheet_details',100,'View Details of a Job Cost Sheet Document'),(129,'manageformulabank',101,'Formula Bank Management'),(130,'manageoverheadabsorptionrate',102,'Absorption Rate Management'),(131,'manageproductmargin',103,'Product Margin');
/*!40000 ALTER TABLE `contextPosition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `countryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryName` varchar(48) NOT NULL,
  `abbreviation` varchar(4) NOT NULL,
  `code` int(3) NOT NULL,
  `timezone` varchar(5) NOT NULL DEFAULT '03:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`countryId`),
  UNIQUE KEY `countryName` (`countryName`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Afghanistan','AF',255,'0300',NULL,NULL,15),(2,'Akrotiri','00',255,'0300',NULL,NULL,15),(3,'Albania','AL',255,'0300',NULL,NULL,15),(4,'Algeria','DZ',255,'0300',NULL,NULL,15),(5,'American Samoa','AS',255,'0300',NULL,NULL,15),(6,'Andorra','AD',255,'0300',NULL,NULL,15),(7,'Angola','AO',255,'0300',NULL,NULL,15),(8,'Anguilla','AI',255,'0300',NULL,NULL,15),(9,'Antarctica','AQ',255,'0300',NULL,NULL,15),(10,'Antigua and Barbuda','AG',255,'0300',NULL,NULL,15),(11,'Argentina','AR',255,'0300',NULL,NULL,15),(12,'Armenia','AM',255,'0300',NULL,NULL,15),(13,'Aruba','AW',255,'0300',NULL,NULL,15),(14,'Ashmore and Cartier Islands','00',255,'0300',NULL,NULL,15),(15,'Australia','AU',255,'0300',NULL,NULL,15),(16,'Austria','AT',255,'0300',NULL,NULL,15),(17,'Azerbaijan','00',255,'0300',NULL,NULL,15),(18,'Bahamas, The','BS',255,'0300',NULL,NULL,15),(19,'Bahrain','BH',255,'0300',NULL,NULL,15),(20,'Bangladesh','BD',255,'0300',NULL,NULL,15),(21,'Barbados','BB',255,'0300',NULL,NULL,15),(22,'Bassas da India','00',255,'0300',NULL,NULL,15),(23,'Belarus','BY',255,'0300',NULL,NULL,15),(24,'Belgium','BE',255,'0300',NULL,NULL,15),(25,'Belize','BZ',255,'0300',NULL,NULL,15),(26,'Benin','BJ',255,'0300',NULL,NULL,15),(27,'Bermuda','BM',255,'0300',NULL,NULL,15),(28,'Bhutan','BT',255,'0300',NULL,NULL,15),(29,'Bolivia','BO',255,'0300',NULL,NULL,15),(30,'Bosnia and Herzegovina','BA',255,'0300',NULL,NULL,15),(31,'Botswana','BW',255,'0300',NULL,NULL,15),(32,'Bouvet Island','BV',255,'0300',NULL,NULL,15),(33,'Brazil','BR',255,'0300',NULL,NULL,15),(34,'British Indian Ocean Territory','IO',255,'0300',NULL,NULL,15),(35,'British Virgin Islands','00',255,'0300',NULL,NULL,15),(36,'Brunei','BN',255,'0300',NULL,NULL,15),(37,'Bulgaria','BG',255,'0300',NULL,NULL,15),(38,'Burkina Faso','BF',255,'0300',NULL,NULL,15),(39,'Burma','MM',255,'0300',NULL,NULL,15),(40,'Burundi','BI',255,'0300',NULL,NULL,15),(41,'Cambodia','KH',255,'0300',NULL,NULL,15),(42,'Cameroon','CM',255,'0300',NULL,NULL,15),(43,'Canada','CA',255,'0300',NULL,NULL,15),(44,'Cape Verde','CV',255,'0300',NULL,NULL,15),(45,'Cayman Islands','KY',255,'0300',NULL,NULL,15),(46,'Central African Republic','CF',255,'0300',NULL,NULL,15),(47,'Chad','TD',255,'0300',NULL,NULL,15),(48,'Chile','CL',255,'0300',NULL,NULL,15),(49,'China','CN',255,'0300',NULL,NULL,15),(50,'Christmas Island','CX',255,'0300',NULL,NULL,15),(51,'Clipperton Island','00',255,'0300',NULL,NULL,15),(52,'Cocos (Keeling) Islands','CC',255,'0300',NULL,NULL,15),(53,'Colombia','CO',255,'0300',NULL,NULL,15),(54,'Comoros','KM',255,'0300',NULL,NULL,15),(55,'Congo, Democratic Republic of th','CD',255,'0300',NULL,NULL,15),(56,'Congo, Republic of the','CG',255,'0300',NULL,NULL,15),(57,'Cook Islands','CK',255,'0300',NULL,NULL,15),(58,'Coral Sea Islands','00',255,'0300',NULL,NULL,15),(59,'Costa Rica','CR',255,'0300',NULL,NULL,15),(60,'Cote d\'Ivoire','CI',255,'0300',NULL,NULL,15),(61,'Croatia','HR',255,'0300',NULL,NULL,15),(62,'Cuba','CU',255,'0300',NULL,NULL,15),(63,'Cyprus','CY',255,'0300',NULL,NULL,15),(64,'Czech Republic','CZ',255,'0300',NULL,NULL,15),(65,'Denmark','DK',255,'0300',NULL,NULL,15),(66,'Dhekelia','00',255,'0300',NULL,NULL,15),(67,'Djibouti','DJ',255,'0300',NULL,NULL,15),(68,'Dominica','DM',255,'0300',NULL,NULL,15),(69,'Dominican Republic','DO',255,'0300',NULL,NULL,15),(70,'Ecuador','EC',255,'0300',NULL,NULL,15),(71,'Egypt','EG',255,'0300',NULL,NULL,15),(72,'El Salvador','SV',255,'0300',NULL,NULL,15),(73,'Equatorial Guinea','GQ',255,'0300',NULL,NULL,15),(74,'Eritrea','ER',255,'0300',NULL,NULL,15),(75,'Estonia','EE',255,'0300',NULL,NULL,15),(76,'Ethiopia','ET',255,'0300',NULL,NULL,15),(77,'Europa Island','00',255,'0300',NULL,NULL,15),(78,'Falkland Islands (Islas Malvinas','FK',255,'0300',NULL,NULL,15),(79,'Faroe Islands','FO',255,'0300',NULL,NULL,15),(80,'Fiji','FJ',255,'0300',NULL,NULL,15),(81,'Finland','FI',255,'0300',NULL,NULL,15),(82,'France','FR',255,'0300',NULL,NULL,15),(83,'French Guiana','GF',255,'0300',NULL,NULL,15),(84,'French Polynesia','PF',255,'0300',NULL,NULL,15),(85,'French Southern and Antarctic La','TF',255,'0300',NULL,NULL,15),(86,'Gabon','GA',255,'0300',NULL,NULL,15),(87,'Gambia, The','GM',255,'0300',NULL,NULL,15),(88,'Gaza Strip','00',255,'0300',NULL,NULL,15),(89,'Georgia','GE',255,'0300',NULL,NULL,15),(90,'Germany','DE',255,'0300',NULL,NULL,15),(91,'Ghana','GH',255,'0300',NULL,NULL,15),(92,'Gibraltar','GI',255,'0300',NULL,NULL,15),(93,'Glorioso Islands','00',255,'0300',NULL,NULL,15),(94,'Greece','GR',255,'0300',NULL,NULL,15),(95,'Greenland','GL',255,'0300',NULL,NULL,15),(96,'Grenada','GD',255,'0300',NULL,NULL,15),(97,'Guadeloupe','GP',255,'0300',NULL,NULL,15),(98,'Guam','GU',255,'0300',NULL,NULL,15),(99,'Guatemala','GT',255,'0300',NULL,NULL,15),(100,'Guernsey','00',255,'0300',NULL,NULL,15),(101,'Guinea','GN',255,'0300',NULL,NULL,15),(102,'Guinea-Bissau','GW',255,'0300',NULL,NULL,15),(103,'Guyana','GY',255,'0300',NULL,NULL,15),(104,'Haiti','HT',255,'0300',NULL,NULL,15),(105,'Heard Island and McDonald Island','HM',255,'0300',NULL,NULL,15),(106,'Holy See (Vatican City)','VA',255,'0300',NULL,NULL,15),(107,'Honduras','HN',255,'0300',NULL,NULL,15),(108,'Hong Kong','HK',255,'0300',NULL,NULL,15),(109,'Hungary','HU',255,'0300',NULL,NULL,15),(110,'Iceland','IS',255,'0300',NULL,NULL,15),(111,'India','IN',255,'0300',NULL,NULL,15),(112,'Indonesia','ID',255,'0300',NULL,NULL,15),(113,'Iran','IR',255,'0300',NULL,NULL,15),(114,'Iraq','IQ',255,'0300',NULL,NULL,15),(115,'Ireland','IE',255,'0300',NULL,NULL,15),(116,'Isle of Man','00',255,'0300',NULL,NULL,15),(117,'Israel','IL',255,'0300',NULL,NULL,15),(118,'Italy','IT',255,'0300',NULL,NULL,15),(119,'Jamaica','JM',255,'0300',NULL,NULL,15),(120,'Jan Mayen','00',255,'0300',NULL,NULL,15),(121,'Japan','JP',255,'0300',NULL,NULL,15),(122,'Jersey','00',255,'0300',NULL,NULL,15),(123,'Jordan','JO',255,'0300',NULL,NULL,15),(124,'Juan de Nova Island','00',255,'0300',NULL,NULL,15),(125,'Kazakhstan','KZ',255,'0300',NULL,NULL,15),(126,'Kenya','KE',255,'0300',NULL,NULL,15),(127,'Kiribati','KI',255,'0300',NULL,NULL,15),(128,'Korea, North','00',255,'0300',NULL,NULL,15),(129,'Korea, South','00',255,'0300',NULL,NULL,15),(130,'Kuwait','KP',255,'0300',NULL,NULL,15),(131,'Kyrgyzstan','KG',255,'0300',NULL,NULL,15),(132,'Laos','LA',255,'0300',NULL,NULL,15),(133,'Latvia','LV',255,'0300',NULL,NULL,15),(134,'Lebanon','LB',255,'0300',NULL,NULL,15),(135,'Lesotho','LS',255,'0300',NULL,NULL,15),(136,'Liberia','LR',255,'0300',NULL,NULL,15),(137,'Libya','LY',255,'0300',NULL,NULL,15),(138,'Liechtenstein','LI',255,'0300',NULL,NULL,15),(139,'Lithuania','LT',255,'0300',NULL,NULL,15),(140,'Luxembourg','LU',255,'0300',NULL,NULL,15),(141,'Macau','MO',255,'0300',NULL,NULL,15),(142,'Macedonia','MK',255,'0300',NULL,NULL,15),(143,'Madagascar','MG',255,'0300',NULL,NULL,15),(144,'Malawi','MW',255,'0300',NULL,NULL,15),(145,'Malaysia','MY',255,'0300',NULL,NULL,15),(146,'Maldives','MV',255,'0300',NULL,NULL,15),(147,'Mali','ML',255,'0300',NULL,NULL,15),(148,'Malta','MT',255,'0300',NULL,NULL,15),(149,'Marshall Islands','MH',255,'0300',NULL,NULL,15),(150,'Martinique','MQ',255,'0300',NULL,NULL,15),(151,'Mauritania','MR',255,'0300',NULL,NULL,15),(152,'Mauritius','MU',255,'0300',NULL,NULL,15),(153,'Mayotte','YT',255,'0300',NULL,NULL,15),(154,'Mexico','MX',255,'0300',NULL,NULL,15),(155,'Micronesia, Federated States of','FM',255,'0300',NULL,NULL,15),(156,'Moldova','MD',255,'0300',NULL,NULL,15),(157,'Monaco','MC',255,'0300',NULL,NULL,15),(158,'Mongolia','MN',255,'0300',NULL,NULL,15),(159,'Montserrat','MS',255,'0300',NULL,NULL,15),(160,'Morocco','MA',255,'0300',NULL,NULL,15),(161,'Mozambique','MZ',255,'0300',NULL,NULL,15),(162,'Namibia','NA',255,'0300',NULL,NULL,15),(163,'Nauru','NR',255,'0300',NULL,NULL,15),(164,'Navassa Island','00',255,'0300',NULL,NULL,15),(165,'Nepal','NP',255,'0300',NULL,NULL,15),(166,'Netherlands','NL',255,'0300',NULL,NULL,15),(167,'Netherlands Antilles','AN',255,'0300',NULL,NULL,15),(168,'New Caledonia','NC',255,'0300',NULL,NULL,15),(169,'New Zealand','NZ',255,'0300',NULL,NULL,15),(170,'Nicaragua','NI',255,'0300',NULL,NULL,15),(171,'Niger','NE',255,'0300',NULL,NULL,15),(172,'Nigeria','NG',255,'0300',NULL,NULL,15),(173,'Niue','NU',255,'0300',NULL,NULL,15),(174,'Norfolk Island','NF',255,'0300',NULL,NULL,15),(175,'Northern Mariana Islands','MP',255,'0300',NULL,NULL,15),(176,'Norway','NO',255,'0300',NULL,NULL,15),(177,'Oman','OM',255,'0300',NULL,NULL,15),(178,'Pakistan','PK',255,'0300',NULL,NULL,15),(179,'Palau','PW',255,'0300',NULL,NULL,15),(180,'Panama','PA',255,'0300',NULL,NULL,15),(181,'Papua New Guinea','PG',255,'0300',NULL,NULL,15),(182,'Paracel Islands','00',255,'0300',NULL,NULL,15),(183,'Paraguay','PY',255,'0300',NULL,NULL,15),(184,'Peru','PE',255,'0300',NULL,NULL,15),(185,'Philippines','PH',255,'0300',NULL,NULL,15),(186,'Pitcairn Islands','PN',255,'0300',NULL,NULL,15),(187,'Poland','PL',255,'0300',NULL,NULL,15),(188,'Portugal','PT',255,'0300',NULL,NULL,15),(189,'Puerto Rico','PR',255,'0300',NULL,NULL,15),(190,'Qatar','QA',255,'0300',NULL,NULL,15),(191,'Reunion','RE',255,'0300',NULL,NULL,15),(192,'Romania','RO',255,'0300',NULL,NULL,15),(193,'Russia','RU',255,'0300',NULL,NULL,15),(194,'Rwanda','RW',255,'0300',NULL,NULL,15),(195,'Saint Helena','00',255,'0300',NULL,NULL,15),(196,'Saint Kitts and Nevis','KN',255,'0300',NULL,NULL,15),(197,'Saint Lucia','LC',255,'0300',NULL,NULL,15),(198,'Saint Pierre and Miquelon','00',255,'0300',NULL,NULL,15),(199,'Saint Vincent and the Grenadines','VC',255,'0300',NULL,NULL,15),(200,'Samoa','WS',255,'0300',NULL,NULL,15),(201,'San Marino','SM',255,'0300',NULL,NULL,15),(202,'Sao Tome and Principe','ST',255,'0300',NULL,NULL,15),(203,'Saudi Arabia','SA',255,'0300',NULL,NULL,15),(204,'Senegal','SN',255,'0300',NULL,NULL,15),(205,'Serbia and Montenegro','RS',255,'0300',NULL,NULL,15),(206,'Seychelles','SC',255,'0300',NULL,NULL,15),(207,'Sierra Leone','SL',255,'0300',NULL,NULL,15),(208,'Singapore','SG',255,'0300',NULL,NULL,15),(209,'Slovakia','SK',255,'0300',NULL,NULL,15),(210,'Slovenia','SI',255,'0300',NULL,NULL,15),(211,'Solomon Islands','SB',255,'0300',NULL,NULL,15),(212,'Somalia','SO',255,'0300',NULL,NULL,15),(213,'South Africa','ZA',255,'0300',NULL,NULL,15),(214,'South Georgia and the South Sand','GS',255,'0300',NULL,NULL,15),(215,'Spain','ES',255,'0300',NULL,NULL,15),(216,'Spratly Islands','00',255,'0300',NULL,NULL,15),(217,'Sri Lanka','LK',255,'0300',NULL,NULL,15),(218,'Sudan','SD',255,'0300',NULL,NULL,15),(219,'Suriname','SR',255,'0300',NULL,NULL,15),(220,'Svalbard','SJ',255,'0300',NULL,NULL,15),(221,'Swaziland','SZ',255,'0300',NULL,NULL,15),(222,'Sweden','SE',255,'0300',NULL,NULL,15),(223,'Switzerland','CH',255,'0300',NULL,NULL,15),(224,'Syria','SY',255,'0300',NULL,NULL,15),(225,'Taiwan','TW',255,'0300',NULL,NULL,15),(226,'Tajikistan','TJ',255,'0300',NULL,NULL,15),(227,'Tanzania','TZ',255,'0300',NULL,NULL,15),(228,'Thailand','TH',255,'0300',NULL,NULL,15),(229,'Timor-Leste','00',255,'0300',NULL,NULL,15),(230,'Togo','TG',255,'0300',NULL,NULL,15),(231,'Tokelau','TK',255,'0300',NULL,NULL,15),(232,'Tonga','TO',255,'0300',NULL,NULL,15),(233,'Trinidad and Tobago','TT',255,'0300',NULL,NULL,15),(234,'Tromelin Island','00',255,'0300',NULL,NULL,15),(235,'Tunisia','TN',255,'0300',NULL,NULL,15),(236,'Turkey','TR',255,'0300',NULL,NULL,15),(237,'Turkmenistan','TM',255,'0300',NULL,NULL,15),(238,'Turks and Caicos Islands','TC',255,'0300',NULL,NULL,15),(239,'Tuvalu','TV',255,'0300',NULL,NULL,15),(240,'Uganda','UG',255,'0300',NULL,NULL,15),(241,'Ukraine','UA',255,'0300',NULL,NULL,15),(242,'United Arab Emirates','AE',255,'0300',NULL,NULL,15),(243,'United Kingdom','GB',255,'0300',NULL,NULL,15),(244,'United States','US',255,'0300',NULL,NULL,15),(245,'Uruguay','UY',255,'0300',NULL,NULL,15),(246,'Uzbekistan','UZ',255,'0300',NULL,NULL,15),(247,'Vanuatu','VU',255,'0300',NULL,NULL,15),(248,'Venezuela','VE',255,'0300',NULL,NULL,15),(249,'Vietnam','VN',255,'0300',NULL,NULL,15),(250,'Virgin Islands','VI',255,'0300',NULL,NULL,15),(251,'Wake Island','00',255,'0300',NULL,NULL,15),(252,'Wallis and Futuna','WF',255,'0300',NULL,NULL,15),(253,'West Bank','00',255,'0300',NULL,NULL,15),(254,'Western Sahara','EH',255,'0300',NULL,NULL,15),(255,'Yemen','YE',255,'0300',NULL,NULL,15),(256,'Zambia','ZM',255,'0300',NULL,NULL,15),(257,'Zimbabwe','ZW',255,'0300',NULL,NULL,15),(258,'South Sudan','SS',255,'0300',NULL,NULL,15);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `currencyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currencyName` varchar(32) NOT NULL,
  `currencyCode` varchar(4) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`currencyId`),
  UNIQUE KEY `currencyName` (`currencyName`),
  UNIQUE KEY `currencyCode` (`currencyCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencyConversion`
--

DROP TABLE IF EXISTS `currencyConversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencyConversion` (
  `conversionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currencyId` int(10) unsigned NOT NULL,
  `value` varchar(32) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`conversionId`),
  KEY `currencyId` (`currencyId`),
  CONSTRAINT `currencyConversion_ibfk_1` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencyConversion`
--

LOCK TABLES `currencyConversion` WRITE;
/*!40000 ALTER TABLE `currencyConversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencyConversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daysOfAWeek`
--

DROP TABLE IF EXISTS `daysOfAWeek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daysOfAWeek` (
  `dayId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dayName` varchar(9) NOT NULL,
  `dayAbbreviation` varchar(4) NOT NULL,
  `offsetValue` int(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dayId`),
  UNIQUE KEY `dayName` (`dayName`),
  UNIQUE KEY `dayAbbreviation` (`dayAbbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daysOfAWeek`
--

LOCK TABLES `daysOfAWeek` WRITE;
/*!40000 ALTER TABLE `daysOfAWeek` DISABLE KEYS */;
INSERT INTO `daysOfAWeek` VALUES (1,'Sunday','Sun',0,NULL,NULL,15),(2,'Monday','Mon',1,NULL,NULL,15),(3,'Tuesday','Tue',2,NULL,NULL,15),(4,'Wednesday','Wed',3,NULL,NULL,15),(5,'Thursday','Thur',4,NULL,NULL,15),(6,'Friday','Fri',5,NULL,NULL,15),(7,'Saturday','Sat',6,NULL,NULL,15);
/*!40000 ALTER TABLE `daysOfAWeek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveryNote`
--

DROP TABLE IF EXISTS `deliveryNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveryNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `deliveryNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveryNote`
--

LOCK TABLES `deliveryNote` WRITE;
/*!40000 ALTER TABLE `deliveryNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveryNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `departmentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`departmentId`),
  UNIQUE KEY `departmentName` (`departmentName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `documentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentName` varchar(32) NOT NULL,
  `documentCode` int(4) unsigned NOT NULL DEFAULT '0',
  `pagename` int(4) unsigned NOT NULL DEFAULT '0',
  `imagename` varchar(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`documentId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (1,'Purchasing Order',0,0,NULL,NULL,NULL,0),(2,'Good Received Note',0,0,NULL,NULL,NULL,0),(3,'Job Ticket',0,0,NULL,NULL,NULL,0),(4,'Stock Requisition',0,0,NULL,NULL,NULL,0),(5,'Store Requisition',0,0,NULL,NULL,NULL,0),(6,'Issue Note',0,0,NULL,NULL,NULL,0),(7,'Finished Good Report',0,0,NULL,NULL,NULL,0),(8,'Price Quote',0,0,NULL,NULL,NULL,0),(9,'Delivery Note',0,0,NULL,NULL,NULL,0),(10,'Invoice',0,0,NULL,NULL,NULL,0),(11,'Job Cost Sheet',0,0,NULL,NULL,NULL,0),(12,'Good Returned Note',0,0,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentTransaction`
--

DROP TABLE IF EXISTS `documentTransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentTransaction` (
  `transactionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction` varchar(128) NOT NULL,
  `docRefNumber` varchar(24) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transactionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentTransaction`
--

LOCK TABLES `documentTransaction` WRITE;
/*!40000 ALTER TABLE `documentTransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentTransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finishedGoodReport`
--

DROP TABLE IF EXISTS `finishedGoodReport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finishedGoodReport` (
  `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportNumber` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reportId`),
  UNIQUE KEY `reportNumber` (`reportNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `finishedGoodReport_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finishedGoodReport`
--

LOCK TABLES `finishedGoodReport` WRITE;
/*!40000 ALTER TABLE `finishedGoodReport` DISABLE KEYS */;
/*!40000 ALTER TABLE `finishedGoodReport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goodReceivedNote`
--

DROP TABLE IF EXISTS `goodReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goodReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `orderId` int(10) unsigned DEFAULT NULL,
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  KEY `orderId` (`orderId`),
  CONSTRAINT `goodReceivedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `goodReceivedNote_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `purchasingOrder` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goodReceivedNote`
--

LOCK TABLES `goodReceivedNote` WRITE;
/*!40000 ALTER TABLE `goodReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `goodReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goodReturnedNote`
--

DROP TABLE IF EXISTS `goodReturnedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goodReturnedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `documentId` (`documentId`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  CONSTRAINT `goodReturnedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `goodReturnedNote_ibfk_2` FOREIGN KEY (`deliveryNoteId`) REFERENCES `deliveryNote` (`noteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goodReturnedNote`
--

LOCK TABLES `goodReturnedNote` WRITE;
/*!40000 ALTER TABLE `goodReturnedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `goodReturnedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `groupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(32) NOT NULL,
  `pId` int(10) unsigned DEFAULT NULL,
  `context` text NOT NULL,
  `rootGroup` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupId`),
  KEY `pId` (`pId`),
  CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`pId`) REFERENCES `groups` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Local Group',NULL,'',0,NULL,NULL,0);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoiceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `amount` varchar(32) NOT NULL DEFAULT '0.0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invoiceId`),
  UNIQUE KEY `invoiceNumber` (`invoiceNumber`),
  KEY `documentId` (`documentId`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`deliveryNoteId`) REFERENCES `deliveryNote` (`noteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoiceDemandBank`
--

DROP TABLE IF EXISTS `invoiceDemandBank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoiceDemandBank` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `invoiceId` (`invoiceId`),
  CONSTRAINT `invoiceDemandBank_ibfk_1` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoiceDemandBank`
--

LOCK TABLES `invoiceDemandBank` WRITE;
/*!40000 ALTER TABLE `invoiceDemandBank` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoiceDemandBank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issueNote`
--

DROP TABLE IF EXISTS `issueNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issueNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `requisitionId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `documentId` (`documentId`),
  KEY `requisitionId` (`requisitionId`),
  CONSTRAINT `issueNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `issueNote_ibfk_2` FOREIGN KEY (`requisitionId`) REFERENCES `stockRequisition` (`reqId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issueNote`
--

LOCK TABLES `issueNote` WRITE;
/*!40000 ALTER TABLE `issueNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `issueNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `itemId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemName` varchar(128) NOT NULL,
  `measureId` int(10) unsigned NOT NULL,
  `categoryId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemId`),
  UNIQUE KEY `itemName` (`itemName`),
  KEY `measureId` (`measureId`),
  KEY `fbk_item_category` (`categoryId`),
  CONSTRAINT `fbk_item_category` FOREIGN KEY (`categoryId`) REFERENCES `itemCategory` (`categoryId`),
  CONSTRAINT `item_ibfk_1` FOREIGN KEY (`measureId`) REFERENCES `measure` (`measureId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemCategory`
--

DROP TABLE IF EXISTS `itemCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemCategory` (
  `categoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemCategory`
--

LOCK TABLES `itemCategory` WRITE;
/*!40000 ALTER TABLE `itemCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemDemandStore`
--

DROP TABLE IF EXISTS `itemDemandStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemDemandStore` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeId` int(10) unsigned NOT NULL,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `itemDemandStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`),
  CONSTRAINT `itemDemandStore_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemDemandStore`
--

LOCK TABLES `itemDemandStore` WRITE;
/*!40000 ALTER TABLE `itemDemandStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemDemandStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemRequisitionStore`
--

DROP TABLE IF EXISTS `itemRequisitionStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemRequisitionStore` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeId` int(10) unsigned NOT NULL,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `itemRequisitionStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`),
  CONSTRAINT `itemRequisitionStore_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemRequisitionStore`
--

LOCK TABLES `itemRequisitionStore` WRITE;
/*!40000 ALTER TABLE `itemRequisitionStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemRequisitionStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemStore`
--

DROP TABLE IF EXISTS `itemStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemStore` (
  `itemStoreId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `itemValue` varchar(32) NOT NULL DEFAULT '0.0',
  `minStockLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `orderingLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `itemPrice` varchar(32) NOT NULL DEFAULT '0.0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemStoreId`),
  UNIQUE KEY `itemId` (`itemId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `itemStore_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`),
  CONSTRAINT `itemStore_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemStore`
--

LOCK TABLES `itemStore` WRITE;
/*!40000 ALTER TABLE `itemStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobCostSheet`
--

DROP TABLE IF EXISTS `jobCostSheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobCostSheet` (
  `jobCostId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobCostNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `jobCostOpeningDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `jobCostWantedDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `jobCostClosingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `productId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jobCostId`),
  KEY `documentId` (`documentId`),
  KEY `productId` (`productId`),
  CONSTRAINT `jobCostSheet_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `jobCostSheet_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobCostSheet`
--

LOCK TABLES `jobCostSheet` WRITE;
/*!40000 ALTER TABLE `jobCostSheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobCostSheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobTicket`
--

DROP TABLE IF EXISTS `jobTicket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobTicket` (
  `ticketId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticketNumber` varchar(24) NOT NULL,
  `ticketDetails` varchar(64) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `openingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `wantedDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(40) DEFAULT NULL,
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ticketId`),
  UNIQUE KEY `ticketNumber` (`ticketNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `jobTicket_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobTicket`
--

LOCK TABLES `jobTicket` WRITE;
/*!40000 ALTER TABLE `jobTicket` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobTicket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobTitle`
--

DROP TABLE IF EXISTS `jobTitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobTitle` (
  `jobId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobName` varchar(64) NOT NULL,
  `context` text NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jobId`),
  UNIQUE KEY `jobName` (`jobName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobTitle`
--

LOCK TABLES `jobTitle` WRITE;
/*!40000 ALTER TABLE `jobTitle` DISABLE KEYS */;
INSERT INTO `jobTitle` VALUES (1,'Local Admin','',NULL,NULL,0);
/*!40000 ALTER TABLE `jobTitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localGoodReceivedNote`
--

DROP TABLE IF EXISTS `localGoodReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localGoodReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localGoodReceivedNote_ibfk_1` FOREIGN KEY (`deliveryNoteId`) REFERENCES `localItemDeliveryNote` (`noteId`),
  CONSTRAINT `localGoodReceivedNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localGoodReceivedNote`
--

LOCK TABLES `localGoodReceivedNote` WRITE;
/*!40000 ALTER TABLE `localGoodReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localGoodReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localItemDeliveryNote`
--

DROP TABLE IF EXISTS `localItemDeliveryNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localItemDeliveryNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `reqId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `reqId` (`reqId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localItemDeliveryNote_ibfk_1` FOREIGN KEY (`reqId`) REFERENCES `localStoreRequisition` (`reqId`),
  CONSTRAINT `localItemDeliveryNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localItemDeliveryNote`
--

LOCK TABLES `localItemDeliveryNote` WRITE;
/*!40000 ALTER TABLE `localItemDeliveryNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localItemDeliveryNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localItemDemandStore`
--

DROP TABLE IF EXISTS `localItemDemandStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localItemDemandStore` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeId` int(10) unsigned NOT NULL,
  `itemId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `storeId` (`storeId`,`itemId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `localitemDemandStore_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `localitemDemandStore_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localItemDemandStore`
--

LOCK TABLES `localItemDemandStore` WRITE;
/*!40000 ALTER TABLE `localItemDemandStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `localItemDemandStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localProductDeliveryNote`
--

DROP TABLE IF EXISTS `localProductDeliveryNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localProductDeliveryNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(32) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `reqId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `reqId` (`reqId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localProductDeliveryNote_ibfk_1` FOREIGN KEY (`reqId`) REFERENCES `localWarehouseRequisition` (`reqId`),
  CONSTRAINT `localProductDeliveryNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localProductDeliveryNote`
--

LOCK TABLES `localProductDeliveryNote` WRITE;
/*!40000 ALTER TABLE `localProductDeliveryNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localProductDeliveryNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localProductDemand`
--

DROP TABLE IF EXISTS `localProductDemand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localProductDemand` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouseId` int(10) unsigned NOT NULL,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `warehouseId` (`warehouseId`,`productId`),
  KEY `productId` (`productId`),
  CONSTRAINT `localProductDemand_ibfk_1` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `localProductDemand_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localProductDemand`
--

LOCK TABLES `localProductDemand` WRITE;
/*!40000 ALTER TABLE `localProductDemand` DISABLE KEYS */;
/*!40000 ALTER TABLE `localProductDemand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localProductReceivedNote`
--

DROP TABLE IF EXISTS `localProductReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localProductReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `deliveryNoteId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  UNIQUE KEY `noteNumber` (`noteNumber`),
  KEY `deliveryNoteId` (`deliveryNoteId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `localProductReceivedNote_ibfk_1` FOREIGN KEY (`deliveryNoteId`) REFERENCES `localProductDeliveryNote` (`noteId`),
  CONSTRAINT `localProductReceivedNote_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localProductReceivedNote`
--

LOCK TABLES `localProductReceivedNote` WRITE;
/*!40000 ALTER TABLE `localProductReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `localProductReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localStoreRequisition`
--

DROP TABLE IF EXISTS `localStoreRequisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localStoreRequisition` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqNo` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `sourceStoreId` int(10) unsigned NOT NULL,
  `destinationStoreId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `reqNo` (`reqNo`),
  KEY `documentId` (`documentId`),
  KEY `sourceStoreId` (`sourceStoreId`),
  KEY `destinationStoreId` (`destinationStoreId`),
  CONSTRAINT `localStoreRequisition_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `localStoreRequisition_ibfk_2` FOREIGN KEY (`sourceStoreId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `localStoreRequisition_ibfk_3` FOREIGN KEY (`destinationStoreId`) REFERENCES `physicalStore` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localStoreRequisition`
--

LOCK TABLES `localStoreRequisition` WRITE;
/*!40000 ALTER TABLE `localStoreRequisition` DISABLE KEYS */;
/*!40000 ALTER TABLE `localStoreRequisition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localWarehouseRequisition`
--

DROP TABLE IF EXISTS `localWarehouseRequisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localWarehouseRequisition` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqNo` varchar(24) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `sourceWarehouseId` int(10) unsigned NOT NULL,
  `destinationWarehouseId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `reqNo` (`reqNo`),
  KEY `documentId` (`documentId`),
  KEY `sourceWarehouseId` (`sourceWarehouseId`),
  KEY `destinationWarehouseId` (`destinationWarehouseId`),
  CONSTRAINT `localWarehouseRequisition_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `localWarehouseRequisition_ibfk_2` FOREIGN KEY (`sourceWarehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `localWarehouseRequisition_ibfk_3` FOREIGN KEY (`destinationWarehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localWarehouseRequisition`
--

LOCK TABLES `localWarehouseRequisition` WRITE;
/*!40000 ALTER TABLE `localWarehouseRequisition` DISABLE KEYS */;
/*!40000 ALTER TABLE `localWarehouseRequisition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `locationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locationName` varchar(64) NOT NULL,
  `conferenceId` int(10) unsigned DEFAULT NULL,
  `physicalAddress` varchar(64) NOT NULL,
  `postalAddress` varchar(64) DEFAULT NULL,
  `gpsCoordinate` varchar(72) DEFAULT NULL,
  `countryId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`locationId`),
  KEY `conferenceId` (`conferenceId`),
  KEY `countryId` (`countryId`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`conferenceId`) REFERENCES `conference` (`conferenceId`),
  CONSTRAINT `location_ibfk_2` FOREIGN KEY (`countryId`) REFERENCES `country` (`countryId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Installation Location',NULL,'Installation Address',NULL,NULL,227,NULL,NULL,0);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `loginId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loginName` varchar(32) DEFAULT NULL,
  `password` char(40) NOT NULL,
  `fullName` varchar(48) DEFAULT NULL,
  `groupId` int(10) unsigned DEFAULT NULL,
  `sexId` int(10) unsigned DEFAULT NULL,
  `maritalId` int(10) unsigned DEFAULT NULL,
  `context` text NOT NULL,
  `jobId` int(10) unsigned DEFAULT NULL,
  `root` tinyint(1) NOT NULL DEFAULT '0',
  `locationId` int(10) unsigned DEFAULT NULL,
  `departmentId` int(10) unsigned DEFAULT NULL,
  `initializationAccount` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loginId`),
  KEY `fk_group` (`groupId`),
  KEY `fk_sex` (`sexId`),
  KEY `fk_marital` (`maritalId`),
  KEY `fk_department` (`departmentId`),
  KEY `fk_location` (`locationId`),
  KEY `fk_jobTitle` (`jobId`),
  CONSTRAINT `fk_department` FOREIGN KEY (`departmentId`) REFERENCES `department` (`departmentId`),
  CONSTRAINT `fk_group` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`),
  CONSTRAINT `fk_jobTitle` FOREIGN KEY (`jobId`) REFERENCES `jobTitle` (`jobId`),
  CONSTRAINT `fk_location` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`),
  CONSTRAINT `fk_marital` FOREIGN KEY (`maritalId`) REFERENCES `marital` (`maritalId`),
  CONSTRAINT `fk_sex` FOREIGN KEY (`sexId`) REFERENCES `sex` (`sexId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,NULL,'b3aca92c793ee0e9b1a9b0a5f5fc044e05140df3',NULL,NULL,NULL,NULL,'',NULL,1,NULL,NULL,0,NULL,NULL,0);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marital`
--

DROP TABLE IF EXISTS `marital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marital` (
  `maritalId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `maritalName` varchar(24) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`maritalId`),
  UNIQUE KEY `maritalName` (`maritalName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marital`
--

LOCK TABLES `marital` WRITE;
/*!40000 ALTER TABLE `marital` DISABLE KEYS */;
INSERT INTO `marital` VALUES (1,'Single',NULL,NULL,15),(2,'Married',NULL,NULL,15),(3,'Divorced',NULL,NULL,15),(4,'Widowed',NULL,NULL,15),(5,'Cohabiting',NULL,NULL,15),(6,'Civil Union',NULL,NULL,15),(7,'Domestic Partner',NULL,NULL,15),(8,'Unmarried Partner',NULL,NULL,15);
/*!40000 ALTER TABLE `marital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measure` (
  `measureId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `measureName` varchar(24) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`measureId`),
  UNIQUE KEY `measureName` (`measureName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure`
--

LOCK TABLES `measure` WRITE;
/*!40000 ALTER TABLE `measure` DISABLE KEYS */;
/*!40000 ALTER TABLE `measure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messagePolicyType`
--

DROP TABLE IF EXISTS `messagePolicyType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messagePolicyType` (
  `typeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeName` varchar(48) NOT NULL,
  `typeCode` int(2) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`typeId`),
  UNIQUE KEY `typeName` (`typeName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messagePolicyType`
--

LOCK TABLES `messagePolicyType` WRITE;
/*!40000 ALTER TABLE `messagePolicyType` DISABLE KEYS */;
/*!40000 ALTER TABLE `messagePolicyType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messageType`
--

DROP TABLE IF EXISTS `messageType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messageType` (
  `typeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeName` varchar(48) NOT NULL,
  `typeCode` int(2) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`typeId`),
  UNIQUE KEY `typeName` (`typeName`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messageType`
--

LOCK TABLES `messageType` WRITE;
/*!40000 ALTER TABLE `messageType` DISABLE KEYS */;
INSERT INTO `messageType` VALUES (1,'Local',1,NULL,NULL,15),(2,'SMS',2,NULL,NULL,15),(3,'Email',3,NULL,NULL,15);
/*!40000 ALTER TABLE `messageType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monthOfAYear`
--

DROP TABLE IF EXISTS `monthOfAYear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthOfAYear` (
  `monthId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `monthName` varchar(9) NOT NULL,
  `monthAbbreviation` varchar(3) NOT NULL,
  `monthNumber` int(2) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`monthId`),
  UNIQUE KEY `monthName` (`monthName`),
  UNIQUE KEY `monthAbbreviation` (`monthAbbreviation`),
  UNIQUE KEY `monthNumber` (`monthNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monthOfAYear`
--

LOCK TABLES `monthOfAYear` WRITE;
/*!40000 ALTER TABLE `monthOfAYear` DISABLE KEYS */;
INSERT INTO `monthOfAYear` VALUES (1,'January','Jan',1,NULL,NULL,15),(2,'February','Feb',2,NULL,NULL,15),(3,'March','Mar',3,NULL,NULL,15),(4,'April','Apr',4,NULL,NULL,15),(5,'May','May',5,NULL,NULL,15),(6,'June','Jun',6,NULL,NULL,15),(7,'July','Jul',7,NULL,NULL,15),(8,'August','Aug',8,NULL,NULL,15),(9,'September','Sep',9,NULL,NULL,15),(10,'October','Oct',10,NULL,NULL,15),(11,'November','Nov',11,NULL,NULL,15),(12,'December','Dec',12,NULL,NULL,15);
/*!40000 ALTER TABLE `monthOfAYear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pHPTimezone`
--

DROP TABLE IF EXISTS `pHPTimezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pHPTimezone` (
  `zoneId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zoneName` varchar(48) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`zoneId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pHPTimezone`
--

LOCK TABLES `pHPTimezone` WRITE;
/*!40000 ALTER TABLE `pHPTimezone` DISABLE KEYS */;
INSERT INTO `pHPTimezone` VALUES (1,'Africa/Dar_es_Salaam',NULL,NULL,15);
/*!40000 ALTER TABLE `pHPTimezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper`
--

DROP TABLE IF EXISTS `paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper` (
  `paperId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paperName` varchar(32) NOT NULL,
  `width` int(4) unsigned NOT NULL DEFAULT '0',
  `height` int(4) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paperId`),
  UNIQUE KEY `paperName` (`paperName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper`
--

LOCK TABLES `paper` WRITE;
/*!40000 ALTER TABLE `paper` DISABLE KEYS */;
/*!40000 ALTER TABLE `paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentReceipt`
--

DROP TABLE IF EXISTS `paymentReceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentReceipt` (
  `receiptId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receiptNumber` varchar(32) NOT NULL,
  `invoiceId` int(10) unsigned NOT NULL,
  `amount` varchar(32) NOT NULL DEFAULT '0.0',
  `paymentTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiptId`),
  KEY `invoiceId` (`invoiceId`),
  CONSTRAINT `paymentReceipt_ibfk_1` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentReceipt`
--

LOCK TABLES `paymentReceipt` WRITE;
/*!40000 ALTER TABLE `paymentReceipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentReceipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentReceiptBank`
--

DROP TABLE IF EXISTS `paymentReceiptBank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentReceiptBank` (
  `bankId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receiptId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bankId`),
  KEY `receiptId` (`receiptId`),
  CONSTRAINT `paymentReceiptBank_ibfk_1` FOREIGN KEY (`receiptId`) REFERENCES `paymentReceipt` (`receiptId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentReceiptBank`
--

LOCK TABLES `paymentReceiptBank` WRITE;
/*!40000 ALTER TABLE `paymentReceiptBank` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentReceiptBank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physicalStore`
--

DROP TABLE IF EXISTS `physicalStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physicalStore` (
  `storeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeName` varchar(32) NOT NULL,
  `locationId` int(10) unsigned NOT NULL,
  `openingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`storeId`),
  KEY `locationId` (`locationId`),
  CONSTRAINT `physicalStore_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physicalStore`
--

LOCK TABLES `physicalStore` WRITE;
/*!40000 ALTER TABLE `physicalStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `physicalStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physicalWarehouse`
--

DROP TABLE IF EXISTS `physicalWarehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physicalWarehouse` (
  `warehouseId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouseName` varchar(32) NOT NULL,
  `locationId` int(10) unsigned NOT NULL,
  `openingDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`warehouseId`),
  KEY `locationId` (`locationId`),
  CONSTRAINT `physicalWarehouse_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physicalWarehouse`
--

LOCK TABLES `physicalWarehouse` WRITE;
/*!40000 ALTER TABLE `physicalWarehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `physicalWarehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priceQuote`
--

DROP TABLE IF EXISTS `priceQuote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priceQuote` (
  `quoteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quoteNumber` varchar(36) NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`quoteId`),
  UNIQUE KEY `quoteNumber` (`quoteNumber`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `priceQuote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priceQuote`
--

LOCK TABLES `priceQuote` WRITE;
/*!40000 ALTER TABLE `priceQuote` DISABLE KEYS */;
/*!40000 ALTER TABLE `priceQuote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `productId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productName` varchar(32) NOT NULL,
  `categoryId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`productId`),
  UNIQUE KEY `productName` (`productName`),
  KEY `fbk_product_category` (`categoryId`),
  CONSTRAINT `fbk_product_category` FOREIGN KEY (`categoryId`) REFERENCES `productCategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productCategory`
--

DROP TABLE IF EXISTS `productCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productCategory` (
  `categoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(32) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productCategory`
--

LOCK TABLES `productCategory` WRITE;
/*!40000 ALTER TABLE `productCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `productCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productDemandStore`
--

DROP TABLE IF EXISTS `productDemandStore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productDemandStore` (
  `demandId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouseId` int(10) unsigned NOT NULL,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demandId`),
  UNIQUE KEY `productId` (`productId`),
  KEY `warehouseId` (`warehouseId`),
  CONSTRAINT `productDemandStore_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `productDemandStore_ibfk_2` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productDemandStore`
--

LOCK TABLES `productDemandStore` WRITE;
/*!40000 ALTER TABLE `productDemandStore` DISABLE KEYS */;
/*!40000 ALTER TABLE `productDemandStore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productGoodReceivedNote`
--

DROP TABLE IF EXISTS `productGoodReceivedNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productGoodReceivedNote` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) DEFAULT '0',
  `code` char(40) DEFAULT NULL,
  `orderId` int(10) unsigned DEFAULT NULL,
  `hcharges` int(17) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteId`),
  KEY `documentId` (`documentId`),
  KEY `orderId` (`orderId`),
  CONSTRAINT `productGoodReceivedNote_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `productGoodReceivedNote_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `productPurchasingOrder` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productGoodReceivedNote`
--

LOCK TABLES `productGoodReceivedNote` WRITE;
/*!40000 ALTER TABLE `productGoodReceivedNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `productGoodReceivedNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productPurchasingOrder`
--

DROP TABLE IF EXISTS `productPurchasingOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productPurchasingOrder` (
  `orderId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `warehouseId` int(10) unsigned NOT NULL,
  `supplierName` varchar(32) DEFAULT NULL,
  `supplierAddress` varchar(64) DEFAULT NULL,
  `supplierPhone` varchar(14) DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `currencyId` int(10) unsigned DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` char(40) NOT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `orderNumber` (`orderNumber`),
  KEY `currencyId` (`currencyId`),
  KEY `warehouseId` (`warehouseId`),
  KEY `documentId` (`documentId`),
  CONSTRAINT `productPurchasingOrder_ibfk_1` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`),
  CONSTRAINT `productPurchasingOrder_ibfk_2` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `productPurchasingOrder_ibfk_3` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productPurchasingOrder`
--

LOCK TABLES `productPurchasingOrder` WRITE;
/*!40000 ALTER TABLE `productPurchasingOrder` DISABLE KEYS */;
/*!40000 ALTER TABLE `productPurchasingOrder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `profileId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profileName` varchar(96) NOT NULL DEFAULT 'Default Profile',
  `systemName` varchar(108) NOT NULL DEFAULT 'Default SYS',
  `logo` varchar(64) DEFAULT NULL,
  `telephoneList` varchar(108) DEFAULT NULL,
  `emailList` varchar(108) DEFAULT NULL,
  `otherCommunication` varchar(108) DEFAULT NULL,
  `locationId` int(10) unsigned DEFAULT NULL,
  `website` varchar(64) DEFAULT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `pHPTimezoneId` int(10) unsigned DEFAULT NULL,
  `dob` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `themeId` int(10) unsigned DEFAULT NULL,
  `firstDayOfAWeekId` int(10) unsigned DEFAULT NULL,
  `xmlFile` varchar(12) NOT NULL DEFAULT 'profile.xml',
  `xmlFileChecksum` char(32) DEFAULT NULL,
  `serverIpAddress` varchar(15) NOT NULL DEFAULT '192.168.1.1',
  `localAreaNetworkMask` varchar(15) NOT NULL DEFAULT '255.255.255.0',
  `maxNumberOfReturnedSearchRecords` int(4) NOT NULL DEFAULT '512',
  `maxNumberOfDisplayedRowsPerPage` int(4) NOT NULL DEFAULT '64',
  `minAgeCriteriaForUsers` int(4) NOT NULL DEFAULT '12',
  `applicationCounter` int(2) unsigned NOT NULL DEFAULT '0',
  `revisionNumber` int(8) unsigned NOT NULL DEFAULT '1',
  `revisionTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `serverMACAddress` varchar(24) DEFAULT NULL,
  `systemHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`profileId`),
  KEY `locationId` (`locationId`),
  KEY `pHPTimezoneId` (`pHPTimezoneId`),
  KEY `themeId` (`themeId`),
  KEY `firstDayOfAWeekId` (`firstDayOfAWeekId`),
  CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`pHPTimezoneId`) REFERENCES `pHPTimezone` (`zoneId`),
  CONSTRAINT `profile_ibfk_3` FOREIGN KEY (`themeId`) REFERENCES `themes` (`themeId`),
  CONSTRAINT `profile_ibfk_4` FOREIGN KEY (`firstDayOfAWeekId`) REFERENCES `daysOfAWeek` (`dayId`),
  CONSTRAINT `profile_ibfk_5` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'Default System Profile','Default SYS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000:00:00:00:00:00',NULL,NULL,'profile.xml',NULL,'192.168.1.1','255.255.255.0',512,64,12,0,1,'0000:00:00:00:00:00',NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchasingOrder`
--

DROP TABLE IF EXISTS `purchasingOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchasingOrder` (
  `orderId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderNumber` varchar(24) DEFAULT NULL,
  `documentId` int(10) unsigned DEFAULT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `supplierName` varchar(32) DEFAULT NULL,
  `supplierAddress` varchar(64) DEFAULT NULL,
  `supplierPhone` varchar(14) DEFAULT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `currencyId` int(10) unsigned DEFAULT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `code` char(40) NOT NULL,
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `orderNumber` (`orderNumber`),
  KEY `documentId` (`documentId`),
  KEY `currencyId` (`currencyId`),
  KEY `fk_constraint_purchasing_order` (`storeId`),
  CONSTRAINT `fk_constraint_purchasing_order` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `purchasingOrder_ibfk_1` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `purchasingOrder_ibfk_2` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchasingOrder`
--

LOCK TABLES `purchasingOrder` WRITE;
/*!40000 ALTER TABLE `purchasingOrder` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchasingOrder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesReport`
--

DROP TABLE IF EXISTS `salesReport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesReport` (
  `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportName` varchar(32) NOT NULL,
  `dateTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `invoiceId` int(10) unsigned NOT NULL,
  `conferenceId` int(10) unsigned DEFAULT NULL,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(14) unsigned NOT NULL,
  `productValue` int(17) unsigned NOT NULL,
  `productSellingPrice` int(17) unsigned NOT NULL,
  `customerName` varchar(32) DEFAULT NULL,
  `locationId` int(10) unsigned DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reportId`),
  KEY `locationId` (`locationId`),
  KEY `conferenceId` (`conferenceId`),
  KEY `productId` (`productId`),
  KEY `invoiceId` (`invoiceId`),
  CONSTRAINT `salesReport_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`),
  CONSTRAINT `salesReport_ibfk_2` FOREIGN KEY (`conferenceId`) REFERENCES `conference` (`conferenceId`),
  CONSTRAINT `salesReport_ibfk_3` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `salesReport_ibfk_4` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesReport`
--

LOCK TABLES `salesReport` WRITE;
/*!40000 ALTER TABLE `salesReport` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesReport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sex`
--

DROP TABLE IF EXISTS `sex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sex` (
  `sexId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sexName` varchar(8) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`sexId`),
  UNIQUE KEY `sexName` (`sexName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sex`
--

LOCK TABLES `sex` WRITE;
/*!40000 ALTER TABLE `sex` DISABLE KEYS */;
INSERT INTO `sex` VALUES (1,'Male',NULL,NULL,15),(2,'Female',NULL,NULL,15);
/*!40000 ALTER TABLE `sex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stockItemRecordCard`
--

DROP TABLE IF EXISTS `stockItemRecordCard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockItemRecordCard` (
  `recordCardId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `recordDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `refNumber` varchar(32) NOT NULL,
  `titleText` varchar(32) DEFAULT NULL,
  `quantity` int(16) NOT NULL DEFAULT '0',
  `unitValue` varchar(32) NOT NULL DEFAULT '0.0',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recordCardId`),
  KEY `storeId` (`storeId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `stockItemRecordCard_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `stockItemRecordCard_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stockItemRecordCard`
--

LOCK TABLES `stockItemRecordCard` WRITE;
/*!40000 ALTER TABLE `stockItemRecordCard` DISABLE KEYS */;
/*!40000 ALTER TABLE `stockItemRecordCard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stockRequisition`
--

DROP TABLE IF EXISTS `stockRequisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockRequisition` (
  `reqId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqNo` varchar(24) NOT NULL,
  `sheetId` int(10) unsigned DEFAULT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `documentId` int(10) unsigned NOT NULL,
  `departmentId` int(10) unsigned DEFAULT NULL,
  `debtAccount` varchar(32) NOT NULL,
  `creationTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `closingTime` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `code` char(40) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `stocked` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(64) DEFAULT NULL,
  `filenameHash` char(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reqId`),
  UNIQUE KEY `reqNo` (`reqNo`),
  KEY `sheetId` (`sheetId`),
  KEY `documentId` (`documentId`),
  KEY `fk_constraint_department` (`departmentId`),
  KEY `fk_constraint_physical_store` (`storeId`),
  CONSTRAINT `fk_constraint_department` FOREIGN KEY (`departmentId`) REFERENCES `department` (`departmentId`),
  CONSTRAINT `fk_constraint_physical_store` FOREIGN KEY (`storeId`) REFERENCES `physicalStore` (`storeId`),
  CONSTRAINT `stockRequisition_ibfk_1` FOREIGN KEY (`sheetId`) REFERENCES `jobCostSheet` (`jobCostId`),
  CONSTRAINT `stockRequisition_ibfk_2` FOREIGN KEY (`documentId`) REFERENCES `document` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stockRequisition`
--

LOCK TABLES `stockRequisition` WRITE;
/*!40000 ALTER TABLE `stockRequisition` DISABLE KEYS */;
/*!40000 ALTER TABLE `stockRequisition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemPolicy`
--

DROP TABLE IF EXISTS `systemPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemPolicy` (
  `policyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `className` varchar(64) NOT NULL,
  `policyCaption` varchar(96) NOT NULL,
  `root` tinyint(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`policyId`),
  UNIQUE KEY `className` (`className`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemPolicy`
--

LOCK TABLES `systemPolicy` WRITE;
/*!40000 ALTER TABLE `systemPolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemlogs`
--

DROP TABLE IF EXISTS `systemlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemlogs` (
  `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logName` varchar(108) NOT NULL,
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemlogs`
--

LOCK TABLES `systemlogs` WRITE;
/*!40000 ALTER TABLE `systemlogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemlogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `testId` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `testName` varchar(12) NOT NULL,
  PRIMARY KEY (`testId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes` (
  `themeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `themeName` varchar(24) NOT NULL,
  `themeFolder` varchar(16) NOT NULL,
  `themeBackgroundColor` varchar(6) NOT NULL DEFAULT 'ffffff',
  `themeBackgroundImage` varchar(16) DEFAULT NULL,
  `themeFontFace` varchar(108) DEFAULT NULL,
  `themeFontSize` varchar(8) DEFAULT NULL,
  `themeFontColor` varchar(6) NOT NULL DEFAULT '000000',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '15',
  PRIMARY KEY (`themeId`),
  UNIQUE KEY `themeName` (`themeName`),
  UNIQUE KEY `themeFolder` (`themeFolder`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (1,'Black Tie','black-tie','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(2,'Blitzer','blitzer','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(3,'Cupertino','cupertino','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(4,'Dark Hive','dark-hive','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(5,'Dot Luv','dot-luv','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(6,'Egg Plant','eggplant','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(7,'Excite Bike','excite-bike','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(8,'Flick','flick','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(9,'Hot Sneaks','hot-sneaks','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(10,'Humanity','humanity','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(11,'Le Frog','le-frog','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(12,'Mint Choc','mint-choc','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(13,'Overcast','overcast','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(14,'Pepper Grinder','pepper-grinder','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(15,'Redmond','redmond','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(16,'Smoothness','smoothness','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(17,'South Street','south-street','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(18,'Start','start','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(19,'Sunny','sunny','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(20,'Swanky Purse','swanky-purse','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(21,'Trontastic','trontastic','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(22,'UI Darkness','ui-darkness','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(23,'UI Lightness','ui-lightness','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15),(24,'Vader','vader','ffffff',NULL,NULL,NULL,'000000',NULL,NULL,15);
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loginId` int(10) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`),
  KEY `loginId` (`loginId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`loginId`) REFERENCES `login` (`loginId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,NULL,NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificationData`
--

DROP TABLE IF EXISTS `verificationData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificationData` (
  `verificationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schemaId` int(10) unsigned NOT NULL,
  `docRefNumber` varchar(24) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `loginId` int(10) unsigned DEFAULT NULL,
  `dt` varchar(19) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`verificationId`),
  UNIQUE KEY `schemaId_2` (`schemaId`,`docRefNumber`),
  KEY `fk_login` (`loginId`),
  CONSTRAINT `fk_login` FOREIGN KEY (`loginId`) REFERENCES `login` (`loginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificationData`
--

LOCK TABLES `verificationData` WRITE;
/*!40000 ALTER TABLE `verificationData` DISABLE KEYS */;
/*!40000 ALTER TABLE `verificationData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificationSchema`
--

DROP TABLE IF EXISTS `verificationSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificationSchema` (
  `verificationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `captionText` varchar(32) NOT NULL,
  `captionError` varchar(32) NOT NULL,
  `jobId` int(10) unsigned NOT NULL,
  `docId` int(10) unsigned NOT NULL,
  `seqno` int(4) unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`verificationId`),
  UNIQUE KEY `docId` (`docId`,`seqno`),
  KEY `jobId` (`jobId`),
  CONSTRAINT `verificationSchema_ibfk_1` FOREIGN KEY (`docId`) REFERENCES `document` (`documentId`),
  CONSTRAINT `verificationschema_ibfk_2` FOREIGN KEY (`jobId`) REFERENCES `jobTitle` (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificationSchema`
--

LOCK TABLES `verificationSchema` WRITE;
/*!40000 ALTER TABLE `verificationSchema` DISABLE KEYS */;
/*!40000 ALTER TABLE `verificationSchema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouse` (
  `warehouseId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `physicalWarehouseId` int(10) unsigned NOT NULL,
  `quantity` int(16) unsigned NOT NULL DEFAULT '0',
  `productValue` varchar(32) NOT NULL DEFAULT '0.0',
  `minStockLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `productionLevel` int(16) unsigned NOT NULL DEFAULT '0',
  `productSellingPrice` varchar(32) NOT NULL DEFAULT '0.0',
  `margin` varchar(16) NOT NULL DEFAULT '1.0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`warehouseId`),
  UNIQUE KEY `productId` (`productId`),
  KEY `physicalWarehouseId` (`physicalWarehouseId`),
  CONSTRAINT `warehouse_ibfk_1` FOREIGN KEY (`physicalWarehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `warehouse_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse`
--

LOCK TABLES `warehouse` WRITE;
/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouseProductRecordCard`
--

DROP TABLE IF EXISTS `warehouseProductRecordCard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouseProductRecordCard` (
  `recordCardId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `warehouseId` int(10) unsigned NOT NULL,
  `recordDate` varchar(19) NOT NULL DEFAULT '00:00:00:00:00:00',
  `refNumber` varchar(32) NOT NULL,
  `titleText` varchar(32) DEFAULT NULL,
  `quantity` int(16) NOT NULL DEFAULT '0',
  `unitValue` varchar(32) NOT NULL DEFAULT '0.0',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recordCardId`),
  KEY `warehouseId` (`warehouseId`),
  KEY `productId` (`productId`),
  CONSTRAINT `warehouseProductRecordCard_ibfk_1` FOREIGN KEY (`warehouseId`) REFERENCES `physicalWarehouse` (`warehouseId`),
  CONSTRAINT `warehouseProductRecordCard_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouseProductRecordCard`
--

LOCK TABLES `warehouseProductRecordCard` WRITE;
/*!40000 ALTER TABLE `warehouseProductRecordCard` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouseProductRecordCard` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-09 13:17:29
