<?php 
	 else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == 13)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$themeId = mysql_real_escape_string($_REQUEST['themeId']);
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
				if (is_null($profile1->getTheme()) || ($themeId != $profile1->getTheme()->getThemeId()))	{
					$profile1->setTheme($themeId); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW
				$profile1->setTheme($themeId);
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$profile1->commitUpdate();
				} catch (Exception $e)	{ die($e->getMessage()); }
			}
			mysql_close($conn);
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$firstname = "";
		$middlename = "";
		$lastname = "";
		$fullname = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
			$login1 = null;
			try {
				$login1 = new Login($database, Login::getStartUpLoginId($database, $conn) ,$conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			mysql_close($conn);	
			$firstname = $login1->getFirstname();
			$middlename = $login1->getMiddlename();
			$lastname = $login1->getLastname();
			$fullname = $login1->getFullname();
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Administrator's Bio</div>
				<div class="ui-sys-panel-body">
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<td><label for="firstname">Administrator's Firstname </label>
									<td><input type="text" name="firstname" id="firstname" size="32" value="<?= $firstname ?>" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Firstname : <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="middlename">Administrator's Middlename </label>
									<input type="text" name="middlename" id="middlename" size="32" value="<?= $middlename ?>" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Middlename : <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="lastname">Administrator's Lastname </label>
									<input type="text" name="lastname" id="lastname" size="32" value="<?= $lastname ?>" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Lastname : <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="fullname">Administrator's Fullname </label>
									<input type="text" name="fullname" id="fullname" size="32" value="<?= $fullname ?>" required pattern="<?= $exprA64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Fullname : <?= $msgA64Name ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="ui-sys-panel-footer"></div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	}
?>