<?php 
include("../config.php");
require_once("../common/validation.php");
require_once("../class/system.php");
$conn=mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$profile1 = null;
$__profileId = Profile::getProfileReference($database, $conn);
try {
	$profile1 = new Profile($database, $__profileId , $conn);
	$profile1->loadXMLFolder("../data/profile");
} catch (Exception $e)	{
	die($e->getMessage());
}
mysql_close($conn);
if ($profile1->isInstallationComplete())	{
	header("Location: ../");
	exit();
}
$dataFolder = "data";
$themeFolder = "sunny";
if (! is_null($profile1->getTheme())) $themeFolder = $profile1->getTheme()->getThemeFolder();
//We Just need to get A Default Timezone so we can extra a year
$timezone="Africa/Dar_es_Salaam";
if (! is_null($profile1->getPHPTimezone())) $timezone = $profile1->getPHPTimezone()->getZoneName();
date_default_timezone_set($timezone);
$date=date("Y:m:d:H:i:s");
$__systemDayOffset = 0;
if (! is_null($profile1->getFirstDayOfAWeek()))	{
	$__systemDayOffset = $profile1->getFirstDayOfAWeek()->getOffsetValue();
}
$systemDate1 = new DateAndTime("Ndimangwa", $date, "Fadhili");
$pagenumber = -1;
$prevpagenumber = 9999;
$thispage=$_SERVER['PHP_SELF'];
//Monitor if we had already done an Application
if (! (isset($_REQUEST['pagenumber']) && isset($_REQUEST['prevpagenumber'])) && (intval($profile1->getApplicationCounter()) > 0))	{
	//We need to jumpdirectly to a page 
	$jumpdirectly="Jump Directly";
}
if (isset($_REQUEST['pagenumber']))	{ $pagenumber=intval($_REQUEST['pagenumber']); }
if (isset($_REQUEST['prevpagenumber']))	{ $prevpagenumber=intval($_REQUEST['prevpagenumber']); }
?>
<html>
<head>
<title><?= $profile1->getProfileName() ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="all" href="../client/jquery-ui-1.11.3/themes/<?= $themeFolder ?>/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/purecss/pure-min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/site.css"/>
<style type="text/css">

</style>
<script type="text/javascript" src="../client/jquery.js"></script>
<script type="text/javascript" src="../client/jquery-ui-1.11.3/jquery-ui.js"></script>
<script type="text/javascript" src="../client/jquery-easy-ticker-master/jquery.easy-ticker.js"></script>
<script type="text/javascript" src="../client/js/jvalidation.js"></script>
<script type="text/javascript" src="../client/js/page.js"></script>
<script type="text/javascript">
(function($)	{
	$(function()	{
		/*Date Handling*/
		$('.datepicker').datepicker({
			dateFormat: 'dd/mm/yy',
			firstDay: <?= $__systemDayOffset ?>,
			changeYear: true,
			yearRange:'1961:2099'
		});
		//Tesiting
		//$('input[type="text"]').textinput();
	});
})(jQuery);
</script>
</head>
<body class="ui-sys-body">

<div class="ui-sys-main">
	<div class="ui-sys-front-header">
		<div class="ui-sys-front-logo mobile-collapse">
<?php 
	$logo = "../".$dataFolder."/".$profile1->getDataFolder()."/logo/default.jpg";
	if (! is_null($profile1->getLogo())) $logo = "../".$dataFolder."/".$profile1->getDataFolder()."/logo/".$profile1->getLogo();
?>
			<img alt="LG" src="<?= $logo ?>"/>
		</div>
		<div class="ui-sys-front-header-1 mobile-collapse">
			<div class="ui-sys-inst-name">INSTALLATION SETUP</div>
		</div>
		<div class="ui-sys-clearboth">&nbsp;</div>
	</div>
<?php 
	if (! is_null($profile1->getSystemName()))	{
?>
	<div class="ui-sys-front-systemname mobile-collapse"><?= $profile1->getSystemName() ?></div>
<?php 
	}
?>
<!--BEGIN OF PUTTING CODE-->
<div class="ui-sys-bg-grid-green">
<?php 
	if (isset($jumpdirectly))	{
?>
		<div class="ui-sys-panel-container ui-widget">
			<div class="ui-sys-panel ui-widget-content">
				<div class="ui-sys-panel-header ui-widget-header">PARTIAL INSTALLATION</div>
				<div class="ui-sys-panel-body">
					The System has detected that you have incomplete Installation, kindly click the button below to proceed from where you ended
					<div class="ui-sys-inline-controls-center">
						<a class="button-link" href="<?= $thispage ?>?prevpagenumber=9999&pagenumber=<?= $profile1->getApplicationCounter() ?>">Proceed with Installation</a>
					</div>
				</div>
				<div class="ui-sys-panel-footer"></div>
			</div>
		</div>
		<div class="ui-sys-clear-both">&nbsp;</div>
<?php
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__FINISH)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$profile1 = null;
		$ulogin1 = null;
		try {
			$profile1 = new Profile($database, $__profileId, $conn);
			$profile1->loadXMLFolder("../data/profile");
			$ulogin1 = Login::getInstallationLoginAccount($database, $conn);
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
		}
		//Beginning UI 
		//Now Everything is set it is up to us to do the final settings of the Profile 
		if (! $profile1->isInstallationComplete())	{
			/*
			If installation is not complete the complete the installation
			*/
			$profile1->setInstallationComplete("1");
			$ulogin1->setRoot("1");
			$ulogin1->setAdmissionTime($systemDate1->getDateAndTimeString());
			$ulogin1->setAdmitted("1");
			try {
				$profile1->commitUpdate();
				$ulogin1->commitUpdate();
			} catch (Exception $e)	{ die($e->getMessage()); }
		} //end -isInstallationComplete
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header"></div>
				<div class="ui-sys-panel-body ui-widget">
					<div class="ui-widget-content">
						<div class="ui-widget-header">Installation Report</div>
						You have successful Installed the System <br />
						Click <a href="../" class="button-link">Here</a> to proceed to the Home Page
					</div>
				</div>
				<div class="ui-sys-panel-footer"></div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<?php
		mysql_close($conn);
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__MENU)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$profile1 = null;
		try {
			$profile1 = new Profile($database, $__profileId, $conn);
			$profile1->loadXMLFolder("../data/profile");
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		//Step 1: Handling Data From the Previous Page --and make sure is note a straight Menu Jump
		if (! isset($_REQUEST['menujump']) && ($prevpagenumber < $pagenumber))	{
			
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$extraInformation = mysql_real_escape_string($_REQUEST['extraInformation']);
			if ($pagenumber -1 < $storedSafePage)	{
				if ($extraInformation != $profile1->getExtraInformation())	{
					$profile1->setExtraInformation($extraInformation); $enableUpdate = true;
				}
			} else {
				$profile1->setExtraInformation($extraInformation);
				$profile1->setApplicationCounter($pagenumber); //The last page to set an application counter
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$profile1->commitUpdate();
				} catch (Exception $e)	{ die($e->getMessage()); }
			}
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
		}
		//Beginning UI 
		//Now Everything is set it is up to us to do the final settings of the Profile 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header"></div>
				<div class="ui-sys-panel-body ui-widget ui-widget-content">
					<div>
						<div class="ui-widget-header">Menus</div>
						<div class="ui-sys-installation-menu">
							<ul>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__INSTITUTION_NAME ?>">NAMES OF SYSTEM && INSTITUTION</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_LOGO ?>">SYSTEM PROFILE LOGO</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_ADDRESSES ?>">INSTITUTION ADDRESSES</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_LOCATION ?>">INSTITUTION LOCATION</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_THEME ?>">PROFILE THEME & WEEK START</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_DOB ?>">WHEN INSTITUTION WAS BORN?</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_LAN ?>">NETWORK ADDRESSES</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_RECORD_LIMITS ?>">SYSTEM RECORD COUNT LIMITS</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_VISION_MISSION ?>">VISION & MISSION</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__ADMIN_DATA_01 ?>">ADMINISTRATOR BIO DATA</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__ADMIN_DATA_02 ?>">ADMINISTRATOR OTHER INFORMATION</a></li>
								<li><a href="<?= $thispage ?>?prevpagenumber=<?= Installation::$__MENU ?>&pagenumber=<?= Installation::$__PROFILE_EXTRA_INFORMATION ?>">ANY OTHER INFORMATION</a></li>
							</ul>
						</div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="ui-sys-panel-footer"></div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_EXTRA_INFORMATION)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$profile1 = null;
		$ulogin1 = null;
		try {
			$profile1 = new Profile($database, $__profileId, $conn);
			$profile1->loadXMLFolder("../data/profile");
			$ulogin1 = Login::getInstallationLoginAccount($database, $conn);
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$loginName = mysql_real_escape_string($_REQUEST['loginName']);
			$email = mysql_real_escape_string($_REQUEST['email']);
			$password = sha1($_REQUEST['password']);
			$firstSecurityQuestionId = mysql_real_escape_string($_REQUEST['firstSecurityQuestionId']);
			$firstSecurityAnswer = mysql_real_escape_string($_REQUEST['firstSecurityAnswer']);
			$secondSecurityQuestionId = mysql_real_escape_string($_REQUEST['secondSecurityQuestionId']);
			$secondSecurityAnswer = mysql_real_escape_string($_REQUEST['secondSecurityAnswer']);
			$enableProfileUpdate = false;
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
				if ($loginName != $ulogin1->getLoginName())	{
					$ulogin1->setLoginName($loginName); $enableUpdate = true;
				}
				if ($email != $ulogin1->getEmail())	{
					$ulogin1->setEmail($email); $enableUpdate = true;
				}
				if ($password != $ulogin1->getPassword())	{
					$ulogin1->setPassword($password); $enableUpdate = true;
				}
				if (is_null($ulogin1->getFirstSecurityQuestion()) || ($firstSecurityQuestionId != $ulogin1->getFirstSecurityQuestion()->getQuestionId()))	{
					$ulogin1->setFirstSecurityQuestion($firstSecurityQuestionId); $enableUpdate = true;
				}
				if ($firstSecurityAnswer != $ulogin1->getFirstSecurityAnswer())	{
					$ulogin1->setFirstSecurityAnswer($firstSecurityAnswer); $enableUpdate = true;
				}
				if (is_null($ulogin1->getSecondSecurityQuestion()) || ($secondSecurityQuestionId != $ulogin1->getSecondSecurityQuestion()->getQuestionId()))	{
					$ulogin1->setSecondSecurityQuestion($secondSecurityQuestionId); $enableUpdate = true;
				}
				if ($secondSecurityAnswer != $ulogin1->getSecondSecurityAnswer())	{
					$ulogin1->setSecondSecurityAnswer($secondSecurityAnswer); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW
				$ulogin1->setLoginName($loginName);
				$ulogin1->setEmail($email);
				$ulogin1->setPassword($password);
				$ulogin1->setFirstSecurityQuestion($firstSecurityQuestionId);
				$ulogin1->setFirstSecurityAnswer($firstSecurityAnswer);
				$ulogin1->setSecondSecurityQuestion($secondSecurityQuestionId);
				$ulogin1->setSecondSecurityAnswer($secondSecurityAnswer);
				$profile1->setApplicationCounter($pagenumber);
				$enableProfileUpdate = true;
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				//This will do a blindly update on profile 
				//However the profile Object will check for this situation 
				try {
					$ulogin1->commitUpdate();
					if ($enableProfileUpdate) $profile1->commitUpdate(); 
				} catch (Exception $e)	{
					die($e->getMessage());
				}
			}
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$extraInformation = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$extraInformation = $profile1->getExtraInformation();
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Extra Information</div>
				<div class="ui-sys-panel-body">
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="extraInformation">Extra Information </label>
									<input type="text" name="extraInformation" id="extraInformation" size="32" value="<?= $extraInformation ?>" notrequired="true" pattern="<?= $exprL64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL64Name ?>" validate_message="Extra Information : <?= $msgL64Name ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__ADMIN_SECURITY)	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$storedSafePage = intval($profile1->getApplicationCounter()); 
		$enableUpdate = false;
		$continueToStepTwo = true;
		$promise1 = null;
		$user1 = null;
		$ulogin1 = null;
		try {
			//We now have the actual User Installation Account
			$user1 = User::getInstallationUserAccount($database, $conn);
			$ulogin1 = $user1->getLogin();
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		//Step 1: Data From the previous page
		if ($prevpagenumber < $pagenumber)	{
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			$logoArray1 = $_FILES['logo'];
			$enablePhotoUpdate = false;
			$enableProfileUpdate = false;
			if ($pagenumber - 1 < $storedSafePage)	{
				if (Object::isThereAnyFileReceivedFromTheClient($logoArray1))	{
					$enablePhotoUpdate = true;
				}
			} else {
				if (Object::isThereAnyFileReceivedFromTheClient($logoArray1))	{
					$enablePhotoUpdate = true;
				}
				$enableUpdate = true;
				$enableProfileUpdate = true;
				$profile1->setApplicationCounter($pagenumber);
			}
			$logoFolder = "../data/users/photo";
			$logofilename = "user_".$user1->getUserId();
			$fileextension = Object::getUploadedFileExtension($logoArray1);
			$uploadedFileName = $logofilename.".".$fileextension;
			if ($enablePhotoUpdate)	{
				//We will just overwrite 
				$validTypes = array("image/jpeg", "image/png", "image/jpg");
				$validExtensions = array("jpeg", "png", "jpg");
				$maximumUploadedSize = 2097153;
				$promise1 = Object::saveUploadedFile($logoArray1, $logoFolder, $logofilename, $validTypes, $validExtensions, $maximumUploadedSize);
				$ulogin1->setPhoto($uploadedFileName);
				$continueToStepTwo = false;
			}
			if (! is_null($promise1) && $promise1->isPromising())	{
				$continueToStepTwo = true;
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$ulogin1->commitUpdate();
					if ($enableProfileUpdate) $profile1->commitUpdate();
				} catch (Exception $e)	{ 
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
					$enableUpdate = false;
				}
			}
		}
		//Step 2: Handling This page ...l788
		//Note During Installation, Only One in the List 
		//We are creating a basic setup Only 
		//Step 2: Handling Data for this Page 
		//$website = "";
		$loginName = "";
		$email = "";
		//Password Display NONE 
		$firstSecurityQuestionId = -1;
		$firstSecurityAnswer = "";
		$secondSecurityQuestionId = -1;
		$secondSecurityAnswer = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$loginName = $ulogin1->getLoginName();
			$email = $ulogin1->getEmail();
			$firstSecurityQuestionId = $ulogin1->getFirstSecurityQuestion()->getQuestionId();
			$firstSecurityAnswer = $ulogin1->getFirstSecurityAnswer();
			$secondSecurityQuestionId = $ulogin1->getSecondSecurityQuestion()->getQuestionId();
			$secondSecurityAnswer = $ulogin1->getSecondSecurityAnswer();
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Security</div>
				<div class="ui-sys-panel-body">
					<div>
<?php  
	if (is_null($promise1) || $promise1->isPromising())	{
?>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="loginName">Administrator Username </label>
									<input type="text" name="loginName" id="loginName" size="32" value="<?= $loginName ?>" required pattern="<?= $expr32Name ?>" validate="true" validate_control="text" validate_expression="<?= $expr32Name ?>" validate_message="Username : <?= $msg32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="email">Administrator Email </label>
									<input type="text" name="email" id="email" size="32" value="<?= $email ?>" required pattern="<?= $exprEmail ?>" validate="true" validate_control="text" validate_expression="<?= $exprEmail ?>%<?= $exprL32Name ?>" validate_message="Email Format : <?= $msgEmail ?>%Email Length: <?= $msgL32Name ?>"/>
								</div>
								<div class="pure-control-group">
										<label for="password">Administrator's Password </label>
										<input type="password" name="password" id="password" size="32" required pattern="<?= $exprL48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL48Name ?>" validate_message="Password : <?= $msgL48Name ?>"/>
								</div>
								<div class="pure-control-group">
										<label for="password">Confirm Password </label>
										<input type="password" id="cpassword" size="32" required pattern="<?= $exprL48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL48Name ?>" validate_message="Password(2) : <?= $msgL48Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="firstSecurityQuestionId">First Security Question </label>
									<select id="firstSecurityQuestionId" name="firstSecurityQuestionId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select First Security Question">
											<option value="_@32767@_">--select--</option>
<?php 
	$list1 = SecurityQuestion::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($firstSecurityQuestionId == $alist1['id']) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
										</select>
								</div>
								<div class="pure-control-group">
										<label for="firstSecurityAnswer">First Security Answer </label>
										<input type="text" name="firstSecurityAnswer" id="firstSecurityAnswer" size="32" value="<?= $firstSecurityAnswer ?>" required pattern="<?= $exprL32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL32Name ?>" validate_message="First Security Answer : <?= $msgL32Name ?>"/>
								</div>
								<div class="pure-control-group">
										<label for="secondSecurityQuestionId">Second Security Question </label>
										<select id="secondSecurityQuestionId" name="secondSecurityQuestionId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Second Security Question">
											<option value="_@32767@_">--select--</option>
<?php 
	$list1 = SecurityQuestion::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($secondSecurityQuestionId == $alist1['id']) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
										</select>
								</div>
								<div class="pure-control-group">
									<label for="secondSecurityAnswer">Second Security Answer </label>
									<input type="text" name="secondSecurityAnswer" id="secondSecurityAnswer" size="32" value="<?= $secondSecurityAnswer ?>" required pattern="<?= $exprL32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL32Name ?>" validate_message="Second Security Answer : <?= $msgL32Name ?>"/>
								</div>
								<div class="pure-controls">
										<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
<?php 
	} else {
?>
		There were problems in Uploading the photo 
		<div class="ui-sys-controls-right">
			<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>
		</div>
<?php
	}
?>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		if (($('#password').val() == $('#cpassword').val()) && ($('#firstSecurityQuestionId').val() != $('#secondSecurityQuestionId').val()))	{
			generalFormSubmission(this, 'form1', 'perror');
		} else {
			$('#perror').html('Password Mismatch or Security Questions are Matching');
		}
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__ADMIN_PHOTO)	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$promise1 = null;
		$user1 = null;
		$ulogin1 = null;
		try {
			//We now have the actual User Installation Account
			$user1 = User::getInstallationUserAccount($database, $conn);
			$ulogin1 = $user1->getLogin();
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		//Step 1: Data From Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			//Need to recreate Profile Object since we have lost the conn string 
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$phone = mysql_real_escape_string($_REQUEST['phone']);
			$jobId = mysql_real_escape_string($_REQUEST['jobId']);
			$groupId = mysql_real_escape_string($_REQUEST['groupId']);
			$enableProfileUpdate = false;
			if ($pagenumber - 1 < $storedSafePage)	{
				//We have data this is an editing 
				if ($phone != $ulogin1->getPhone())	{
					$ulogin1->setPhone($phone); $enableUpdate = true;
				}
				if (is_null($ulogin1->getJobTitle()) || ($jobId != $ulogin1->getJobTitle()->getJobId()))	{
					$ulogin1->setJobTitle($jobId); $enableUpdate = true;
				}
				if (is_null($ulogin1->getGroup()) || ($groupId != $ulogin1->getGroup()->getGroupId()))	{
					$ulogin1->setGroup($groupId); $enableUpdate = true;
				}
			} else {
				//Update All
				$ulogin1->setPhone($phone);
				$ulogin1->setJobTitle($jobId);
				$ulogin1->setGroup($groupId);
				$enableUpdate = true;
				//We need to Update ApplicationCounter 
				$enableProfileUpdate = true;
				$profile1->setApplicationCounter($pagenumber);
			}
			//Entering Saving Mode 
			if ($enableUpdate)	{
				try {
					$ulogin1->commitUpdate();
					if ($enableProfileUpdate) $profile1->commitUpdate();
				} catch (Exception $e)	{ 
					$promise1 = new Promise();
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
				}
			}
		} //end--step 1
		//Step 2: Data Intended for this page only 
		$ipath = "../data/users/photo/";
		$logo = $logo = $ipath."default.jpg";
		$photoname = "default.jpg";
		$trackchange = 0;
		if ($pagenumber < $storedSafePage)	{
			$logo = $ipath.$ulogin1->getPhoto();
			$photoname = $ulogin1->getPhoto();;
			$trackchange = 1;
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Administrator's Profile Photo</div>
				<div class="ui-sys-panel-body">
<?php 
	if (is_null($promise1) || $promise1->isPromising())	{
?>
<!--Beginning a photo container-->	
<div class="photocontainer">
	<div class="photodisplay">
		<img id="__id_image_photo_container" alt="PIC" src="<?= $logo ?>"/>
	</div>
	<div class="photodata">
		<form id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
			<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
			<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
			<table>
				<tr>
					<td><label for="__id_image_photo_control">Select Photo</label></td>
					<td><input  id="__id_image_photo_control" type="file" class="ui-sys-file-upload" name="logo" data-trackchange="<?= $trackchange ?>" value="<?= $photoname ?>" accept="image/*"/></td>
				</tr>
				<tr>
					<td colspan="2" id="perror" class="ui-sys-inline-controls-center ui-sys-error-message"></td>
				</tr>
				<tr>
					<td colspan="2" class="ui-sys-inline-controls-right"> 
					<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>
					<input type="submit" value="Upload"/></td>
				</tr>
			</table>
		</form>
	</div>
	<div class="ui-sys-clear-both">&nbsp;</div>
</div>
<!--Ending a photo container-->
<?php 
	} else {
?>
		<div class="ui-state-error">
			Failed to Save Some of Administrator's data <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
		<div class="ui-sys-inline-controls-right">
			<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>
		</div>
<?php
	}
?>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both">&nbsp;</div>
<script type="text/javascript">
					function showImage(_fileSource, _imageDestination)	{
						var fileSource1 = document.getElementById(_fileSource);
						if (! fileSource1) return;
						var imageDestination1 = document.getElementById(_imageDestination);
						if (! imageDestination1) return;
						var fileReader1 = new FileReader();
						$(fileReader1).on('load', function(event)	{
							imageDestination1.src = this.result;
						});
						$(fileSource1).on('change', function(event)	{
							fileReader1.readAsDataURL(fileSource1.files[0]);
						});
					}
					showImage('__id_image_photo_control', '__id_image_photo_container');
					(function($)	{
						$('#__id_image_photo_control').on('change', function(event)	{
							$('#__id_image_photo_control').data('trackchange', '1');
							showImage('__id_image_photo_control', '__id_image_photo_container');
						});
						$('#form1').on('submit', function(event)	{
							event.preventDefault();
							//Make use of track changes to validate further
							var fileControl1 = document.getElementById('__id_image_photo_control');
							if (! fileControl1) return;
							generalFormSubmission(this, 'form1', 'perror');
							if (parseInt($(fileControl1).data('trackchange')) == 1)	{
								generalFormSubmission(this, 'form1', 'perror');
							} else {
								$('#perror').html("No Update were done");
							} //end-if-else
						});
					})(jQuery);
</script>
<?php
		mysql_close($conn);
	}  else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__ADMIN_DATA_02)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$profile1 = null;
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$ulogin1 = null;
		try {
			$profile1 = new Profile($database, $__profileId, $conn);
			$profile1->loadXMLFolder("../data/profile");
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$fullname = mysql_real_escape_string($_REQUEST['fullname']);
			$dob = DateAndTime::convertFromGUIDateFormatToSystemDateAndTimeFormat($_REQUEST['dob']);
			$sexId = mysql_real_escape_string($_REQUEST['sexId']);
			$maritalId = mysql_real_escape_string($_REQUEST['maritalId']);
			$locationId = mysql_real_escape_string($_REQUEST['locationId']);
			$departmentId = mysql_real_escape_string($_REQUEST['departmentId']);
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW -- Already Account exists 
				$enableProfileUpdate = false;
				$ulogin1 = null;
				try {
					$ulogin1 = Login::getInstallationLoginAccount($database, $conn);
				} catch(Exception $e)	{
					$promise1->setReason($e->getReason());
					$promise1->setPromise(false);
				}
				if ($promise1->isPromising() && ($fullname != $ulogin1->getFullname()))	{
					$ulogin1->setFullname($fullname); $enableUpdate = true;
				}
				if ($promise1->isPromising() && (is_null($ulogin1->getDOB()) || ($dob != $ulogin1->getDOB()->getDateAndTimeString())))	{
					$ulogin1->setDOB($dob); $enableUpdate = true;
				}
				if ($promise1->isPromising() && (is_null($ulogin1->getSex()) || ($sexId != $ulogin1->getSex()->getSexId())))	{
					$ulogin1->setSex($sexId); $enableUpdate = true;
				}
				if ($promise1->isPromising() && (is_null($ulogin1->getMarital()) || ($maritalId != $ulogin1->getMarital()->getMaritalId())))	{
					$ulogin1->setMarital($maritalId); $enableUpdate = true;
				}
				if ($promise1->isPromising() && (is_null($ulogin1->getLocation()) || ($locationId != $ulogin1->getLocation()->getLocationId())))	{
					$ulogin1->setLocation($locationId); $enableUpdate = true;
				}
				if ($promise1->isPromising() && (is_null($ulogin1->getDepartment()) || ($departmentId != $ulogin1->getDepartment()->getDepartmentId())))	{
					$ulogin1->setDepartment($departmentId); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW ; Brand New Account Need to be installed 
				$loginId = null;
				$userId = null;
				try {
					//Clear Previous Installation Accounts if Any
					Login::removeAllExistingInstallationAccounts($database, $conn);
					//Here should be the only Initialization place
					$loginId = Login::initializeLoginRecordForUser($database, $conn, System::getCodeString(8), true);
					$userId = User::addUserAccountWithLoginReference($database, $conn, $loginId);
					$ulogin1 = new Login($database, $loginId, $conn);
				} catch (Exception $e)	{
					//Roll Back on error 
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
					try {
						if (! is_null($userId))	{
							$user1 = new User($database, $userId, $conn);
							$user1->commitDelete();
						}
						if (! is_null($loginId))	{
							$ulogin1 = new Login($database, $loginId, $conn);
							$ulogin1->commitDelete();
						}
					} catch (Exception $e)	{
						$promise1->setReason("Rollback ".$e->getMessage());
						$promise1->setPromise(false);
					}
				}
				if ($promise1->isPromising())	{
					$ulogin1->setFullname($fullname);
					$ulogin1->setDOB($dob);
					$ulogin1->setSex($sexId);
					$ulogin1->setMarital($maritalId);
					$ulogin1->setLocation($locationId);
					$ulogin1->setDepartment($departmentId);
					$profile1->setApplicationCounter($pagenumber);
					$enableProfileUpdate = true;
					$enableUpdate = true;
				}
			}
			if ($enableUpdate)	{
				/*
				There are possibilities that the profile were not touched 
				That is not the case , because every object is checking internally if 
				there exists update list prior to actual commiting update 
				*/
				try {
					if (is_null($ulogin1)) Object::shootException("Login Account could not be initialized");
					//Check Age of the user
					//You need to check age of the user 
					$systemTime1 = new DateAndTime("Ndimangwa", Object::getCurrentTimestamp(), "Fadhili");
					$timeDifference1 = (new DateAndTime("Ndimangwa", Object::getCurrentTimestamp(), "Fadhili"))->dateDifference($ulogin1->getDOB());
					if (is_null($timeDifference1)) Object::shootException("Date Difference could not be calculated");
					$minAgeCriteria = $profile1->getMinimumAgeCriteriaForUsers();
					if (! ($timeDifference1->isPositive() && ($timeDifference1->getYear() >= $profile1->getMinimumAgeCriteriaForUsers()))) Object::shootException("The user should have atleast $minAgeCriteria years of age");
					//End of Checking Age
					$ulogin1->commitUpdate();
					if ($enableProfileUpdate) $profile1->commitUpdate();
				} catch (Exception $e)	{ 
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
				}
			}
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$phone = "";
		$jobId = -1;
		$groupId = -1;
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			if (is_null($ulogin1)) $ulogin1 = Login::getInstallationLoginAccount($database, $conn);
			$phone = $ulogin1->getPhone();
			$jobId = $ulogin1->getJobTitle()->getJobId();
			$groupId = $ulogin1->getGroup()->getGroupId();
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Administrator's Information</div>
				<div class="ui-sys-panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="phone">Administrator's Phone </label>
									<input type="text" name="phone" id="phone" size="32" value="<?= $phone ?>" required pattern="<?= $exprPhone ?>" validate="true" validate_control="text" validate_expression="<?= $exprPhone ?>" validate_message="Phone: <?= $msgPhone ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="jobId">Administrator's Job Title </label>
									<select id="jobId" name="jobId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Job Title">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = JobTitle::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $jobId) $selected="selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="groupId">Administrator's Group </label>
									<select id="groupId" name="groupId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Group">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Group::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $groupId) $selected="selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
<?php 
	} else {
?>
		<div class="ui-state-error">
			There were problems in saving Administrator's data <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
		<div class="ui-sys-inline-controls-right">
			<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>
		</div>
<?php
	}
?>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__ADMIN_DATA_01)	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$promise1 = null;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$vision = mysql_real_escape_string($_REQUEST['vision']);
			$mission = mysql_real_escape_string($_REQUEST['mission']);
			$missionAndVision1 = array();
			$missionAndVision1['vision'] = $vision;
			$missionAndVision1['mission'] = $mission;
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
			} else {
				//INSERT WINDOW
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			$promise1 = $profile1->backupXMLFile();
			$promise1 = $profile1->clearMissionAndVisions();
			$promise1 = $profile1->addMissionAndVision($missionAndVision1);
			if ($enableUpdate && $promise1->isPromising())	{
				try	{
					$profile1->commitUpdate();
				} catch (Exception $e)	{ 
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
					$enableUpdate = false;
				}
			}
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$fullname = "";
		$dob = "";
		$sexId = -1;
		$maritalId = -1;
		$locationId = -1;
		$departmentId = -1;
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			try {
				//You need to Initialize promise 
				if (is_null($promise1))	{
					$promise1 = new Promise();
					$promise1->setPromise(true);
				}
				//Check the Previous Promise if false 
				if (! $promise1->isPromising()) Object::shootException($promise1->getReason());
				//Let us go to business 
				$ulogin1 = Login::getInstallationLoginAccount($database, $conn);
				$fullname = $ulogin1->getFullname();
				$dob = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($ulogin1->getDOB());
				$sexId = $ulogin1->getSex()->getSexId();
				$maritalId = $ulogin1->getMarital()->getMaritalId();
				$locationId = $ulogin1->getLocation()->getLocationId();
				$departmentId = $ulogin1->getDepartment()->getDepartmentId();
			} catch (Exception $e)	{
				$promise1->setReason($e->getMessage());
				$promise1->setPromise(false);
			}
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Administrator's Information</div>
				<div class="ui-sys-panel-body">
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<td><label for="fullname">Administrator's Name </label>
									<td><input type="text" name="fullname" id="fullname" size="32" value="<?= $fullname ?>" required pattern="<?= $exprA48Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA48Name ?>" validate_message="Admin Name : <?= $msgA48Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="dob">Administrator's Date of Birth </label>
									<input class="datepicker" type="text" name="dob" id="dob" size="32" value="<?= $dob ?>" required pattern="<?= $exprDate ?>" validate="true" validate_control="text" validate_expression="<?= $exprDate ?>" validate_message="Date : <?= $msgDate ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="sexId">Administrator's Sex </label>
									<select id="sexId" name="sexId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Sex">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Sex::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $sexId) $selected="selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="maritalId">Administrator's Marital </label>
									<select id="maritalId" name="maritalId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Marital">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Marital::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $maritalId) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="locationId">Administrator's Location </label>
									<select id="locationId" name="locationId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Location">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Location::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $locationId) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="departmentId">Administrator's Department </label>
									<select id="departmentId" name="departmentId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Department">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Department::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $departmentId) $selected = "selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	}   else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_VISION_MISSION)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$promise1 = null;
		$enableUpdate = false;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$maxNumberOfReturnedSearchRecords = mysql_real_escape_string($_REQUEST['maxNumberOfReturnedSearchRecords']);
			$maxNumberOfDisplayedRowsPerPage = mysql_real_escape_string($_REQUEST['maxNumberOfDisplayedRowsPerPage']);
			$minAgeCriteriaForUsers = mysql_real_escape_string($_REQUEST['minAgeCriteriaForUsers']);
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
				if ($maxNumberOfReturnedSearchRecords != $profile1->getMaximumNumberOfReturnedSearchRecords())	{
					$profile1->setMaximumNumberOfReturnedSearchRecords($maxNumberOfReturnedSearchRecords); $enableUpdate = true;
				}
				if ($maxNumberOfDisplayedRowsPerPage != $profile1->getMaximumNumberOfDisplayedRowsPerPage())	{
					$profile1->setMaximumNumberOfDisplayedRowsPerPage($maxNumberOfDisplayedRowsPerPage); $enableUpdate = true;
				}
				if ($minAgeCriteriaForUsers != $profile1->getMinimumAgeCriteriaForUsers())	{
					$profile1->setMinimumAgeCriteriaForUsers($minAgeCriteriaForUsers); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW
				$profile1->setMaximumNumberOfReturnedSearchRecords($maxNumberOfReturnedSearchRecords);
				$profile1->setMaximumNumberOfDisplayedRowsPerPage($maxNumberOfDisplayedRowsPerPage);
				$profile1->setMinimumAgeCriteriaForUsers($minAgeCriteriaForUsers);
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				//Perform Verify First 
				try {
					$profile1->commitUpdate();
				} catch (Exception $e)	{
					$promise1 = new Promise();
					$promise1->setPromise(false);
					$promise1->setReason($e->getMessage());
					$enableUpdate = false;
				}
			}
			mysql_close($conn);
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$vision = "";
		$mission = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$missionAndVision1 = $profile1->getMissionAndVisionList();
			if (! is_null($missionAndVision1))	{
				$missionAndVision1 = $missionAndVision1[0];
				$mission = $missionAndVision1->getElementsByTagName('mission')->item(0)->nodeValue;
				$vision = $missionAndVision1->getElementsByTagName('vision')->item(0)->nodeValue;
				//Substitute carriage return and line feed
				$mission = str_replace('\r\n', PHP_EOL, $mission);
				$mission = str_replace('\r', PHP_EOL, $mission);
				$mission = str_replace('\n', PHP_EOL, $mission);
				$vision = str_replace('\r\n', PHP_EOL, $vision);
				$vision = str_replace('\r', PHP_EOL, $vision);
				$vision = str_replace('\n', PHP_EOL, $vision);
			}
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Vision & Mission</div>
				<div class="ui-sys-panel-body">
<?php 
	if (is_null($promise1) || $promise1->isPromising())	{
?>
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="vision">Vision </label>
									<textarea name="vision" id="vision" rows="4" cols="80" required pattern="<?= $exprL512Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL512Name ?>" validate_message="Vision: <?= $msgL512Name ?>"><?= $vision ?></textarea>
								</div>
								<div class="pure-control-group">
									<label for="mission">Mission </label>
									<textarea name="mission" id="mission" rows="4" cols="80" required pattern="<?= $exprL512Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL512Name ?>" validate_message="Mission : <?= $msgL512Name ?>"><?= $mission ?></textarea>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								 <div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
<?php 
	} else {
?>
		<div class="ui-state-error">
			There were problems in saving records or age criterias <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
		<div>
			<a href="<?= $thispage ?>?pagenumber=<?= $prevpagenumber ?>&prevpagenumber=<?= $pagenumber ?>">Click here to go back</a>
		</div>
<?php
	}
?>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_RECORD_LIMITS)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$promise1 = null;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$serverIpAddress = mysql_real_escape_string($_REQUEST['serverIpAddress']);
			$localAreaNetworkMask = mysql_real_escape_string($_REQUEST['localAreaNetworkMask']);
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
				if ($serverIpAddress != $profile1->getServerIpAddress())	{
					$profile1->setServerIpAddress($serverIpAddress); $enableUpdate=true;
				}
				if ($localAreaNetworkMask != $profile1->getLocalAreaNetworkMask())	{
					$profile1->setLocalAreaNetworkMask($localAreaNetworkMask); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW
				$profile1->setServerIpAddress($serverIpAddress);
				$profile1->setLocalAreaNetworkMask($localAreaNetworkMask);
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				//Perform Verify First 
				$promise1 = new Promise();
				$promise1->setPromise(true);
				//Simulate promise
				//Return promise1
				if (! is_null($promise1) && $promise1->isPromising())	{
					try {
						$network1 = new Network($serverIpAddress, $localAreaNetworkMask);
						$profile1->commitUpdate();
					} catch (Exception $e)	{ 
						$promise1->setReason($e->getMessage());
						$promise1->setPromise(false);
					}
				}
			}
			mysql_close($conn);
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$maxNumberOfReturnedSearchRecords = "";
		$maxNumberOfDisplayedRowsPerPage = "";
		$minAgeCriteriaForUsers = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$maxNumberOfDisplayedRowsPerPage = $profile1->getMaximumNumberOfDisplayedRowsPerPage();
			$maxNumberOfReturnedSearchRecords = $profile1->getMaximumNumberOfReturnedSearchRecords();
			$minAgeCriteriaForUsers = $profile1->getMinimumAgeCriteriaForUsers();
		}
		//Beginning UI 
		//We must make sure the promise is set and has return valid response
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Records and Age Criteria</div>
				<div class="ui-sys-panel-body">
					<div>
<?php 
	if (is_null($promise1) ||  $promise1->isPromising())	{
?>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="maxNumberOfDisplayedRowsPerPage">Maximum Number Of Displayed Rows Per Page </label>
									<input type="text" name="maxNumberOfDisplayedRowsPerPage" id="maxNumberOfDisplayedRowsPerPage" size="32" value="<?= $maxNumberOfDisplayedRowsPerPage ?>" required pattern="<?= $expr2Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr2Number ?>" validate_message="Maximum Number Of Displayed Rows Per Page : <?= $msg2Number ?>"/>
								</div>
								
								<div class="pure-control-group">
									<label for="maxNumberOfReturnedSearchRecords">Maximum Number Of Returned Search Records </label>
									<input type="text" name="maxNumberOfReturnedSearchRecords" id="maxNumberOfReturnedSearchRecords" size="32" value="<?= $maxNumberOfReturnedSearchRecords ?>" required pattern="<?= $expr4Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr4Number ?>" validate_message="Maximum Number Of Returned Search Records : <?= $msg4Number ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="minAgeCriteriaForUsers">Minimum Age Criteria For Users </label>
									<input type="text" name="minAgeCriteriaForUsers" id="minAgeCriteriaForUsers" size="32" value="<?= $minAgeCriteriaForUsers ?>" required pattern="<?= $expr2Number ?>" validate="true" validate_control="text" validate_expression="<?= $expr2Number ?>" validate_message="Minimum Age Criteria For Users : <?= $msg2Number ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
<?php 
	} else {
		//Not Promising 
?>
		<div class="ui-state-error">
		There were problems in your Network Settings <br />
		Details : <?= $promise1->getReason() ?>
		</div>
		<div class="ui-sys-inline-controls-right">
			<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>
		</div>
<?php
	} 
?>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_LAN)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$promise1 = null;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			$dob = DateAndTime::convertFromGUIDateFormatToSystemDateAndTimeFormat(mysql_real_escape_string($_REQUEST['dob']));
			if ($pagenumber -1 < $storedSafePage)	{
				if (is_null($profile1->getDOB()) || ($dob != $profile1->getDOB()->getDateAndTimeString()))	{
					$profile1->setDOB($dob); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW
				$profile1->setDOB($dob); 
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					//If Necessary to save you need to make date Comparison 
					// dob should be before today 
					if ($profile1->getDOB()->compareDateAndTime(new DateAndTime("Ndimangwa", Object::getCurrentTimestamp(), "Fadhili")) > 0) Object::shootException("The Institution can not be born on the future");
					$profile1->commitUpdate();
				} catch (Exception $e)	{
					$promise1 = new Promise();
					$promise1->setPromise(false);
					$promise1->setReason($e->getMessage());
					$enableUpdate = false;
				}
			}
			mysql_close($conn);
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		//Step 2: Handling Data for this Page 
		$serverIpAddress = "";
		$localAreaNetworkMask = "";
		if ($pagenumber < $storedSafePage)	{
			$serverIpAddress = $profile1->getServerIpAddress();
			$localAreaNetworkMask = $profile1->getLocalAreaNetworkMask();
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Local Area Network Settings</div>
				<div class="ui-sys-panel-body">
<?php 
	if (is_null($promise1) || $promise1->isPromising())	{
?>
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="serverIpAddress">Server IP Address </label>
									<input type="text" name="serverIpAddress" id="serverIpAddress" size="32" value="<?= $serverIpAddress ?>" required pattern="<?= $exprIpAddress ?>" validate="true" validate_control="text" validate_expression="<?= $exprIpAddress ?>" validate_message="Server IP Address : <?= $msgIpAddress ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="localAreaNetworkMask">Local Area Network Mask </label>
									<input type="text" name="localAreaNetworkMask" id="localAreaNetworkMask" size="32" value="<?= $localAreaNetworkMask ?>" required pattern="<?= $exprIpAddress ?>" validate="true" validate_control="text" validate_expression="<?= $exprIpAddress ?>" validate_message="Local Area Network Mask : <?= $msgIpAddress ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								 <div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
<?php 
	} else {
?>
		<div class="ui-state-error">
			There were problem in saving previous data <br/>
			Details : <?= $promise1->getReason() ?>
		</div>
		<div>
			<a href="<?= $thispage ?>?pagenumber=<?= $prevpagenumber ?>&prevpagenumber=<?= $pagenumber ?>">Click here to go back</a>
		</div>
<?php
	}
?>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_DOB)	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$firstDayOfAWeekId = mysql_real_escape_string($_REQUEST['firstDayOfAWeekId']);
			$themeId = mysql_real_escape_string($_REQUEST['themeId']);
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
				if (is_null($profile1->getFirstDayOfAWeek()) || ($firstDayOfAWeekId != $profile1->getFirstDayOfAWeek()->getDayId()))	{
					$profile1->setFirstDayOfAWeek($firstDayOfAWeekId); $enableUpdate=true;
				}
				if (is_null($profile1->getTheme()) || ($themeId != $profile1->getTheme()->getThemeId()))	{
					$profile1->setTheme($themeId); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW
				$profile1->setFirstDayOfAWeek($firstDayOfAWeekId);
				$profile1->setTheme($themeId);
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$profile1->commitUpdate();
				} catch (Exception $e) { die($e->getMessage()); }
			}
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$dob = "";
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$dob = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($profile1->getDOB());
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Date of Birth</div>
				<div class="ui-sys-panel-body">
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="dob">Date of Birth </label>
									<input class="datepicker" type="text" name="dob" id="dob" size="32" value="<?= $dob ?>" required pattern="<?= $exprDate ?>" validate="true" validate_control="text" validate_expression="<?= $exprDate ?>" validate_message="Date: <?= $msgDate ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
<?php 
	//We need to do the first day of a week Adjustment
	//The data submitted from the previous page still 
	//have not taken the effect 
	if (isset($_REQUEST['firstDayOfAWeekId']))	{
		$firstDayOfAWeek1 = null;
		try {
			$firstDayOfAWeek1 = new DaysOfAWeek($database, $_REQUEST['firstDayOfAWeekId'], $conn);
			$__systemDayOffset = $firstDayOfAWeek1->getOffsetValue();
		} catch (Exception $e)	{ die($e->getMessage()); }
	}
?>
(function($)	{
	$('.datepicker').removeClass('datepicker').datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay: <?= $__systemDayOffset ?>,
		changeYear: true,
		yearRange:'1961:2099'
	});
	//$('.datepicker').datepicker('option', 'firstDay', <?=  $__systemDayOffset ?>);
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	}  else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_THEME)	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$city = mysql_real_escape_string($_REQUEST['city']);
			$website = mysql_real_escape_string($_REQUEST['website']);
			$locationId = mysql_real_escape_string($_REQUEST['locationId']);
			$pHPTimezoneId = mysql_real_escape_string($_REQUEST['pHPTimezoneId']);			
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
				if ($website != $profile1->getWebsite())	{
					$profile1->setWebsite($website); $enableUpdate = true;
				}
				if ($city != $profile1->getCity())	{
					$profile1->setCity($city); $enableUpdate = true;
				}
				if (is_null($profile1->getLocation()) || ($locationId != $profile1->getLocation()->getLocationId()))	{
					$profile1->setLocation($locationId); $enableUpdate = true;
				}
				if (is_null($profile1->getPHPTimezone()) || ($pHPTimezoneId != $profile1->getPHPTimezone()->getZoneId()))	{
					$profile1->setPHPTimezone($pHPTimezoneId); $enableUpdate = true;
				}
			} else {
				//INSERT WINDOW
				$profile1->setWebsite($website); 
				$profile1->setCity($city);
				$profile1->setLocation($locationId);
				$profile1->setPHPTimezone($pHPTimezoneId);
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$profile1->commitUpdate();
				} catch (Exception $e)	{ die($e->getMessage()); }
			}
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$firstDayOfAWeekId = -1;
		$themeId = -1;
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$firstDayOfAWeekId = $profile1->getFirstDayOfAWeek()->getDayId();
			$themeId = $profile1->getTheme()->getThemeId();
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Profile Theme & First Day</div>
				<div class="ui-sys-panel-body">
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="firstDayOfAWeekId">First Day Of A Week </label>
									<select id="firstDayOfAWeekId" name="firstDayOfAWeekId" validate="true" validate_control="select" validate_expression="select" validate_message="Please, Select First Day of A week">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = DaysOfAWeek::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $firstDayOfAWeekId)	$selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="themeId">Profile Theme </label>
									<select id="themeId" name="themeId" validate="true" validate_control="select" validate_expression="select" validate_message="Please, Select Profile Theme">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Theme::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if ($alist1['id'] == $themeId)	$selected = "selected";
?>
		<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								 <div class="pure-controls">
									<span>
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_LOCATION)	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$enableDOMUpdate = false;
		$promise1 = null;
		//Step1 Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			$email = mysql_real_escape_string($_REQUEST['emailList']);
			$fax = mysql_real_escape_string($_REQUEST['fax']);
			$otherCommunication = mysql_real_escape_string($_REQUEST['otherCommunication']);
			if ($pagenumber - 1 < $storedSafePage)	{
				if ($telephone != $profile1->getTelephoneList()[0])	{
					$profile1->setTelephoneList($telephone); $enableUpdate = true;
				}
				if ($email != $profile1->getEmailList()[0])	{
					$profile1->setEmailList($email); $enableUpdate = true;
				}
				if ($fax != $profile1->getFax())	{
					$profile1->setFax($fax); $enableUpdate = true;
				}
				if ($otherCommunication != $profile1->getOtherCommunication()[0])	{
					$profile1->setOtherCommunication($otherCommunication); $enableUpdate = true;
				}
			} else {
				$profile1->setTelephoneList($telephone);
				$profile1->setEmailList($email); 
				$profile1->setFax($fax);
				$profile1->setOtherCommunication($otherCommunication);
				$profile1->setApplicationCounter($pagenumber);
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$profile1->commitUpdate();
				} catch (Exception $e)	{
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
					$enableUpdate = false; 
				}
			}
		}
		//Step 2
		$city = "";
		$website = "";
		$locationId = -1;
		$pHPTimezoneId = -1;
		if ($pagenumber < $storedSafePage)	{
			$city = $profile1->getCity();
			$website = $profile1->getWebsite();
			$locationId = $profile1->getLocation()->getLocationId();
			$pHPTimezoneId = $profile1->getPHPTimezone()->getZoneId();
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-data-capture ui-sys-panel">
				<div class="ui-sys-panel-header"><?= $profile1->getProfileName() ?>'s Location</div>
				<div class="ui-sys-panel-body">
					<div>
<?php 
	if (is_null($promise1) || $promise1->isPromising()) {
?>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="city">City </label></td>
									<input type="text" name="city" id="city" size="32" value="<?= $city ?>" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="City Format : <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="website">Website </label></td>
									<input type="text" name="website" id="website" size="32" value="<?= $website ?>" required pattern="<?= $exprWebsite ?>" validate="true" validate_control="text" validate_expression="<?= $exprWebsite ?>%<?= $exprA64Name ?>" validate_message="Website Format: <?= $msgWebsite ?>%Website Length: <?= $msgA64Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="locationId">Location </label>
									<select id="locationId" name="locationId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Location">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Location::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($profile1->getLocation()) && ($alist1['id'] == $profile1->getLocation()->getLocationId())) $selected="selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="pHPTimezoneId">PHPTimezone </label>
									<select id="pHPTimezoneId" name="pHPTimezoneId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select PHPTimezone">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = PHPTimezone::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($profile1->getPHPTimezone()) && ($alist1['id'] == $profile1->getPHPTimezone()->getZoneId())) $selected="selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								 <div class="pure-controls">
									<span class="ui-sys-inline-controls-right">
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
<?php 
	} else {
?>
		There were problem in Saving Communication Data <br/>
		Reason : <?= $promise1->getReason() ?>
<?php	
	}
?>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_ADDRESSES)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		$continueToStepTwo = true;
		$promise1 = null;
		//Step 1: Data From the previous page
		if ($prevpagenumber < $pagenumber)	{
			$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
				$profile1->loadXMLFolder("../data/profile");
			} catch (Exception $e)	{
				die($e->getMessage());
			}
			$logoArray1 = $_FILES['logo'];
			$enablePhotoUpdate = false;
			if ($pagenumber - 1 < $storedSafePage)	{
				if (Object::isThereAnyFileReceivedFromTheClient($logoArray1))	{
					$enablePhotoUpdate = true;
				}
			} else {
				if (Object::isThereAnyFileReceivedFromTheClient($logoArray1))	{
					$enablePhotoUpdate = true;
				}
				$enableUpdate = true;
				$profile1->setApplicationCounter($pagenumber);
			}
			$logoFolder = "../data/profile/logo";
			$logofilename = "uploaded_sys_logo";
			$fileextension = Object::getUploadedFileExtension($logoArray1);
			$uploadedFileName = $logofilename.".".$fileextension;
			if ($enablePhotoUpdate)	{
				//We will just overwrite 
				$validTypes = array("image/jpeg", "image/png", "image/jpg");
				$validExtensions = array("jpeg", "png", "jpg");
				$maximumUploadedSize = 2097153;
				$promise1 = Object::saveUploadedFile($logoArray1, $logoFolder, $logofilename, $validTypes, $validExtensions, $maximumUploadedSize);
				$continueToStepTwo = false;
			}
			if (! is_null($promise1) && $promise1->isPromising())	{
				$profile1->setLogo($uploadedFileName);
				$continueToStepTwo = true;
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$profile1->commitUpdate();
				} catch (Exception $e)	{ 
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
					$enableUpdate = false;
				}
			}
			mysql_close($conn);
		}
		//Step 2: Handling This page
		//Note During Installation, Only One in the List 
		//We are creating a basic setup Only 
		$telephoneList = "";
		$emailList = "";
		$fax = "";
		$otherCommunication = "";
		if ($pagenumber < $storedSafePage)	{
			//Assign All These Are Arrays
			$telephoneList = $profile1->getTelephoneList();
			$emailList = $profile1->getEmailList();
			$fax = $profile1->getFax();
			$otherCommunication = $profile1->getOtherCommunication();
			/* Converting to strings */
			$telephoneList = $telephoneList[0];
			$emailList = $emailList[0];
			$otherCommunication = $otherCommunication[0];
		}
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header"><?= $profile1->getProfileName() ?>'s Addresses</div>
				<div class="ui-sys-panel-body">
					<div>
<?php 
	if (is_null($promise1) || $promise1->isPromising())	{
?>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="telephoneList">Telephone </label>
									<input type="text" name="telephoneList" id="telephoneList" size="32" value="<?= $telephoneList ?>" required pattern="<?= $exprPhone ?>" validate="true" validate_control="text" validate_expression="<?= $exprPhone ?>" validate_message="Telephone: <?= $msgPhone ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="emailList">Email </label>
									<input type="text" name="emailList" id="emailList" size="32" value="<?= $emailList ?>" required pattern="<?= $exprEmail ?>" validate="true" validate_control="text" validate_expression="<?= $exprEmail ?>%<?= $exprA32Name ?>" validate_message="Email Format: <?= $msgEmail ?>%Email Length: <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="fax">Fax </label>
									<input type="text" name="fax" id="fax" size="32" value="<?= $fax ?>" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprPhone ?>" validate_message="Fax: <?= $msgPhone ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="otherCommunication">Other Communication </label>
									<input type="text" name="otherCommunication" id="otherCommunication" size="32" value="<?= $otherCommunication ?>" notrequired="true" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Other Communication: <?= $msgA64Name ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								 <div class="pure-controls">
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
								</div>
							</fieldset>
						</form>	
<?php 
	} else {
?>
		There were problems in Uploading your Photo <br />
		Problem Details: <i><?= $promise1->getReason() ?></i>
		<div class="ui-sys-inline-controls-right">
			<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>
		</div>
<?php
	}
?>
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both">&nbsp;</div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__PROFILE_LOGO)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		//Step 1: Data From Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			//Need to recreate Profile Object since we have lost the conn string 
			$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
			$profile1 = null;
			try {
				$profile1 = new Profile($database, $__profileId, $conn);
			} catch (Exception $e)	{ die($e->getMessage()); }
			$profileName = mysql_real_escape_string($_REQUEST['profileName']);
			$systemName = mysql_real_escape_string($_REQUEST['systemName']);
			$tinNumber = mysql_real_escape_string($_REQUEST['tinNumber']);
			if ($pagenumber - 1 < $storedSafePage)	{
				//We have data this is an editing 
				if (strcmp($profileName, $profile1->getProfileName()) != 0)	{
					$profile1->setProfileName($profileName); $enableUpdate = true;
				}
				if (strcmp($systemName, $profile1->getSystemName()) != 0)	{
					$profile1->setSystemName($systemName); $enableUpdate = true;
				}
				if (strcmp($tinNumber, $profile1->getTinNumber()) != 0)	{
					$profile1->setTinNumber($tinNumber); $enableUpdate = true;
				}
			} else {
				//Update All 
				$profile1->setProfileName($profileName);
				$profile1->setSystemName($systemName);
				$profile1->setTinNumber($tinNumber);
				$enableUpdate = true;
				//We need to Update ApplicationCounter 
				$profile1->setApplicationCounter($pagenumber);
			}
			//Entering Saving Mode 
			if ($enableUpdate)	{
				try {
					$profile1->commitUpdate();
				} catch (Exception $e)	{ die($e->getMessage()); }
			}
			mysql_close($conn);
		} //end--step 1
		//Step 2: Data Intended for this page only 
		$logo = $logo = "../".$dataFolder."/".$profile1->getDataFolder()."/logo/default.jpg";
		$photoname = "default.jpg";
		$trackchange = 0;
		if ($pagenumber < $storedSafePage)	{
			$logo = "../".$dataFolder."/".$profile1->getDataFolder()."/logo/".$profile1->getLogo();
			$photoname = $profile1->getLogo();
			$trackchange = 1;
		}
		//Beginning UI 
?>
		<div class="ui-sys-panel-container mobile-collapse">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header"><?= $profile1->getProfileName() ?>'s Logo</div>
				<div class="ui-sys-panel-body">
<!--Beginning a photo container-->	
<div class="photocontainer">
	<div class="photodisplay">
		<img id="__id_image_photo_container" alt="PIC" src="<?= $logo ?>"/>
	</div>
	<div class="photodata">
		<form id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
			<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
			<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
			<table>
				<tr>
					<td><label for="__id_image_photo_control">Select Logo</label></td>
					<td><input  id="__id_image_photo_control" type="file" class="ui-sys-file-upload" name="logo" data-trackchange="<?= $trackchange ?>" value="<?= $photoname ?>" accept="image/*"/></td>
				</tr>
				<tr>
					<td colspan="2" id="perror" class="ui-sys-inline-controls-center ui-sys-error-message"></td>
				</tr>
				<tr>
					<td colspan="2" class="ui-sys-inline-controls-right"> 
					<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>
					<input type="submit" value="Upload"/></td>
				</tr>
			</table>
		</form>
	</div>
	<div class="ui-sys-clear-both">&nbsp;</div>
</div>
<!--Ending a photo container-->
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both">&nbsp;</div>
<script type="text/javascript">
					function showImage(_fileSource, _imageDestination)	{
						var fileSource1 = document.getElementById(_fileSource);
						if (! fileSource1) return;
						var imageDestination1 = document.getElementById(_imageDestination);
						if (! imageDestination1) return;
						var fileReader1 = new FileReader();
						$(fileReader1).on('load', function(event)	{
							imageDestination1.src = this.result;
						});
						$(fileSource1).on('change', function(event)	{
							fileReader1.readAsDataURL(fileSource1.files[0]);
						});
					}
					showImage('__id_image_photo_control', '__id_image_photo_container');
					(function($)	{
						$('#__id_image_photo_control').on('change', function(event)	{
							$('#__id_image_photo_control').data('trackchange', '1');
							showImage('__id_image_photo_control', '__id_image_photo_container');
						});
						$('#form1').on('submit', function(event)	{
							event.preventDefault();
							//Make use of track changes to validate further
							var fileControl1 = document.getElementById('__id_image_photo_control');
							if (! fileControl1) return;
							generalFormSubmission(this, 'form1', 'perror');
							if (parseInt($(fileControl1).data('trackchange')) == 1)	{
								generalFormSubmission(this, 'form1', 'perror');
							} else {
								$('#perror').html("No Update were done");
								return false;
							} //end-if-else
						});
					})(jQuery);
</script>
<?php
	} else if (isset($prevpagenumber) && isset($pagenumber) && $pagenumber == Installation::$__INSTITUTION_NAME)	{
		$storedSafePage = intval($profile1->getApplicationCounter());
		$enableUpdate = false;
		//Step 2: Handling Data Intended just for this page 
		$profileName = "";
		$systemName = "";
		$tinNumber = "";
		if ($pagenumber < $storedSafePage)	{
			$profileName = $profile1->getProfileName();
			$systemName = $profile1->getSystemName();
			$tinNumber = $profile1->getTinNumber();
		}
?>	
		<div class="mobile-collapse ui-sys-panel-container">
			<div class="ui-sys-panel ui-sys-data-capture">
				<div class="ui-sys-panel-header">Names of System and Institution</div>
				<div class="ui-sys-panel-body">
					<div>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="profileName">Name of the Institution </label>
									<input type="text" name="profileName" id="profileName" size="32" value="<?= $profileName ?>" required pattern="<?= $expr96Name ?>" validate="true" validate_control="text" validate_expression="<?= $expr96Name ?>" validate_message="Name Of the Institution: <?= $msg96Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="systemName">Name of the System </label>
									<input type="text" name="systemName" id="systemName" size="32" value="<?= $systemName ?>" required pattern="<?= $expr96Name ?>" validate="true" validate_control="text" validate_expression="<?= $expr96Name ?>" validate_message="Name of the System: <?= $msg96Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label title="Tax Identification Number" for="tinNumber">TIN Number </label>
									<input type="text" name="tinNumber" id="tinNumber" size="32" value="<?= $tinNumber ?>" required pattern="<?= $exprL32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL32Name ?>" validate_message="TIN Number : <?= $msgL32Name ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message" ></span>
								</div>
								<div class="pure-controls">
									<span class="ui-sys-inline-controls-right">
										<a class="button-link" href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= $pagenumber - 1 ?>">&lt;&lt; Previous</a>&nbsp;&nbsp;
								<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="ui-sys-panel-footer ui-sys-inline-controls-center">
					<div class="ui-sys-installation-menu-link">
<?php
	if ($storedSafePage >=  Installation::$__MENU)	{
?>
		<a href="<?= $thispage ?>?prevpagenumber=<?= $pagenumber ?>&pagenumber=<?= Installation::$__MENU ?>&menujump=true">Go to Menu Page</a>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both">&nbsp;</div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
	} else {
		//This should be done once 
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$profile1 = null;
		try {
			$profile1 = new Profile($database, $__profileId, $conn);
			$profile1->setRevisionNumber("1");
			$profile1->setRevisionTime($systemDate1->getDateAndTimeString());
			$profile1->commitUpdate();
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		mysql_close($conn);
?>
		<div class="ui-sys-oi-left-fill mobile-collapse">
			<div class="ui-sys-panel-container ui-sys-panel">
				<div class="ui-sys-panel-header">Welcome</div>
				<div class="ui-sys-panel-body ui-sys-center-text">
					<div class="ui-widget ui-widget-content">
				Welcome to the Installation Page of Student Management Information System <br/>
			This wizard will guide you step by step on the Installation of this System
					</div>
				</div>
				<div class="ui-sys-panel-footer">
					<div class="ui-sys-right-text-in-block">
						<a class="button-link" href="<?= $thispage ?>?prevpagenumber=0&pagenumber=1">Next &gt;&gt;</a>
					</div>
				</div>
			</div>
		</div>
		<div class="ui-sys-clear-both">&nbsp;</div>
<?php
	}
?>
</div>
<!--END OF PUTTING CODE-->
	<div class="ui-sys-footer mobile-collapse">
<?php   
	//You must have a DateAndTime Object carrying date, we are interested with only year 
	//So any default_timezone is okay with us at this point 
	include("../template/footer.php");
?>
	</div>
</div>

</body>
</html>